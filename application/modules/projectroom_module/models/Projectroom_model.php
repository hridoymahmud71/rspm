<?php
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Projectroom_model extends  CI_Model{

        public function __construct() {
        parent::__construct();
        $this->load->database();

    }

    /*--------------Check Permisson Starts----------------------------------*/

    public function checkIf_AssignedStaff($project_id,$staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as s');
        $this->db->where('s.project_id', $project_id);
        $this->db->where('s.staff_id',$staff_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if($num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    public function checkIf_RelatedClient($project_id,$client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.project_id', $project_id);
        $this->db->where('p.client_id',$client_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if($num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

    /*--------------Check Permisson Ends----------------------------------*/

    public function getAProject_WithClient($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.project_id', $project_id);
        $this->db->where('p.deletion_status!=',1);
        $this->db->join('users as u', 'p.client_id=u.id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getAProject_includingDeletedOnes($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function getAssignedStaff($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as st');
        $this->db->join('users as u', 'st.staff_id=u.id');
        $this->db->join('rspm_tbl_project as p', 'st.project_id=p.project_id');
        $this->db->where('p.project_id', $project_id);
        $this->db->where('p.deletion_status!=',1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



}