
<!--projectroom menu starts-->
<?php $this->lang->load('projectroom_menu_section') ?>

    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo lang('box_title_projectroom_sections') ?></h3>

            <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body no-padding" style="display: none;">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="<?= base_url().'projectroom_module/project_overview/' .$project_id ?>">
                        <i class="fa fa-gg"></i>
                        <i class="fa fa-info pull-right"></i>
                        <?php echo lang('item_project_overview_text')?>
                    </a>
                </li>
                <!--only for admin -->
                <?php if($is_admin == 'admin'){?>
                    <li>
                        <a href="<?php echo base_url().'project_module/edit_project/'.$project_id ?>">
                            <i class="fa fa-gg"></i>
                            <i class="fa fa-pencil-square-o pull-right"></i>
                            <?php echo lang('item_edit_project_text')?>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?= base_url().'task_module/show_tasks/all_tasks/' .$project_id ?>">
                        <i class="fa fa-tasks"></i>
                        <i class="fa fa-eye pull-right"></i>
                        <?php echo lang('item_all_tasks_text')?>
                    </a>
                </li>
                <?php if($is_client == 'not_client') { ?>
                <li>
                    <a href="<?= base_url().'task_module/show_tasks/my_tasks/' .$project_id ?>">
                        <i class="fa fa-tasks"></i>
                        <i class="fa fa-eye pull-right"></i>
                        <?php echo lang('item_my_tasks_text')?>
                    </a>
                </li>
                <?php } ?>
                <?php if($is_admin == 'admin') { ?>
                    <li>
                        <a href="<?= base_url().'task_module/add_task/' .$project_id ?>">
                            <i class="fa fa-tasks"></i>
                            <i class="fa fa-plus-square pull-right"></i>
                            <?php echo lang('item_add_tasks_text')?>
                        </a>
                    </li>
                <?php } ?>
                <li><a href="<?= base_url().'file_manager_module/files_in_a_project/' .$project_id ?>"">
                    <i class="fa fa-files-o"></i>
                    <i class="fa fa-eye pull-right"></i>
                    <?php echo lang('item_project_files_text')?>
                    </a>
                </li>
                <li><a href="<?= base_url()
                    .'file_manager_module/get_upload_file_form_with_project_without_task/'
                    .$project_id ?>">
                    <i class="fa fa-files-o"></i>
                    <i class="fa fa-upload pull-right"></i>
                        <?php echo lang('item_upload_files_in_a_project_text')?>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url().'support_module/show_project_ticket_list/'.$project_id  ?>">
                        <i class="fa fa-ticket"></i>
                        <i class="fa fa-eye pull-right"></i>
                        <?php echo lang('item_support_or_ticket_text')?>
                    </a>
                </li>
                <?php if($is_client =='client') { ?>
                    <li>
                        <a href="<?= base_url().'support_module/add_ticket_by_project/'.$project_id ?>">
                            <i class="fa fa-ticket"></i>
                            <i class="fa fa-plus-square pull-right"></i>
                            <?php echo lang('item_add_ticket_text')?>
                        </a>
                    </li>
                <?php } ?>
                <?php if($is_admin == 'admin' || $is_client == 'client' ) { ?>
                <li><a href="<?php echo base_url().'invoice_module/show_project_invoice_list/'.$project_id ?>">
                        <i class="fa fa-list-alt"></i>
                        <i class="fa fa-eye pull-right"></i>
                        <?php echo lang('item_invoices_text')?>
                    </a>
                </li>
                <?php } ?>
                <!--only for admin -->
                <?php if($is_admin == 'admin') { ?>
                    <li><a href="<?php echo base_url().'invoice_module/add_invoice_by_project/'.$project_id ?>">
                            <i class="fa fa-list-alt"></i>
                            <i class="fa fa-plus-square pull-right"></i>
                            <?php echo lang('item_add_invoice_text')?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /. box -->
<!--projectroom menu ends-->