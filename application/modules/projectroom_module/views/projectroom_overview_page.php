<div class="content-wrapper" style="min-height: 1126px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= lang('page_title_text') ?>
            <small><?= $a_project_with_client->project_name ?></small>
        </h1>
        <ol class="breadcrumb">
            <?php if($is_admin == 'admin'){ ?>
                <li>
                    <a href="<?= base_url().'project_module/all_projects'?>"><i class="fa fa-gg"></i>
                        <?= lang('breadcrumb_all_projects_page_text') ?>
                    </a>
                    |
                    <a href="<?= base_url().'project_module/my_projects'?>"><i class="fa fa-gg"></i>
                        <?= lang('breadcrumb_my_projects_page_text') ?>
                    </a>
                </li>
            <?php } ?>

            <?php if($is_admin == 'not_admin'){ ?>
            <li><a href="<?= base_url().'project_module/my_projects'?>"><i class="fa fa-gg"></i><?= lang('breadcrumb_my_projects_page_text') ?></a></li>
            <?php } ?>

            <!--<li>
                <a href="<?/*= base_url() . 'projectroom_module/' .$a_project_with_client->project_id */?>">
                    <?/*= $a_project_with_client->project_name */?>
                </a>
            </li>-->
            <li class="active"><?= lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!--projectroom menu starts-->
            <div class="col-md-4">
                <?php echo $projectroom_menu_section ?>
            </div>
            <!--projectroom menu ends-->

            <!--overview wrap starts-->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <strong><h3 class="box-title"><?= lang('box_title_text') ?></h3></strong>

                        <!--only for admin /edit project-->
                        <?php if($is_admin == 'admin') { ?>
                        <a href="<?= base_url() .'project_module/edit_project/'.$a_project_with_client->project_id ?>"
                           style="color:#2b2b2b" class="pull-right">
                            <i class="fa fa-pencil-square-o fa-lg"></i>
                        </a>
                        <?php } ?>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if ($a_project_with_client) { ?>
                            <!--project details starts-->
                            <strong><i class="fa fa-gg margin-r-5"></i><?= lang('project_details_text') ?></strong>
                            <p><!--empty space--></p>
                            <div class="box-body table-responsive no-padding table-bordered">
                                <table class="table table-hover text-muted">
                                    <tbody>
                                    <tr>
                                        <td><?= lang('project_name_text') ?></td>
                                        <td><?= $a_project_with_client->project_name ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('project_progress_text') ?></td>
                                        <td>
                                            <span class="<?php
                                            if ($a_project_with_client->progress < 33) {
                                                echo 'label label-danger pull-right';
                                            } else if ($a_project_with_client->progress > 66) {
                                                echo 'label label-success pull-right';
                                            } else {
                                                echo 'label label-primary pull-right';
                                            }
                                            ?>">
                                                    <?php echo $a_project_with_client->progress . '%' ?>
                                                </span>
                                            &nbsp;
                                            <div class="progress progress-xs">
                                                <div class="<?php
                                                if ($a_project_with_client->progress < 33) {
                                                    echo 'progress-bar progress-bar-danger';
                                                } else if ($a_project_with_client->progress > 66) {
                                                    echo 'progress-bar progress-bar-success';
                                                } else {
                                                    echo 'progress-bar progress-bar-primary';
                                                }
                                                ?>"
                                                     style="width: <?php echo $a_project_with_client->progress . '%' ?> "></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('project_status_text') ?></td>

                                        <?php if ($a_project_with_client->status != 0) { ?>
                                            <td>
                                                <span class="label label-primary"><?= lang('project_active_text') ?></span>
                                            </td>
                                        <?php } else { ?>
                                            <td>
                                                <span class="label label-default"><?= lang('project_inactive_text') ?></span>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?= lang('project_start_date_text') ?></td>
                                        <td><?= $a_project_with_client->start_date ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('project_end_date_text') ?></td>
                                        <td><?= $a_project_with_client->end_date ?></td>
                                    </tr>
                                    <!--only for admin starts-->
                                    <?php if($is_admin == 'admin') { ?>
                                    <tr>
                                        <td><?= lang('project_creation_date_text') ?></td>
                                        <td><?= $a_project_with_client->created_date ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('project_modification_date_text') ?></td>
                                        <td><?= $a_project_with_client->modified_date ?></td>
                                    </tr>
                                    <?php } ?>
                                    <!--only for admin ends-->
                                    </tbody>
                                </table>
                            </div>
                            <!--project details ends-->

                            <hr>

                            <!--client details starts-->
                            <strong><i class="fa fa-user margin-r-5"></i><?= lang('client_details_text') ?></strong>
                            <p><!--empty space--></p>
                            <div class="box-body table-responsive no-padding table-bordered">
                                <table class="table table-hover text-muted">
                                    <tbody>
                                    <tr>
                                        <td><?= lang('client_name_text') ?></td>
                                        <td>
                                            <a href="<?php echo base_url()
                                                .'user_profile_module/user_profile_overview/' .$a_project_with_client->client_id?>">
                                            <span class="label label-primary">
                                                <?= $a_project_with_client->first_name . ' ' . $a_project_with_client->last_name ?>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('client_compamy_name_text') ?></td>
                                        <td><?= $a_project_with_client->company ?></td>

                                    </tr>
                                    <tr>
                                        <td><?= lang('client_email_text') ?></td>
                                        <td><?= $a_project_with_client->email ?></td>
                                    </tr>
                                    <!--only for admin starts-->
                                    <tr>
                                        <td><?= lang('client_phone_no_text') ?></td>
                                        <td><?= $a_project_with_client->phone ?></td>
                                    </tr>
                                    <!--only for admin ends-->
                                    </tbody>
                                </table>
                            </div>
                            <!--client details ends-->
                        <?php } ?>

                        <hr>

                        <!--staffs starts-->
                        <strong><i class="fa fa-users margin-r-5"></i><?= lang('assigned_staffs_text') ?>
                        </strong>
                        <p><!--empty space--></p>
                        <table class="table table-hover text-muted">
                            <?php if ($assigned_staffs) {
                                foreach ($assigned_staffs as $an_assigned_staff) { ?>

                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url()
                                                .'user_profile_module/user_profile_overview/' .$an_assigned_staff->id?>">
                                                <?php if ($an_assigned_staff->active != 0) { ?>
                                                    <span class="label label-primary">
                                                                <?php echo $an_assigned_staff->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff->last_name ?>
                                                            </span>
                                                <?php } else { ?>
                                                    <span class="label label-default">
                                                                <?php echo $an_assigned_staff->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff->last_name
                                                                    . ' '
                                                                    . lang('staff_inactive_text')
                                                                ?>

                                                            </span>
                                                <?php } ?>
                                            </a>
                                        </td>
                                    </tr>

                                <?php } ?>

                            <?php } else { ?>

                                <tr>
                                    <td>
                                        <div class="text-muted"><?= lang('no_staff_assigned_text') ?></div>
                                    </td>
                                </tr>

                            <?php } ?>
                        </table>

                        <!--staffs ends-->
                        <hr>

                        <?php if ($invoice_info) { ?>
                            <!--invoice starts (odmin only)-->
                            <strong><i class="fa fa fa-list-alt margin-r-5"></i><?= lang('Invoice_text') ?></strong>

                            <p><!--empty space--></p>
                            <div class="box-body table-responsive no-padding table-bordered">
                                <table class="table table-hover text-muted">
                                    <tbody>
                                    <tr>
                                        <td><?= lang('invoice_num_text') ?></td>
                                        <td>
                                            <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/user_id_here' ?>"><span
                                                        class="label label-primary">INV_325547852</span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('invoice_balance_text') ?></td>
                                        <td>€ 22564.225</td>

                                    </tr>
                                    <tr>
                                        <td><?= lang('invoice_status_text') ?></td>
                                        <td><span class="label-warning">Pending</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--invoice ends-->
                        <?php } ?>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!--overview wrap ends-->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>