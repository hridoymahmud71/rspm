<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectroomController extends MX_Controller
{
    function __construct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Projectroom_model');

        $this->load->library('custom_datetime_library');

    }

    public function checkPermission_to_enter_Projectroom($project_id,$user_id)
    {
        if (!$this->ion_auth->is_admin()) {
            $is_related_client = $this->Projectroom_model->checkIf_RelatedClient($project_id,$user_id);
            $is_assigned_staff = $this->Projectroom_model->checkIf_AssignedStaff($project_id,$user_id);

            if($is_related_client || $is_assigned_staff){
                return true;
            }else{
                return false;
            }

        }else{
            return true;
        }
    }

    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    public function index()
    {
        $project_id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if($this->checkPermission_to_enter_Projectroom($project_id,$this->session->userdata('user_id')) == true){
            $this->showProjectOverview($project_id);
        }else{
            redirect('users/auth/need_permission');
        }

    }

    public function showProjectOverview($project_id)
    {
        $this->lang->load('projectroom_overview');

        $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        $data['invoice_info'] = null;

        $data['a_project_with_client'] = $this->Projectroom_model->getAProject_WithClient($project_id);
        $data['assigned_staffs'] = $this->Projectroom_model->getAssignedStaff($project_id);


        $data['a_project_including_deleted_ones'] =
            $this->Projectroom_model->getAProject_includingDeletedOnes($project_id);

        if($data['a_project_including_deleted_ones']->status == 0
            && $data['a_project_including_deleted_ones']->deletion_status != 1
            &&  $data['is_admin'] == 'not_admin'){
            redirect('users/auth/need_permission');
        }

        if($data['a_project_including_deleted_ones']->deletion_status == 1) {
            redirect('users/auth/does_not_exist');
        }

        /*converting timestamp to user's given format*/

        if ($data['a_project_with_client']->start_date == 0 || $data['a_project_with_client']->start_date == null || empty($data['a_project_with_client']->start_date)) {
            $data['a_project_with_client']->start_date = $this->lang->line('no_date_available_text');

        }else{
            $data['a_project_with_client']->start_date =
                $this->custom_datetime_library->convert_and_return_TimestampToDate($data['a_project_with_client']->start_date);
        }

        if ($data['a_project_with_client']->end_date == 0 || $data['a_project_with_client']->end_date == null || empty($data['a_project_with_client']->end_date)) {
            $data['a_project_with_client']->end_date = $this->lang->line('no_date_available_text');
        } else {
            $data['a_project_with_client']->end_date =
                $this->custom_datetime_library->convert_and_return_TimestampToDate($data['a_project_with_client']->end_date);
        }

        if ($data['a_project_with_client']->created_date == 0 || $data['a_project_with_client']->created_date == null || empty($data['a_project_with_client']->created_date)) {
            $data['a_project_with_client']->created_date = $this->lang->line('no_date_available_text');
        } else {
            $data['a_project_with_client']->created_date =
                $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($data['a_project_with_client']->created_date);
        }

        if ($data['a_project_with_client']->modified_date == 0 || $data['a_project_with_client']->modified_date == null || empty($data['a_project_with_client']->modified_date)) {
            $data['a_project_with_client']->modified_date = $this->lang->line('no_date_available_text');
        } else {
            $data['a_project_with_client']->modified_date =
                $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($data['a_project_with_client']->modified_date);
        }



        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("projectroom_module/projectroom_overview_page", $data);
        $this->load->view("common_module/footer");
    }

}