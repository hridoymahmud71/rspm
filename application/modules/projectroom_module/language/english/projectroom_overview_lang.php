<?php

$lang['page_title_text'] = 'Project Room';

$lang['breadcrumb_all_projects_page_text'] = 'All Projects';
$lang['breadcrumb_my_projects_page_text'] = 'My Projects';
$lang['breadcrumb_page_text'] = 'Project Overview';

$lang['box_title_text'] = 'Project Overview';

/*main sections*/
$lang['project_details_text'] = 'Project Details';
$lang['client_details_text'] = 'Client Details';
$lang['assigned_staffs_text'] = 'Assigned Staff\'s';
$lang['Invoice_text'] = 'Invoice';


/*rows of the project table*/
$lang['project_name_text'] = 'Project Name';
$lang['project_progress_text'] = 'Project Progress';
$lang['project_status_text'] = 'Project Status';

$lang['project_active_text'] = 'Active';
$lang['project_inactive_text'] = 'Inactive';

$lang['project_start_date_text'] = 'Project Start Date';
$lang['project_end_date_text'] = 'Project End Date';

$lang['project_creation_date_text'] = 'Project Project Creation Date';
$lang['project_modification_date_text'] = 'Project Modification Date';

$lang['no_date_available_text'] = 'No Date Available';

/*rows of the client table*/
$lang['client_name_text'] = 'Client\'s Name';
$lang['client_compamy_name_text'] = 'Company Name';
$lang['client_email_text'] = 'Client\'s Email';
$lang['client_phone_no_text'] = 'Client\'s phone no.';

/*staff*/
$lang['no_staff_assigned_text'] = 'No staff is assigned yet.';
$lang['staff_inactive_text'] = '(Inactive)';

/*rows of the invoice table*/
$lang['invoice_num_text'] = 'Invoice No.';
$lang['invoice_balance_text'] = 'Invoice Balance';
$lang['invoice_status_text'] = 'Invoice Status';

