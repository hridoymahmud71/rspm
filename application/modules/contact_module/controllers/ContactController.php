<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }


        //$this->load->model('Contact_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
    }

    public function getContactInfo()
    {
        $a_settings_code = 'contact_settings';
        $data['admin_contact_email'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'admin_contact_email');
        $data['admin_contact_phone'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'admin_contact_phone');

        $data['company_contact_email'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_contact_email');
        $data['company_contact_phone'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_contact_phone');
        $data['company_contact_address'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_contact_address');

        $data['company_facebook_id'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_facebook_id');
        $data['company_twitter_id'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_twitter_id');
        $data['company_youtube_id'] = $this->custom_settings_library->getASettingsValue($a_settings_code, 'company_youtube_id');


        if ($data['admin_contact_email'] == null || $data['admin_contact_email'] == '') {

            $data['admin_contact_email'] = $this->lang->line('unavailable_text');
        }
        if ($data['admin_contact_phone'] == null || $data['admin_contact_phone'] == '') {

            $data['admin_contact_phone'] = $this->lang->line('unavailable_text');
        }

        /*----------------------------*/

        if ($data['company_contact_email'] == null || $data['company_contact_email'] == '') {

            $data['company_contact_email'] = $this->lang->line('unavailable_text');
        }
        if ($data['company_contact_phone'] == null || $data['company_contact_phone'] == '') {

            $data['company_contact_phone'] = $this->lang->line('unavailable_text');
        }
        if ($data['company_contact_address'] == null || $data['company_contact_address'] == '') {

            $data['company_contact_address'] = $this->lang->line('unavailable_text');
        }

        /*----------------------------*/

        if ($data['company_facebook_id'] == null || $data['company_facebook_id'] == '') {

            $data['company_facebook_id'] = $this->lang->line('unavailable_text');
        }
        if ($data['company_twitter_id'] == null || $data['admin_contact_email'] == '') {

            $data['company_twitter_id'] = $this->lang->line('company_twitter_id');
        }
        if ($data['company_youtube_id'] == null || $data['company_youtube_id'] == '') {

            $data['company_youtube_id'] = $this->lang->line('company_youtube_id');
        }

        /*----------------------------*/

        return $data;

    }

    public function showContactInfo()
    {

        $this->lang->load('contact_view');

        $data = array();

        $data = $this->getContactInfo();




        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("contact_module/contact_view_page", $data);
        $this->load->view("common_module/footer");
    }

}