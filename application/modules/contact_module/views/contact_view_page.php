<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrum_home_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrum_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-6">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('admin_box_title_text') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered -table-striped table-responsive">
                            <tr>
                                <td><?php echo lang('system_admin_contact_email_text') ?>
                                    <span class="pull-right"><i class="fa fa-envelope"></i></span>
                                </td>

                                <td><?php echo $admin_contact_email ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('system_admin_contact_phone_text') ?>
                                   <span class="pull-right"><i class="fa fa-phone"></i></span>
                                </td>

                                <td><?php echo $admin_contact_phone ?></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <!-- Default box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('company_box_title_text') ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered -table-striped table-responsive">
                            <tr>
                                <td><?php echo lang('company_contact_email_text') ?>
                                    <span class="pull-right"><i class="fa fa-envelope"></i></span>
                                </td>
                                <td><?php echo $company_contact_email ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('company_contact_phone_text') ?>
                                    <span class="pull-right"><i class="fa fa-phone"></i></span>

                                </td>
                                <td><?php echo $company_contact_phone ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('company_contact_address_text') ?>
                                    <span class="pull-right"><i class="fa fa-building"></i></span>
                                </td>
                                <td><?php echo $company_contact_address ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><?php echo lang('company_facebook_id_text') ?>
                                    <span class="pull-right"><i class="fa fa-facebook"></i></span>
                                </td>
                                <td><?php echo $company_facebook_id ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('company_twitter_id_text') ?>
                                    <span class="pull-right"><i class="fa fa-twitter"></i></span>
                                </td>
                                <td><?php echo $company_twitter_id ?></td>
                            </tr>
                            <tr>
                                <td><?php echo lang('company_youtube_text') ?>
                                    <span class="pull-right"><i class="fa fa-youtube-play"></i></span>
                                </td>
                                <td><?php echo $company_youtube_id ?></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->