<?php

/*page texts*/
$lang['page_title_text'] = 'Contact';
$lang['page_subtitle_text'] = 'Contact Info of Admin & Company';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Contact View';


/*admin*/
$lang['admin_box_title_text'] = 'System Admin\'s contact info';

$lang['system_admin_contact_email_text'] = 'System admin\'s contact email';
$lang['system_admin_contact_phone_text'] = 'System admin\'s contact phone';

/*company*/
$lang['company_box_title_text'] = 'Company\'s contact info';

$lang['company_contact_email_text'] = 'Company\'s contact email';
$lang['company_contact_phone_text'] = 'Company\'s contact phone';
$lang['company_contact_address_text'] = 'Company\'s contact address';

$lang['company_facebook_id_text'] = 'Company\'s Facebook Id';
$lang['company_twitter_id_text'] = 'Company\'s Twitter Id';
$lang['company_youtube_text'] = 'Company\'s Youtube Id';

/*other*/
$lang['unavailable_text'] = 'Unavailable';

?>