<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['project_module/all_projects'] = 'ProjectController';
$route['project_module/get_all_projects_by_ajax'] = 'ProjectController/getAllProjectsByAjax';
$route['project_module/get_corresponding_projects_by_ajax'] = 'ProjectController/getCorrespondingProjectsByAjax';

$route['project_module/get_active_clients_by_ajax'] = 'ProjectController/getActiveClientsByAjax';
$route['project_module/get_active_staffs_by_ajax'] = 'ProjectController/getActiveStaffsByAjax';


$route['project_module/add_project'] = 'ProjectController/addProject';
$route['project_module/save_projectpro_info'] = 'ProjectController/saveProjectInfo';
$route['project_module/delete_project/(:any)'] = 'ProjectController/deleteProjectInfo';
$route['project_module/delete_my_project/(:any)'] = 'ProjectController/deleteProjectInfo';

$route['project_module/edit_project/(:any)'] = 'ProjectController/editProject';
$route['project_module/update'] = 'ProjectController/updateProject';
//$route['project_module/update_my_project'] = 'ProjectController/updateProject';

$route['project_module/my_projects'] = 'ProjectController/corespondingProject';
//(:any) == $user_id
$route['project_module/my_projects/seen_by_other/(:any)'] = 'ProjectController/corespondingProject_seenByOther';
$route['project_module/my_project_edit/(:any)'] = 'ProjectController/editProject';

$route['project_module/projectroom/(:any)'] = 'ProjectRoomController/projectroom';
$route['project_module/add_task'] = 'ProjectRoomController/addTask';

$route['project_module/all_projects/activate_project/(:any)'] = 'ProjectController/activateProject';
$route['project_module/my_projects/activate_project/(:any)'] = 'ProjectController/activateProject';

$route['project_module/all_projects/deactivate_project/(:any)'] = 'ProjectController/deactivateProject';
$route['project_module/my_projects/deactivate_project/(:any)'] = 'ProjectController/deactivateProject';

$route['project_module/all_projects/make_project_complete/(:any)'] = 'ProjectController/makeProjectComplete';
$route['project_module/my_projects/make_project_complete/(:any)'] = 'ProjectController/makeProjectComplete';

