<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Modified By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class ProjectController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }
        //$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('Project_model');
        $this->load->model('users/models/Ion_auth_model');
        $this->lang->load('project');
        $this->load->library('settings_module/Custom_settings_library');
        $this->load->library('users/Ion_auth');
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        $data['which_project'] = 'all_projects';

        $this->lang->load('project');
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("all_project_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getAllProjectsByAjax()
    {
        $projects = array();
        $table_data = array();

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'project_name';
        $columns[1] = 'client_name';
        $columns[2] = 'company';
        $columns[3] = 'progress';
        $columns[4] = 'budget';
        $columns[5] = 'start_date';
        $columns[6] = 'end_date';
        $columns[7] = 'status';
        $columns[8] = 'action';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['project_name'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['client_name'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['client_company'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['progress'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['budget'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][7]['search']['value'])) {
            $specific_filters['status'] = $requestData['columns'][7]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Project_model->countProjectsByAjax(false, false);


        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Project_model->countProjectsByAjax($common_filter_value, $specific_filters);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $projects = $this->Project_model->getProjectsByAjax($common_filter_value, $specific_filters, $order, $limit);


        if ($projects == false || empty($projects) || $projects == null) {
            $projects = false;
            $table_data = false;
        }

        $last_query = $this->db->last_query();

        $currency_position = $this->custom_settings_library->getASettingsValue('currency_settings', 'currency_position');
        if (empty($currency_position) || $currency_position == '' || $currency_position == null) {
            $currency_position = false;
        }
        $currencies = $this->Project_model->allCurrency();


        if ($projects) {

            $i = 0;
            foreach ($projects as $a_project) {

                $row = new stdClass();

                $row->project_name = $a_project->project_name;
                /*client name starts*/
                $client_tooltip = $a_project->first_name . ' ' . $a_project->last_name;
                $client_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_project->client_id;
                $client_name_anchor = '<a ' . 'title="' . $client_tooltip . '"' . ' href="' . $client_url . '"' . ' >' . $a_project->first_name . ' ' . $a_project->last_name . '</a>';
                $row->client_name = $client_name_anchor;
                /*client name ends*/

                $row->company = $a_project->company;

                /*date starts*/
                $projects[$i]->st_dt = new stdClass();
                $projects[$i]->en_dt = new stdClass();

                $projects[$i]->st_dt->timestamp = $a_project->start_date;
                $projects[$i]->en_dt->timestamp = $a_project->end_date;

                if ($a_project->start_date == 0 || $a_project->start_date == false || $a_project->start_date == null) {
                    $projects[$i]->st_dt->display = $this->lang->line('no_date_text');
                } else {
                    $projects[$i]->st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_project->start_date);
                }

                if ($a_project->end_date == 0 || $a_project->start_date == false || $a_project->start_date == null) {
                    $projects[$i]->en_dt->display = $this->lang->line('no_date_text');
                } else {
                    $projects[$i]->en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_project->end_date);
                }

                $row->st_dt = $projects[$i]->st_dt;
                $row->en_dt = $projects[$i]->en_dt;
                /*date ends*/

                /*progress starts*/
                $projects[$i]->prog = new stdClass();
                $projects[$i]->prog->int = $a_project->progress;

                $progress_span_class = '';
                $progressbar_inner_div_class = '';
                if ($a_project->progress < 33) {
                    $progress_span_class = 'label label-danger pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-danger';
                } else if ($a_project->progress > 66) {
                    $progress_span_class = 'label label-success pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-success';
                } else {
                    $progress_span_class = 'label label-primary pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-primary';
                }

                $progress_span = '<span ' . 'class="' . $progress_span_class . '"' . ' >' . $a_project->progress . '%' . '</span>';

                $progressbar_inner_div_style = 'width:' . $a_project->progress . '%';
                $progressbar_inner_div = '<div ' . ' class="' . $progressbar_inner_div_class . '"' . 'style="' . $progressbar_inner_div_style . '""' . '>' . '</div>';
                $progressbar_outer_div = '<div class="progress progress-xs" >' . $progressbar_inner_div . '</div>';

                $projects[$i]->prog->html = $progress_span . '&nbsp;' . $progressbar_outer_div;

                $row->prog = $projects[$i]->prog;
                /*progress ends*/

                /*budget starts*/
                $projects[$i]->budg = new stdClass();
                $projects[$i]->budg->dec = $a_project->budget;

                $currency_sign = '';
                foreach ($currencies as $a_currency) {
                    if ($projects[$i]->currency_id == $a_currency->currency_id) {
                        $currency_sign = $a_currency->currency_sign;
                    }
                }

                $projects[$i]->budg->html = '';
                if (!$currency_position) {
                    $projects[$i]->budg->html = $currency_sign . ' ' . $a_project->budget;
                } else {
                    if ($currency_position == 'left_along') {
                        $projects[$i]->budg->html = $currency_sign . ' ' . $a_project->budget;
                    }
                    if ($currency_position == 'right_along') {
                        $projects[$i]->budg->html = $a_project->budget . ' ' . $currency_sign;
                    }
                    if ($currency_position == 'left_far') {
                        $projects[$i]->budg->html = $currency_sign . ' &nbsp;&nbsp; ' . $a_project->budget;
                    }
                    if ($currency_position == 'right_far') {
                        $projects[$i]->budg->html = $a_project->budget . ' &nbsp;&nbsp; ' . $currency_sign;
                    }
                }

                $row->budg = $projects[$i]->budg;
                /*budget ends*/


                /*active - inactive starts*/
                $projects[$i]->sts = new stdClass();
                $projects[$i]->sts->int = $a_project->status;

                if ($a_project->status == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'project_module/all_projects/deactivate_project/' . $a_project->project_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'project_module/all_projects/activate_project/' . $a_project->project_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $projects[$i]->sts->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;

                $row->sts = $projects[$i]->sts;
                /*active - inactive ends*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'projectroom_module/project_overview/' . $a_project->project_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $upload_tooltip = $this->lang->line('tooltip_upload_text');
                $upload_url = base_url() . 'file_manager_module/get_upload_file_form_with_project_without_task/' . $a_project->project_id;
                $upload_anchor =
                    '<a ' . ' title="' . $upload_tooltip . '" ' . ' href="' . $upload_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-upload fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'project_module/edit_project/' . $a_project->project_id;
                $edit_anchor =
                    '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $complete_tooltip = $this->lang->line('tooltip_complete_text');
                $complete_url = base_url() . 'project_module/all_projects/make_project_complete/' . $a_project->project_id;
                $complete_anchor =
                    '<a ' . ' title="' . $complete_tooltip . '" ' . ' href="' . $complete_url . '" ' . ' class="complete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'project_module/delete_project/' . $a_project->project_id;
                $delete_anchor =
                    '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="delete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $projects[$i]->action = '';
                $projects[$i]->action .= $view_anchor . '&nbsp;&nbsp;';
                $projects[$i]->action .= $upload_anchor . '&nbsp;&nbsp;';
                if ($projects[$i]->progress != 100) {
                    $projects[$i]->action .= $complete_anchor . '&nbsp;&nbsp;';
                }
                $projects[$i]->action .= $edit_anchor . '&nbsp;&nbsp;';
                $projects[$i]->action .= $delete_anchor;

                $row->action = $projects[$i]->action;
                /*action ends*/

                $table_data[] = $row;

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $table_data;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    public function corespondingProject_seenByOther()
    {
        $user_id = $this->uri->segment(4);
        $this->corespondingProject($seen_by_other = true, $user_id);
    }

    public function corespondingProject($seen_by_other = false, $given_user_id = false)
    {
        $data['which_project'] = 'my_projects';

        if ($seen_by_other == true) {
            $data['seen_by'] = 'other';
        } else {
            $data['seen_by'] = 'me';
        }

        $this->lang->load('coresponding_project');

        if ($seen_by_other && $given_user_id) {
            $user_id = $given_user_id;
        } else {
            $user_id = $this->session->userdata('user_id');
        }

        $data['user_id'] = $user_id;
        $data['user_info'] = $this->Project_model->getUser($user_id);

        $admin_group_id = $this->ion_auth->in_group('admin', $user_id);

        $data['projects_belongs_to'] = '';
        if ($this->ion_auth->in_group('staff', $user_id)) {
            $data['projects_belongs_to'] = 'staff';
        }
        if ($this->ion_auth->in_group('client', $user_id)) {
            $data['projects_belongs_to'] = 'client';
        }

        if ($data['projects_belongs_to'] == '') {
            redirect('users/auth/does_not_exist');
        }

        if ($data['projects_belongs_to'] == 'staff' && $this->ion_auth->in_group('client')) {
            redirect('users/auth/need_permission');
        }

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }


        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("myprojects_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getCorrespondingProjectsByAjax()
    {
        $this->lang->load('coresponding_project');

        $projects = array();
        $table_data = array();

        $requestData = $_REQUEST;
        $is_admin = $requestData['is_admin'];
        $is_client = $requestData['is_client'];
        $is_staff = $requestData['is_staff'];

        $which_project = $requestData['which_project'];
        $seen_by = $requestData['seen_by'];
        $user_id = $requestData['user_id'];
        $projects_belongs_to = $requestData['projects_belongs_to'];
        //print_r($requestData);

        $staff_id = false;
        if ($projects_belongs_to == 'staff') {
            $staff_id = $user_id;
        }
        $client_id = false;
        if ($projects_belongs_to == 'client') {
            $client_id = $user_id;
        }


        $columns[0] = 'project_name';
        $columns[1] = 'client_name';
        $columns[2] = 'company';
        $columns[3] = 'progress';
        $columns[4] = 'budget';
        $columns[5] = 'start_date';
        $columns[6] = 'end_date';
        $columns[7] = 'status';
        $columns[8] = 'action';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['project_name'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['client_name'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['client_company'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['progress'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['budget'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][7]['search']['value'])) {
            $specific_filters['status'] = $requestData['columns'][7]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        //before query , remember only one can be true between $staff_id and $client_id

        $totalData = $this->Project_model->countProjectsByAjax(false, false, $client_id, $staff_id);


        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Project_model->countProjectsByAjax($common_filter_value, $specific_filters, $client_id, $staff_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $projects = $this->Project_model->getProjectsByAjax($common_filter_value, $specific_filters, $order, $limit, $client_id, $staff_id);


        if ($projects == false || empty($projects) || $projects == null) {
            $projects = false;
            $table_data = false;
        }

        $last_query = $this->db->last_query();

        $currency_position = $this->custom_settings_library->getASettingsValue('currency_settings', 'currency_position');
        if (empty($currency_position) || $currency_position == '' || $currency_position == null) {
            $currency_position = false;
        }
        $currencies = $this->Project_model->allCurrency();


        if ($projects) {

            $i = 0;
            foreach ($projects as $a_project) {

                $row = new stdClass();

                $row->project_name = $a_project->project_name;
                /*client name starts*/
                $client_tooltip = $a_project->first_name . ' ' . $a_project->last_name;
                $client_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_project->client_id;
                $client_name_anchor = '<a ' . 'title="' . $client_tooltip . '"' . ' href="' . $client_url . '"' . ' >' . $a_project->first_name . ' ' . $a_project->last_name . '</a>';
                $row->client_name = $client_name_anchor;
                /*client name ends*/

                $row->company = $a_project->company;

                /*date starts*/
                $projects[$i]->st_dt = new stdClass();
                $projects[$i]->en_dt = new stdClass();

                $projects[$i]->st_dt->timestamp = $a_project->start_date;
                $projects[$i]->en_dt->timestamp = $a_project->end_date;

                if ($a_project->start_date == 0 || $a_project->start_date == false || $a_project->start_date == null) {
                    $projects[$i]->st_dt->display = $this->lang->line('no_date_text');
                } else {
                    $projects[$i]->st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_project->start_date);
                }

                if ($a_project->end_date == 0 || $a_project->start_date == false || $a_project->start_date == null) {
                    $projects[$i]->en_dt->display = $this->lang->line('no_date_text');
                } else {
                    $projects[$i]->en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_project->end_date);
                }

                $row->st_dt = $projects[$i]->st_dt;
                $row->en_dt = $projects[$i]->en_dt;
                /*date ends*/

                /*progress starts*/
                $projects[$i]->prog = new stdClass();
                $projects[$i]->prog->int = $a_project->progress;

                $progress_span_class = '';
                $progressbar_inner_div_class = '';
                if ($a_project->progress < 33) {
                    $progress_span_class = 'label label-danger pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-danger';
                } else if ($a_project->progress > 66) {
                    $progress_span_class = 'label label-success pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-success';
                } else {
                    $progress_span_class = 'label label-primary pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-primary';
                }

                $progress_span = '<span ' . 'class="' . $progress_span_class . '"' . ' >' . $a_project->progress . '%' . '</span>';

                $progressbar_inner_div_style = 'width:' . $a_project->progress . '%';
                $progressbar_inner_div = '<div ' . ' class="' . $progressbar_inner_div_class . '"' . 'style="' . $progressbar_inner_div_style . '""' . '>' . '</div>';
                $progressbar_outer_div = '<div class="progress progress-xs" >' . $progressbar_inner_div . '</div>';

                $projects[$i]->prog->html = $progress_span . '&nbsp;' . $progressbar_outer_div;

                $row->prog = $projects[$i]->prog;
                /*progress ends*/

                /*budget starts*/
                $projects[$i]->budg = new stdClass();
                $projects[$i]->budg->dec = $a_project->budget;

                $currency_sign = '';
                foreach ($currencies as $a_currency) {
                    if ($projects[$i]->currency_id == $a_currency->currency_id) {
                        $currency_sign = $a_currency->currency_sign;
                    }
                }

                $projects[$i]->budg->html = '';
                if (!$currency_position) {
                    $projects[$i]->budg->html = $currency_sign . ' ' . $a_project->budget;
                } else {
                    if ($currency_position == 'left_along') {
                        $projects[$i]->budg->html = $currency_sign . ' ' . $a_project->budget;
                    }
                    if ($currency_position == 'right_along') {
                        $projects[$i]->budg->html = $a_project->budget . ' ' . $currency_sign;
                    }
                    if ($currency_position == 'left_far') {
                        $projects[$i]->budg->html = $currency_sign . ' &nbsp;&nbsp; ' . $a_project->budget;
                    }
                    if ($currency_position == 'right_far') {
                        $projects[$i]->budg->html = $a_project->budget . ' &nbsp;&nbsp; ' . $currency_sign;
                    }
                }

                $row->budg = $projects[$i]->budg;
                /*budget ends*/


                /*active - inactive starts*/
                $projects[$i]->sts = new stdClass();
                $projects[$i]->sts->int = $a_project->status;

                if ($a_project->status == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'project_module/all_projects/deactivate_project/' . $a_project->project_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'project_module/all_projects/activate_project/' . $a_project->project_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $this->lang->line('tooltip_activate_text') . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $projects[$i]->sts->html = '';
                if ($this->ion_auth->is_admin()) {
                    $projects[$i]->sts->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                } else {
                    $projects[$i]->sts->html = $status_span;
                }


                $row->sts = $projects[$i]->sts;
                /*active - inactive ends*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'projectroom_module/project_overview/' . $a_project->project_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $upload_tooltip = $this->lang->line('tooltip_upload_text');
                $upload_url = base_url() . 'file_manager_module/get_upload_file_form_with_project_without_task/' . $a_project->project_id;
                $upload_anchor =
                    '<a ' . ' title="' . $upload_tooltip . '" ' . ' href="' . $upload_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-upload fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'project_module/edit_project/' . $a_project->project_id;
                $edit_anchor =
                    '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $complete_tooltip = $this->lang->line('tooltip_complete_text');
                $complete_url = base_url() . 'project_module/all_projects/make_project_complete/' . $a_project->project_id;
                $complete_anchor =
                    '<a ' . ' title="' . $complete_tooltip . '" ' . ' href="' . $complete_url . '" ' . ' class="complete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'project_module/delete_project/' . $a_project->project_id;
                $delete_anchor =
                    '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="delete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $projects[$i]->action = '';
                if (
                    $is_admin == 'admin'
                    ||
                    (
                        $projects[$i]->status == 1 &&
                        ($projects_belongs_to == 'client' || $projects_belongs_to == 'staff')
                        && $seen_by == 'me'
                    )
                ) {
                    $projects[$i]->action .= $view_anchor . '&nbsp;&nbsp;';
                    $projects[$i]->action .= $upload_anchor . '&nbsp;&nbsp;';
                }

                if ($is_admin == 'admin') {
                    if ($projects[$i]->progress != 100) {
                        $projects[$i]->action .= $complete_anchor . '&nbsp;&nbsp;';
                    }
                    $projects[$i]->action .= $edit_anchor . '&nbsp;&nbsp;';
                    $projects[$i]->action .= $delete_anchor;
                }


                $row->action = $projects[$i]->action;
                /*action ends*/

                $table_data[] = $row;

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        $json_data['data'] = $table_data;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getActiveClientsByAjax()
    {

        $this->lang->load('project');

        $search_filter = false;
        if (!empty($this->input->get('q'))) {
            $search_filter = $this->input->get('q');
        }

        $search_filter = trim($search_filter);

        $page = $this->input->get('page');
        if (!$this->input->get('page')) {
            $page = 1;
        }

        $limit = 10;

        $total_count = $this->Project_model->countActiveUsersByAjax($group_id = 3, $search_filter); //client group id = 3
        $offset = ($page - 1) * $limit;

        //echo 'page:'.$page.' total_count:'.$total_count.' offset:'.$offset;die();
        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count; //bool

        $active_clients = $this->Project_model->getActiveUsers_byAjax($group_id = 3, $search_filter, $limit, $offset); //client group id = 3
        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();
        if ($active_clients) {


            foreach ($active_clients as $a_client) {
                $cl = array();
                $cl['id'] = $a_client->id;
                $cl['text'] = $a_client->first_name . ' ' . $a_client->last_name;
                $cl['company'] = $a_client->company;

                $items = $cl;
                $json_data['items'][] = $items;
            }
        } else {
            $cl = array();
            $cl['id'] = '0';
            $cl['text'] = $this->lang->line('no_client_found_text');
            $cl['company'] = $this->lang->line('no_company_text');

            $items = $cl;
            $json_data['items'][] = $items;
        }

        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        //$json_data['last_query'] = $last_query;
        echo json_encode($json_data);
    }

    public function getActiveStaffsByAjax()
    {
        $this->lang->load('project');

        $search_filter = false;
        if (!empty($this->input->get('q'))) {
            $search_filter = $this->input->get('q');
        }

        $search_filter = trim($search_filter);

        $page = $this->input->get('page');
        if (!$this->input->get('page')) {
            $page = 1;
        }

        $limit = 10;

        $total_count = $this->Project_model->countActiveUsersByAjax($group_id = 4, $search_filter); //staff group id = 3
        $offset = ($page - 1) * $limit;

        //echo 'page:'.$page.' total_count:'.$total_count.' offset:'.$offset;die();
        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count; //bool

        $active_staffs = $this->Project_model->getActiveUsers_byAjax($group_id = 4, $search_filter, $limit, $offset); //staff group id = 4

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();
        if ($active_staffs) {


            foreach ($active_staffs as $a_staff) {
                $st = array();
                $st['id'] = $a_staff->id;
                $st['text'] = $a_staff->first_name . ' ' . $a_staff->last_name;

                $items = $st;
                $json_data['items'][] = $items;
            }
        } else {
            $st = array();
            $st['id'] = 0;
            $st['text'] = $this->lang->line('no_staff_found_text');

            $items = $st;
            $json_data['items'][] = $items;
        }

        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        //$json_data['last_query'] = $last_query;
        echo json_encode($json_data);
    }

    public function addProject()
    {
        $data['dateformat'] = $this->custom_datetime_library->get_dateformat_PHP_to_jQueryUI();
        $exist_result = $this->custom_settings_library->ifSettingsTypeExist('currency_settings', 'currency_sign');

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        if ($exist_result) {
            $currency_settings = $this->custom_settings_library->getASettings('currency_settings', 'currency_name');
            $currency_setting_name = $currency_settings->settings_value;
            $currency_setting_id = $this->Project_model->currencySettingId($currency_setting_name);
            $test_currency = $currency_setting_id->currency_id;

            if ($test_currency) {
                $data['check_currency'] = $test_currency;
            }

        } else {

            $data['check_currency'] = "";
        }

        $staff_group_id = 4;
        // here staff id 4 for show the name to assign project
        $client_group_id = 3;
        $data['queryresult'] = $this->Project_model->getAllStaff($staff_group_id);
        $data['allClient'] = $this->Project_model->getAllUsers($client_group_id);
        $data['allCurrency'] = $this->Project_model->allCurrency();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("create_project_page", $data);
        $this->load->view("common_module/footer");
    }

    public function saveProjectInfo()
    {
        $data['project_name'] = $this->input->post('project_name');
        $data['client_id'] = $this->input->post('client_id');
        //$data['company_name'] = $this->input->post('company_name');
        $this->form_validation->set_rules('project_name', str_replace(':', '', $this->lang->line('label_project_name_text')),
            'required|is_unique[rspm_tbl_project.project_name]|max_length[30]', array(
                'required' => $this->lang->line('project_name_required'),
                'is_unique' => $this->lang->line('project_name_unique'),
                'max_length' => $this->lang->line('project_name_max_length')
            )
        );
        $this->form_validation->set_rules('client_id', str_replace(':', '', $this->lang->line('label_client_text')), 'required', array(
                'required' => $this->lang->line('client_name_required')
            )
        );
        if ($this->input->post('budget') != '' || $this->input->post('budget') != NULL) {
            $this->form_validation->set_rules('budget', 'budget', 'decimal', array(
                'decimal' => $this->lang->line('budget_val_text')
            ));
        }
        if ($this->form_validation->run() == true) {
            $data['project_name'] = $this->input->post('project_name');
            $data['client_id'] = $this->input->post('client_id');
            //$data['company_name'] = $this->input->post('company_name');
            $data['progress'] = $this->input->post('progress');
            $data['status'] = $this->input->post('status');
            $data['currency_id'] = $this->input->post('currency');
            $data['budget'] = $this->input->post('budget');

            $start_date = $this->input->post('date');
            $end_date = $this->input->post('end_date');

            if (!$this->input->post('date') || $this->input->post('date') == '') {
                $data['start_date'] = 0;
            } else {
                $data['start_date'] = $this->custom_datetime_library->convert_and_return_DateToTimestamp($start_date);
            }

            if (!$this->input->post('end_date') || $this->input->post('end_date') == '') {
                $data['end_date'] = 0;
            } else {
                $data['end_date'] = $this->custom_datetime_library->convert_and_return_DateToTimestamp($end_date);
            }

            $data['created_date'] = $this->custom_datetime_library->getCurrentTimestamp();
            $data['modified_date'] = $this->custom_datetime_library->getCurrentTimestamp();
            $this->Project_model->saveProject($data);
            $assigned_staffs = $this->input->post('assigned_staffs');
            $project_id = $this->db->insert_id();

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $data['client_id'],                                                     //2.    $created_for
                'project',                                                              //3.    $type
                $project_id,                                                            //4.    $type_id
                'created',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            for ($i = 0; $i < count($assigned_staffs); $i++) {
                $assigned_staffs[$i];
                $project_id;
                $this->Project_model->assignStaff($assigned_staffs[$i], $project_id);

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $assigned_staffs[$i],                                                   //2.    $created_for
                    'project',                                                              //3.    $type
                    $project_id,                                                            //4.    $type_id
                    'assigned_staff',                                                       //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'staff',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

            }

            $this->session->set_flashdata('project_add_success', $this->lang->line('project_add_success'));
            $this->session->set_flashdata('project_id', $project_id);
            redirect(base_url() . 'project_module/all_projects');

        } else {
            $this->session->set_flashdata('val_error', validation_errors());
            $this->session->set_flashdata('project_name', $this->input->post('project_name'));
            $this->session->set_flashdata('company_name', $this->input->post('company_name'));
            $this->session->set_flashdata('status', $this->input->post('status'));
            $this->session->set_flashdata('date', $this->input->post('date'));
            $this->session->set_flashdata('end_date', $this->input->post('end_date'));
            $client_group_id = 3;
            $data['allClient'] = $this->Project_model->getAllUsers($client_group_id);

            if ($this->input->post('client_id')) {
                $this->session->set_flashdata('client_id', $this->input->post('client_id'));
            }

            if ($this->input->post('company_name')) {
                $this->session->set_flashdata('company_name', $this->input->post('company_name'));
            }

            if ($this->input->post('progress')) {
                $this->session->set_flashdata('progress', $this->input->post('progress'));
            }

            if ($this->input->post('assigned_staffs[]')) {
                $this->session->set_flashdata('assigned_staffs', $this->input->post('assigned_staffs[]'));
            }
            //should be change
            if ($this->input->post('currency')) {
                //should be change
                $this->session->set_flashdata('currency', $this->input->post('currency'));
            }

            if ($this->input->post('budget')) {
                $this->session->set_flashdata('budget', $this->input->post('budget'));
            }
            redirect(base_url() . 'project_module/add_project');
        }
    }

    function numeric_wcomma($str)
    {
        return preg_match('/^[0-9,]+$/', $str);
    }

    public function deleteProjectInfo()
    {
        $check_page = $this->uri->segment(2);
        $project_id = $this->uri->segment(3);
        $queryresult = $this->Project_model->deactivate_project($project_id);

        $this->Project_model->deleteProject($project_id);
        $this->session->set_flashdata('delete_success', $this->lang->line('delete_success'));
        $this->session->set_flashdata('project_delete_success', $this->lang->line('project_delete_success'));

        /*creating log starts*/

        /*for a client*/
        $project_info = $this->Project_model->getProject($project_id);
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $project_info->client_id,                                               //2.    $created_for
            'project',                                                              //3.    $type
            $project_id,                                                            //4.    $type_id
            'deleted',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );

        /*for assigned staffs*/
        $all_assigned_staffs_array = $this->Project_model->getAllAssignedStaffsArray($project_id);

        if ($all_assigned_staffs_array) {
            foreach ($all_assigned_staffs_array as $an_assingned_staff) {
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $an_assingned_staff['staff_id'],                                        //2.    $created_for
                    'project',                                                              //3.    $type
                    $project_id,                                                            //4.    $type_id
                    'deleted',                                                              //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'staff',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
            }
        }
        /*creating log ends*/


        $this->deassignStaffsFromProject($project_id);
        $this->deassignStaffsFromProjectTasks($project_id);
        $this->deactivateProjectTasks($project_id);
        $this->deleteProjectTasks($project_id);


        if ($check_page == "delete_project") {
            redirect(base_url() . 'project_module/all_projects');
        } else {
            redirect(base_url() . 'project_module/my_projects');
        }

    }

    /*-------------------------------------------------------------*/
    //on project delete . . .

    public function deassignStaffsFromProject($project_id)
    {
        $this->Project_model->deassignStaffsFromProject($project_id);
    }

    public function deactivateProjectTasks($project_id)
    {
        $this->Project_model->deactivateProjectTasks($project_id);
    }

    public function deleteProjectTasks($project_id)
    {
        $this->Project_model->deleteProjectTasks($project_id);
    }

    public function deassignStaffsFromProjectTasks($project_id)
    {
        $tasks = $this->Project_model->getTasks($project_id);

        if ($tasks) {
            foreach ($tasks as $a_task) {
                $this->deassignStaffsFromTask($a_task->task_id);
            }
        }
    }

    public function deassignStaffsFromTask($task_id)
    {
        $this->Project_model->deassignStaffsFromTask($task_id);
    }

    /*-------------------------------------------------------------*/

    public function editProject()
    {
        $this->lang->load('project_edit');
        $data['dateformat'] = $this->custom_datetime_library->get_dateformat_PHP_to_jQueryUI();

        $project_id = $this->uri->segment(3);

        $data['check_page'] = $this->uri->segment(2);

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        // print_r($data['check_page']); die();
        $data['project_info_for_edit'] = $this->Project_model->getProject($project_id);
        if ($data['project_info_for_edit']->start_date == 0 || $data['project_info_for_edit']->start_date == NULL) {
            $data['project_info_for_edit']->start_date = "";
        } else {
            $data['project_info_for_edit']->start_date = $this->custom_datetime_library->convert_and_return_TimestampToDate($data['project_info_for_edit']->start_date);
        }
        if ($data['project_info_for_edit']->end_date == 0 || $data['project_info_for_edit']->end_date == NULL) {
            $data['project_info_for_edit']->end_date = "";
        } else {
            $data['project_info_for_edit']->end_date = $this->custom_datetime_library->convert_and_return_TimestampToDate($data['project_info_for_edit']->end_date);
        }

        $client_group_id = 3;


        $json_client = array(); // contain 1 value
        $client_info = $this->Project_model->getUser($data['project_info_for_edit']->client_id);
        $json_client = json_encode($client_info);
        $data['json_client'] = $json_client;

        $assigned_staffs = $this->Project_model->getAssignedStaffs($project_id);
        $data['json_assigned_staffs'] = false;
        if ($assigned_staffs) {

            $prepare_assigned_staffs = array();
            foreach ($assigned_staffs as $an_assigned_staff) {
                $an_assigned_staff_info = $this->Project_model->getUser($an_assigned_staff->staff_id);
                $prepare_assigned_staffs[] = $an_assigned_staff_info;
            }

            $data['json_assigned_staffs'] = json_encode($prepare_assigned_staffs);
        }


        $staff_group_id = 4;

        $data['getAllCurency'] = $this->Project_model->allCurrency();

        //print_r($data['project_info_for_edit']);die();
        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("edit_project_page", $data);
        $this->load->view("common_module/footer");
    }


    public function getCurrencyPosition()
    {
        $currency_pos_exists = $this->custom_settings_library->ifSettingsTypeExist('currency_settings', 'currency_position');
        if ($currency_pos_exists) {
            $currency_position_settings = $this->custom_settings_library->getASettings('currency_settings', 'currency_position');
            return $currency_position_settings->settings_value;
        } else {
            return '';
        }
    }

    public function updateProject()
    {
        $this->lang->load('project_update');                                //for log


        $data['project_name'] = $this->input->post('project_name');
        $data['client_id'] = $this->input->post('client_id');
        $project_id = $data['project_id'] = $this->input->post('project_id');

        $check_unique_of_pro_name = $this->Project_model->checkunique($data['project_id'], $data['project_name']);
        if ($check_unique_of_pro_name) {
            $this->session->set_flashdata('alredy_exist', $this->lang->line('project_name_unique'));
            redirect(base_url() . 'project_module/edit_project/' . $project_id);
        }
        $this->form_validation->set_rules('project_name', str_replace(':', '', $this->lang->line('label_project_name_text')), 'required|max_length[30]', array(
                'required' => $this->lang->line('project_name_required'),
                'max_length' => $this->lang->line('project_name_max_length')
            )
        );
        $this->form_validation->set_rules('client_id', str_replace(':', '', $this->lang->line('label_client_text')), 'required', array(
                'required' => $this->lang->line('client_name_required')
            )
        );

        if ($this->form_validation->run() == TRUE) {
            $data['project_id'] = $this->input->post('project_id');
            $check_page = $this->input->post('check_page');
            $data['project_name'] = $this->input->post('project_name');
            $data['client_id'] = $this->input->post('client_id');
            $data['project_id'] = $this->input->post('project_id');
            //$data['company_name'] = $this->input->post('company_name');
            $data['progress'] = $this->input->post('progress');
            $data['status'] = $this->input->post('status');

            $start_date = $this->input->post('date');
            $end_date = $this->input->post('end_date');

            //trim is working here . . .

            if (
                trim($this->input->post('date')) === ''
                || $this->input->post('date') == ''
                || $this->input->post('date') == null
                || empty($this->input->post('date'))
            ) {
                $data['start_date'] = 0;

            } else {

                $data['start_date'] = $this->custom_datetime_library->convert_and_return_DateToTimestamp($start_date);
            }

            if (
                trim($this->input->post('date')) === ''
                || $this->input->post('date') == ''
                || $this->input->post('date') == null
                || empty($this->input->post('date'))
            ) {
                $data['end_date'] = 0;
            } else {

                $data['end_date'] = $this->custom_datetime_library->convert_and_return_DateToTimestamp($end_date);
            }


            $data['currency_id'] = $this->input->post('currency');
            $data['budget'] = $this->input->post('budget');
            $data['modified_date'] = $this->custom_datetime_library->getCurrentTimestamp();


            $project_id = $data['project_id'];

            /*creating change_list for log starts*/
            $project_info_from_db = $this->Project_model->getProject($project_id);
            $change_list = array();

            $change_list['change_type'] = 'project';

            if ($project_info_from_db->project_name != $data['project_name']) {

                $change_list['project_name']['need_to_convert_as'] = '';
                $change_list['project_name']['field_name'] = $this->lang->line('log_field_project_name_text');
                $change_list['project_name']['from'] = $project_info_from_db->project_name;
                $change_list['project_name']['to'] = $data['project_name'];
            }
            if ($project_info_from_db->client_id != $data['client_id']) {

                $change_list['client']['need_to_convert_as'] = 'user_profile';
                $change_list['client']['field_name'] = $this->lang->line('log_field_client_text');
                $change_list['client']['from'] = $project_info_from_db->client_id;
                $change_list['client']['to'] = $data['client_id'];
            }
            /*if ($project_info_from_db->company_name != $data['company_name']) {

                $change_list['company_name']['need_to_convert_as'] = '';
                $change_list['company_name']['field_name'] = $this->lang->line('log_field_company_name_text');
                $change_list['company_name']['from'] = $project_info_from_db->company_name;
                $change_list['company_name']['to'] = $data['company_name'];
            }*/

            if ($project_info_from_db->progress != $data['progress']) {

                $change_list['progress']['need_to_convert_as'] = 'percentage';
                $change_list['progress']['field_name'] = $this->lang->line('log_field_progress_text');
                $change_list['progress']['from'] = $project_info_from_db->progress;
                $change_list['progress']['to'] = $data['progress'];
            }


            if ($project_info_from_db->start_date != 0) {
                $db_st_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($project_info_from_db->start_date);
            } else {
                $db_st_datestring = '';
            }

            if ($data['start_date'] != '') {
                $pst_st_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($data['start_date']);
            } else {
                $pst_st_datestring = '';
            }

            if ($db_st_datestring != $pst_st_datestring) {

                $change_list['start_date']['need_to_convert_as'] = 'date';
                $change_list['start_date']['field_name'] = $this->lang->line('log_field_start_date_text');
                $change_list['start_date']['from'] = $project_info_from_db->start_date;
                $change_list['start_date']['to'] = $data['start_date'];
            }

            if ($project_info_from_db->end_date != 0) {
                $db_end_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($project_info_from_db->end_date);
            } else {
                $db_end_datestring = '';
            }
            if ($data['end_date'] != '') {
                $pst_end_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($data['end_date']);
            } else {
                $pst_end_datestring = '';
            }

            if ($db_end_datestring != $pst_end_datestring) {

                $change_list['end_date']['need_to_convert_as'] = 'date';
                $change_list['end_date']['field_name'] = $this->lang->line('log_field_end_date_text');
                $change_list['end_date']['from'] = $project_info_from_db->end_date;
                $change_list['end_date']['to'] = $data['end_date'];
            }

            if ($project_info_from_db->status != $data['status']) {

                $change_list['status']['need_to_convert_as'] = 'status';
                $change_list['status']['field_name'] = $this->lang->line('log_field_status_text');
                $change_list['status']['from'] = $project_info_from_db->status;
                $change_list['status']['to'] = $data['status'];
            }


            if ($project_info_from_db->currency_id != $data['currency_id']) {

                $a_currency_row = $this->Project_model->getCurrency($project_info_from_db->currency_id);
                $change_list['currency']['need_to_convert_as'] = '';
                $change_list['currency']['field_name'] = $this->lang->line('log_field_currency_text');
                $change_list['currency']['from'] = $a_currency_row->currency_short_name;
                $old_currency_sign = $a_currency_row->currency_sign;

                $another_currency_row = $this->Project_model->getCurrency($data['currency_id']);
                $change_list['currency']['need_to_convert_as'] = '';
                $change_list['currency']['field_name'] = $this->lang->line('log_field_currency_text');
                $change_list['currency']['to'] = $another_currency_row->currency_short_name;
                $new_currency_sign = $another_currency_row->currency_sign;

            } else {

                $a_currency_row = $this->Project_model->getCurrency($project_info_from_db->currency_id);
                $old_currency_sign = $a_currency_row->currency_sign;
                $new_currency_sign = $a_currency_row->currency_sign;
            }

            $a_currency_row = $this->Project_model->getCurrency($data['currency_id']);
            $currency_position = $this->getCurrencyPosition();

            if ($project_info_from_db->budget != $data['budget']) {

                if ($currency_position == 'left_along') {
                    $change_list['budget']['need_to_convert_as'] = '';
                    $change_list['budget']['field_name'] = $this->lang->line('log_field_budget_text');
                    $change_list['budget']['from'] = $old_currency_sign . ' ' . $project_info_from_db->budget;
                    $change_list['budget']['to'] = $new_currency_sign . ' ' . $data['budget'];

                } else if ($currency_position == 'right_along') {

                    $change_list['budget']['need_to_convert_as'] = '';
                    $change_list['budget']['field_name'] = $this->lang->line('log_field_budget_text');
                    $change_list['budget']['from'] = $project_info_from_db->budget . ' ' . $old_currency_sign;
                    $change_list['budget']['to'] = $data['budget'] . ' ' . $new_currency_sign;

                } else if ($currency_position == 'left_far') {

                    $change_list['budget']['need_to_convert_as'] = '';
                    $change_list['budget']['field_name'] = $this->lang->line('log_field_budget_text');
                    $change_list['budget']['from'] = $old_currency_sign . ' ' . $project_info_from_db->budget;
                    $change_list['budget']['to'] = $new_currency_sign . ' ' . $data['budget'];

                } else {

                    //'right_far' or '';
                    $change_list['budget']['need_to_convert_as'] = '';
                    $change_list['budget']['field_name'] = $this->lang->line('log_field_budget_text');
                    $change_list['budget']['from'] = $project_info_from_db->budget . ' ' . $old_currency_sign;
                    $change_list['budget']['to'] = $data['budget'] . ' ' . $new_currency_sign;
                }

            }


            if ($change_list) {
                $encoded_change_list = json_encode($change_list);
            } else {
                $encoded_change_list = '';
            }
            /*creating change_list for log ends*/

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $data['client_id'],                                                     //2.    $created_for
                'project',                                                              //3.    $type
                $project_id,                                                            //4.    $type_id
                'updated',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                $encoded_change_list                                                    //13.   $change_list
            );

            if ($project_info_from_db->client_id != $data['client_id']) {

                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $project_info_from_db->client_id,                                       //2.    $created_for
                    'project',                                                              //3.    $type
                    $project_id,                                                            //4.    $type_id
                    'deallocated_client',                                                   //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'client',                                                               //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );


                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $data['client_id'],                                                     //2.    $created_for
                    'project',                                                              //3.    $type
                    $project_id,                                                            //4.    $type_id
                    'allocated_client',                                                     //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'client',                                                               //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    $encoded_change_list                                                    //13.   $change_list
                );
            }

            /*for assigned staffs*/
            $assigned_staffs = $this->input->post('assigned_staffs');
            if ($assigned_staffs) {
                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                //1.    $created_by
                        $an_assingned_staff,                                                //2.    $created_for
                        'project',                                                          //3.    $type
                        $project_id,                                                        //4.    $type_id
                        'updated',                                                          //5.    $activity
                        'admin',                                                            //6.    $activity_by
                        'staff',                                                            //7.    $activity_for
                        '',                                                                 //8.    $sub_type
                        '',                                                                 //9.    $sub_type_id
                        '',                                                                 //10.   $super_type
                        '',                                                                 //11.   $super_type_id
                        '',                                                                 //12.   $other_information
                        $encoded_change_list                                                //13.   $change_list
                    );
                }
            }

            /*creating log ends*/

            //$this->Project_model->updateProject($data);

            $assigned_staffs = $this->input->post('assigned_staffs');
            $get_all_assigned_staffs_array = $this->Project_model->getAllAssignedStaffsArray($project_id);

            /*this if block is made true beacase else is unnecessary and clashes with the log creation logic*/
            if (1) {

                $hold_db_staff = array();
                if ($get_all_assigned_staffs_array) {
                    foreach ($get_all_assigned_staffs_array as $d_staff) {
                        array_push($hold_db_staff, $d_staff['staff_id']);
                    }
                }

                $is_assigned = false;                                                   //for log purposes
                if ($assigned_staffs) {
                    foreach ($assigned_staffs as $an_assigned_staff) {
                        if (in_array($an_assigned_staff, $hold_db_staff) == false) {

                            $project_id;
                            $this->Project_model->update_insert($project_id, $an_assigned_staff);
                            $is_assigned = true;                                          //for log purposes

                            /*creating log starts*/
                            $this->custom_log_library->createALog
                            (
                                $this->session->userdata('user_id'),                         //1.    $created_by
                                $an_assigned_staff,                                          //2.    $created_for
                                'project',                                                   //3.    $type
                                $project_id,                                                 //4.    $type_id
                                'assigned_staff',                                            //5.    $activity
                                'admin',                                                     //6.    $activity_by
                                'staff',                                                     //7.    $activity_for
                                '',                                                          //8.    $sub_type
                                '',                                                          //9.    $sub_type_id
                                '',                                                          //10.   $super_type
                                '',                                                          //11.   $super_type_id
                                '',                                                          //12.   $other_information
                                $encoded_change_list                                         //13.   $change_list
                            );
                            /*creating log ends*/

                        }
                    }
                }

                $is_deassigned = false;                                                  //for log purposes
                if ($hold_db_staff) {
                    foreach ($hold_db_staff as $a_db_staff) {
                        if (in_array($a_db_staff, $assigned_staffs) == false) {

                            $project_id;
                            $this->Project_model->update_delete($project_id, $a_db_staff);
                            $is_deassigned = true;                                          //for log purposes

                            /*creating log starts*/
                            $this->custom_log_library->createALog
                            (
                                $this->session->userdata('user_id'),                           //1.    $created_by
                                $a_db_staff,                                                   //2.    $created_for
                                'project',                                                     //3.    $type
                                $project_id,                                                   //4.    $type_id
                                'deassigned_staff',                                            //5.    $activity
                                'admin',                                                       //6.    $activity_by
                                'staff',                                                       //7.    $activity_for
                                '',                                                            //8.    $sub_type
                                '',                                                            //9.    $sub_type_id
                                '',                                                            //10.   $super_type
                                '',                                                            //11.   $super_type_id
                                '',                                                            //12.   $other_information
                                ''                                                             //13.   $change_list
                            );
                            /*creating log ends*/

                        }
                    }
                }

                $iter_nas = 0;
                $newly_assigned_staffs = '';
                foreach ($assigned_staffs as $an_assigned_staff) {

                    if ($iter_nas != 0) {
                        $newly_assigned_staffs .= ',' . $an_assigned_staff;
                    } else {
                        $newly_assigned_staffs .= $an_assigned_staff;
                    }
                    $iter_nas++;
                }

                $iter_pas = 0;
                $previously_assigned_staffs = '';
                foreach ($get_all_assigned_staffs_array as $an_pr_assigned_staff) {
                    if ($iter_pas != 0) {
                        $previously_assigned_staffs .= ',' . $an_pr_assigned_staff['staff_id'];
                    } else {
                        $previously_assigned_staffs .= $an_pr_assigned_staff['staff_id'];
                    }
                    $iter_pas++;
                }


                $staff_changed_list['staffs']['need_to_convert_as'] = 'user_list';
                $staff_changed_list['staffs']['field_name'] = $this->lang->line('log_field_staffs_text');
                $staff_changed_list['staffs']['from'] = $previously_assigned_staffs;
                $staff_changed_list['staffs']['to'] = $newly_assigned_staffs;

                $encoded_staff_changed_list = json_encode($staff_changed_list);


                if ($is_assigned || $is_deassigned) {

                    /*creating log starts*/
                    /*for client*/
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                           //1.    $created_by
                        $data['client_id'],                                            //2.    $created_for
                        'project',                                                     //3.    $type
                        $project_id,                                                   //4.    $type_id
                        'staffs_changed_announce',                                     //5.    $activity
                        'admin',                                                       //6.    $activity_by
                        'client',                                                      //7.    $activity_for
                        '',                                                            //8.    $sub_type
                        '',                                                            //9.    $sub_type_id
                        '',                                                            //10.   $super_type
                        '',                                                            //11.   $super_type_id
                        '',                                                            //12.   $other_information
                        $encoded_staff_changed_list                                    //13.   $change_list
                    );
                    /*creating log ends*/

                    foreach ($assigned_staffs as $an_assigned_staff) {

                        /*for staffs*/
                        $this->custom_log_library->createALog
                        (
                            $this->session->userdata('user_id'),                           //1.    $created_by
                            $an_assigned_staff,                                            //2.    $created_for
                            'project',                                                     //3.    $type
                            $project_id,                                                   //4.    $type_id
                            'staffs_changed_announce',                                     //5.    $activity
                            'admin',                                                       //6.    $activity_by
                            'staff',                                                       //7.    $activity_for
                            '',                                                            //8.    $sub_type
                            '',                                                            //9.    $sub_type_id
                            '',                                                            //10.   $super_type
                            '',                                                            //11.   $super_type_id
                            '',                                                            //12.   $other_information
                            $encoded_staff_changed_list                                    //13.   $change_list
                        );
                        /*creating log ends*/
                    }

                }


                $this->Project_model->updateProject($data);

                $this->session->set_flashdata('project_update', $this->lang->line('project_update_success'));
                $this->session->set_flashdata('project_id', $this->input->post('project_id'));
                $this->session->set_flashdata('project_name', $this->input->post('project_name'));

                //  redirect(base_url() . 'project_module/all_projects');
                if ($check_page == 'my_project_edit') {
                    redirect(base_url() . 'project_module/my_projects');
                } else {
                    redirect(base_url() . 'project_module/all_projects');
                }
            } else {
                /*this block is made off because it's unnecessary and clashes with the log-creation logic*/

                $project_id;
                $this->Project_model->update_delete_all($project_id);

                $this->session->set_flashdata('project_update', $this->lang->line('project_update_success'));
                $this->session->set_flashdata('project_id', $this->input->post('project_id'));
                $this->session->set_flashdata('project_name', $this->input->post('project_name'));

                if ($check_page == 'my_project_edit') {
                    redirect(base_url() . 'project_module/my_projects');
                } else {
                    redirect(base_url() . 'project_module/all_projects');
                }
            }
        } else {
            $this->session->set_flashdata('val_error', validation_errors());
            redirect(base_url() . 'project_module/edit_project/' . $project_id);
        }
    }

    function deactivateProject()
    {
        $which_project = $this->uri->segment(2);

        $project_id = $this->uri->segment(4);
        $is_deactivated = $this->Project_model->deactivate_project($project_id);

        if ($is_deactivated == true) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('project_id', $project_id);
            $this->session->set_flashdata('project_deactivate_success', $this->lang->line('project_deactivate_success_text'));

            /*creating log starts*/

            /*for a client*/
            $project_info = $this->Project_model->getProject($project_id);
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                //1.    $created_by
                $project_info->client_id,                                           //2.    $created_for
                'project',                                                          //3.    $type
                $project_id,                                                        //4.    $type_id
                'deactivated',                                                      //5.    $activity
                'admin',                                                            //6.    $activity_by
                'client',                                                           //7.    $activity_for
                '',                                                                 //8.    $sub_type
                '',                                                                 //9.    $sub_type_id
                '',                                                                 //10.   $super_type
                '',                                                                 //11.   $super_type_id
                '',                                                                 //12.   $other_information
                ''                                                                  //13.   $change_list
            );

            /*for assigned staffs*/
            $all_assigned_staffs_array = $this->Project_model->getAllAssignedStaffsArray($project_id);

            if ($all_assigned_staffs_array) {
                foreach ($all_assigned_staffs_array as $an_assingned_staff) {
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                //1.    $created_by
                        $an_assingned_staff['staff_id'],                                    //2.    $created_for
                        'project',                                                          //3.    $type
                        $project_id,                                                        //4.    $type_id
                        'deactivated',                                                      //5.    $activity
                        'admin',                                                            //6.    $activity_by
                        'staff',                                                            //7.    $activity_for
                        '',                                                                 //8.    $sub_type
                        '',                                                                 //9.    $sub_type_id
                        '',                                                                 //10.   $super_type
                        '',                                                                 //11.   $super_type_id
                        '',                                                                 //12.   $other_information
                        ''                                                                  //13.   $change_list
                    );
                }
            }

            $this->makeProjectTasksInactive($project_id);
            redirect('project_module/' . $which_project);
        }

    }

    function activateProject()
    {
        $which_project = $this->uri->segment(2);

        $project_id = $this->uri->segment(4);
        $is_activated = $this->Project_model->activate_project($project_id);
        if ($is_activated == true) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('project_id', $project_id);
            $this->session->set_flashdata('project_activate_success', $this->lang->line('project_activate_success_text'));

            /*creating log starts*/

            /*for a client*/
            $project_info = $this->Project_model->getProject($project_id);
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $project_info->client_id,                                               //2.    $created_for
                'project',                                                              //3.    $type
                $project_id,                                                            //4.    $type_id
                'activated',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            /*for assigned staffs*/
            $all_assigned_staffs_array = $this->Project_model->getAllAssignedStaffsArray($project_id);

            if ($all_assigned_staffs_array) {

                foreach ($all_assigned_staffs_array as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff['staff_id'],                                   //2.    $created_for
                        'project',                                                         //3.    $type
                        $project_id,                                                       //4.    $type_id
                        'activated',                                                       //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        '',                                                                //10.   $super_type
                        '',                                                                //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        ''                                                                 //13.   $change_list
                    );

                }

            }

            /*creating log ends*/


            redirect(base_url() . 'project_module/' . $which_project);
        }

    }

    /*----------------------------------------------------*/
    // on project activate deactivate
    public function makeProjectTasksInactive($project_id)
    {
        $this->Project_model->makeProjectTasksInactive($project_id);
    }

    public function makeProjectTasksActive($project_id)
    {
        $this->Project_model->makeProjectTasksActive($project_id);
    }

    /*----------------------------------------------------*/


    public function checkAdmin()
    {
        if ($this->ion_auth->is_admin()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function makeProjectComplete()
    {
        $this->lang->load('project');

        $which_project = $this->uri->segment(2);        //for redirecton need to know
        $project_id = $this->uri->segment(4);

        $is_made_completed = $this->Project_model->makeProjectComplete($project_id);

        if ($is_made_completed == true) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('project_id', $project_id);
            $this->session->set_flashdata('project_complete_success', $this->lang->line('project_complete_success_text'));

            /*creating log starts*/

            /*for a client*/
            $project_info = $this->Project_model->getProject($project_id);
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $project_info->client_id,                                               //2.    $created_for
                'project',                                                              //3.    $type
                $project_id,                                                            //4.    $type_id
                'completed',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            /*for assigned staffs*/
            $all_assigned_staffs_array = $this->Project_model->getAllAssignedStaffsArray($project_id);

            foreach ($all_assigned_staffs_array as $an_assingned_staff) {
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $an_assingned_staff['staff_id'],                                        //2.    $created_for
                    'project',                                                              //3.    $type
                    $project_id,                                                            //4.    $type_id
                    'completed',                                                            //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'staff',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
            }

            /*creating log ends*/

            redirect('project_module/' . $which_project);
        }

    }


}
