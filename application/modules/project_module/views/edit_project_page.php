<style>
    .select2-selection__choice {
        background-color: #428bca !important;
    }

    .select2-selection__choice__remove {
        color: white !important;
    }

    .select2-selection__choice__remove:hover {
        color: #d9534f !important;
        font-size: larger;
    }


</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'project_module/all_projects' ?>">
                        <?php echo lang('breadcrumb_all_projects_section_text') ?>
                    </a>
                    <?php echo ' | ' ?>
                <?php } ?>

                <a href="<?php echo base_url() . 'project_module/my_projects' ?>">
                    <?php echo lang('breadcrumb_my_projects_section_text') ?>
                </a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('project_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php
                            if ($this->session->flashdata('val_error')) {
                                print_r($this->session->flashdata('val_error'));
                            }
                            ?>

                            <?php
                            if ($this->session->flashdata('alredy_exist')) {
                                print_r($this->session->flashdata('alredy_exist'));
                            }
                            ?>


                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form action="<?php
                    echo base_url() . 'project_module/update';
                    ?>" role="form" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <input type="hidden" name="project_id"
                                   value="<?php echo $project_info_for_edit->project_id; ?>">

                            <div class="form-group">
                                <div class="form-group">
                                    <label for="project_name"><?php echo lang('label_project_name_text') ?></label>

                                    <input type="text" name="project_name" class="form-control" id="project_name"
                                           placeholder="<?php echo lang('placeholder_project_name_text') ?>"
                                           maxlength="30"
                                           value="<?php echo $project_info_for_edit->project_name; ?>">

                                </div>

                                <div class="form-group">
                                    <label for="client_name"><?php echo lang('label_client_text') ?></label>

                                    <select id='client' name="client_id" class="client form-control select2"
                                            data-placeholder="<?php echo lang('placeholder_client_text') ?>"
                                            style="width: 100%;">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="company_name"><?php echo lang('label_company_text') ?></label>

                                    <input type="text" name="company_name" class="form-control" id="company_name"
                                           readonly
                                           placeholder="<?php echo lang('placeholder_company_text') ?>"
                                           value="<?php echo $project_info_for_edit->company ?>"
                                    >

                                    <input type="hidden" name="check_page" class="form-control" id="check_page"
                                           value="<?php echo $check_page; ?>"
                                    >

                                </div>

                                <div class="form-group">
                                    <label for="currency"><?php echo lang('label_currency_text') ?></label>
                                    <select id='currency' name="currency" class="form-control"
                                            placeholder="<?php echo lang('label_currency_text') ?>"
                                            style="width: 100%;" data-live-search="true">


                                        <?php foreach ($getAllCurency as $value) { ?>
                                            <option value="<?php echo $value->currency_id ?>" <?php
                                            if ($project_info_for_edit->currency_id == $value->currency_id) {
                                                echo 'selected';
                                            }
                                            ?>>
                                                <?php
                                                echo $value->currency_sign;

                                                ?>
                                            </option>
                                        <?php } ?>
                                    </select>

                                </div>


                                <div class="form-group">
                                    <label for="budget"><?php echo lang('budget_text') ?></label>

                                    <input type="text" name="budget" class="form-control" id="budget"
                                           placeholder="<?php echo lang('placeholder_budget_text') ?> "
                                           value="<?php echo $project_info_for_edit->budget; ?>">


                                    <input type="hidden" name="project_id"
                                           value="<?php echo $project_info_for_edit->project_id; ?>">

                                </div>


                                <div class="form-group ">
                                    <input id="edit_progress" type="hidden"
                                           value="<?php echo $project_info_for_edit->progress; ?>">

                                    <label><?php echo lang('progress_text') ?>
                                        <div style="color: 	#428bca ;font-size: larger">
                                            <input id="show_progressamount" name="progress" type="text"
                                                   style="border: transparent;width: 30px" value="">
                                            <span>%</span>
                                        </div>
                                    </label>

                                    <input id="progressamount" type="hidden" name="progress" value=""
                                           style="color: #003399; border: white;width: 20px;margin-left: 200px">

                                    <div id="slider"></div>
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('date_text') ?></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date" class="form-control pull-right" id="txtFromDate"
                                               readonly style="background-color: inherit"
                                               value="<?php
                                               echo $project_info_for_edit->start_date; ?>"
                                        >
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('end_date_text') ?></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="end_date" class="form-control pull-right"
                                               id="txtToDate"
                                               readonly style="background-color: inherit"
                                               value="<?php
                                               echo $project_info_for_edit->end_date;

                                               ?>"
                                        >
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('assign_text') ?></label>

                                    <select id="staffs" name="assigned_staffs[]" class="staffs form-control"

                                            data-placeholder="<?php echo lang('multiselect_dafault_text') ?>"
                                            style="width: 100%;">
                                    </select>

                                    <select id="selrsnew2" style="width:300px" />
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('status_select_text') ?></label>

                                    <select class="form-control select" name="status"
                                            data-placeholder="<?php echo lang('status_select_text') ?>"
                                            style="width: 100%;">

                                        <option value="1"
                                            <?php if ($project_info_for_edit->status == 1) {
                                                echo 'selected';
                                            } ?>
                                        >
                                            <?php echo lang('active_text') ?>
                                        </option>

                                        <option value="0"
                                            <?php if ($project_info_for_edit->status == 0) {
                                                echo 'selected';
                                            } ?>

                                        >
                                            <?php echo lang('inactive_text') ?>
                                        </option>
                                    </select>
                                </div>


                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?>
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------->
<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<script>
    $('#datepicker').datepicker({
        autoclose: true
    });
</script>
<script>
    $('#datepicker_2').datepicker({
        autoclose: true
    });
</script>

<script>
    //get active clients by ajax
    var initial_client = [];
    var client = JSON.parse('<?php echo $json_client ?>');
    initial_client.push({id: client.id, text: client.first_name + ' ' + client.last_name, company: client.company});

    console.log('initial_client');
    console.log(initial_client);


    $(function () {

        $(".client").select2({
            data: initial_client,
            ajax: {
                url: '<?php echo base_url() . 'project_module/get_active_clients_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,
                allowClear: true,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data);
                     console.log(params);
                     console.log(data.more_pages);*/

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work

            }

        });

        $(".client").trigger('change');

    });


</script>

<script>
    $(function () {
        $('.client').on("select2:select", function (e) {

            var value = $(e.currentTarget).find("option:selected").val();
            //alert(value);
            var data = $('.client').select2('data');
            $('#company_name').val(data[0].company);

        });
    })
</script>

<script>
    $('#selrsnew2').select2({
        multiple:true,
        ajax:{}})
        .select2({
            data:
                [{"id":"2127","text":"Henry Ford"},{"id":"2199","text":"Tom Phillips"}]
        });
    $('#selrsnew2').trigger('change');


    var initial_assigned_staffs = [];
    var st_selected = [];

    <?php if($json_assigned_staffs) { ?>
    var assigned_staffs = JSON.parse('<?php echo $json_assigned_staffs ?>');

    for (var k = 0; k < assigned_staffs.length; k++) {
        initial_assigned_staffs.push({
            id: assigned_staffs[k].id,
            text: assigned_staffs[k].first_name + ' ' + assigned_staffs[k].last_name
        });
    }

    var PRESELECTED_FRUITS = [
        { id: '1', text: 'Apple' },
        { id: '2', text: 'Mango' },
        { id: '3', text: 'Orange' }
    ];

    /*initial_assigned_staffs.push({
     id: assigned_staffs[0].id,
     text: assigned_staffs[0].first_name + ' ' + assigned_staffs[0].last_name
     });*/
    console.log('initial_assigned_staffs');
    console.log(initial_assigned_staffs);
    <?php } ?>


    $(function () {

        $(".staffs").select2({

            multiple: true,
            delay: 500,
            data: PRESELECTED_FRUITS,

             ajax: {
                url: '<?php echo base_url() . 'project_module/get_active_staffs_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,


                /*data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                */
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data);
                     console.log(params);
                     console.log(data.more_pages);*/

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work

            }

        });

        $(".staffs").trigger('change');
    })


</script>


<script>
    $(document).ready(function () {
        var prograss_rate = $("#edit_progress").val();

        $("#slider").slider({

            value: prograss_rate,
            min: 0,
            max: 100,
            step: 1,
            slide: function (event, ui) {
                $("#progressamount").val(ui.value);
                $("#show_progressamount").val(ui.value);

            }
        });
        $("#progressamount").val($("#slider").slider("value"));
        $("#show_progressamount").val($("#slider").slider("value"));

    });

</script>
<script>

    $(document).ready(function () {

        $("#txtFromDate").datepicker({
            numberOfMonths: 2,
            dateFormat: '<?php echo $dateformat?>',
            onSelect: function (selected) {
                $("#txtToDate").datepicker("option", "minDate", selected)
            }
        });
        $("#txtToDate").datepicker({
            numberOfMonths: 2,
            dateFormat: '<?php echo $dateformat?>',
            onSelect: function (selected) {
                $("#txtFromDate").datepicker("option", "maxDate", selected)
            }
        });
    });


</script>




