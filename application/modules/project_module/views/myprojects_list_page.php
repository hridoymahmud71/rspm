<div class="content-wrapper">


    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'project_module/all_projects' ?>"><?php echo lang('breadcrumb_section_all_projects_text') ?></a>
                    |
                <?php } ?>
                <?php if ($seen_by == 'me') { ?>
                    <a href="<?php echo base_url() . 'project_module/my_projects' ?>"><?php echo lang('breadcrumb_my_projects_section_text') ?></a>
                <?php } ?>
                <?php if ($seen_by == 'other') { ?>
                    <a href="<?php echo base_url() . 'project_module/my_projects/seen_by_other/' . $user_id ?>"><?php echo lang('breadcrumb_users_projects_section_text') ?></a>
                <?php } ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <?php if ($this->session->flashdata('project_update')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap " id="add-success-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#add-success-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body"><?php echo $this->session->flashdata('project_update'); ?>
                    &nbsp;
                    <a href="<?php echo base_url() . 'projectroom_module/'
                        . $this->session->flashdata('project_id') ?>">
                        <?= lang('go_to_project_room_text') ?>
                    </a>
                    &nbsp;
                    <a href="<?php echo base_url() . 'file_manager_module/get_upload_file_form_with_project_without_task/'
                        . $this->session->flashdata('project_id')
                    ?>">
                        <?php echo lang('upload_file_text') ?>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
    <div></div>

    <?php if ($this->session->flashdata('success')) { ?>
        <br>

        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap" id="success-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#success-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body">
                    <?php
                    if ($this->session->flashdata('project_complete_success')) {
                        echo $this->session->flashdata('project_complete_success');
                    }

                    if ($this->session->flashdata('project_activate_success')) {
                        echo $this->session->flashdata('project_activate_success');
                    }

                    if ($this->session->flashdata('project_deactivate_success')) {
                        echo $this->session->flashdata('project_deactivate_success');
                    }

                    ?>
                    &nbsp;
                    <a href="<?php echo base_url() . 'projectroom_module/project_overview/' . $this->session->flashdata('project_id') ?>">
                        <?php echo lang('go_to_project_room_text') ?>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php if ($seen_by == 'me') { ?>
                                <?php echo lang('table_title_my_projects_text') ?>
                            <?php } ?>
                            <?php if ($seen_by == 'other') { ?>
                                <?php echo lang('table_title_users_projects_text') ?>
                                &nbsp;
                                |
                                <?php echo lang('projects_of_text') ?>
                                :
                                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_info->id ?>">
                                    <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                </a>
                            <?php } ?>

                        </h3>

                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis" data-column="0"><?php echo lang('column_project_title_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('client_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('company_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="3"><?php echo lang('progress_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="4"><?php echo lang('budget_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="5"><?php echo lang('start_date_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="6"><?php echo lang('end_date_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="7"><?php echo lang('status_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="8"><?php echo lang('action_text') ?></a>
                        </div>

                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3" border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label><?php echo lang('column_project_title_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col1" data-column="1">
                                    <td align="center"><label for=""><?php echo lang('client_name_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col1_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col1_regex" type="checkbox"></td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col1_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col2" data-column="2">
                                    <td align="center"><label for=""><?php echo lang('company_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col2_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col2_regex" type="checkbox"></td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col2_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col3" data-column="3">
                                    <td align="center"><label for=""><?php echo lang('progress_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col3_filter" type="hidden">
                                        <select id="custom_progress_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="0-20">0% - 20%</option>
                                            <option value="21-40">21% - 40%</option>
                                            <option value="41-60">41% - 60%</option>
                                            <option value="61-80">61% - 80%</option>
                                            <option value="81-100">81% - 100%</option>
                                            <option value="0-99"><?php echo lang('option_incomplete_text') ?></option>
                                            <option value="100"><?php echo lang('option_complete_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="filter_col7" data-column="7">
                                    <td align="center"><label for=""><?php echo lang('status_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col7_filter" type="hidden">
                                        <select id="custom_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="active"><?php echo lang('option_active_text') ?></option>
                                            <option value="inactive"><?php echo lang('option_inactive_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="my_project-table" class="table table-bordered table-hover table-responsive ">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_project_title_text') ?></th>
                                <th><?php echo lang('client_name_text') ?></th>
                                <th><?php echo lang('company_name_text') ?></th>
                                <th><?php echo lang('progress_text') ?></th>
                                <th><?php echo lang('budget_text') ?></th>
                                <th><?php echo lang('start_date_text') ?></th>
                                <th><?php echo lang('end_date_text') ?></th>
                                <th><?php echo lang('status_text') ?></th>
                                <th><?php echo lang('action_text') ?></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                            <tr>
                                <th><?php echo lang('column_project_title_text') ?></th>
                                <th><?php echo lang('client_name_text') ?></th>
                                <th><?php echo lang('company_name_text') ?></th>
                                <th><?php echo lang('progress_text') ?></th>
                                <th><?php echo lang('budget_text') ?></th>
                                <th><?php echo lang('start_date_text') ?></th>
                                <th><?php echo lang('end_date_text') ?></th>
                                <th><?php echo lang('status_text') ?></th>
                                <th><?php echo lang('action_text') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!--------------------------------------------------------------------------------------------------------------------->

<style>
    #my_project-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #my_project-table td,
    #my_project-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>
<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'project_base_assets/base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


        $('#my_project-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_project_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_project_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            columns: [
                {data: "project_name"},
                {data: "client_name"},
                {data: "company"},
                {
                    data: {
                        _: "prog.html",
                        sort: "prog.int"
                    }
                },
                {
                    data: {
                        _: "budg.html",
                        sort: "budg.dec"
                    }
                },
                {
                    data: {
                        _: "st_dt.display",
                        sort: "st_dt.timestamp"
                    }
                },
                {
                    data: {
                        _: "en_dt.display",
                        sort: "en_dt.timestamp"
                    }
                },
                {
                    data: {
                        _: "sts.html",
                        sort: "sts.int"
                    }
                },
                {data: "action"}

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [8]} /*, { visible: false, targets: [2,5] }*/
            ],

            /*aaSorting: [[6, 'desc']],*/

            ajax: {
                url: "<?php echo base_url() . 'project_module/get_corresponding_projects_by_ajax' ?>",                   // json datasource
                type: "post",
                data: function (data) {
                    data.is_admin = '<?php echo $is_admin?>';
                    data.is_client = '<?php echo $is_client?>';
                    data.is_staff = '<?php echo $is_staff?>';
                    data.which_project = '<?php echo $which_project?>';
                    data.seen_by = '<?php echo $seen_by?>';
                    data.user_id = '<?php echo $user_id?>';
                    data.projects_belongs_to = '<?php echo $projects_belongs_to?>';
                },

                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#my_project-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#my_project-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_progress_filter').on('change', function () {

            if ($('#custom_progress_filter').val() == 'all') {
                $('#col3_filter').val('');
                filterColumn(3);
            } else {
                $('#col3_filter').val($('#custom_progress_filter').val());
                filterColumn(3);
            }

        });
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col7_filter').val('');
                filterColumn(7);
            } else {
                $('#col7_filter').val($('#custom_status_filter').val());
                filterColumn(7);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {

        $('.complete-confirmation').click(function (e) {
            var href = $(this).attr('href');

            swal({
                    title: "<?php echo lang('swal_complete_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "darkgreen",
                    confirmButtonText: "<?php echo lang('swal_complete_confirm_button_text')?>",
                    cancelButtonText: "<?php echo lang('swal_complete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

        $('.delete-confirmation').click(function (e) {
            var href = $(this).attr('href');

            swal({
                    title: "<?php echo lang('swal_delete_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?php echo lang('swal_delete_confirm_button_text')?>",
                    cancelButtonText: "<?php echo lang('swal_delete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

    }
</script>



