<style>
    .select2-selection__choice {
        background-color: #428bca !important;
    }

    .select2-selection__choice__remove {
        color: white !important;
    }

    .select2-selection__choice__remove:hover {
        color: #d9534f !important;
        font-size: larger;
    }

</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_add_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>">
                    <i class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?>
                </a>
            </li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'project_module/all_projects' ?>">
                        <?php echo lang('breadcrumb_all_projects_section_text') ?>
                    </a>
                    <?php echo ' | ' ?>
                <?php } ?>

                <a href="<?php echo base_url() . 'project_module/my_projects' ?>">
                    <?php echo lang('breadcrumb_my_projects_section_text') ?>
                </a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_add_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('add_form_box_title') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">

                            <?php
                            if ($this->session->flashdata('val_error')) {
                                print_r($this->session->flashdata('val_error'));
                            }
                            ?>
                            <?php
                            if ($this->session->flashdata('project_name_require_field')) {
                                print_r($this->session->flashdata('project_name_require_field'));
                            }
                            ?>

                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form action="<?php echo base_url() . 'project_module/save_projectpro_info'; ?>" role="form" id=""
                          name="save_project" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <div class="form-group">
                                    <label for="project_name"><?php echo lang('label_project_name_text') ?></label>

                                    <input type="text" name="project_name" class="form-control" id="project_name"
                                           placeholder="<?php echo lang('placeholder_project_name_text') ?>"
                                           maxlength="30"
                                           value="<?php
                                           if ($this->session->flashdata('project_name')) {
                                               echo $this->session->flashdata('project_name');
                                           }
                                           ?>"
                                    >
                                </div>

                                <div class="form-group">
                                    <label for="client_id"><?php echo lang('label_client_text') ?></label>

                                    <select name="client_id" class="client form-control select2"
                                            data-placeholder="<?php echo lang('placeholder_client_text') ?>"
                                            style="width: 100%;">
                                    </select>
                                </div>

                                <div class="form-group" id="newval">

                                    <label for="company_name"><?php echo lang('label_company_text') ?></label>

                                    <input type="text" name="company_name" class="form-control"
                                           id="company_name"
                                           placeholder="<?php echo lang('placeholder_company_text') ?>"
                                           value="" readonly
                                    >
                                </div>


                                <div class="form-group">

                                    <label for="currency"><?php echo lang('label_currency_text') ?></label>
                                    <select id='currency' name="currency" class="form-control"
                                            placeholder="<?php echo lang('label_currency_text') ?>"
                                            style="width: 100%;" data-live-search="true">

                                        <?php foreach ($allCurrency as $value) { ?>
                                            <option value="<?php echo $value->currency_id ?>" <?php
                                            if ($this->session->flashdata('currency') == $value->currency_id) {
                                                echo 'selected';
                                            } else if ($value->currency_id == $check_currency) {
                                                echo 'selected';
                                            } else {
                                                echo '';
                                            }
                                            ?>>
                                                <?php
                                                if ($check_currency != "" || $check_currency != NULL) {
                                                    if ($value->currency_id == $check_currency) {
                                                        echo $value->currency_sign . "(Default)";
                                                    } else {
                                                        echo $value->currency_sign;
                                                    }
                                                } else {
                                                    echo $value->currency_sign;
                                                }
                                                ?> </option>
                                        <?php } ?>
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label><?php echo lang('budget_text') ?></label>

                                    <input type="text" name="budget" class="form-control"
                                           placeholder="<?php echo lang('placeholder_budget_text') ?> "
                                           value="<?php
                                           if ($this->session->flashdata('budget')) {
                                               echo $this->session->flashdata('budget');
                                           }
                                           ?>"
                                    >
                                </div>
                                <div class="form-group ">
                                    <label><?php echo lang('progress_text') ?>
                                        <div style="color: 	#428bca ;font-size: larger">
                                            <input id="show_progressamount" type="text"
                                                   style="border: transparent;width: 30px" value="0">
                                            <span>%</span>
                                        </div>
                                    </label>

                                    <input value="" style=" border: white">
                                    <input id="progressamount" class="" type="hidden" name="progress"
                                           value="<?php
                                           if ($this->session->flashdata('progress')) {
                                               echo $this->session->flashdata('progress');
                                           }
                                           ?>">

                                    <div id="slider"></div>
                                </div>

                                <div class="form-group">
                                    <label><?php echo lang('date_text') ?></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date" class="form-control pull-right" id="txtFromDate"
                                               placeholder="<?php echo lang('placeholder_start_date_text') ?> "
                                               readonly style="background-color: inherit"
                                               value="<?php
                                               if ($this->session->flashdata('date')) {
                                                   echo $this->session->flashdata('date');
                                               }
                                               ?>"
                                        >
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('end_date_text') ?></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="end_date" class="form-control pull-right"
                                               placeholder="<?php echo lang('placeholder_end_date_text') ?> "
                                               id="txtToDate"
                                               readonly style="background-color: inherit"
                                               value="<?php
                                               if ($this->session->flashdata('end_date')) {
                                                   echo $this->session->flashdata('end_date');
                                               }
                                               ?>"
                                        >
                                    </div>
                                    <!-- /.input group -->
                                </div>


                                <div class="form-group">
                                    <label><?php echo lang('assign_text') ?></label>

                                    <select name="assigned_staffs[]" class="staffs form-control select2"
                                            multiple="multiple"
                                            data-placeholder="<?php echo lang('placeholder_assign_staff_text') ?>"
                                            style="width: 100%;">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo lang('status_select_text') ?></label>

                                    <select class="form-control select" name="status"
                                            data-placeholder="<?php echo lang('status_select_text') ?>"
                                            style="width: 100%;">

                                        <option value="1"
                                            <?php
                                            if ($this->session->flashdata('status') == 1) {
                                                echo 'selected';
                                            }
                                            ?>
                                        >
                                            <?php echo lang('active_text') ?>
                                        </option>
                                        <option value="0"
                                            <?php
                                            if ($this->session->flashdata('status') == 0) {
                                                echo 'selected';
                                            }
                                            ?>
                                        >
                                            <?php echo lang('inactive_text') ?>
                                        </option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">

                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"
                                    onclick="CheckDecimal(document.save_project.budget)"><?php echo lang('button_submit_text') ?></button>
                        </div>

                    </form>
                </div>
                <!-- /.box --> </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--------------------------------------------------------------------------------------------------------------------->
<script>
    $('#datepicker').datepicker({
        autoclose: true
    });
</script>

<script>
    $('#datepicker_2').datepicker({
        autoclose: true
    });
</script>


<script>
    //get active clients by ajax

    $(function () {

        $(".client").select2({
            ajax: {
                url: '<?php echo base_url() . 'project_module/get_active_clients_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,
                allowClear: true,

                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data);
                     console.log(params);
                     console.log(data.more_pages);*/

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work

            }

        });


    });


</script>


<script>
    $(function () {
        $('.client').on("select2:select", function (e) {

            var value = $(e.currentTarget).find("option:selected").val();
            //alert(value);

            var data = $('.client').select2('data');
            $('#company_name').val(data[0].company);
        });
    })
</script>

<script>

    $(function () {
        $(".staffs").select2({
            ajax: {
                url: '<?php echo base_url() . 'project_module/get_active_staffs_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,

                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data);
                     console.log(params);
                     console.log(data.more_pages);*/

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work

            }

        });
    })


</script>

<script>
    var prograss_rate = $("#progressamount").val();
    $("#slider").slider({
        value: prograss_rate,
        min: 0,
        max: 100,
        step: 1,
        slide: function (event, ui) {
            $("#progressamount").val(ui.value);
            $("#show_progressamount").val(ui.value);
        }
    });
    $("#progressamount").val($("#slider").slider("value"));
    $("#show_progressamount").val($("#slider").slider("value"));
</script>

<script>

    $(document).ready(function () {
        $("#txtFromDate").datepicker({
            numberOfMonths: 2,
            dateFormat: '<?php echo $dateformat ?>',
            onSelect: function (selected) {
                $("#txtToDate").datepicker("option", "minDate", selected)
            }
        });
        $("#txtToDate").datepicker({
            numberOfMonths: 2,
            dateFormat: '<?php echo $dateformat ?>',
            onSelect: function (selected) {
                $("#txtFromDate").datepicker("option", "maxDate", selected)
            }
        });
    });

</script>



