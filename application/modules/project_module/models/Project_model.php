<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Project_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function getAllStaff($group_id)
    {
        $this->db->select('*');
        $this->db->from('users_groups');
        $this->db->where('group_id=', $group_id);
        $this->db->join('groups', 'users_groups.group_id=groups.id');
        $this->db->join('users', 'users_groups.user_id = users.id');
        $this->db->where('users.active=', 1);
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function getAllUsers($group_id)
    {
        $this->db->select('*');
        $this->db->from('users_groups');
        $this->db->where('group_id=', $group_id);
        $this->db->join('groups', 'users_groups.group_id=groups.id');
        $this->db->join('users', 'users_groups.user_id = users.id');
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }


    public function saveProject($data)
    {
        $this->db->insert('rspm_tbl_project', $data);
    }

    public function assignStaff($staffs, $id)
    {
        $data['staff_id'] = $staffs;
        $data['project_id'] = $id;
        $this->db->insert('rspm_tbl_project_assigned_staff', $data);
    }

    public function getAllProjects()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('rspm_tbl_project.deletion_status!=', 1);
        $this->db->join('users', ' rspm_tbl_project.client_id=users.id');
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function deleteProject($id)
    {

        $this->db->set('deletion_status', 1);
        $this->db->where('project_id', $id);
        $this->db->update('rspm_tbl_project');
    }

    public function getProject($id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('rspm_tbl_project.project_id=', $id);
        $this->db->join('users', ' rspm_tbl_project.client_id=users.id');
        $query = $this->db->get();
        $queryresult = $query->row();
        return $queryresult;
    }

    public function getAllAssignedStaffsArray($id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('project_id=', $id);
        $query = $this->db->get();
        $queryresult = $query->result_array();
        return $queryresult;
    }

    public function assignStaffEdit($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id=', $id);
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function allCurrency()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function currencySettingId($sign)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_name', $sign);
        $query = $this->db->get();
        $queryresult = $query->row();
        return $queryresult;
    }

    public function getCurrency($currency_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_id', $currency_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updateProject($data)
    {
        $project_id = $data['project_id'];
        $this->db->where('project_id', $project_id);
        $this->db->update("rspm_tbl_project", $data);
        return $project_id;
    }

    public function assignStaffupdate($staffs, $id)
    {
        $this->db->where('project_id', $id);
        $this->db->update('rspm_tbl_project_assigned_staff', $staffs);
    }

    public function assignStaffupdateinsert($staffs, $id)
    {
        $data['staff_id'] = $staffs;
        $data['project_id'] = $id;
        $this->db->insert('rspm_tbl_project_assigned_staff', $data);
    }

    public function deleteStuff($id, $project_id)
    {
        $this->db->where('staff_id!=', $id);
        $this->db->where('project_id!=', $project_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function update_insert($project_id, $staffid)
    {
        $data['staff_id'] = $staffid;
        $data['project_id'] = $project_id;
        $this->db->insert('rspm_tbl_project_assigned_staff', $data);
    }

    public function update_delete($project_id, $staffid)
    {
        $data['staff_id'] = $staffid;
        $data['project_id'] = $project_id;
        $this->db->where('project_id=', $project_id);
        $this->db->where('staff_id=', $staffid);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function update_delete_all($project_id)
    {
        $this->db->where('project_id=', $project_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function select_all_project_using_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('staff_id=', $user_id);
        $this->db->join('rspm_tbl_project', 'rspm_tbl_project_assigned_staff.project_id=rspm_tbl_project.project_id');
        $this->db->where('rspm_tbl_project.deletion_status!=', 1);
        $this->db->join('users u', 'rspm_tbl_project.client_id=u.id');
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function all_project_info_using_project_id($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id=', $project_id);
        $this->db->join('rspm_tbl_project_assigned_staff', 'rspm_tbl_project.project_id=rspm_tbl_project_assigned_staff.project_id');
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function deactivate_project($project_id)
    {
        $this->db->set('status', 0);
        $this->db->where('project_id=', $project_id);
        $this->db->update('rspm_tbl_project');

        return true;
    }

    public function activate_project($project_id)
    {
        $this->db->set('status', 1);
        $this->db->where('project_id=', $project_id);
        $this->db->update('rspm_tbl_project');

        return true;
    }

    public function checkunique($project_id, $project_name)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_name=', $project_name);
        $this->db->where('project_id!=', $project_id);
        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function for_user_group($user_id)
    {
        $this->db->select('*');
        $this->db->from('users_groups');
        $this->db->where('user_id=', $user_id);
        $query = $this->db->get();
        $queryresult = $query->row();
        return $queryresult;
    }

    public function select_all_project_using_customer_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('client_id=', $user_id);
        $this->db->join('users u', 'rspm_tbl_project.client_id=u.id');
        $this->db->where('rspm_tbl_project.deletion_status!=', 1);

        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function makeProjectComplete($project_id)
    {
        $this->db->set('progress', 100);
        $this->db->where('project_id', $project_id);
        $this->db->update('rspm_tbl_project');

        return true;
    }

    /*-----------------------------------------------------------*/
    public function getTasks($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*on project delete . . . */

    public function deassignStaffsFromProject($project_id)
    {
        $this->db->where('project_id', $project_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function deactivateProjectTasks($project_id)
    {
        $this->db->set('task_status', 0);
        $this->db->where('project_id', $project_id);
        $this->db->update('rspm_tbl_task');
    }

    public function deleteProjectTasks($project_id)
    {
        $this->db->set('task_deletion_status', 1);
        $this->db->where('project_id', $project_id);
        $this->db->update('rspm_tbl_task');
    }


    public function deassignStaffsFromTask($task_id)
    {
        $this->db->where('task_id', $task_id);
        $this->db->delete('rspm_tbl_task_assigned_staff');
    }
    /*-----------------------------------------------------------*/
    /*on project activate deactivate . . . */

    public function makeProjectTasksActive($project_id)
    {
        $this->db->set('task_status', 1);
        $this->db->where('project_id', $project_id);
        $this->db->update('rspm_tbl_task');
    }

    public function makeProjectTasksInactive($project_id)
    {
        $this->db->set('task_status', 0);
        $this->db->where('project_id', $project_id);
        $this->db->update('rspm_tbl_task');
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $user_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getUsers_byAjax($group_id, $search_filter, $limit, $offset)
    {
        $this->db->select('
                            u.id as id,
                            u.first_name as first_name,
                            u.last_name as last_name, 
                            u.company as company,
                            u.active as active,
                            u.deletion_status as deletion_status,
                            ug.id as g_serial
                          ');

        $this->db->from('users as u');

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id', $group_id);

        if ($search_filter != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $search_filter);
            $this->db->or_like('u.last_name', $search_filter);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $search_filter);
            $this->db->group_end();
        }

        $this->db->limit($limit, $offset);


        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getActiveUsers_byAjax($group_id, $search_filter, $limit, $offset)
    {
        $this->db->select('
                            u.id as id,
                            u.first_name as first_name,
                            u.last_name as last_name,
                             u.company as company,
                            u.active as active,
                            u.deletion_status as deletion_status,
                            ug.id as g_serial
                          ');

        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id', $group_id);

        if ($search_filter != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $search_filter);
            $this->db->or_like('u.last_name', $search_filter);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $search_filter);
            $this->db->group_end();
        }


        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countUsersByAjax($group_id, $search_filter)
    {
        $this->db->select('
                            u.id as id,
                            u.first_name as first_name,
                            u.last_name as last_name,
                             u.company as company,
                            u.active as active,
                            u.deletion_status as deletion_status,
                            ug.id as g_serial
                          ');

        $this->db->from('users as u');

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id', $group_id);

        if ($search_filter != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $search_filter);
            $this->db->or_like('u.last_name', $search_filter);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $search_filter);
            $this->db->group_end();
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveUsersByAjax($group_id, $search_filter)
    {
        $this->db->select('
                            u.id as id,
                            u.first_name as first_name,
                            u.last_name as last_name,
                            u.company as company,
                            u.active as active,
                            u.deletion_status as deletion_status,
                            ug.id as g_serial
                          ');

        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id', $group_id);

        if ($search_filter != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $search_filter);
            $this->db->or_like('u.last_name', $search_filter);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $search_filter);
            $this->db->group_end();
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function countProjectsByAjax($common_filter_value = false, $specific_filters = false, $client_id = false, $staff_id = false)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('users as u', ' p.client_id=u.id');

        if ($client_id && !$staff_id) {
            $this->db->where('p.client_id', $client_id);
        }

        if ($staff_id && !$client_id) {
            $this->db->join('rspm_tbl_project_assigned_staff as pas', ' p.project_id=pas.project_id');
            $this->db->where('pas.staff_id', $staff_id);
        }

        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('p.project_name', $common_filter_value);
            $this->db->or_like('p.progress', $common_filter_value);
            $this->db->or_like('p.budget', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'project_name') {
                    $this->db->like('p.' . $column_name, $filter_value);
                }

                if ($column_name == 'client_name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'client_company') {
                    $this->db->like('u.company', $filter_value);
                }

                if ($column_name == 'progress') {
                    if ($filter_value == 100) {
                        $this->db->where('p.progress', 100);
                    } else {
                        $min_max_range = explode('-', $filter_value);
                        $min_progress = $min_max_range[0];
                        $max_progress = $min_max_range[1];
                        $this->db->where('p.progress >=', $min_progress);
                        $this->db->where('p.progress <=', $max_progress);
                    }
                }

                if ($column_name == 'status') {
                    if ($filter_value == 'active') {
                        $this->db->where('p.status', 1);
                    } else {
                        $this->db->where('p.status!=', 1);
                    }

                }
            }
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getProjectsByAjax($common_filter_value = false, $specific_filters = false, $order, $limit, $client_id = false, $staff_id = false)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('users as u', ' p.client_id=u.id');

        if ($client_id && !$staff_id) {
            $this->db->where('p.client_id', $client_id);
        }

        if ($staff_id && !$client_id) {
            $this->db->join('rspm_tbl_project_assigned_staff as pas', ' p.project_id=pas.project_id');
            $this->db->where('pas.staff_id', $staff_id);
        }

        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('p.project_name', $common_filter_value);
            $this->db->or_like('p.progress', $common_filter_value);
            $this->db->or_like('p.budget', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'project_name') {
                    $this->db->like('p.' . $column_name, $filter_value);
                }

                if ($column_name == 'client_name') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'client_company') {
                    $this->db->like('u.company', $filter_value);
                }

                if ($column_name == 'progress') {
                    if ($filter_value == 100) {
                        $this->db->where('p.progress', 100);
                    } else {
                        $min_max_range = explode('-', $filter_value);
                        $min_progress = $min_max_range[0];
                        $max_progress = $min_max_range[1];
                        $this->db->where('p.progress >=', $min_progress);
                        $this->db->where('p.progress <=', $max_progress);
                    }
                }

                if ($column_name == 'status') {
                    if ($filter_value == 'active') {
                        $this->db->where('p.status', 1);
                    } else {
                        $this->db->where('p.status!=', 1);
                    }

                }
            }
        }


        if ($order['column'] == 'client_name') {
            $this->db->order_by('u.first_name');
        } else if ($order['column'] == 'company') {
            $this->db->order_by('u.company');
        } else {
            $this->db->order_by('p.' . $order['column'], $order['by']);
        }

        $this->db->limit($limit['length'], $limit['start']);


        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getAssignedStaffs($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->where('pas.project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



}
