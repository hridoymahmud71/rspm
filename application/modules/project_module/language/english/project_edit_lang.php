<?php

/*page texts*/
$lang['project_title_text'] = 'Project title';
$lang['budget_text'] = 'Budget';
$lang['client_type_text'] = 'Client/Associated person';
$lang['company_text'] = 'Company';
$lang['start_date_text'] = 'Start Date';
$lang['end_date_text'] = 'End Date';
$lang['status_text'] = 'Status';

$lang['action_text'] = 'Action';
$lang['table_title_text'] = 'All Project';
$lang['page_title_text'] = 'Project';
$lang['page_subtitle_text'] = 'Edit Project Details';
$lang['page_subtitle_add_text'] = 'Add Project Details';
/*$lang['breadcrumb_page_add_text'] = 'Add Project';*/

$lang['breadcrumb_all_projects_section_text'] = 'All Projects';
$lang['breadcrumb_my_projects_section_text'] = 'My Projects';
$lang['breadcrumb_page_text'] = 'Edit Project';
$lang['breadcrumb_home_text'] = 'Home';

$lang['project_title_text'] = "Edit Your Project Details";
$lang['label_project_name_text'] = 'Project Title';
$lang['placeholder_project_name_text'] = 'Enter Your Project Title';
$lang['label_company_text'] = 'Company Name';

$lang['select_client_text'] = 'Select A Client';

$lang['placeholder_company_text'] = 'Company Name here (according to Client Name)';
$lang['label_client_text'] = 'Client Name';
$lang['placeholder_client_text'] = 'Client Name';
$lang['placeholder_start_date_text'] = "Click here to set Project's start date";
$lang['placeholder_end_date_text'] = "Click here to set Project's end date";
$lang['label_budget_text'] = 'Budget';
$lang['placeholder_budget_text'] = 'Enter Your Budget Here.(Example:200.00)';

$lang['date_text'] = 'Start Date';
$lang['assign_text'] = 'Assign to:';
$lang['multiselect_dafault_text'] = 'Select staff(s) to assign';

$lang['active_text'] = 'Active';
$lang['inactive_text'] = 'Inactive';
$lang['status_select_text'] = 'Status';
$lang['button_submit_text'] = 'Submit';
$lang['project_name_required'] = 'Project Title field is required';
$lang['client_name_required'] = 'Client\'s Name field is required';
$lang['project_name_unique'] = 'This Project already exist';
$lang['project_name_max_length'] = 'Project Name Cannot be more than 30 characters';
$lang['project_add_success'] = 'Project added successfully';
$lang['project_delete_success'] = 'Project successfully deleted';
$lang['end_date_text'] = 'End Date';
$lang['label_currency_text'] = 'Currency';
$lang['budget_text'] = 'Budget';
$lang['budget_val_text'] = 'Budget should be a decimal number';
$lang['progress_text'] = 'Progress Status';
$lang['project_update_success'] = 'Project Updated Successfully';

