<?php

/*page texts*/
$lang['add_project_button_text'] = 'Add Project';

$lang['project_title_text'] = 'Project title';
$lang['budget_text'] = 'Budget';
$lang['client_type_text'] = 'Client';
$lang['company_text'] = 'Company';
$lang['progress_text'] = 'Progress';
$lang['start_date_text'] = 'Start Date';
$lang['end_date_text'] = 'End Date';
$lang['status_text'] = 'Status';
$lang['no_project_text'] = 'No project exist';
$lang['project_name_unique'] = 'This Project already exist';
$lang['project_name_max_length'] = 'Project Name Cannot be more than 30 characters';
$lang['project_name_require_field'] = 'Project Title is required';

$lang['select_client_text'] = 'Select A Client';

$lang['action_text'] = 'Action';
$lang['table_title_text'] = 'All Project';
$lang['page_title_text'] = 'Projects';
$lang['page_subtitle_text'] = 'Project List';
$lang['successfull_text'] = 'Successfull';
$lang['upload_file_text'] = 'Upload File';

$lang['page_subtitle_add_text'] = 'Add Project Details';

$lang['breadcrumb_page_add_text'] = 'Add Project';
$lang['breadcrumb_section_text'] = 'Projects';
$lang['breadcrumb_page_text'] = 'Project List';
$lang['breadcrumb_home_text'] = 'Home';

$lang['breadcrumb_all_projects_section_text'] = 'All Projects';
$lang['breadcrumb_my_projects_section_text'] = 'My Projects';

$lang['project_title_text'] = "Project Title";
$lang['label_project_name_text'] = 'Project Title';
$lang['placeholder_project_name_text'] = 'Enter Your Project Title';
$lang['label_company_text'] = 'Company Name';
$lang['placeholder_company_text'] = 'Company Name here according to Client Name';
$lang['label_client_text'] = 'Client Name';
$lang['placeholder_client_text'] =  'Select Client (Enter atleast 3 charachters)';
$lang['placeholder_start_date_text'] = "Click here for Project's start date";
$lang['placeholder_end_date_text'] = "Click here for Project's end date";
$lang['label_budget_text'] = 'Budget';
$lang['placeholder_budget_text'] = 'Enter Your Budget Here.(Example:200.00)';

$lang['date_text'] = 'Start Date';
$lang['assign_text'] = 'Assign to:';
$lang['placeholder_assign_staff_text'] = 'Assign staffs to the project (Enter atleast 3 charachters)';

$lang['active_text'] = 'Active';
$lang['inactive_text'] = 'Inactive';

$lang['status_select_text'] = 'Status';
$lang['button_submit_text'] = 'Submit';
$lang['project_name_required'] = 'Project Title field is required';
$lang['client_name_required'] = 'Client Name field is required';
$lang['project_add_success'] = 'Project add successfully';
$lang['project_delete_success'] = 'Project successfully deleted';
$lang['end_date_text'] = 'End Date';
$lang['label_currency_text'] = 'Currency';
$lang['budget_text'] = 'Budget';
$lang['budget_val_text'] = 'Budget should be a decimal number';
$lang['progress_text'] = 'Progress';
$lang['project_update_success'] = 'Project Update Successfully';

/*swal texts*/
$lang['swal_complete_title_text'] = 'Are you sure to set this project as complete ?';
$lang['swal_complete_confirm_button_text'] = 'Yes, make these project complete';
$lang['swal_complete_cancel_button_text'] = 'No, keep as it is';

$lang['swal_delete_title_text'] = 'Are you sure to delete this project ?';
$lang['swal_delete_confirm_button_text'] = 'Yes, delete the project';
$lang['swal_delete_cancel_button_text'] = 'No, keep this project';

//----------------------------------------
$lang['go_to_project_room_text'] = 'Go to Projectroom';

/*sucess messages*/
$lang['successfull_text'] = 'Successfull';
$lang['go_to_project_room_text'] = 'Go to Projectroom';

$lang['project_deactivate_success_text'] = 'Project Successfully Deactivated';
$lang['project_activate_success_text'] = 'Project Successfully Activated';

$lang['project_complete_success_text'] = 'Project Successfully Set as Completed';

/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Project Active';
$lang['tooltip_deactivate_text'] = 'Make Project Deactive';

$lang['tooltip_view_text'] = 'View Project ';
$lang['tooltip_edit_text'] = 'Edit Project ';
$lang['tooltip_delete_text'] = 'Delete Project ';
$lang['tooltip_complete_text'] = 'Make Project Complete';
$lang['tooltip_upload_text'] = 'Upload a File';

/*------------------------*/
$lang['add_form_box_title'] = 'Add Project Details';

/*filter and others*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_complete_text'] = 'Complete';
$lang['option_incomplete_text'] = 'Incomplete';

$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';


/*--------------------------------------*/
$lang['no_client_found_text'] = 'No Client Found';
$lang['no_staff_found_text'] = 'No Staff Found';
$lang['no_company_text'] = 'No Company';

/*---------------------------------------*/

$lang['loading_text'] = 'Loading Projects . . .';
$lang['no_date_text'] = 'No Date Available';
$lang['no_client_found_text'] = 'No Client Found';
$lang['no_staff_found_text'] = 'No Staff Found';


?>