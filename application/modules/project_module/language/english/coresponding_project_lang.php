<?php

/*page texts*/
$lang['project_title_text'] = 'Project title';
$lang['budget_text'] = 'Budget';
$lang['client_type_text'] = 'Client/Associated person';
$lang['client_name_text'] = 'Client';
$lang['company_name_text'] = 'Company';
$lang['company_text'] = 'Company';
$lang['start_date_text'] = 'Start Date';
$lang['end_date_text'] = 'End Date';
$lang['status_text'] = 'Status';
$lang['no_project_text'] = 'No project exist';
$lang['action_text'] = 'Action';

$lang['table_title_my_projects_text'] = 'My Projects';
$lang['table_title_users_projects_text'] = 'User\'s Projects';
$lang['projects_of_text'] = 'Projects of';

$lang['page_title_text'] = 'Project';
$lang['page_subtitle_small_text'] = 'Add Task';

$lang['page_subtitle_text'] = 'Projects List';

$lang['breadcrumb_section_all_projects_text'] = 'All Projects';
$lang['breadcrumb_my_projects_section_text'] = 'My Projects';
$lang['breadcrumb_users_projects_section_text'] = 'Users Projects';

$lang['breadcrumb_page_text'] = 'Project List';
$lang['breadcrumb_home_text'] = 'Home';
$lang['column_project_title_text'] = 'Project Title';
$lang['column_client_name_text'] = 'Client Name';
$lang['project_title_text'] = 'Provide Project Detail';
$lang['label_project_name_text'] = 'Task Title';
$lang['placeholder_project_name_text'] = 'Task Title';
$lang['label_company_text'] = 'Company Name';
$lang['placeholder_company_text'] = 'Company Name';
$lang['label_client_text'] = 'Client Name';
$lang['placeholder_client_text'] = 'Client Name';
$lang['label_budget_text'] = 'Budget';
$lang['placeholder_budget_text'] = 'Budget';

$lang['date_text'] = 'Start Date';
$lang['assign_text'] = 'Assign to:';
$lang['multiselect_dafault_text'] = 'Assign Staff(s) to project';

$lang['new_text'] = 'New';
$lang['progress_text'] = 'In Progress';
$lang['completed_text'] = 'Completed';
$lang['cancelled_text'] = 'Cancelled';
$lang['onhold_text'] = 'Onhold';
$lang['status_select_text'] = 'Task Status';
$lang['button_submit_text'] = 'Submit';
$lang['project_name_required'] = 'Project Title field is required';
$lang['client_name_required'] = 'Client Name field is required';
$lang['project_add_success'] = 'Project added successfully';
$lang['project_delete_success'] = 'Project successfully deleted';
$lang['end_date_text'] = 'Due Date';
$lang['label_currency_text'] = 'Currency';
$lang['budget_text'] = 'Budget';
$lang['budget_val_text'] = 'Budget should be a decimal number';
$lang['progress_text'] = 'Progress';
$lang['overview_text'] = 'Overview';
$lang['file_manager_text'] = 'File Manager';
$lang['task_text'] = 'Task';
$lang['action_text'] = 'Action';
$lang['project_update_success'] = 'Project Updated Successfully';
$lang['label_project_description_text'] = 'Description';

$lang['upload_file_text'] = 'Upload File';

$lang['active_text'] = 'Active';
$lang['inactive_text'] = 'Inactive';

$lang['unavailable_text'] = 'Unavailable';

/*swal texts*/
$lang['swal_title_text'] = 'Are you sure to delete this project ?';
$lang['swal_confirm_button_text'] = 'Yes, delete the project';
$lang['swal_cancel_button_text'] = 'No, keep this project';

//----------------------------------------
$lang['go_to_project_room_text'] = 'Go to Projectroom';

/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Project Active';
$lang['tooltip_deactivate_text'] = 'Make Project Deactive';

$lang['tooltip_view_text'] = 'View Project ';
$lang['tooltip_edit_text'] = 'Edit Project ';
$lang['tooltip_delete_text'] = 'Delete Project ';
$lang['tooltip_complete_text'] = 'Make Project Complete';
$lang['tooltip_upload_text'] = 'Upload a File';

/*--------------------------------------*/
$lang['no_client_found_text'] = 'No Client Found';
$lang['no_staff_found_text'] = 'No Staff Found';
$lang['no_company_text'] = 'No Company';

/*---------------------------------------*/

$lang['loading_text'] = 'Loading Projects . . .';
$lang['no_date_text'] = 'No Date Available';
$lang['no_client_found_text'] = 'No Client Found Text';
$lang['no_staff_found_text'] = 'No Staff Found Text';





