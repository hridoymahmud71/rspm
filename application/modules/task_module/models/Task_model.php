<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Task_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function getProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
    /* --------------------------------for task list------------------------------------------------------------------*/
    //of a specific project
    public function getAllTasks($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.project_id', $project_id);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    //of a specific project
    public function getStaffTasks($project_id, $staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.project_id', $project_id);
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
        $this->db->where('tas.staff_id', $staff_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAllTasksFromAllProjects()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getStaffTasks_fromAllRelatedProjects($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
        $this->db->where('tas.staff_id', $staff_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getClientTasks_fromRelatedProjects($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 't.project_id = p.project_id');
        $this->db->where('p.client_id', $client_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    public function countTasksByAjax(
        $common_filter_value = false,
        $specific_filters = false,
        $project_id = false,
        $client_id = false,
        $staff_id = false,
        $which_tasks
    )
    {
        $this->db->select('
                             t.task_id as task_id,
                             t.project_id as task_project_id,
                             t.task_number as task_number,
                             t.task_title as task_title,
                             t.task_priority as task_priority,
                             t.task_progress as task_progress,
                             t.task_status as task_status,
                             t.task_deletion_status as task_deletion_status,
                             t.task_start_time as task_start_time,
                             t.task_end_time as task_end_time,
                             t.created_at as task_created_at,
                             t.modified_at as task_modified_at,
                             
                             p.project_id as project_id,
                             p.project_name as project_name,
                             p.client_id as project_client_id,
                             
                             u.id as client_user_id,
                             u.first_name as client_first_name,
                             u.last_name as client_last_name,
                        ');

        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 't.project_id = p.project_id');
        $this->db->join('users as u', 'p.client_id = u.id');

        //logic starts
        //no need to write condition for all_tasks_from_all_projects as it's the default condition

        if ($which_tasks == 'my_tasks_from_my_projects' && !$project_id && $client_id && !$staff_id) {
            $this->db->where('u.id', $client_id);
            //loading a client's tasks from all projects
        }

        if ($which_tasks == 'my_tasks_from_my_projects' && !$project_id && !$client_id && $staff_id) {
            $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
            $this->db->where('tas.staff_id', $staff_id);
            //loading an staff's tasks from all projects
        }

        if ($which_tasks == 'all_tasks' && $project_id && !$client_id && !$staff_id) {
            $this->db->where('p.project_id', $project_id);
            //loading all tasks in a project

        }

        if ($which_tasks == 'all_tasks' && $project_id && $client_id && !$staff_id) {
            $this->db->where('p.project_id', $project_id);
            //loading all tasks in a project for client
        }

        if ($which_tasks == 'my_tasks' && $project_id && !$client_id && $staff_id) {
            $this->db->where('p.project_id', $project_id);
            $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
            $this->db->where('tas.staff_id', $staff_id);
            //loading the tasks in a project for specific staffs
        }
        //logic ends

        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('t.task_number', $common_filter_value);
            $this->db->or_like('t.task_title', $common_filter_value);
            $this->db->or_like('p.project_name', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);

            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'task_number' || $column_name == 'task_title') {
                    $this->db->like('t.' . $column_name, $filter_value);
                }

                if ($column_name == 'task_project') {
                    $this->db->like('p.project_name', $filter_value);
                }

                if ($column_name == 'task_client') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'task_priority') {
                    $this->db->where('t.task_priority', $filter_value);
                }

                if ($column_name == 'task_progress') {
                    if ($filter_value == 100) {
                        $this->db->where('t.task_progress', 100);
                    } else {
                        $min_max_range = explode('-', $filter_value);
                        $min_progress = $min_max_range[0];
                        $max_progress = $min_max_range[1];
                        $this->db->where('t.task_progress >=', $min_progress);
                        $this->db->where('t.task_progress <=', $max_progress);
                    }
                }

                if ($column_name == 'task_status') {
                    if ($filter_value == 'active') {
                        $this->db->where('t.task_status', 1);
                    } else {
                        $this->db->where('t.task_status!=', 1);
                    }

                }
            }
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getTasksByAjax(
        $common_filter_value = false,
        $specific_filters = false,
        $order,
        $limit,
        $project_id = false,
        $client_id = false,
        $staff_id = false,
        $which_tasks
    )
    {
        $this->db->select('
                             t.task_id as task_id,
                             t.project_id as task_project_id,
                             t.task_number as task_number,
                             t.task_title as task_title,
                             t.task_priority as task_priority,
                             t.task_progress as task_progress,
                             t.task_status as task_status,
                             t.task_deletion_status as task_deletion_status,
                             t.task_start_time as task_start_time,
                             t.task_end_time as task_end_time,
                             t.created_at as task_created_at,
                             t.modified_at as task_modified_at,
                             
                             p.project_id as project_id,
                             p.project_name as project_name,
                             p.client_id as project_client_id,
                             
                             u.id as client_user_id,
                             u.first_name as client_first_name,
                             u.last_name as client_last_name
                        ');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 't.project_id = p.project_id');
        $this->db->join('users as u', 'p.client_id = u.id');

        //logic starts
        //no need to write condition for all_tasks_from_all_projects as it's the default condition

        if ($which_tasks == 'my_tasks_from_my_projects' && !$project_id && $client_id && !$staff_id) {
            $this->db->where('u.id', $client_id);
            //loading a client's tasks from all projects
        }

        if ($which_tasks == 'my_tasks_from_my_projects' && !$project_id && !$client_id && $staff_id) {
            $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
            $this->db->where('tas.staff_id', $staff_id);
            //loading an staff's tasks from all projects
        }

        if ($which_tasks == 'all_tasks' && $project_id && !$client_id && !$staff_id) {
            $this->db->where('p.project_id', $project_id);
            //loading all tasks in a project

        }

        if ($which_tasks == 'all_tasks' && $project_id && $client_id && !$staff_id) {
            $this->db->where('p.project_id', $project_id);
            //loading all tasks in a project for client
        }

        if ($which_tasks == 'my_tasks' && $project_id && !$client_id && $staff_id) {
            $this->db->where('p.project_id', $project_id);
            $this->db->join('rspm_tbl_task_assigned_staff as tas', 't.task_id = tas.task_id');
            $this->db->where('tas.staff_id', $staff_id);
            //loading the tasks in a project for specific staffs
        }
        //logic ends


        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('t.task_number', $common_filter_value);
            $this->db->or_like('t.task_title', $common_filter_value);
            $this->db->or_like('p.project_name', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);

            $this->db->group_end();
        }


        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'task_number' || $column_name == 'task_title') {
                    $this->db->like('t.' . $column_name, $filter_value);
                }

                if ($column_name == 'task_project') {
                    $this->db->like('p.project_name', $filter_value);
                }

                if ($column_name == 'task_client') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'task_priority') {
                    $this->db->where('t.task_priority', $filter_value);
                }

                if ($column_name == 'task_progress') {
                    if ($filter_value == 100) {
                        $this->db->where('t.task_progress', 100);
                    } else {
                        $min_max_range = explode('-', $filter_value);
                        $min_progress = $min_max_range[0];
                        $max_progress = $min_max_range[1];
                        $this->db->where('t.task_progress >=', $min_progress);
                        $this->db->where('t.task_progress <=', $max_progress);
                    }
                }

                if ($column_name == 'task_status') {
                    if ($filter_value == 'active') {
                        $this->db->where('t.task_status', 1);
                    } else {
                        $this->db->where('t.task_status!=', 1);
                    }

                }
            }
        }


        if ($order['column'] == 'task_client') {
            $this->db->order_by('u.first_name');
        } else if ($order['column'] == 'task_project') {
            $this->db->order_by('p.project_name');
        } else {
            $this->db->order_by('t.' . $order['column'], $order['by']);
        }

        $this->db->limit($limit['length'], $limit['start']);


        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getProjectAndClientOfATask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_id', $task_id);
        $this->db->where('t.task_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 't.project_id = p.project_id');
        $this->db->join('users as u', 'p.client_id = u.id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    /* --------------------------------for task form------------------------------------------------------------------*/
    public function getActiveStaffsOfProject($staff_group_id, $project_id)
    {
        $this->db->select('*');
        $this->db->from('users_groups as ug');
        $this->db->where('ug.group_id', $staff_group_id);
        $this->db->join('users as u', 'ug.user_id = u.id');
        $this->db->where('u.active', 1);
        $this->db->join('rspm_tbl_project_assigned_staff as pas', 'pas.staff_id = u.id');
        $this->db->where('pas.project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUserInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('users  as u');
        $this->db->where('u.id', $user_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function insertTask($data)
    {
        $this->db->insert('rspm_tbl_task', $data);
        return $this->db->insert_id();
    }

    public function insertStaff($an_assigned_staff, $task_id, $project_id)
    {
        $this->db->set('staff_id', $an_assigned_staff);
        $this->db->set('task_id', $task_id);
        $this->db->set('project_id', $project_id);
        $this->db->insert('rspm_tbl_task_assigned_staff');
    }

    public function deleteStaff($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('rspm_tbl_task_assigned_staff');
    }

    public function updateTask($task_id, $data)
    {
        $this->db->where('task_id', $task_id);
        $this->db->update('rspm_tbl_task', $data);
    }

    /*-----------------------------------*/
    public function getTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_id', $task_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getAssignedStaffs($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.task_id', $task_id);
        $this->db->join('users as u', 'tas.staff_id = u.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*-------------------------Actions From Task List-----------------------------------------------------------------*/

    public function makeTaskComplete($task_id, $modified_at)
    {
        $this->db->set('task_progress', 100);
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function deleteTask($task_id, $modified_at)
    {
        $this->db->set('task_deletion_status', 1);
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function activateTask($task_id, $modified_at)
    {
        $this->db->set('task_status', 1);
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function deactivateTask($task_id, $modified_at)
    {
        $this->db->set('task_status', 0);
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function makeTaskPriorityHigh($task_id, $modified_at)
    {
        $this->db->set('task_priority', 'high');
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function makeTaskPriorityNormal($task_id, $modified_at)
    {
        $this->db->set('task_priority', 'normal');
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function makeTaskPriorityLow($task_id, $modified_at)
    {
        $this->db->set('task_priority', 'low');
        $this->db->set('modified_at', $modified_at);
        $this->db->where('task_id', $task_id);

        $this->db->update('rspm_tbl_task');
        return true;
    }

    public function getAssignedStaffs_toProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->join('users as u', 'pas.staff_id=u.id');
        $this->db->join('rspm_tbl_project as p', 'pas.project_id=p.project_id');
        $this->db->where('p.project_id', $project_id);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAssignedStaffs_toTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->join('users as u', 'tas.staff_id=u.id');
        $this->db->join('rspm_tbl_task as t', 'tas.task_id=t.task_id');
        $this->db->where('t.task_id', $task_id);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    //permission to  enter task

    public function checkIf_AssignedStaff($task_id, $staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.task_id', $task_id);
        $this->db->where('tas.staff_id', $staff_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkIf_RelatedClient($task_id, $client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->where('t.task_id', $task_id);
        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }


}