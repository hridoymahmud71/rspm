<div class="content-wrapper" style="min-height: 1126px;">
    &nbsp;
    <?php

    if (
        $is_admin == 'admin'
        &&
        ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks')
        &&
        $project_info != null

    ) { ?>
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">

                    <a class="btn btn-primary"
                       href="<?php echo base_url() . 'task_module/add_task/' . $project_info->project_id ?>">
                        <?php echo lang('add_button_text') ?>
                        &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($which_tasks == 'all_tasks') {
                echo lang('page_title_all_tasks_text');
            }
            if ($which_tasks == 'my_tasks') {
                echo lang('page_title_my_tasks_text');
            }
            if ($which_tasks == 'all_tasks_from_all_projects') {
                echo lang('page_title_all_tasks_text');
            }
            if ($which_tasks == 'my_tasks_from_my_projects') {
                if ($seen_by == 'me') {
                    echo lang('page_title_my_tasks_text');
                }

                if ($seen_by == 'other') {
                    echo lang('page_title_users_tasks_text');
                }
            }


            ?>
            <small>
                <?php
                if ($which_tasks == 'all_tasks_from_all_projects') {
                    echo lang('page_subtitle_all_tasks_from_all_projects_text');
                }
                if ($which_tasks == 'my_tasks_from_my_projects') {
                    if ($seen_by == 'me') {
                        echo lang('page_subtitle_my_tasks_from_my_projects_text');
                    }

                    if ($seen_by == 'other') {
                        echo lang('page_subtitle_users_tasks_from_my_projects_text');
                    }
                }
                ?>

                <?php
                if ($project_info && ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks')) {
                    echo '(' . $project_info->project_name . ')';
                } ?>
            </small>
        </h1>
        <br>
        <ol class="breadcrumb">
            <?php if ($is_admin == 'admin') { ?>
                <li>
                    <a href="<?= base_url() . 'project_module/all_projects' ?>">
                        <i class="fa fa-gg"></i>
                        <?= lang('breadcrumb_all_projects_page_text') ?>
                    </a>
                    |
                    <?php if ($seen_by == 'me') { ?>
                        <a href="<?= base_url() . 'project_module/my_projects' ?>">
                            <i class="fa fa-gg"></i><?= lang('breadcrumb_my_projects_page_text') ?>
                        </a>
                    <?php } ?>
                    <?php if ($seen_by == 'other') { ?>
                        <a href="<?= base_url() . 'project_module/my_projects/seen_by_others/' . $user_id ?>">
                            <i class="fa fa-gg"></i><?= lang('breadcrumb_users_projects_page_text') ?>
                        </a>
                    <?php } ?>
                </li>
            <?php } ?>

            <?php if ($is_admin == 'not_admin') { ?>
                <li>
                    <?php if ($seen_by == 'me') { ?>
                        <a href="<?= base_url() . 'project_module/my_projects' ?>">
                            <i class="fa fa-gg"></i><?= lang('breadcrumb_my_projects_page_text') ?>
                        </a>
                    <?php } ?>
                    <?php if ($seen_by == 'other') { ?>
                        <a href="<?= base_url() . 'project_module/my_projects/seen_by_others/' . $user_id ?>">
                            <i class="fa fa-gg"></i><?= lang('breadcrumb_users_projects_page_text') ?>
                        </a>
                    <?php } ?>
                </li>
            <?php } ?>

            <?php if ($project_info) { ?>
                <li><a href="<?= base_url() . 'projectroom_module/project_overview/' . $project_info->project_id ?> ">
                        <?= $project_info->project_name ?>
                    </a>
                </li>
            <?php } ?>
            <li class="active">
                <?php if ($which_tasks == 'all_tasks' || $which_tasks == 'all_tasks_from_all_projects') {
                    echo lang('breadcrumb_all_tasks_page_text');
                } else {
                    if ($seen_by == 'me') {
                        echo lang('breadcrumb_my_tasks_page_text');
                    } else {
                        echo lang('breadcrumb_users_tasks_page_text');
                    }
                }
                ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-md-4">
                <?php if ($projectroom_menu_section) {
                    echo $projectroom_menu_section;
                } ?>
            </div>
        </div>
        <!--projectroom menu ends-->


        <div class="row">

            <!--successs panel starts-->
            <?php if ($this->session->flashdata('success')) { ?>
                <br>

                <div class="col-md-6">
                    <div class="panel panel-success copyright-wrap" id="success-panel">
                        <div class="panel-heading"><?= $this->session->flashdata('success') ?>
                            <button type="button" class="close" data-target="#success-panel" data-dismiss="alert"><span
                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                        </div>
                        <div class="panel-body">
                            <!--panel body starts-->
                            <?php
                            if ($this->session->flashdata('task_completed')) {
                                echo $this->session->flashdata('task_completed');
                            }

                            if ($this->session->flashdata('task_deleted')) {
                                echo $this->session->flashdata('task_deleted');
                            }

                            if ($this->session->flashdata('task_activated')) {
                                echo $this->session->flashdata('task_activated');
                            }

                            if ($this->session->flashdata('task_deactivated')) {
                                echo $this->session->flashdata('task_deactivated');
                            }

                            if ($this->session->flashdata('task_prirority_made_high')) {
                                echo $this->session->flashdata('task_prirority_made_high');
                            }

                            if ($this->session->flashdata('task_prirority_made_normal')) {
                                echo $this->session->flashdata('task_prirority_made_normal');
                            }

                            if ($this->session->flashdata('task_prirority_made_low')) {
                                echo $this->session->flashdata('task_prirority_made_low');
                            }

                            ?>
                            &nbsp;
                            <?php if (!$this->session->flashdata('task_deleted')) { ?>
                                <a href="<?php echo base_url() . 'task_module/task_overview/'
                                    . $this->session->flashdata('project_id') . '/'
                                    . $this->session->flashdata('task_id');
                                ?> ">
                                    <?= lang('see_task_text') ?>
                                </a>
                            <?php } ?>
                            <!--panel body ends-->
                            &nbsp;
                            &nbsp;
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--successs panel ends-->

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">

                        <h3 class="box-title">
                            <?php if ($which_tasks == 'all_tasks' || $which_tasks == 'all_tasks_from_all_projects') {
                                echo lang('box_title_all_tasks_text');
                            } else {
                                if ($seen_by == 'me') {
                                    echo lang('box_title_my_tasks_text');
                                }
                                if ($seen_by == 'other') {
                                    echo lang('box_title_users_tasks_text');

                                }
                            }
                            ?>

                            <?php if ($seen_by == 'other' && $user_info) { ?>
                                |
                                <?php echo lang('task_of_text') ?>
                                :
                                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_info->id ?>">
                                    <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                </a>
                            <?php } ?>


                        </h3>
                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis" data-column="0"><?php echo lang('column_task_number_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('column_task_title_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('column_task_project_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="3"><?php echo lang('column_task_client_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="4"><?php echo lang('column_task_progress_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="5"><?php echo lang('column_task_start_time_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="6"><?php echo lang('column_task_end_time_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="7"><?php echo lang('column_task_priority_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="8"><?php echo lang('column_task_status_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="9"><?php echo lang('column_actions_text') ?></a>
                        </div>
                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                   border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label><?php echo lang('column_task_number_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col1" data-column="1">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_task_title_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col1_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col1_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col1_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <?php if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') { ?>
                                    <tr id="filter_col2" data-column="2">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_task_project_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col2_filter" type="text">
                                        </td>

                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col2_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col2_smart"
                                                                  checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                    <tr id="filter_col3" data-column="3">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_task_client_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col3_filter" type="text">
                                        </td>

                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col3_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col3_smart"
                                                                  checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                <?php } ?>
                                <tr id="filter_col4" data-column="4">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_task_progress_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col4_filter" type="hidden">
                                        <select id="custom_progress_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="0-20">0% - 20%</option>
                                            <option value="21-40">21% - 40%</option>
                                            <option value="41-60">41% - 60%</option>
                                            <option value="61-80">61% - 80%</option>
                                            <option value="81-100">81% - 100%</option>
                                            <option value="0-99"><?php echo lang('option_incomplete_text') ?></option>
                                            <option value="100"><?php echo lang('option_complete_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="filter_col7" data-column="7">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_task_priority_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col7_filter" type="hidden">
                                        <select id="custom_priority_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="high"><?php echo lang('option_high_text') ?></option>
                                            <option value="normal"><?php echo lang('option_normal_text') ?></option>
                                            <option value="low"><?php echo lang('option_low_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="filter_col8" data-column="8">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_task_status_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col8_filter" type="hidden">
                                        <select id="custom_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="active"><?php echo lang('option_active_text') ?></option>
                                            <option value="inactive"><?php echo lang('option_inactive_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- /.box-body -->
                    <div class="box-body">
                        <table id="task-table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_task_title_text') ?></th>
                                <th><?php echo lang('column_task_project_text') ?></th>
                                <th><?php echo lang('column_task_client_text') ?></th>
                                <th><?php echo lang('column_task_progress_text') ?></th>
                                <th><?php echo lang('column_task_start_time_text') ?></th>
                                <th><?php echo lang('column_task_end_time_text') ?></th>
                                <th><?php echo lang('column_task_priority_text') ?></th>
                                <th><?php echo lang('column_task_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_task_title_text') ?></th>
                                <th><?php echo lang('column_task_project_text') ?></th>
                                <th><?php echo lang('column_task_client_text') ?></th>
                                <th><?php echo lang('column_task_progress_text') ?></th>
                                <th><?php echo lang('column_task_start_time_text') ?></th>
                                <th><?php echo lang('column_task_end_time_text') ?></th>
                                <th><?php echo lang('column_task_priority_text') ?></th>
                                <th><?php echo lang('column_task_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!---------------------------------------------------------------------------------------------------------->

<style>
    #task-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #task-table td,
    #task-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'project_base_assets/base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";

        var task_table = $('#task-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_task_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_task_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            columns: [
                {data: "task_number"},
                {data: "task_title"},
                {data: "project_name"},
                {data: "client_name"},
                {
                    data: {
                        _: "prog.html",
                        sort: "prog.int"
                    }
                },

                {
                    data: {
                        _: "st_dt.display",
                        sort: "st_dt.timestamp"
                    }
                },
                {
                    data: {
                        _: "en_dt.display",
                        sort: "en_dt.timestamp"
                    }
                },
                {
                    data: {
                        _: "prio.html",
                        sort: "prio.int"
                    }

                }, {
                    data: {
                        _: "sts.html",
                        sort: "sts.int"
                    }
                },
                {data: "action"}

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [7, 9]} /*, { visible: false, targets: [2,5] }*/
            ],

            aaSorting: [[6, 'asc']],

            ajax: {
                url: "<?php echo base_url() . 'task_module/get_tasks_by_ajax' ?>",                   // json datasource
                type: "post",
                data: function (data) {
                    data.is_admin = '<?php if ($is_admin) {
                        echo $is_admin;
                    }?>';
                    data.is_client = '<?php if ($is_client) {
                        echo $is_client;
                    }?>';
                    data.is_staff = '<?php if ($is_staff) {
                        echo $is_staff;
                    }?>';
                    data.which_tasks = '<?php if ($which_tasks) {
                        echo $which_tasks;
                    }?>';
                    data.seen_by = '<?php if ($seen_by) {
                        echo $seen_by;
                    }?>';
                    data.user_id = '<?php if ($user_id) {
                        echo $user_id;
                    }?>'; // of whose data is being seen .
                    data.project_id = '<?php if ($project_id) {
                        echo $project_id;
                    }?>';
                    data.tasks_belongs_to = '<?php if ($tasks_belongs_to) {
                        echo $tasks_belongs_to;
                    }?>';
                },

                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>

<script>
    /*column toggle*/
    $(function () {

        var table = $('#task-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#task-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>


<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_progress_filter').on('change', function () {

            if ($('#custom_progress_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_progress_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
        $('#custom_priority_filter').on('change', function () {

            if ($('#custom_priority_filter').val() == 'all') {
                $('#col7_filter').val('');
                filterColumn(7);
            } else {
                $('#col7_filter').val($('#custom_priority_filter').val());
                filterColumn(7);
            }

        });
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col8_filter').val('');
                filterColumn(8);
            } else {
                $('#col8_filter').val($('#custom_status_filter').val());
                filterColumn(8);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {

        $('.complete-confirmation').click(function (e) {
            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_complete_title_text')?>",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "darkgreen",
                    confirmButtonText: "<?= lang('swal_complete_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_complete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });


        $('.delete-confirmation').click(function (e) {
            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_delete_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_delete_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_delete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

    }


</script>
