<style>
    .select2-selection__choice {
        background-color: #428bca !important;
    }

    .select2-selection__choice__remove {
        color: white !important;
    }

    .select2-selection__choice__remove:hover {
        color: #d9534f !important;
        font-size: larger;
    }

</style>
<!--.................................................................................................................-->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

            <?php
            if ($which_form == 'add_form') {
                echo lang('page_title_add_text');
                echo '&nbsp;';
                echo '<small>(' . $project_info->project_name . ')</small>';
            } else {
                echo lang('page_title_edit_text');
                echo '&nbsp;';
                echo '<small>(' . $project_info->project_name . ')</small>';
            }
            ?>

            <small>
                <?php if (($which_form == 'add_form')) {
                    echo lang('page_subtitle_add_text');
                } else {
                    echo lang('page_subtitle_edit_text');
                }
                ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'projectroom_module/project_overview/' . $project_id ?>">
                    <i class="fa fa-gg"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'task_module/show_tasks/all_tasks/' . $project_id ?>">
                        <?php echo lang('breadcrumb_all_tasks_page_text') ?>
                    </a>
                    &nbsp;| &nbsp;
                    <a href="<?php echo base_url() . 'task_module/show_tasks/my_tasks/' . $project_id ?>">
                        <?php echo lang('breadcrumb_my_tasks_page_text') ?>
                    </a>

                <?php } else { ?>
                    <a href="<?php echo base_url() . 'task_module/show_tasks/my_tasks/' . $project_id ?>">
                        <?php echo lang('breadcrumb_my_tasks_page_text') ?>
                    </a>
                <?php } ?>
            </li>
            <li class="active">
                <?php
                if ($which_form == 'add_form') {
                    echo lang('breadcrumb_add_form_text');
                } else {
                    echo lang('breadcrumb_edit_form_text');
                }

                ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?php echo $projectroom_menu_section ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php
                            if ($which_form == 'add_form') {
                                echo lang('box_title_create_text');
                            } else {
                                echo lang('box_title_edit_text');
                            }
                            ?>
                        </h3>
                        <br><br>
                        <?php if ($which_form == 'edit_form') { ?>
                            <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                                <?php
                                if ($this->session->flashdata('validation_errors'))
                                    echo $this->session->flashdata('validation_errors');
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                            <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                                <?php
                                if ($this->session->flashdata('staff_required'))
                                    echo $this->session->flashdata('staff_required');
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        <?php } ?>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php
                            if ($val_errors)
                                echo $val_errors;
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <?php
                            if ($this->session->flashdata('task_add_success')) {
                                echo $this->session->flashdata('task_add_success');
                            }
                            ?>
                            <?php if ($this->session->flashdata('task_add_success')) { ?>
                                &nbsp;
                                <a href="<?php echo base_url() . 'task_module/task_overview/'
                                    . $this->session->flashdata('project_id')
                                    . '/'
                                    . $this->session->flashdata('task_id')
                                ?>">
                                    <?= lang('see_task_text') ?>
                                </a>
                                <br>
                                <a href="<?php echo base_url()
                                    . 'file_manager_module/get_upload_file_form_with_project_with_task/'
                                    . $this->session->flashdata('project_id')
                                    . '/'
                                    . $this->session->flashdata('task_id')
                                ?>">
                                    <?= lang('upload_file_text') ?>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <?php
                            if ($this->session->flashdata('task_update_success')) {
                                echo $this->session->flashdata('task_update_success');
                            }
                            ?>
                            <?php if ($this->session->flashdata('task_update_success')) { ?>
                                &nbsp;
                                <a href="<?php echo base_url() . 'task_module/task_overview/'
                                    . $this->session->flashdata('project_id')
                                    . '/'
                                    . $this->session->flashdata('task_id')
                                ?>">
                                    <?= lang('see_task_text') ?>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-2"></div>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . $form_action ?>" role="form" id="" method="post"
                          enctype="multipart/form-data">

                        <input type="hidden" name="which_form" value="<?= $which_form ?>">
                        <input type="hidden" name="project_id" value="<?= $project_id ?>">
                        <?php if ($which_form == 'edit_form') { ?>
                            <input type="hidden" name="task_id" value="<?= $task_info->task_id ?>">
                        <?php } ?>
                        <input type="hidden" name="is_admin" value="<?= $is_admin ?>">

                        <!--need client id for log purpose-->
                        <input type="hidden" name="client_id" value="<?= $project_info->client_id ?>">

                        <div class="box-body">
                            <div class="form-group">
                                <label for="task_number"><?php echo lang('label_task_number_text') ?></label>

                                <input type="text" name="task_number" class="form-control" id="task_number"
                                       value="<?php
                                       if ($which_form == 'add_form') {
                                           echo $task_number; //generated string
                                       } else if ($which_form == 'edit_form') {
                                           echo $task_info->task_number; //from db
                                       } else {
                                           echo '';
                                       }
                                       ?>"

                                       placeholder="" readonly>
                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_title"><?php echo lang('label_task_title_text') ?></label>

                                <input type="text" name="task_title" class="form-control"
                                       id="task_title"
                                       value="<?php echo $task_info->task_title; ?>"
                                       placeholder="<?php echo lang('placeholder_task_title_text') ?>"
                                >
                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_description"><?php echo lang('label_task_description_text') ?></label>

                                <textarea class="form-control" name="task_description" id="task_description"
                                          rows="3"
                                          placeholder="<?php echo lang('placeholder_task_description_text') ?>"
                                          style="width: 100%; height: 300px;"
                                ><?php echo $task_info->task_description; ?></textarea>

                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_priority"><?php echo lang('label_task_priority_text') ?></label>

                                <select class="form-control" name="task_priority" id="task_priority">

                                    <option value="normal"
                                        <?php if ($task_info->task_priority == 'normal') {
                                            echo 'selected';
                                        } else {
                                            echo '';
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_task_priority_normal_text') ?>
                                    </option>

                                    <option value="high"
                                        <?php if ($task_info->task_priority == 'high') {
                                            echo 'selected';
                                        } else {
                                            echo '';
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_task_priority_high_text') ?>
                                    </option>

                                    <option value="low"
                                        <?php if ($task_info->task_priority == 'low') {
                                            echo 'selected';
                                        } else {
                                            echo '';
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_task_priority_low_text') ?>
                                    </option>

                                </select>

                            </div>


                            <div class="form-group ">
                                <label><?php echo lang('label_task_progress_text') ?>
                                    <div style="color: 	#428bca ;font-size: larger">
                                        <input id="show_task_progress" type="text"
                                               style="border: transparent;width: 30px" value="0">
                                        <span>%</span>
                                    </div>
                                </label>

                                <input value="" style=" border: white">
                                <input id="task_progress" class="" type="hidden" name="task_progress"
                                       value="<?php
                                       if ($task_info->task_progress != '') {
                                           echo $task_info->task_progress;
                                       } else {
                                           echo '0';
                                       }
                                       ?>">
                                <div id="slider" class=""></div>
                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="assigned_staffs[]"><?php echo lang('label_assign_staffs_text') ?>
                                    <br>
                                    <div style="color: darkred">
                                        <?php if ($active_staffs == null || empty($active_staffs)) {
                                            echo lang('no_active_staff_in_project_text'); ?>
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'project_module/edit_project/' . $project_id ?>"><?= lang('add_staff_in_project_text') ?></a>
                                        <?php } ?>
                                    </div>
                                </label>

                                <select name="assigned_staffs[]" class="form-control form-group select2"
                                        multiple="multiple"
                                        data-placeholder="<?php echo lang('placeholder_assign_staffs_text') ?>"
                                        id=""
                                        style="width: 100%"

                                >

                                    <?php foreach ($active_staffs as $an_active_staff) { ?>
                                        <option value="<?php echo $an_active_staff->user_id ?>"
                                            <?php if ($assigned_staffs) {
                                                foreach ($assigned_staffs as $an_assigned_staff) {
                                                    if ($an_assigned_staff->staff_id == $an_active_staff->user_id) {
                                                        echo 'selected';
                                                    }
                                                }
                                            } else if ($chosen_staffs) {
                                                foreach ($chosen_staffs as $a_chosen_staff) {
                                                    if ($a_chosen_staff == $an_active_staff->user_id) {
                                                        echo 'selected';
                                                    }
                                                }
                                            } else {
                                                echo '';
                                            }
                                            ?>
                                        >
                                            <?php echo $an_active_staff->first_name . ' ' . $an_active_staff->last_name ?>
                                        </option>
                                    <?php } ?>

                                </select>

                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_start_time"><?php echo lang('label_task_start_time_text') ?></label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="task_start_time" class="form-control" id="task_start_time"
                                           value="<?php echo $task_info->task_start_time; ?>"
                                           placeholder="<?php echo lang('placeholder_task_start_time_text') ?>"
                                           style="background-color: inherit" readonly>
                                </div>

                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_end_time"><?php echo lang('label_task_end_time_text') ?></label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="task_end_time" class="form-control" id="task_end_time"
                                           value="<?php echo $task_info->task_end_time; ?>"
                                           placeholder="<?php echo lang('placeholder_task_end_time_text') ?>"
                                           style="background-color: inherit" readonly>
                                </div>

                            </div>

                            <div class="form-group" <?php if ($is_admin == 'not_admin') echo 'hidden' ?>>
                                <label for="task_status"><?php echo lang('label_task_status_text') ?></label>

                                <!--testing: unresolved-->

                                <?php

                                /*echo $task_info->task_status
                                    . '_dt|fls_ '
                                    . $this->session->flashdata('task_status')
                                    .'--flstp:'.gettype($this->session->flashdata('task_status'))
                                */
                                ?>

                                <select class="form-control" name="task_status" id="task_status">
                                    <option value="1" class=""
                                        <?php
                                        if ($task_info->task_status == 1) {
                                            echo 'selected';
                                        } else {
                                            echo '';
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_task_status_active_text') ?>
                                    </option>

                                    <option value="0" class=""
                                        <?php
                                        if ($task_info->task_status == 0) {
                                            echo 'selected';
                                        } else {
                                            echo '';
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_task_status_inactive_text') ?>
                                    </option>

                                </select>

                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">

                            <button type="submit" id=""
                                    class="btn btn-primary">
                                <?php
                                if ($which_form == 'add_form') {
                                    echo lang('button_submit_create_text');
                                } else {
                                    echo lang('button_submit_update_text');
                                }
                                ?>
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--.................................................................................................................-->

<!--tiny mce-->
<script>
    tinymce.init({
        selector: '#task_description',
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt'
    })
</script>

<!--task progress-->
<script>
    var progress = $("#task_progress").val();
    $("#slider").slider({
        value: progress,
        min: 0,
        max: 100,
        step: 1,
        slide: function (event, ui) {
            $("#task_progress").val(ui.value);
            $("#show_task_progress").val(ui.value);
        }
    });
    $("#task_progress").val($("#slider").slider("value"));
    $("#show_task_progress").val($("#slider").slider("value"));
</script>

<script>
    //Initialize Select2 Elements
    $(".select2").select2();
</script>

<!--jquery datetimepicker #an addon of jqury datepicker see:http://trentrichardson.com/examples/timepicker/-->
<script>
    var startDateTextBox = $("#task_start_time");
    var endDateTextBox = $("#task_end_time");

    startDateTextBox.datetimepicker({
        numberOfMonths: 2,
        stepHour: 1,
        stepMinute: 5,
        addSliderAccess: true,
        sliderAccessArgs: {touchonly: false},
        dateFormat: '<?php echo $dateformat ?>',
        timeFormat: '<?php echo $timeformat ?>',
        showMillisec: false,
        showMicrosec: false,
        showTimezone: false,

        onClose: function (dateText, inst) {
            if (endDateTextBox.val() != '') {
                var testStartDate = startDateTextBox.datetimepicker('getDate');
                var testEndDate = endDateTextBox.datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    endDateTextBox.datetimepicker('setDate', testStartDate);
            }
            else {
                endDateTextBox.val(dateText);
            }
        },
        onSelect: function (selectedDateTime) {
            endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
        }
    });
    endDateTextBox.datetimepicker({
        numberOfMonths: 2,
        stepHour: 1,
        stepMinute: 5,
        addSliderAccess: true,
        sliderAccessArgs: {touchonly: false},
        dateFormat: '<?php echo $dateformat ?>',
        timeFormat: '<?php echo $timeformat ?>',
        showMillisec: false,
        showMicrosec: false,
        showTimezone: false,

        onClose: function (dateText, inst) {
            if (startDateTextBox.val() != '') {
                var testStartDate = startDateTextBox.datetimepicker('getDate');
                var testEndDate = endDateTextBox.datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    startDateTextBox.datetimepicker('setDate', testEndDate);
            }
            else {
                startDateTextBox.val(dateText);
            }
        },
        onSelect: function (selectedDateTime) {
            startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
        }
    });
</script>


