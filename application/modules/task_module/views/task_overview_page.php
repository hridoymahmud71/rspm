<div class="content-wrapper" style="min-height: 1126px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= lang('page_title_text') ?>
            <small>
                <?= lang('page_subtitle_text') ?>
                &nbsp;
                <?= $project_info->project_name ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() . 'projectroom_module/project_overview/'.$project_id ?>"><i class="fa fa-gg"></i>
                    <?= $project_info->project_name ?>
                </a>
            </li>
            <li>
                <a href="<?= base_url() . 'task_module/show_tasks/all_tasks/'.$project_id ?>">
                    <?= lang('breadcrumb_all_tasks_page_text') ?>
                </a>
                &nbsp;
                |
                &nbsp;
                <a href="<?= base_url() . 'task_module/show_tasks/my_tasks/'.$project_id ?>">
                    <?= lang('breadcrumb_my_tasks_page_text') ?>
                </a>
            </li>

            <li class="active"><?= lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!--projectroom menu starts-->
            <div class="col-md-4">
                <?php echo $projectroom_menu_section ?>
            </div>
            <!--projectroom menu ends-->

            <!--overview wrap starts-->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <strong><h3 class="box-title"><?= lang('box_title_text') ?></h3></strong>

                        <!--only for admin /edit task-->
                        <?php if ($is_admin == 'admin' || $is_staff == 'staff') { ?>
                            <a title="<?php echo lang('tooltip_edit_task_text') ?>" href="<?= base_url() . 'task_module/edit_task/'.$a_task->task_id ?>"
                               style="color:#2b2b2b;margin: 1%" class="pull-right">
                                <i class="fa fa-pencil-square-o fa-lg"></i>
                            </a>
                        <?php } ?>

                        <a title="<?php echo lang('tooltip_upload_file_text') ?>" style="color: #2b2b2b;margin: 1%" href="<?php echo base_url()
                            . 'file_manager_module/get_upload_file_form_with_project_with_task/'
                            . $a_task->project_id
                            . '/'
                            . $a_task->task_id ?>"
                           class="pull-right"><i class="fa fa-upload fa-lg"
                                                 aria-hidden="true"></i>
                        </a>
                        <a title="<?php echo lang('tooltip_task_files_text') ?>" style="color: #2b2b2b;margin: 1%" href="<?php echo base_url()
                            . 'file_manager_module/files_with_project_with_task/'
                            . $a_task->project_id
                            . '/'
                            . $a_task->task_id ?>"
                           class="pull-right"><i class="fa fa-file-o fa-lg"
                                                 aria-hidden="true"></i>
                        </a>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <?php if ($a_task) { ?>
                            <!--task details starts-->
                            <strong><i class="fa fa-th margin-r-5"></i><?= lang('task_details_text') ?></strong>
                            <p><!--empty space--></p>
                            <div class="box-body table-responsive no-padding ">
                                <table class="table table-hover text-muted table-bordered table-responsive">
                                    <tbody>
                                    <tr>
                                        <td><?= lang('task_number_text') ?></td>
                                        <td><?= $a_task->task_number ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('task_title_text') ?></td>
                                        <td><?= $a_task->task_title ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('task_description_text') ?></td>
                                        <td>
                                            <texarea rows="3" id="task_description"
                                                     style="background-color: inherit;width: 100%;height: 300px">
                                                <?php echo $a_task->task_description ?>
                                            </texarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('task_progress_text') ?></td>
                                        <td>
                                            <span class="<?php
                                            if ($a_task->task_progress < 33) {
                                                echo 'label label-danger pull-right';
                                            } else if ($a_task->task_progress > 66) {
                                                echo 'label label-success pull-right';
                                            } else {
                                                echo 'label label-primary pull-right';
                                            }
                                            ?>">
                                            <?php echo $a_task->task_progress . '%' ?>
                                        </span>
                                            &nbsp;
                                            <div class="progress progress-xs">
                                                <div class="<?php
                                                if ($a_task->task_progress < 33) {
                                                    echo 'progress-bar progress-bar-danger';
                                                } else if ($a_task->task_progress > 66) {
                                                    echo 'progress-bar progress-bar-success';
                                                } else {
                                                    echo 'progress-bar progress-bar-primary';
                                                }
                                                ?>"
                                                     style="width: <?php echo $a_task->task_progress . '%' ?> "></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('task_status_text') ?></td>

                                        <?php if ($a_task->task_status != 0) { ?>
                                            <td>
                                                <span class="label label-primary"><?= lang('task_active_text') ?></span>
                                            </td>
                                        <?php } else { ?>
                                            <td>
                                                <span class="label label-default"><?= lang('task_inactive_text') ?></span>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?= lang('task_start_time_text') ?></td>
                                        <td><?= $a_task->task_start_time ?></td>

                                    </tr>
                                    <tr>
                                        <td><?= lang('task_end_time_text') ?></td>
                                        <td><?= $a_task->task_end_time ?></td>
                                    </tr>
                                    <!--only for admin starts-->
                                    <?php if ($is_admin == 'admin') { ?>
                                        <tr>
                                            <td><?= lang('task_created_at_text') ?></td>
                                            <td><?= $a_task->created_at ?></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang('task_modified_at_text') ?></td>
                                            <td><?= $a_task->modified_at ?></td>
                                        </tr>
                                    <?php } ?>
                                    <!--only for admin ends-->
                                    </tbody>
                                </table>
                            </div>
                            <!--task details ends-->

                        <?php } ?>

                        <hr>

                        <!--staffs starts-->
                        <strong><i class="fa fa-users margin-r-5"></i><?= lang('assigned_staffs_text') ?>
                        </strong>
                        <p><!--empty space--></p>
                        <table class="table table-hover text-muted table-bordered table-responsive">
                            <tr>
                                <th>In Project</th>
                                <th>In Task</th>
                            </tr>
                            <?php if ($assigned_staffs_to_project) {
                                foreach ($assigned_staffs_to_project as $an_assigned_staff_to_project) { ?>

                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url()
                                                .'user_profile_module/user_profile_overview/'.$an_assigned_staff_to_project->id ?>">
                                                <?php if ($an_assigned_staff_to_project->active != 0) { ?>
                                                    <span class="label label-primary">
                                                                <?php echo $an_assigned_staff_to_project->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff_to_project->last_name ?>
                                                            </span>
                                                <?php } else { ?>
                                                    <span class="label label-default">
                                                                <?php echo $an_assigned_staff_to_project->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff_to_project->last_name
                                                                    . ' '
                                                                    . lang('staff_inactive_text')
                                                                ?>

                                                            </span>
                                                <?php } ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php if ($assigned_staffs_to_task) {
                                                foreach ($assigned_staffs_to_task as $an_assigned_staff_to_task) {
                                                    if ($an_assigned_staff_to_task->id == $an_assigned_staff_to_project->id) { ?>
                                                        <a href="<?php echo base_url()
                                                            .'user_profile_module/user_profile_overview/'.$an_assigned_staff_to_task->id ?>">
                                                            <?php if ($an_assigned_staff_to_task->active != 0) { ?>
                                                                <span class="label label-primary">
                                                                <?php echo $an_assigned_staff_to_task->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff_to_task->last_name ?>
                                                            </span>
                                                            <?php } else { ?>
                                                                <span class="label label-default">
                                                                <?php echo $an_assigned_staff_to_task->first_name
                                                                    . ' '
                                                                    . $an_assigned_staff_to_task->last_name
                                                                    . ' '
                                                                    . lang('staff_inactive_text')
                                                                ?>
                                                            </span>
                                                            <?php } ?>
                                                        </a>
                                                    <?php } ?>
                                                    <?php
                                                }
                                            } ?>


                                        </td>
                                    </tr>

                                <?php } ?>

                            <?php } else { ?>

                                <tr>
                                    <td>
                                        <div class="text-muted"><?= lang('no_staff_assigned_text') ?></div>
                                    </td>
                                    <td>

                                    </td>
                                </tr>

                            <?php } ?>
                        </table>

                        <!--staffs ends-->


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!--overview wrap ends-->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script>
    tinymce.init({
        selector: '#task_description',
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        readonly: 1
    })
</script>