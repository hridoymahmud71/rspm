<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// BaseUrl/show_tasks/All TAsks Or My Tasks/Project Id
$route['task_module/show_tasks/(:any)/(:any)'] = 'TaskController/showTasks';
$route['task_module/show_all_tasks_from_all_projects'] = 'TaskController/showAllTasksFromAllProjects';
$route['task_module/show_my_tasks_from_my_projects'] = 'TaskController/showMyTasksFromMyProjects';

$route['task_module/show_my_tasks_from_my_projects/seen_by_other/(:any)'] = 'TaskController/showMyTasksFromMyProjects_seenByOther';
$route['task_module/get_tasks_by_ajax'] = 'TaskController/getTasks_byAjax';

// BaseUrl/Add task/Project Id      #tasks belongs to which project ?
$route['task_module/posted_add_task'] = 'TaskController/repostAddTask';
$route['task_module/add_task/(:any)'] = 'TaskController/addTask';

// BaseUrl/Edit task/Task Id        #no need to get project id , will come from data
$route['task_module/edit_task/(:any)'] = 'TaskController/editTask';

$route['task_module/create_task'] = 'TaskController/createTask';
$route['task_module/update_task'] = 'TaskController/updateTask';

// task_module/task_overview/(:project_id)/(:task_id)
$route['task_module/task_overview/(:any)/(:any)'] = 'TaskController/showTaskOverview';

/*delete , activate|deactivate , prioritize*/
$route['task_module/all_tasks_from_all_projects/make_task_complete/(:any)'] = 'TaskController/makeTaskComplete';
$route['task_module/all_tasks_from_all_projects/delete_task/(:any)'] = 'TaskController/deleteTask';
$route['task_module/all_tasks_from_all_projects/activate_task/(:any)'] = 'TaskController/activateTask';
$route['task_module/all_tasks_from_all_projects/deactivate_task/(:any)'] = 'TaskController/deactivateTask';

$route['task_module/all_tasks_from_all_projects/make_task_priority_high/(:any)'] = 'TaskController/makeTaskPriorityHigh';
$route['task_module/all_tasks_from_all_projects/make_task_priority_normal/(:any)'] = 'TaskController/makeTaskPriorityNormal';
$route['task_module/all_tasks_from_all_projects/make_task_priority_low/(:any)'] = 'TaskController/makeTaskPriorityLow';

$route['task_module/all_tasks/make_task_complete/(:any)'] = 'TaskController/makeTaskComplete';
$route['task_module/all_tasks/delete_task/(:any)'] = 'TaskController/deleteTask';
$route['task_module/all_tasks/activate_task/(:any)'] = 'TaskController/activateTask';
$route['task_module/all_tasks/deactivate_task/(:any)'] = 'TaskController/deactivateTask';

$route['task_module/all_tasks/make_task_priority_high/(:any)'] = 'TaskController/makeTaskPriorityHigh';
$route['task_module/all_tasks/make_task_priority_normal/(:any)'] = 'TaskController/makeTaskPriorityNormal';
$route['task_module/all_tasks/make_task_priority_low/(:any)'] = 'TaskController/makeTaskPriorityLow';


$route['task_module/my_tasks_from_my_projects/make_task_complete/(:any)'] = 'TaskController/makeTaskComplete';
$route['task_module/my_tasks_from_my_projects/delete_task/(:any)'] = 'TaskController/deleteTask';
$route['task_module/my_tasks_from_my_projects/activate_task/(:any)'] = 'TaskController/activateTask';
$route['task_module/my_tasks_from_my_projects/deactivate_task/(:any)'] = 'TaskController/deactivateTask';

$route['task_module/my_tasks_from_my_projects/make_task_priority_high/(:any)'] = 'TaskController/makeTaskPriorityHigh';
$route['task_module/my_tasks_from_my_projects/make_task_priority_normal/(:any)'] = 'TaskController/makeTaskPriorityNormal';
$route['task_module/my_tasks_from_my_projects/make_task_priority_low/(:any)'] = 'TaskController/makeTaskPriorityLow';

$route['task_module/my_tasks/make_task_complete/(:any)'] = 'TaskController/makeTaskComplete';
$route['task_module/my_tasks/delete_task/(:any)'] = 'TaskController/deleteTask';
$route['task_module/my_tasks/activate_task/(:any)'] = 'TaskController/activateTask';
$route['task_module/my_tasks/deactivate_task/(:any)'] = 'TaskController/deactivateTask';

$route['task_module/my_tasks/make_task_priority_high/(:any)'] = 'TaskController/makeTaskPriorityHigh';
$route['task_module/my_tasks/make_task_priority_normal/(:any)'] = 'TaskController/makeTaskPriorityNormal';
$route['task_module/my_tasks/make_task_priority_low/(:any)'] = 'TaskController/makeTaskPriorityLow';

//project_id/task_id




