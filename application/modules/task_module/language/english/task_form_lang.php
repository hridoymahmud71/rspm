<?php

/*page_text :both add task form and edit task form*/
$lang['page_title_add_text'] = 'Add Task';
$lang['page_title_edit_text'] = 'Edit Task';

$lang['page_subtitle_add_text'] = 'Add a new task';
$lang['page_subtitle_edit_text'] = 'Edit Task :';

$lang['breadcrumb_home_text'] = 'Project Overview';

$lang['breadcrumb_all_tasks_page_text'] = 'All Tasks';
$lang['breadcrumb_my_tasks_page_text'] = 'My Tasks';

$lang['breadcrumb_add_form_text'] = 'Add Form';
$lang['breadcrumb_edit_form_text'] = 'Edit Form';

$lang['box_title_create_text'] = 'Create Task Form';
$lang['box_title_edit_text'] = 'Edit Task Form';

$lang['button_submit_create_text'] = 'Create Task';
$lang['button_submit_update_text'] = 'Update Task';


/*form texts*/
$lang['label_task_number_text'] = 'Task Number';
$lang['label_task_title_text'] = 'Task Title';
$lang['label_task_description_text'] = 'Task Description';
$lang['label_task_progress_text'] = 'Task Progress';

$lang['label_assign_staffs_text'] = 'Assign Stuff(s)';

$lang['label_task_start_time_text'] = 'Task Starts';
$lang['label_task_end_time_text'] = 'Task Deadline';

$lang['label_task_priority_text'] = 'Task Priority';
$lang['option_task_priority_normal_text'] = 'Normal';
$lang['option_task_priority_high_text'] = 'High';
$lang['option_task_priority_low_text'] = 'Low';


$lang['label_task_status_text'] = 'Task Status';
$lang['option_task_status_active_text'] = 'Active';
$lang['option_task_status_inactive_text'] = 'Inactive';

$lang['placeholder_task_title_text'] = 'Enter Task\'s Title';
$lang['placeholder_task_description_text'] = 'Enter Task\'s Description';
$lang['placeholder_assign_staffs_text'] = 'Select stuff(s) to assign';

$lang['placeholder_task_start_time_text'] = 'Enter Task\'s Starting Time';
$lang['placeholder_task_end_time_text'] = 'Enter Task\'s Ending Time';

$lang['no_active_staff_in_project_text'] = 'No active member is found for this project';
$lang['add_staff_in_project_text'] = 'First, Add staff(s) to the project.';

/*form validation errors*/
$lang['task_title_required_text'] = 'Task\'s Title is required';
$lang['task_start_time_required_text'] = 'Task\'s Start Time is required';
$lang['task_end_time_required_text'] = 'Task\'s Deadline is required';

$lang['staff_required_text'] = 'At least one staff is required';

/*success messages*/
$lang['task_add_success_text'] = 'Added succesfully';
$lang['task_update_success_text'] = 'Updated succesfully';

$lang['see_task_text'] = 'See Task';
$lang['upload_file_text'] = 'Upload File';

/*for log purposes*/
$lang['log_field_task_title_text'] = 'Task Title';
$lang['log_field_task_description_text'] = 'Task Description';
$lang['log_field_task_priority_text'] = 'Task Priority';
$lang['log_field_task_progress_text'] = 'Task Progress';
$lang['log_field_task_status_text'] = 'Task Status';
$lang['log_field_task_start_time_text'] = 'Task Start Time';
$lang['log_field_task_end_time_text'] = 'Task Deadline';

$lang['log_field_staffs_text'] = 'Staffs';

?>