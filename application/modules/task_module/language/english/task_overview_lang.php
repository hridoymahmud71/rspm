<?php

$lang['page_title_text'] = 'Task Overview';
$lang['page_subtitle_text'] = 'A task of:';

$lang['breadcrumb_all_tasks_page_text'] = 'All Tasks';
$lang['breadcrumb_my_tasks_page_text'] = 'My Tasks';
$lang['breadcrumb_page_text'] = ' Task Overview';

$lang['box_title_text'] = 'Overview';

/*main sections*/
$lang['task_details_text'] = 'Task Details';

$lang['assigned_staffs_text'] = 'Assigned Staff\'s';


/*rows of the project table*/

$lang['task_number_text'] = 'Task No.';
$lang['task_title_text'] = 'Task Title';
$lang['task_description_text'] = 'Task Description';
$lang['task_progress_text'] = 'Task Progress';
$lang['task_status_text'] = 'Task Status';

$lang['task_active_text'] = 'Active';
$lang['task_inactive_text'] = 'Inactive';

$lang['task_start_time_text'] = 'Task Start Time';
$lang['task_end_time_text'] = 'Task End Time';

$lang['task_created_at_text'] = 'Task Created At';
$lang['task_modified_at_text'] = 'Task Modified At';

/*staff*/
$lang['staff_inactive_text'] = '(Inactive)';
$lang['no_staff_assigned_text'] = 'No staff is assigned yet.';


/*tooplip*/
$lang['tooltip_edit_task_text'] = 'Edit Task';
$lang['tooltip_task_files_text'] = 'Task Files';
$lang['tooltip_upload_file_text'] = 'Upload File';


