<?php

/*page_text :both all tasks and my tasks*/
$lang['page_title_all_tasks_text'] = 'All Tasks';
$lang['page_title_my_tasks_text'] = 'My Tasks';
$lang['page_title_users_tasks_text'] = 'User\'s Tasks';

$lang['page_subtitle_admin_text'] = 'Add Tasks | Edit Tasks | Delete Tasks | View Tasks';

$lang['page_subtitle_all_tasks_from_all_projects_text'] = 'From All Projects';
$lang['page_subtitle_my_tasks_from_my_projects_text'] = 'From My Projects';
$lang['page_subtitle_my_tasks_from_users_projects_text'] = 'From User\'s Projects';

$lang['page_subtitle_non_admin_text'] = 'View Tasks';

$lang['breadcrumb_all_projects_page_text'] = 'All Projects';
$lang['breadcrumb_my_projects_page_text'] = 'My Projects';
$lang['breadcrumb_users_projects_page_text'] = 'User\'s Projects';

$lang['breadcrumb_all_tasks_page_text'] = 'All Tasks';
$lang['breadcrumb_my_tasks_page_text'] = 'My Tasks';
$lang['breadcrumb_users_tasks_page_text'] = 'User\'s Tasks';

$lang['box_title_all_tasks_text'] = 'All Tasks List';
$lang['box_title_my_tasks_text'] = 'My Tasks List';
$lang['box_title_users_tasks_text'] = 'User\'s Tasks List';
$lang['task_of_text'] = 'Tasks of';

$lang['add_button_text'] = 'Add A Task';

$lang['see_task_text'] = 'See Task';

$lang['no_task_found_text'] = 'No task is found';

/*table texts*/
$lang['toggle_column_text'] = 'Toggle Columns';
$lang['option_all_text'] = 'All';
$lang['option_complete_text'] = 'Complete';
$lang['option_incomplete_text'] = 'Incomplete';

$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_high_text'] = 'High';
$lang['option_normal_text'] = 'Normal';
$lang['option_low_text'] = 'Low';

$lang['column_task_number_text'] = 'Task Number';
$lang['column_task_title_text'] = 'Task Title';
$lang['column_task_project_text'] = 'Project';
$lang['column_task_client_text'] = 'Client';
$lang['column_task_progress_text'] = 'Task Progress';
$lang['column_task_status_text'] = 'Task Status';
$lang['column_task_priority_text'] = 'Task Priority';
$lang['column_change_task_priority_text'] = 'Change Priority';
$lang['column_task_start_time_text'] = 'Task Started';
$lang['column_task_end_time_text'] = 'Task Deadline';
$lang['column_actions_text'] = 'Actions';

/*inner table*/
$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

$lang['priority_high_text'] = 'High';
$lang['priority_normal_text'] = 'Normal';
$lang['priority_low_text'] = 'Low';

/*sweet alert texts*/
$lang['swal_delete_title_text'] = 'Are You sure to delete this task ?';
$lang['swal_delete_confirm_button_text'] = 'Yes, delete this task';
$lang['swal_delete_cancel_button_text'] = 'No, keep this task';

$lang['swal_complete_title_text'] = 'Is this task completed ?';
$lang['swal_complete_confirm_button_text'] = 'Yes, this task is complete';
$lang['swal_complete_cancel_button_text'] = 'No, keep as it is';

/*success panel messages*/
$lang['panel_head_success_text'] = 'Successfull';

$lang['task_completed_text'] = 'Task Completed';
$lang['task_deleted_text'] = 'Task Deleted';
$lang['task_activated_text'] = 'Task Activated';
$lang['task_deactivated_text'] = 'Task Deactivated';

$lang['task_prirority_made_high_text'] = 'Task priority set to high';
$lang['task_prirority_made_normal_text'] = 'Task priority set to normal';
$lang['task_prirority_made_low_text'] = 'Task priority set to low';
//task_activated_text

/*tooltip text*/
$lang['tooltip_make_priority_high_text'] = 'Make Task Priority High';
$lang['tooltip_make_priority_normal_text'] = 'Make Task Priority Normal';
$lang['tooltip_make_priority_low_text'] = 'Make Task Priority Low';

$lang['tooltip_activate_text'] = 'Make Task Active';
$lang['tooltip_deactivate_text'] = 'Make Task Deactive';

$lang['tooltip_view_text'] = 'View Task ';
$lang['tooltip_edit_text'] = 'Edit Task ';
$lang['tooltip_delete_text'] = 'Delete Task ';
$lang['tooltip_complete_text'] = 'Make Task Complete';
$lang['tooltip_upload_text'] = 'Upload a File';
$lang['tooltip_see_files_text'] = 'See File(s)';

/*-----------------*/
$lang['no_datetime_text'] = 'Date & Time Not Available';
$lang['loading_text'] = 'Loading Tasks . . .';

$lang['no_task_found_text'] = 'No Task is Found';
$lang['no_matching_task_found_text'] = 'No matching Task is found';


/*for log purposes*/
$lang['log_field_task_priority_text'] = 'Task priority';



?>