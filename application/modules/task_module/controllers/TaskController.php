<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TaskController extends MX_Controller
{
    function __construct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('projectroom_module/Projectroom_model');
        $this->load->model('Task_model');

        $this->load->library('form_validation');

        // application/libraries
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');

    }


    public function showTasks()
    {
        $which_tasks = $this->uri->segment(3);
        $project_id = $this->uri->segment(4);

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if ($which_tasks == 'all_tasks') { //in a specific project
            $this->showAllTasks($project_id);
        }

        if ($which_tasks == 'my_tasks') { // in a specific project
            $this->showMyTasks($project_id, $this->session->userdata('user_id'));
        }
    }

    public function showAllTasksFromAllProjects()
    {
        if (!$this->ion_auth->is_admin()) {
            $data['is_admin'] = 'not_admin';
            redirect('users/auth/need_permission');
        } else {
            $data['is_admin'] = 'admin';
        }

        $data['project_info'] = null;

        $data['which_tasks'] = 'all_tasks_from_all_projects';

        $this->showTaskList($data, 'unavailable');
    }

    public function showMyTasksFromMyProjects_seenByOther()
    {
        $user_id = $this->uri->segment(4);
        $this->showMyTasksFromMyProjects($seen_by_other = true, $user_id);
    }

    //show tasks form all related projects for staff or client
    public function showMyTasksFromMyProjects($seen_by_other = false, $given_user_id = false)
    {
        if (!$this->ion_auth->is_admin()) {
            $data['is_admin'] = 'not_admin';
        } else {
            $data['is_admin'] = 'admin';
        }

        $data['project_info'] = null;

        $data['which_tasks'] = 'my_tasks_from_my_projects';

        if ($given_user_id) {
            $user_id = $given_user_id;
        } else {
            $user_id = $this->session->userdata('user_id');
        }

        $this->showTaskList($data, 'unavailable', $seen_by_other, $given_user_id);
    }

    //show all tasks of a project
    public function showAllTasks($project_id)
    {

        if (!$this->ion_auth->is_admin()) {
            $data['is_admin'] = 'not_admin';
        } else {
            $data['is_admin'] = 'admin';
        }

        $data['project_info'] = $this->Task_model->getProject($project_id);

        $data['which_tasks'] = 'all_tasks';


        $this->showTaskList($data, $project_id);
    }

    //show a specific user's(staff or client) tasks  from a specific project
    public function showMyTasks($project_id, $user_id)
    {
        if (!$this->ion_auth->is_admin()) {
            $data['is_admin'] = 'not_admin';
        } else {
            $data['is_admin'] = 'admin';
        }

        $data['project_info'] = $this->Task_model->getProject($project_id);

        $data['which_tasks'] = 'my_tasks';


        $this->showTaskList($data, $project_id);

    }

    //view function for task list
    public function showTaskList($data, $project_id, $seen_by_other = false, $given_user_id = false)
    {
        $this->lang->load('task_list');

        if ($project_id != 'unavailable') {
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view
        } else {
            $data['projectroom_menu_section'] = null;
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($seen_by_other) {
            $data['seen_by'] = 'other';
        } else {
            $data['seen_by'] = 'me';
        }

        $data['project_id'] = $project_id;

        $data['tasks_belongs_to'] = false;
        if ($given_user_id && $seen_by_other) {
            if ($this->ion_auth->in_group('client', $given_user_id)) {
                $data['tasks_belongs_to'] = 'client';
            }
            if ($this->ion_auth->in_group('staff', $given_user_id)) {
                $data['tasks_belongs_to'] = 'staff';
            }
        }

        if (!$given_user_id && !$seen_by_other) {
            if ($this->ion_auth->in_group('client')) {
                $data['tasks_belongs_to'] = 'client';
            }
            if ($this->ion_auth->in_group('staff')) {
                $data['tasks_belongs_to'] = 'staff';
            }
        }


        if ($given_user_id) {
            $data['user_id'] = $given_user_id;
            $data['user_info'] = $this->Task_model->getUserInfo($given_user_id);
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
            $data['user_info'] = null;
        }


        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("task_module/task_list_page", $data);
        $this->load->view("common_module/footer");
    }

    // can load any 1 of 4 lists to the view
    public function getTasks_byAjax()
    {

        $this->lang->load('task_list');

        $tasks = array();
        $table_data = array();

        $requestData = $_REQUEST;
        $is_admin = $requestData['is_admin'];
        $is_client = $requestData['is_client'];
        $is_staff = $requestData['is_staff'];

        $which_tasks = $requestData['which_tasks'];
        $seen_by = $requestData['seen_by'];
        $user_id = $requestData['user_id'];
        $project_id = $requestData['project_id'];
        $tasks_belongs_to = $requestData['tasks_belongs_to'];
        //print_r($requestData);

        if ($project_id == 'unavailable') {
            $project_id = false;
        }

        $staff_id = false;
        if ($tasks_belongs_to == 'staff') {
            $staff_id = $user_id;
        }
        $client_id = false;
        if ($tasks_belongs_to == 'client') {
            $client_id = $user_id;
        }


        $columns[0] = 'task_number';
        $columns[1] = 'task_title';
        $columns[2] = 'task_project';
        $columns[3] = 'task_client';
        $columns[4] = 'task_progress';
        $columns[5] = 'task_start_time';
        $columns[6] = 'task_end_time';
        $columns[7] = 'task_priority';
        $columns[8] = 'task_status';
        $columns[9] = 'action';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['task_number'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['task_title'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['task_project'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['task_client'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['task_progress'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][7]['search']['value'])) {
            $specific_filters['task_priority'] = $requestData['columns'][7]['search']['value'];
        }

        if (!empty($requestData['columns'][8]['search']['value'])) {
            $specific_filters['task_status'] = $requestData['columns'][8]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        //before query , remember only one can be true between $staff_id and $client_id

        $totalData = $this->Task_model->countTasksByAjax(false, false, $project_id, $client_id, $staff_id, $which_tasks);


        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Task_model->countTasksByAjax($common_filter_value, $specific_filters, $project_id, $client_id, $staff_id, $which_tasks);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $tasks = $this->Task_model->getTasksByAjax($common_filter_value, $specific_filters, $order, $limit, $project_id, $client_id, $staff_id, $which_tasks);


        if ($tasks == false || empty($tasks) || $tasks == null) {
            $tasks = false;
            $table_data = false;
        }

        $last_query = $this->db->last_query();

        if ($tasks) {

            $i = 0;
            foreach ($tasks as $a_task) {

                $row = new stdClass();

                $row->task_number = $a_task->task_number;
                $row->task_title = $a_task->task_title;

                /*project name starts*/
                $project_tooltip = $a_task->project_name;
                $project_url = base_url() . 'projectroom_module/project_overview/' . $a_task->project_id;
                $project_name_anchor =
                    '<a ' . 'title="' . $project_tooltip . '"' . ' href="' . $project_url . '"' . ' >'
                    . $a_task->project_name
                    . '</a>';
                $row->project_name = $project_name_anchor;
                /*project name ends*/

                /*client name starts*/
                $client_tooltip = $a_task->client_first_name . ' ' . $a_task->client_last_name;
                $client_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_task->client_user_id;
                $client_name_anchor =
                    '<a ' . 'title="' . $client_tooltip . '"' . ' href="' . $client_url . '"' . ' >'
                    . $a_task->client_first_name . ' ' . $a_task->client_last_name
                    . '</a>';
                $row->client_name = $client_name_anchor;
                /*client name ends*/

                /*datetime starts*/
                $tasks[$i]->st_dt = new stdClass();
                $tasks[$i]->en_dt = new stdClass();

                $tasks[$i]->st_dt->timestamp = $a_task->task_start_time;
                $tasks[$i]->en_dt->timestamp = $a_task->task_end_time;

                if ($a_task->task_start_time == 0 || $a_task->task_start_time == false || $a_task->task_start_time == null) {
                    $tasks[$i]->st_dt->display = $this->lang->line('no_datetime_text');
                } else {
                    $tasks[$i]->st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_task->task_start_time);
                }

                if ($a_task->task_end_time == 0 || $a_task->task_end_time == false || $a_task->task_end_time == null) {
                    $tasks[$i]->en_dt->display = $this->lang->line('no_datetime_text');
                } else {
                    $tasks[$i]->en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_task->task_end_time);
                }

                $row->st_dt = $tasks[$i]->st_dt;
                $row->en_dt = $tasks[$i]->en_dt;
                /*datetime ends*/

                /*progress starts*/
                $tasks[$i]->prog = new stdClass();
                $tasks[$i]->prog->int = $a_task->task_progress;

                $progress_span_class = '';
                $progressbar_inner_div_class = '';
                if ($a_task->task_progress < 33) {
                    $progress_span_class = 'label label-danger pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-danger';
                } else if ($a_task->task_progress > 66) {
                    $progress_span_class = 'label label-success pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-success';
                } else {
                    $progress_span_class = 'label label-primary pull-right';
                    $progressbar_inner_div_class = 'progress-bar progress-bar-primary';
                }

                $progress_span = '<span ' . 'class="' . $progress_span_class . '"' . ' >' . $a_task->task_progress . '%' . '</span>';

                $progressbar_inner_div_style = 'width:' . $a_task->task_progress . '%';
                $progressbar_inner_div =
                    '<div ' . ' class="' . $progressbar_inner_div_class . '"' . 'style="' . $progressbar_inner_div_style . '""' . '>' . '</div>';
                $progressbar_outer_div = '<div class="progress progress-xs" >' . $progressbar_inner_div . '</div>';

                $tasks[$i]->prog->html = $progress_span . '&nbsp;' . $progressbar_outer_div;

                $row->prog = $tasks[$i]->prog;
                /*progress ends*/

                /*priority starts*/
                $tasks[$i]->prio = new stdClass();
                $tasks[$i]->prio->int = 0;


                $make_priority_low_anchor_span =
                    '<span class="label label-danger"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>';

                $make_priority_normal_anchor_span =
                    '<span class="label label-primary"><i class="fa fa-arrows-h" aria-hidden="true"></i></span>';

                $make_priority_high_anchor_span =
                    '<span class="label label-success"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></span>';


                $make_priority_low_anchor_tooltip = $this->lang->line('tooltip_make_priority_low_text');
                $make_priority_normal_anchor_tooltip = $this->lang->line('tooltip_make_priority_normal_text');
                $make_priority_high_anchor_tooltip = $this->lang->line('tooltip_make_priority_high_text');

                $make_priority_low_anchor_url = base_url() . 'task_module/' . $which_tasks . '/make_task_priority_low/' . $a_task->task_id;
                $make_priority_normal_anchor_url = base_url() . 'task_module/' . $which_tasks . '/make_task_priority_normal/' . $a_task->task_id;
                $make_priority_high_anchor_url = base_url() . 'task_module/' . $which_tasks . '/make_task_priority_high/' . $a_task->task_id;

                $make_priority_low_anchor =
                    '<a ' . 'title="' . $make_priority_low_anchor_tooltip . '"' . ' href="' . $make_priority_low_anchor_url . '"' . '>'
                    . $make_priority_low_anchor_span
                    . '</a>';
                $make_priority_normal_anchor =
                    '<a ' . 'title="' . $make_priority_normal_anchor_tooltip . '"' . ' href="' . $make_priority_normal_anchor_url . '"' . '>'
                    . $make_priority_normal_anchor_span
                    . '</a>';
                $make_priority_high_anchor =
                    '<a ' . 'title="' . $make_priority_high_anchor_tooltip . '"' . ' href="' . $make_priority_high_anchor_url . '"' . '>'
                    . $make_priority_high_anchor_span
                    . '</a>';


                $priority_anchors = '';
                $priority_main_span_class = '';
                $priority_main_span_text = '';

                if ($a_task->task_priority == 'low') {
                    $tasks[$i]->prio->int = 10;
                    $priority_main_span_class = 'label label-danger';
                    $priority_main_span_text = $this->lang->line('priority_low_text');
                    $priority_anchors = $make_priority_normal_anchor . '&nbsp;' . $make_priority_high_anchor;
                }
                if ($a_task->task_priority == 'normal') {
                    $tasks[$i]->prio->int = 20;
                    $priority_main_span_class = 'label label-primary';
                    $priority_main_span_text = $this->lang->line('priority_normal_text');
                    $priority_anchors = $make_priority_low_anchor . '&nbsp;' . $make_priority_high_anchor;
                }
                if ($a_task->task_priority == 'high') {
                    $tasks[$i]->prio->int = 30;
                    $priority_main_span_class = 'label label-success';
                    $priority_main_span_text = $this->lang->line('priority_high_text');
                    $priority_anchors = $make_priority_low_anchor . '&nbsp;' . $make_priority_normal_anchor;
                }

                $priority_main_span = '<span ' . 'class="' . $priority_main_span_class . '"' . '>' . $priority_main_span_text . '</span>';
                $tasks[$i]->prio->html = $priority_main_span . '<br><br>';

                if ($this->ion_auth->is_admin()) {
                    $tasks[$i]->prio->html .= '&nbsp;' . $priority_anchors;
                }
                $row->prio = $tasks[$i]->prio;
                /*priority ends*/

                /*active - inactive starts*/
                $tasks[$i]->sts = new stdClass();
                $tasks[$i]->sts->int = $a_task->task_status;

                if ($a_task->task_status == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'task_module/' . $which_tasks . '/deactivate_task/' . $a_task->task_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'task_module/' . $which_tasks . '/activate_task/' . $a_task->task_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">'
                        . $status_anchor_span
                        . '</a>';
                }

                $tasks[$i]->sts->html = '';
                if ($this->ion_auth->is_admin()) {
                    $tasks[$i]->sts->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                } else {
                    $tasks[$i]->sts->html = $status_span;
                }

                $row->sts = $tasks[$i]->sts;
                /*active - inactive ends*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'task_module/task_overview/' . $a_task->project_id . '/' . $a_task->task_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $upload_tooltip = $this->lang->line('tooltip_upload_text');
                $upload_url =
                    base_url() . 'file_manager_module/get_upload_file_form_with_project_with_task/'
                    . $a_task->project_id . '/' . $a_task->task_id;
                $upload_anchor =
                    '<a ' . ' title="' . $upload_tooltip . '" ' . ' href="' . $upload_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-upload fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'task_module/edit_task/' . $a_task->task_id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $see_files_tooltip = $this->lang->line('tooltip_see_files_text');
                $see_files_url =
                    base_url() . 'file_manager_module/files_with_project_with_task/'
                    . $a_task->project_id . '/' . $a_task->task_id;
                $see_files_anchor =
                    '<a ' . ' title="' . $see_files_tooltip . '" ' . ' href="' . $see_files_url . '" ' . ' class="" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-file-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $complete_tooltip = $this->lang->line('tooltip_complete_text');
                $complete_url = base_url() . 'task_module/' . $which_tasks . '/make_task_complete/' . $a_task->task_id;
                $complete_anchor =
                    '<a ' . ' title="' . $complete_tooltip . '" ' . ' href="' . $complete_url . '" ' . ' class="complete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'task_module/' . $which_tasks . '/delete_task/' . $a_task->task_id;
                $delete_anchor =
                    '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="delete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $tasks[$i]->action = '';
                if (
                    $is_admin == 'admin'
                    ||
                    (
                        $tasks[$i]->task_status == 1 &&
                        ($tasks_belongs_to == 'client' || $tasks_belongs_to == 'staff')
                        && $seen_by == 'me'
                    )
                ) {
                    $tasks[$i]->action .= $view_anchor . '&nbsp;&nbsp;';
                    $tasks[$i]->action .= $upload_anchor . '&nbsp;&nbsp;';
                    $tasks[$i]->action .= $see_files_anchor . '&nbsp;&nbsp;';
                }

                if ($is_admin == 'admin' ||
                    (
                        $tasks[$i]->task_status == 1 &&
                        $is_staff == 'staff' &&
                        $tasks_belongs_to == 'staff' &&
                        $seen_by == 'me')
                ) {
                    if ($tasks[$i]->task_progress != 100) {
                        $tasks[$i]->action .= $complete_anchor . '&nbsp;&nbsp;';
                    }
                    $tasks[$i]->action .= $edit_anchor . '&nbsp;&nbsp;';

                }

                if ($is_admin == 'admin') {
                    $tasks[$i]->action .= $delete_anchor;
                }


                $row->action = $tasks[$i]->action;
                /*action ends*/

                $table_data[] = $row;

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        $json_data['data'] = $table_data;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];

        $json_data['project_id'] = $project_id;
        $json_data['client_id'] = $client_id;
        $json_data['staff_id'] = $staff_id;
        $json_data['which_tasks'] = $which_tasks;
        $json_data['task_belongs_to'] = $tasks_belongs_to;
        $json_data['seen_by'] = $seen_by;*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));
    }

    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    //loading add form ...
    public function addTask()
    {
        if ($this->input->post()) {
            $this->repostAddTask();
        } else {
            $data['project_id'] = $this->uri->segment(3);

            $data['task_number'] = $this->get_generated_task_number();

            $data['which_form'] = 'add_form';
            $data['form_action'] = 'task_module/add_task/' . $data['project_id'];

            /*empty data for add form*/

            $data['task_info'] = new stdClass();

            $data['task_info']->task_title = '';
            $data['task_info']->task_description = '';
            $data['task_info']->task_start_time = '';
            $data['task_info']->task_end_time = '';

            $data['task_info']->task_status = 0;
            $data['task_info']->task_progress = 0;
            $data['task_info']->task_priority = 'normal';

            $data['assigned_staffs'] = null;
            $data['chosen_staffs'] = null; //set value while adding and validation error
            $data['val_errors'] = null;

            $this->showTaskForm($data);
        }


    }

    //load add form after post:when add form has val error(s) #edit form post doesn't go through this
    public function repostAddTask()
    {
        $data['project_id'] = $this->input->post('project_id');

        $data['task_number'] = $this->get_generated_task_number();

        $data['which_form'] = 'add_form';
        $data['form_action'] = 'task_module/add_task/' . $data['project_id'];

        $data['chosen_staffs'] = null; // assigned when validation is false
        $data['val_errors'] = null;

        $data['task_info'] = new stdClass();

        if ($this->validateForm() != 'validated') {

            //$this->validateForm() returns validation_errors or staff_required if not true
            $data['val_errors'] = $this->validateForm();


            if ($this->input->post('task_title')) {
                $data['task_info']->task_title = $this->input->post('task_title');
            } else {
                $data['task_info']->task_title = '';
            }

            if ($this->input->post('task_description')) {
                $data['task_info']->task_description = $this->input->post('task_description');
            } else {
                $data['task_info']->task_description = '';
            }

            if ($this->input->post('task_start_time')) {
                $data['task_info']->task_start_time = $this->input->post('task_start_time');
            } else {
                $data['task_info']->task_start_time = '';
            }

            if ($this->input->post('task_end_time')) {
                $data['task_info']->task_end_time = $this->input->post('task_end_time');
            } else {
                $data['task_info']->task_end_time = '';
            }

            if ($this->input->post('task_status')) {
                $data['task_info']->task_status = $this->input->post('task_status');
            } else {
                $data['task_info']->task_status = 0;
            }

            if ($this->input->post('task_progress')) {
                $data['task_info']->task_progress = $this->input->post('task_progress');
            } else {
                $data['task_info']->task_progress = 0;
            }

            if ($this->input->post('task_priority')) {
                $data['task_info']->task_priority = $this->input->post('task_priority');
            } else {
                $data['task_info']->task_priority = 'normal';
            }

            $data['chosen_staffs'] = $this->input->post('assigned_staffs');

            $data['assigned_staffs'] = null;

            $this->showTaskForm($data);

        } else {
            $this->createTask();
        }
    }

    /*
     * task_num is an unique identifying string to differentiate between tasks
     * Task Title can be same, so this is necessary
     * */
    public function get_generated_task_number()
    {
        $task_prefix = 'tsk_';

        $time_zone = $this->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('YmdHis');

        $task_number = $task_prefix . $datetime . '_r' . rand(10000, 99999);

        return $task_number;
    }

    //loading edit form ...
    public function editTask()
    {
        $task_id = $this->uri->segment(3);

        $data['which_form'] = 'edit_form';
        $data['form_action'] = 'task_module/update_task';

        $data['task_info'] = $this->Task_model->getTask($task_id);

        $data['task_info']->task_start_time =
            $this->custom_datetime_library
                ->convert_and_return_TimestampToDateAndTime($data['task_info']->task_start_time);

        $data['task_info']->task_end_time =
            $this->custom_datetime_library
                ->convert_and_return_TimestampToDateAndTime($data['task_info']->task_end_time);

        $data['chosen_staffs'] = null;  //set value while adding and validation error
        $data['project_id'] = $data['task_info']->project_id;
        $data['assigned_staffs'] = $this->Task_model->getAssignedStaffs($task_id);

        $data['val_errors'] = null; //as it is set from flashdata

        $this->showTaskForm($data);
    }

    //loading form structure ...
    public function showTaskForm($data)
    {
        $this->lang->load('task_form');

        $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($data['project_id']); //view
        $data['project_info'] = $this->Task_model->getProject($data['project_id']);

        $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($data['project_id']); //view

        $staff_group_id = 4;
        $client_group_id = 2;

        $data['active_staffs'] = $this->Task_model->getActiveStaffsOfProject($staff_group_id, $data['project_id']);

        $data['dateformat'] = $this->custom_datetime_library->get_dateformat_PHP_to_jQueryUI();
        $data['timeformat'] = $this->custom_datetime_library->get_timeformat_PHP_to_jQueryUI();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("task_module/task_form_page", $data);
        $this->load->view("common_module/footer");
    }

    public function createTask()
    {
        $this->lang->load('task_form');

        $project_id = $this->input->post('project_id');
        $client_id = $this->input->post('client_id');

        $data['project_id'] = $this->input->post('project_id');
        $data['task_number'] = $this->input->post('task_number');
        $data['task_title'] = $this->input->post('task_title');
        $data['task_description'] = $this->input->post('task_description');
        $data['task_priority'] = $this->input->post('task_priority');
        $data['task_progress'] = $this->input->post('task_progress');
        $data['task_status'] = $this->input->post('task_status');

        /*converting date-times*/
        $task_start_time = $this->input->post('task_start_time');
        $task_end_time = $this->input->post('task_end_time');

        $data['task_start_time'] =
            $this->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp($task_start_time);
        $data['task_end_time'] =
            $this->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp($task_end_time);

        $data['created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data['modified_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        $task_id = $this->Task_model->insertTask($data);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $client_id,                                                             //2.    $created_for
            'task',                                                                 //3.    $type
            $task_id,                                                               //4.    $type_id
            'created',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $project_id,                                                            //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $assigned_staffs = $this->input->post('assigned_staffs'); //array of id

        foreach ($assigned_staffs as $an_assigned_staff) {
            $this->Task_model->insertStaff($an_assigned_staff, $task_id, $project_id);

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $an_assigned_staff,                                                     //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'assigned_staff',                                                       //5.    $activity
                'admin',                                                                //6.    $activity_by
                'staff',                                                                //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $project_id,                                                            //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

        }

        $this->session->set_flashdata('task_add_success', $data['task_number']
            . ' '
            . $this->lang->line('task_add_success_text')
        );
        $this->session->set_flashdata('project_id', $project_id);
        $this->session->set_flashdata('task_id', $task_id);

        redirect('task_module/add_task/' . $project_id);
    }

    public function updateTask()
    {
        $this->lang->load('task_form');

        $project_id = $this->input->post('project_id');
        $task_id = $this->input->post('task_id');
        $client_id = $this->getClientId($task_id);

        if ($this->validateForm() != 'validated') {

            redirect('task_module/edit_task/' . $task_id);
        }

        if ($this->ion_auth->is_admin()) {
            $is_admin = 'admin';
        } else {
            $is_admin = 'not_admin';
        }

        $data['project_id'] = $this->input->post('project_id');
        $data['task_number'] = $this->input->post('task_number');
        $data['task_title'] = $this->input->post('task_title');
        $data['task_description'] = $this->input->post('task_description');
        $data['task_priority'] = $this->input->post('task_priority');
        $data['task_progress'] = $this->input->post('task_progress');
        $data['task_status'] = $this->input->post('task_status');

        /*converting date-times*/
        $task_start_time = $this->input->post('task_start_time');
        $task_end_time = $this->input->post('task_end_time');

        $data['task_start_time'] =
            $this->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp($task_start_time);
        $data['task_end_time'] =
            $this->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp($task_end_time);

        $data['modified_at'] = $this->custom_datetime_library->getCurrentTimestamp();


        $posted_assigned_staffs = $this->input->post('assigned_staffs'); //array of ids
        $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id); // mutiple rows

        $dbtbl_assigned_staffs = array();                       // will be an array of ids


        /*creating changelist for log starts*/
        $change_list = array();
        $change_list['change_type'] = 'task';

        $task_info_from_db = $this->Task_model->getTask($task_id);

        if ($task_info_from_db->task_title != $data['task_title']) {
            $change_list['task_title']['need_to_convert_as'] = '';
            $change_list['task_title']['field_name'] = $this->lang->line('log_field_task_title_text');
            $change_list['task_title']['from'] = $task_info_from_db->task_title;
            $change_list['task_title']['to'] = $data['task_title'];
        }

        if ($task_info_from_db->task_description != $data['task_description']) {
            $change_list['task_description']['need_to_convert_as'] = 'large_text';
            $change_list['task_description']['field_name'] = $this->lang->line('log_field_task_description_text');
            $change_list['task_description']['from'] = '';
            $change_list['task_description']['to'] = '';
        }

        if ($task_info_from_db->task_priority != $data['task_priority']) {
            $change_list['task_priority']['need_to_convert_as'] = 'priority';
            $change_list['task_priority']['field_name'] = $this->lang->line('log_field_task_priority_text');
            $change_list['task_priority']['from'] = $task_info_from_db->task_priority;
            $change_list['task_priority']['to'] = $data['task_priority'];
        }

        if ($task_info_from_db->task_progress != $data['task_progress']) {
            $change_list['task_progress']['need_to_convert_as'] = 'progress';
            $change_list['task_progress']['field_name'] = $this->lang->line('log_field_task_progress_text');
            $change_list['task_progress']['from'] = $task_info_from_db->task_progress;
            $change_list['task_progress']['to'] = $data['task_progress'];
        }

        if ($task_info_from_db->task_status != $data['task_status']) {
            $change_list['task_status']['need_to_convert_as'] = 'status';
            $change_list['task_status']['field_name'] = $this->lang->line('log_field_task_status_text');
            $change_list['task_status']['from'] = $task_info_from_db->task_status;
            $change_list['task_status']['to'] = $data['task_status'];
        }

        if ($task_info_from_db->task_start_time != $data['task_start_time']) {

            $change_list['task_start_time']['need_to_convert_as'] = 'datetime';
            $change_list['task_start_time']['field_name'] = $this->lang->line('log_field_task_start_time_text');
            $change_list['task_start_time']['from'] = $task_info_from_db->task_start_time;
            $change_list['task_start_time']['to'] = $data['task_start_time'];
        }

        if ($task_info_from_db->task_end_time != $data['task_start_time']) {

            $change_list['task_end_time']['need_to_convert_as'] = 'datetime';
            $change_list['task_end_time']['field_name'] = $this->lang->line('log_field_task_end_time_text');
            $change_list['task_end_time']['from'] = $task_info_from_db->task_end_time;
            $change_list['task_end_time']['to'] = $data['task_end_time'];
        }

        if ($change_list) {
            $encoded_change_list = json_encode($change_list);
        } else {
            $encoded_change_list = '';
        }
        /*creating changelist for log ends*/

        /*which kind  of user is updating the task*/
        if ($is_admin == 'admin') {
            $activity_by = 'admin';
        } else {
            $activity_by = 'not_admin';
        }


        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $client_id,                                                             //2.    $created_for
            'task',                                                                 //3.    $type
            $task_id,                                                               //4.    $type_id
            'updated',                                                              //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $project_id,                                                            //11.   $super_type_id
            '',                                                                     //12.   $other_information
            $encoded_change_list                                                    //13.   $change_list
        );
        /*creating log ends*/


        /*updating assigned staffs*/
        foreach ($assigned_staffs as $an_assigned_staff) {
            $dbtbl_assigned_staffs[] = $an_assigned_staff->staff_id;
        }

        $is_assigned = false;                                                                   //for log purposes
        if ($posted_assigned_staffs) {
            foreach ($posted_assigned_staffs as $a_posted_assigned_staff) {
                if (!in_array($a_posted_assigned_staff, $dbtbl_assigned_staffs)) {

                    $this->Task_model->insertStaff($a_posted_assigned_staff, $task_id, $project_id);
                    $is_assigned = true;                                                        //for log purposes

                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        $a_posted_assigned_staff,                                               //2.    $created_for
                        'task',                                                                 //3.    $type
                        $task_id,                                                               //4.    $type_id
                        'assigned_staff',                                                       //5.    $activity
                        $activity_by,                                                           //6.    $activity_by
                        'staff',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        'project',                                                              //10.   $super_type
                        $project_id,                                                            //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        $encoded_change_list                                                    //13.   $change_list
                    );
                    /*creating log ends*/
                }
            }
        }


        $is_deassigned = false;                                                                 //for log purposes
        if ($dbtbl_assigned_staffs) {
            foreach ($dbtbl_assigned_staffs as $a_dbtbl_assigned_staff) {
                if (!in_array($a_dbtbl_assigned_staff, $posted_assigned_staffs)) {
                    $this->Task_model->deleteStaff($a_dbtbl_assigned_staff);
                    $is_deassigned = true;                                                      //for log purposes

                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        $a_dbtbl_assigned_staff,                                                //2.    $created_for
                        'task',                                                                 //3.    $type
                        $task_id,                                                               //4.    $type_id
                        'deassigned_staff',                                                     //5.    $activity
                        $activity_by,                                                           //6.    $activity_by
                        'staff',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        'project',                                                              //10.   $super_type
                        $project_id,                                                            //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                    /*creating log ends*/
                }
            }
        }


        $iter_nas = 0;
        $newly_assigned_staffs = '';
        foreach ($posted_assigned_staffs as $a_posted_assigned_staff) {

            if ($iter_nas != 0) {
                $newly_assigned_staffs .= ',' . $a_posted_assigned_staff;
            } else {
                $newly_assigned_staffs .= $a_posted_assigned_staff;
            }
            $iter_nas++;
        }

        $iter_pas = 0;
        $previously_assigned_staffs = '';
        foreach ($dbtbl_assigned_staffs as $a_dbtbl_assigned_staff) {
            if ($iter_pas != 0) {
                $previously_assigned_staffs .= ',' . $a_dbtbl_assigned_staff;
            } else {
                $previously_assigned_staffs .= $a_dbtbl_assigned_staff;
            }
            $iter_pas++;
        }

        $staff_changed_list['staffs']['need_to_convert_as'] = 'user_list';
        $staff_changed_list['staffs']['field_name'] = $this->lang->line('log_field_staffs_text');
        $staff_changed_list['staffs']['from'] = $previously_assigned_staffs;
        $staff_changed_list['staffs']['to'] = $newly_assigned_staffs;

        $encoded_staff_changed_list = json_encode($staff_changed_list);

        if ($is_assigned || $is_deassigned) {

            /*creating log starts*/
            /*for client*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                           //1.    $created_by
                $client_id,                                                    //2.    $created_for
                'task',                                                        //3.    $type
                $task_id,                                                      //4.    $type_id
                'staffs_changed_announce',                                     //5.    $activity
                $activity_by,                                                  //6.    $activity_by
                'client',                                                      //7.    $activity_for
                '',                                                            //8.    $sub_type
                '',                                                            //9.    $sub_type_id
                'project',                                                     //10.   $super_type
                $project_id,                                                   //11.   $super_type_id
                '',                                                            //12.   $other_information
                $encoded_staff_changed_list                                    //13.   $change_list
            );
            /*creating log ends*/

            foreach ($posted_assigned_staffs as $a_posted_assigned_staff) {

                /*for staffs*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                           //1.    $created_by
                    $a_posted_assigned_staff,                                      //2.    $created_for
                    'task',                                                        //3.    $type
                    $task_id,                                                      //4.    $type_id
                    'staffs_changed_announce',                                     //5.    $activity
                    $activity_by,                                                  //6.    $activity_by
                    'staff',                                                       //7.    $activity_for
                    '',                                                            //8.    $sub_type
                    '',                                                            //9.    $sub_type_id
                    'project',                                                     //10.   $super_type
                    $project_id,                                                   //11.   $super_type_id
                    '',                                                            //12.   $other_information
                    $encoded_staff_changed_list                                    //13.   $change_list
                );
                /*creating log ends*/
            }

        }


        $this->Task_model->updateTask($task_id, $data);
        $this->session->set_flashdata('task_update_success', $data['task_number']
            . ' '
            . $this->lang->line('task_update_success_text')
        );
        $this->session->set_flashdata('project_id', $project_id);
        $this->session->set_flashdata('task_id', $task_id);

        redirect('task_module/edit_task/' . $task_id);
    }

    public function validateForm()
    {
        $this->lang->load('task_form');

        $this->input->post('task_title');
        $this->input->post('task_start_time');
        $this->input->post('task_end_time');

        $this->form_validation->set_rules('task_title', 'task_title', 'required', array(
                'required' => $this->lang->line('task_title_required_text')
            )
        );

        $this->form_validation->set_rules('task_start_time', 'task_start_time', 'required', array(
                'required' => $this->lang->line('task_start_time_required_text')
            )
        );

        $this->form_validation->set_rules('task_end_time', 'task_end_time', 'required', array(
                'required' => $this->lang->line('task_end_time_required_text')
            )
        );

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('validation_errors', validation_errors());

            $validation_errors = validation_errors();

            return $validation_errors;
        }

        $assigned_staffs = $this->input->post('assigned_staffs');

        if (empty($assigned_staffs) || !isset($assigned_staffs)) {
            $this->session->set_flashdata('staff_required', $this->lang->line('staff_required_text'));
            $staff_required = $this->lang->line('staff_required_text');
            return $staff_required;
        }

        return 'validated';
    }

    /*-------------------Actions--------------------------------------------------------------------------------------*/

    public function makeTaskComplete()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();

        $is_completed = $this->Task_model->makeTaskComplete($task_id, $modified_at);

        if ($is_completed == true) {

            $task_info = $this->Task_model->getTask($task_id);
            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));

            $this->session->set_flashdata('task_completed', $task_info->task_number
                . ' '
                . $this->lang->line('task_completed_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'completed',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'completed',                                                       //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        ''                                                                 //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function deleteTask()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();

        $is_deactivated = $this->Task_model->deactivateTask($task_id, $modified_at);
        $is_deleted = $this->Task_model->deleteTask($task_id, $modified_at);

        if ($is_deactivated && $is_deleted) {

            $task_info = $this->Task_model->getTask($task_id);
            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));

            $this->session->set_flashdata('task_deleted', $task_info->task_number
                . ' '
                . $this->lang->line('task_deleted_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'deleted',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'deleted',                                                         //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        ''                                                                 //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function activateTask()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();
        $is_activated = $this->Task_model->activateTask($task_id, $modified_at);

        if ($is_activated == true) {

            $task_info = $this->Task_model->getTask($task_id);

            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));
            $this->session->set_flashdata('project_id', $task_info->project_id);
            $this->session->set_flashdata('task_id', $task_info->task_id);

            $this->session->set_flashdata('task_activated', $task_info->task_number
                . ' '
                . $this->lang->line('task_activated_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'activated',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'activated',                                                       //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        ''                                                                 //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function deactivateTask()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();
        $is_deactivated = $this->Task_model->deactivateTask($task_id, $modified_at);

        if ($is_deactivated == true) {

            $task_info = $this->Task_model->getTask($task_id);

            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));
            $this->session->set_flashdata('project_id', $task_info->project_id);
            $this->session->set_flashdata('task_id', $task_info->task_id);

            $this->session->set_flashdata('task_deactivated', $task_info->task_number
                . ' '
                . $this->lang->line('task_deactivated_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'deactivated',                                                          //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'deactivated',                                                     //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        ''                                                                 //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function makeTaskPriorityHigh()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $task_info = $this->Task_model->getTask($task_id);
        $priority_from = $task_info->task_priority;

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();
        $is_made_high = $this->Task_model->makeTaskPriorityHigh($task_id, $modified_at);

        if ($is_made_high == true) {

            $task_info = $this->Task_model->getTask($task_id);
            $priority_to = $task_info->task_priority;

            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));
            $this->session->set_flashdata('project_id', $task_info->project_id);
            $this->session->set_flashdata('task_id', $task_info->task_id);

            $this->session->set_flashdata('task_prirority_made_high', $task_info->task_number
                . ' '
                . $this->lang->line('task_prirority_made_high_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $change_list['task_prirority']['need_to_convert_as'] = 'priority';
            $change_list['task_prirority']['field_name'] = $this->lang->line('log_field_task_priority_text');
            $change_list['task_prirority']['from'] = $priority_from;
            $change_list['task_prirority']['to'] = $priority_to;

            if ($change_list) {
                $encoded_change_list = json_encode($change_list);
            } else {
                $encoded_change_list = '';
            }

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'priority_changed',                                                     //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                $encoded_change_list                                                    //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'priority_changed',                                                //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        $encoded_change_list                                               //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function makeTaskPriorityNormal()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $task_info = $this->Task_model->getTask($task_id);
        $priority_from = $task_info->task_priority;

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();
        $is_made_normal = $this->Task_model->makeTaskPriorityNormal($task_id, $modified_at);

        if ($is_made_normal == true) {

            $task_info = $this->Task_model->getTask($task_id);
            $priority_to = $task_info->task_priority;

            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));
            $this->session->set_flashdata('project_id', $task_info->project_id);
            $this->session->set_flashdata('task_id', $task_info->task_id);

            $this->session->set_flashdata('task_prirority_made_normal', $task_info->task_number
                . ' '
                . $this->lang->line('task_prirority_made_normal_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $change_list['task_prirority']['need_to_convert_as'] = 'priority';
            $change_list['task_prirority']['field_name'] = $this->lang->line('log_field_task_priority_text');
            $change_list['task_prirority']['from'] = $priority_from;
            $change_list['task_prirority']['to'] = $priority_to;

            if ($change_list) {
                $encoded_change_list = json_encode($change_list);
            } else {
                $encoded_change_list = '';
            }

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'priority_changed',                                                     //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                $encoded_change_list                                                    //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'priority_changed',                                                //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        $encoded_change_list                                               //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function makeTaskPriorityLow()
    {
        $this->lang->load('task_list');

        $task_id = $this->uri->segment(4);
        $which_tasks = $this->uri->segment(2);

        $task_info = $this->Task_model->getTask($task_id);
        $priority_from = $task_info->task_priority;

        $modified_at = $this->custom_datetime_library->getCurrentTimestamp();
        $is_made_low = $this->Task_model->makeTaskPriorityLow($task_id, $modified_at);

        if ($is_made_low == true) {

            $task_info = $this->Task_model->getTask($task_id);
            $priority_to = $task_info->task_priority;

            $this->session->set_flashdata('success', $this->lang->line('panel_head_success_text'));
            $this->session->set_flashdata('project_id', $task_info->project_id);
            $this->session->set_flashdata('task_id', $task_info->task_id);

            $this->session->set_flashdata('task_prirority_made_low', $task_info->task_number
                . ' '
                . $this->lang->line('task_prirority_made_low_text')
            );

            /*creating log starts*/
            $client_id = $this->getClientId($task_id);

            $change_list['task_prirority']['need_to_convert_as'] = 'priority';
            $change_list['task_prirority']['field_name'] = $this->lang->line('log_field_task_priority_text');
            $change_list['task_prirority']['from'] = $priority_from;
            $change_list['task_prirority']['to'] = $priority_to;

            if ($change_list) {
                $encoded_change_list = json_encode($change_list);
            } else {
                $encoded_change_list = '';
            }

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $client_id,                                                             //2.    $created_for
                'task',                                                                 //3.    $type
                $task_id,                                                               //4.    $type_id
                'priority_changed',                                                     //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $task_info->project_id,                                                 //11.   $super_type_id
                '',                                                                     //12.   $other_information
                $encoded_change_list                                                    //13.   $change_list
            );

            $assigned_staffs = $this->Task_model->getAssignedStaffs($task_id);

            if ($assigned_staffs) {

                foreach ($assigned_staffs as $an_assingned_staff) {

                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                               //1.    $created_by
                        $an_assingned_staff->staff_id,                                     //2.    $created_for
                        'task',                                                            //3.    $type
                        $task_id,                                                          //4.    $type_id
                        'priority_changed',                                                //5.    $activity
                        'admin',                                                           //6.    $activity_by
                        'staff',                                                           //7.    $activity_for
                        '',                                                                //8.    $sub_type
                        '',                                                                //9.    $sub_type_id
                        'project',                                                         //10.   $super_type
                        $task_info->project_id,                                            //11.   $super_type_id
                        '',                                                                //12.   $other_information
                        $encoded_change_list                                               //13.   $change_list
                    );

                }
            }
            /*creating log ends*/

            if ($which_tasks == 'all_tasks' || $which_tasks == 'my_tasks') {
                redirect('task_module/show_tasks/' . $which_tasks . '/' . $task_info->project_id);
            }

            if ($which_tasks == 'all_tasks_from_all_projects' || $which_tasks == 'my_tasks_from_my_projects') {
                redirect('task_module/show_' . $which_tasks);
            }
        }
    }

    public function getClientId($task_id)
    {
        $a_task_info = $this->Task_model->getTask($task_id);

        $a_project_info = $this->Task_model->getProject($a_task_info->project_id);

        return $a_project_info->client_id;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function checkPermission_to_enter_TaskOverview($task_id, $user_id)
    {
        if (!$this->ion_auth->is_admin()) {
            $is_related_client = $this->Task_model->checkIf_RelatedClient($task_id, $user_id);
            $is_assigned_staff = $this->Task_model->checkIf_AssignedStaff($task_id, $user_id);

            if ($is_related_client || $is_assigned_staff) {
                return true;
            } else {
                return false;
            }

        } else {
            return true;
        }
    }

    public function showTaskOverview()
    {
        $this->lang->load('task_overview');

        $project_id = $this->uri->segment(3);
        $task_id = $this->uri->segment(4);

        if ($this->checkPermission_to_enter_TaskOverview($task_id, $this->session->userdata('user_id')) == false) {
            redirect('users/auth/need_permission');
        }

        $data['project_id'] = $project_id;
        $data['task_id'] = $task_id;

        $data['project_info'] = $this->Task_model->getProject($project_id);

        if (!$this->ion_auth->is_admin()) {
            $data['is_admin'] = 'not_admin';
        } else {
            $data['is_admin'] = 'admin';
        }

        $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view

        $data['a_task'] = $this->Task_model->getTask($task_id);


        if ($data['a_task']->task_status == 0
            && $data['a_task']->task_deletion_status != 1
            && $data['is_admin'] == 'not_admin'
        ) {
            redirect('users/auth/need_permission');
        }

        if ($data['a_task']->task_deletion_status == 1) {
            redirect('users/auth/does_not_exist');
        }


        /*converting timestamp to user's given format*/
        $data['a_task']->task_start_time =
            $this->custom_datetime_library->convert_and_return_TimestampToDate($data['a_task']->task_start_time);
        $data['a_task']->task_end_time =
            $this->custom_datetime_library->convert_and_return_TimestampToDate($data['a_task']->task_end_time);
        $data['a_task']->created_at =
            $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($data['a_task']->created_at);
        $data['a_task']->modified_at =
            $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($data['a_task']->modified_at);

        $data['assigned_staffs_to_project'] = $this->Task_model->getAssignedStaffs_toProject($project_id);
        $data['assigned_staffs_to_task'] = $this->Task_model->getAssignedStaffs_toTask($task_id);

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("task_module/task_overview_page", $data);
        $this->load->view("common_module/footer");
    }


}

