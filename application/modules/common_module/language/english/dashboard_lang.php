<?php

$lang['page_title_text'] = 'Dashboard';

$lang['page_subtitle_admin_dashboard_text'] = 'Admin\'s Dashboard';
$lang['page_subtitle_client_dashboard_text'] = 'Client\'s Dashboard';
$lang['page_subtitle_staff_dashboard_text'] = 'Staff\'s Dashboard';


$lang['breadcrum_home_text'] = 'Dashboard';
$lang['breadcrum_page_text'] = 'Dashboard View';

$lang['go_text'] = 'Go';

$lang['admin_section_text'] = 'Admin\'s Section';
$lang['my_section_text'] = 'My Section';

/*settings*/
$lang['settings_text'] = 'Settings';
$lang['general_settings_text'] = 'General Settings';
$lang['contact_settings_text'] = 'Contact Settings';
$lang['image_settings_text'] = 'Image Settings';
$lang['file_settings_text'] = 'File Settings';
$lang['currency_settings_text'] = 'Currency Settings';
$lang['datetime_settings_text'] = 'Date & Time Settings';
$lang['email_settings_text'] = 'Email Settings';


/*user*/
$lang['number_of_active_users_text'] = 'No. of Active Users';
$lang['number_of_active_admins_text'] = 'No. of Active Admins';
$lang['number_of_active_clients_text'] = 'No. of Active Clients';
$lang['number_of_active_staffs_text'] = 'No. of Active Staffs';
$lang['see_all_users_text'] = 'See All Users';


/*project*/
$lang['all_running_projects_text'] = 'All Running Projects';
$lang['my_running_projects_text'] = 'My Running Projects';

$lang['see_all_projects_text'] = 'See All Projects';
$lang['see_my_projects_text'] = 'See My Projects';

/*task*/
$lang['all_running_tasks_text'] = 'All Running Tasks';
$lang['my_running_tasks_text'] = 'My Running Tasks';
$lang['see_all_tasks_text'] = 'See All Tasks';
$lang['see_my_tasks_text'] = 'See My Tasks';

/*file*/
$lang['files_text'] = 'File(s)';
$lang['all_files_text'] = 'All Files';
$lang['my_files_text'] = 'My Files';

$lang['see_all_files_text'] = 'See All Files';
$lang['see_my_files_text'] = 'See My Files';

/*ticket*/
$lang['all_unsolved_tickets_text'] = 'All Unsolved Tickets';
$lang['my_unsolved_tickets_text'] = 'My Unsolved Tickets';

$lang['see_all_tickets_text'] = 'See All Tickets';
$lang['see_my_tickets_text'] = 'See My Tickets';

/**/
$lang['all_unclear_invoice_text'] = 'All Unclear Invoice';
$lang['my_unclear_invoice_text'] = 'My Unclear Invoice';

$lang['see_all_invoice_text'] = 'See All Invoices';
$lang['see_my_invoice_text'] = 'See My Invoices';



?>