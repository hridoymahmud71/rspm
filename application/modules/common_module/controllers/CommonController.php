<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        // $this->load->library('form_validation');
        // $this->form_validation->CI =& $this;


        $this->load->model('Common_model');

        // application/settings_module/library/custom_settings_library*
        //$this->load->library('settings_module/custom_settings_library');

        // applicationlibrary/custom_image_library*
        $this->load->library('custom_image_library');

        //$user_data = $this->ion_auth->user()->row_array();

        $user_data = $this->getDetailedUserData_asArray();
        $this->session->set_userdata($user_data);

        $image_directory = $this->getImageDirectory();
        $this->session->set_userdata('image_directory', $image_directory);

    }

    public function index()
    {
        $this->lang->load('dashboard');

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        /*user*/
        $data['active_user_count'] = $this->Common_model->countActiveUsers();
        $data['active_admin_count'] = $this->Common_model->countActiveAdmins($admin_group_id = 1);
        $data['active_client_count'] = $this->Common_model->countActiveClients($client_group_id = 3);
        $data['active_staff_count'] = $this->Common_model->countActiveStaffs($staff_group_id = 4);

        /*counting  . . .*/
        $data['all_running_project_count'] = $this->Common_model->countAllRunningProjects();
        $data['all_running_task_count'] = $this->Common_model->countAllRunningTasks();
        $data['all_file_count'] = $this->Common_model->countAllFiles();
        $data['all_unsolved_ticket_count'] = $this->Common_model->countAllUnsolvedTickets();
        $data['all_unclear_invoice_count'] = $this->Common_model->countAllUnclearInvoices();

        if ($data['is_client'] == 'client') {
            $data['my_running_project_count'] =
                $this->Common_model->countClientRunningProjects($this->session->userdata('user_id'));

            $data['my_running_task_count'] =
                $this->Common_model->countClientRunningTasks($this->session->userdata('user_id'));

            $data['my_unsolved_ticket_count'] =
                $this->Common_model->countClientUnsolvedTickets($this->session->userdata('user_id')); //$ticket_created_by

            //only for client , not for staff
            $data['my_unclear_invoice_count'] =
                $this->Common_model->countMyUnclearInvoices($this->session->userdata('user_id'));
        }

        if ($data['is_staff'] == 'staff') {
            $data['my_running_project_count'] =
                $this->Common_model->countStaffRunningProjects($this->session->userdata('user_id'));

            $data['my_running_task_count'] =
                $this->Common_model->countStaffRunningTasks($this->session->userdata('user_id'));

            $data['my_unsolved_ticket_count'] =
                $this->Common_model->countStaffUnsolvedTickets($this->session->userdata('user_id'));
        }

        $this->load->view("header");
        $this->load->view("common_left");
        $this->load->view("dashboard_page", $data);
        $this->load->view("footer");
    }


    /*----------------------------------------------------------------------------------------------------------------*/
    /*
     * using to get profile image
     * gets called by ajax
     *
     * */
    public function getDetailedUserData()
    {
        $user_data = $this->Common_model->getDetailedUserData($this->session->userdata('user_id'));

        return $user_data;
    }

    public function getDetailedUserData_asArray()
    {
        $user_data = $this->Common_model->getDetailedUserData_asArray($this->session->userdata('user_id'));

        return $user_data;
    }

    public function getDetailedUserData_withAjax()
    {
        $user_data = $this->getDetailedUserData();

        $encoded_user_data = json_encode($user_data);
        print_r($encoded_user_data);
    }

    public function getImageDirectory()
    {
        $image_directory = $this->custom_image_library->getMainImageDirectory();
        return $image_directory;
    }

    public function get_UserProfileImage_and_Directory_withAjax()
    {
        $user_data = $this->getDetailedUserData();
        $data['user_profile_image'] = $user_data->user_profile_image;
        $data['image_directory'] = $this->getImageDirectory();

        $encoded_data = json_encode($data);
        print_r($encoded_data);
    }

    public function checkIfUserIsActive_withAjax()
    {
        $user_data = $this->getDetailedUserData();

        print_r($user_data);

        if ($user_data->active == 0) {

            $this->ion_auth->logout();
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getMyRunningTasks_withAjax()
    {
        $running_tasks = array();
        $running_tasks = false;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($data['is_staff'] == 'staff') {
            $running_tasks = $this->Common_model->getStaffRunningTasks($this->session->userdata('user_id'));
        }

        if ($data['is_client'] == 'client') {
            $running_tasks = $this->Common_model->getClientRunningTasks($this->session->userdata('user_id'));
        }


        if ($running_tasks) {
            print_r(json_encode($running_tasks));

        } else {
            echo json_encode('no_task');
        }

    }


    public function countMyRunningTasks_withAjax()
    {

        $count_running_tasks = 0;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }


        if ($data['is_staff'] == 'staff') {
            $count_running_tasks = $this->Common_model->countStaffRunningTasks($this->session->userdata('user_id'));
        }

        if ($data['is_client'] == 'client') {
            $count_running_tasks = $this->Common_model->countClientRunningTasks($this->session->userdata('user_id'));
        }

        echo $count_running_tasks;


    }

}
