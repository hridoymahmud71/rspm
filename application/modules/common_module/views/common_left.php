<?php

$this->lang->load('common_left');

?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!--profile image starts-->
                <?php
                if (
                    ($this->session->userdata('image_directory') != '')
                    &&
                    ($this->session->userdata('user_profile_image') != '')
                ) {
                    ?>
                    <img src="<?php echo base_url()
                        . $this->session->userdata('image_directory')
                        . '/'
                        . $this->session->userdata('user_profile_image');
                    ?>"
                         class="img-circle user_profile_image" alt="User Image">

                <?php } else { ?>
                    <img src="<?php echo base_url() ?>project_base_assets/base_demo_images/user_profile_image_demo.png"
                         class="img-circle user_profile_image" alt="User Image">
                <?php } ?>
                <!--profile image ends-->
            </div>
            <div class="pull-left info">
                <p id="user_first_name"><?php if ($this->session->userdata('first_name')) {
                        echo $this->session->userdata('first_name');
                    } else {
                        echo 'No Name';
                    } ?>
                </p>
            </div>
        </div>
        <!-- search form -->

        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo lang('cl_menu_main_navigation_text') ?></li>
            <li>
                <a href="<?php echo base_url() ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo lang('cl_menu_dashboard_text') ?></span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span><?php echo lang('cl_menu_settings_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/general_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_general_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/contact_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_contact_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/image_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_image_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/file_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_file_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/currency_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_currency_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/datetime_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_datetime_settings_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'settings_module/email_settings' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_settings_sub_menu_email_settings_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span><?php echo lang('cl_menu_users_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() . 'users/auth' ?>"><i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_users_sub_menu_users_text') ?>
                        </a>
                    </li>
                    <li><a href="<?php echo base_url() . 'users/auth/show_user_groups' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_users_sub_menu_groups_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gg"></i>
                    <span><?php echo lang('cl_menu_project_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'project_module/all_projects' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_project_sub_menu_all_project_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'project_module/my_projects' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_project_sub_menu_my_project_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'project_module/add_project' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_project_sub_menu_add_project_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tasks"></i>
                    <span><?php echo lang('cl_menu_task_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'task_module/show_all_tasks_from_all_projects' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_task_sub_menu_all_tasks_from_all_projects_text') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() . 'task_module/show_my_tasks_from_my_projects' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_task_sub_menu_all_my_task_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder-open"></i>
                    <span><?php echo lang('cl_menu_file_manager_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() . 'file_manager_module/all_folders' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_file_manager_sub_menu_all_files_text') ?>
                        </a>
                    </li>

                    <li><a href="<?php echo base_url() . 'file_manager_module/my_folders' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_file_manager_sub_menu_my_files_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-ticket"></i>
                    <span><?php echo lang('cl_menu_ticket_or_support_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'support_module/show_ticket_list/all' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_ticket_or_support_sub_menu_all_tickets_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'support_module/show_ticket_list/my' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_ticket_or_support_sub_menu_my_tickets_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'support_module/add_ticket' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_ticket_or_support_sub_menu_create_ticket_text') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span><?php echo lang('cl_menu_invoice_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_invoice_sub_menu_all_invoices_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_invoice_sub_menu_my_invoices_text') ?>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() . 'invoice_module/add_invoice' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_invoice_sub_menu_create_invoice_text') ?>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-address-book"></i>
                    <span><?php echo lang('cl_menu_contact_text') ?></span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url() . 'contact_module/show_contact_info/' ?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo lang('cl_menu_contact_sub_menu_contact_page_text') ?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

