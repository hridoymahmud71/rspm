<!DOCTYPE html>
<html>
<head>
    <?php $this->lang->load('header') ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RS PM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <!--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">-->

    <!-- jQuery -->
    <script src="<?php echo base_url() ?>project_base_assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() ?>project_base_assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>project_base_assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>project_base_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/dist/css/skins/_all-skins.min.css">

    <!-- DataTables -->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>project_base_assets/plugins/datatables/dataTables.bootstrap.css">
    <script src="<?php echo base_url() ?>project_base_assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>project_base_assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!--bootstap datepicker-->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/plugins/datepicker/datepicker3.css">
    <script src="<?php echo base_url() ?>project_base_assets/plugins/datepicker/bootstrap-datepicker.js"></script>

    <!--select2-->
    <script src="<?php echo base_url() ?>project_base_assets/plugins/select2/select2.full.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/plugins/select2/select2.css">

    <!--sweet alert-->
    <script src="<?php echo base_url() ?>project_base_assets/sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>project_base_assets/sweetalert-master/dist/sweetalert.css">

    <!--dropzone-->
    <script src="<?php echo base_url() ?>project_base_assets/dropzone-4.3.0/dist/dropzone.js"></script>

    <!--moment js /must load before bootstrap datetimepicker-->
    <!--<script src="<?php /*echo base_url()*/ ?>project_base_assets/moment-js/Moment.js"></script>-->

    <!--bootstrap datepicker-->
    <!--
    <link rel="stylesheet" href="<?php /*echo base_url()*/ ?>project_base_assets/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">
    <script src="<?php /*echo base_url()*/ ?>project_base_assets/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>
    -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/dropzone-4.3.0/dist/dropzone.css">
    <!--jquery ui-->
    <script src="<?php echo base_url() ?>project_base_assets/plugins/jQueryUI/jquery-ui.js"
            type="text/javascript">
    </script>
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/plugins/jQueryUI/jquery-ui.css">
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->


    <!--jquery timepicker-->
    <script src="<?php echo base_url() ?>project_base_assets/jQuery-Timepicker-Addon-master/dist/jquery-ui-timepicker-addon.min.js"
            type="text/javascript"></script>
    <link rel="stylesheet"
          href="<?php echo base_url() ?>project_base_assets/jQuery-Timepicker-Addon-master/dist/jquery-ui-timepicker-addon.min.css">

    <!--ck editor-->
    <script src="<?php echo base_url() ?>project_base_assets/ckeditor/ckeditor.js" type="text/javascript"></script>

    <!--tiny mce-->
    <script src="<?php echo base_url() ?>project_base_assets/tinymce/js/tinymce/tinymce.js"
            type="text/javascript"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url() ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>RS</b>PM</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg" style="font-size: smaller"><b>RS </b>Project Management</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tasks"></i>
                            <span id="running_task_count" class="label label-danger running_task_count">?</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header" id="have_running_tasks"
                                style="display:none;font-size: larger;color: #2b2b2b"><?php echo lang('task_notification_running_tasks_text') ?>
                            <li class="header" id="dont_have_running_tasks"
                                style="display:none;font-size: larger;color: #2b2b2b">
                                <?php echo lang('task_notification_no_running_tasks_text') ?>
                            </li>

                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu" id="running_tasks">
                                    <!-- List of Items will be append by the jquery ajax func -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="<?php echo base_url() . 'task_module/show_my_tasks_from_my_projects' ?>">
                                    <?php echo lang('task_notification_see_my_tasks_text') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <!--profile image starts-->
                            <?php
                            if (
                                ($this->session->userdata('image_directory') != '')
                                &&
                                ($this->session->userdata('user_profile_image') != '')
                            ) {
                                ?>
                                <img src="<?php echo base_url()
                                    . $this->session->userdata('image_directory')
                                    . '/'
                                    . $this->session->userdata('user_profile_image');
                                ?>"
                                     class="user-image user_profile_image" alt="User Image">

                            <?php } else { ?>
                                <img src="<?php echo base_url() ?>project_base_assets/base_demo_images/user_profile_image_demo.png"
                                     class="user-image user_profile_image" alt="User Image">
                            <?php } ?>
                            <!--profile image ends-->

                            <span id="identity"
                                  class="hidden-xs"><?php echo $this->session->userdata('identity') ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <!--profile image starts-->
                                <?php
                                if (
                                    ($this->session->userdata('image_directory') != '')
                                    &&
                                    ($this->session->userdata('user_profile_image') != '')
                                ) {
                                    ?>
                                    <img src="<?php echo base_url()
                                        . $this->session->userdata('image_directory')
                                        . '/'
                                        . $this->session->userdata('user_profile_image');
                                    ?>"
                                         class="img-circle user_profile_image" alt="User Image">

                                <?php } else { ?>
                                    <img src="<?php echo base_url() ?>project_base_assets/base_demo_images/user_profile_image_demo.png"
                                         class="img-circle user_profile_image" alt="User Image">
                                <?php } ?>
                                <!--profile image ends-->

                                <p id="user_fullname">
                                    <?php echo $this->session->userdata('first_name')
                                        . ' '
                                        . $this->session->userdata('last_name') ?>
                                </p>
                                <p>
                                    <small id="user_position">
                                        <?php
                                        if ($this->session->userdata('user_position') != '') {
                                            echo $this->session->userdata('user_position');
                                        } else {
                                            echo 'No Position';
                                        }

                                        ?>
                                    </small>
                                </p>
                                <p id="user_company">
                                    <?php
                                    if ($this->session->userdata('company') != '') {
                                        echo $this->session->userdata('company');
                                    } else {
                                        echo 'No Company';
                                    }

                                    ?>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a style="width: 100%"
                                           href="<?php echo base_url() . 'user_profile_module/user_profile_overview/'
                                               . $this->session->userdata('user_id') ?>"
                                           class="btn btn-default btn-block">
                                            <?php echo lang('profile_text') ?>
                                        </a>
                                    </div>

                                    <div class="col-xs-6 text-center">
                                        <a style="width: 100%"
                                           href="<?php echo base_url() . 'user_profile_module/user_timeline/'
                                               . $this->session->userdata('user_id') ?>"
                                           class="btn btn-default btn-block">
                                            <?php echo lang('timeline_text') ?>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?php echo base_url() . 'users/auth/logout' ?>"
                                       class="btn btn-danger btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>

        </nav>
    </header>


    <style>
        .navbar-nav > .user-menu > .dropdown-menu > li.user-header > img {
            margin: 2px !important;
        }

        .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
            height: auto !important;
        }

        .navbar-nav > .user-menu > .dropdown-menu > li.user-header > p {
            margin: 1px !important;
        }

    </style>

    <!--user data load-->
    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/get_detailed_userdata_with_ajax'?>',
                type: 'GET',
                success: function (userdata_result) {
                    var userdata = JSON.parse(userdata_result);

                    $('#identity').html(userdata.identity)
                    $('#user_first_name').html(userdata.first_name);
                    $('#user_fullname').html(userdata.first_name + ' ' + userdata.last_name);
                    $('#user_position').html(userdata.user_position);
                    $('#user_company').html(userdata.company);
                }
            });

        });
    </script>

    <!--profile_image_load-->
    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/get_user_profile_image_and_directory_with_ajax'?>',
                type: 'GET',
                success: function (img_result) {
                    var img_data = JSON.parse(img_result);

                    var user_profile_image = img_data.user_profile_image;
                    var image_directory = img_data.image_directory;

                    if (user_profile_image != '') {
                        $('.user_profile_image').attr('src', '<?php echo base_url()?>' + image_directory + '/' + user_profile_image);
                    }

                }
            })

        })
    </script>

    <!--check if user is active-->
    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/check_if_user_is_active_with_ajax'?>',
                type: 'GET',
                success: function (res) {
                    // nothing to write
                }
            });

        });
    </script>


    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/get_my_running_tasks_with_ajax'?>',
                type: 'GET',
                dataType: "json",
                success: function (tasks) {

                    if (tasks != 'no_task') {


                        var i = 0;

                        for (var task in tasks) {


                            var task_progress = "<small class='pull-right'>" + tasks[i].task_progress + '%' + '</small>';
                            var task_title = '<h3>' + tasks[i].task_title + task_progress + '</h3>';
                            var task_url = "<?php echo base_url() . 'task_module/task_overview/'?>" + tasks[i].project_id + '/' + tasks[i].task_id;

                            var progress_bar_style = "style='width:" + tasks[i].task_progress + "%' ";
                            var progress_bar_ariavaluenow = "aria-valuenow='" + tasks[i].task_progress + "' ";
                            var progress_bar_span = "<span class='sr-only'>" + tasks[i].task_progress + "% Complete</span>";

                            var progress_bar_color = 'progress-bar-blue';

                            if (tasks[i].task_progress < 33) {
                                progress_bar_color = 'progress-bar-red';
                            } else if (tasks[i].task_progress > 66) {
                                progress_bar_color = 'progress-bar-green';
                            } else {
                                progress_bar_color = 'progress-bar-blue';
                            }

                            var progress_bar = "<div class='progress xs'><div class='progress-bar " + progress_bar_color + " ' " + progress_bar_style + "role='progressbar' " + progress_bar_ariavaluenow + "aria-valuemin='0' aria-valuemax='100'>" + progress_bar_span + "</div></div>";


                            var anchor = "<a href=' " + task_url + " ' >" + task_title + progress_bar + "</a>";


                            var list_item = '<li>' + anchor + '</li>';

                            $('#running_tasks').append(list_item);

                            i++;


                        }

                    }


                }
            });

        });
    </script>

    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/count_my_running_tasks_with_ajax'?>',
                type: 'GET',

                success: function (count) {
                    var count = parseInt(count);

                    if (count == 0) {
                        $('#running_task_count').removeClass('label-danger');
                        $('#running_task_count').addClass('label-success');

                        $('#dont_have_running_tasks').css('display:block');
                        $('#have_running_tasks').css('display:none');
                    }

                    if (count > 0) {
                        $('#have_running_tasks').css('display', 'block');
                        $('#dont_have_running_tasks').css('display', 'none');
                    }

                    if (count >= 1 && count <= 5) {
                        $('#running_task_count').removeClass('label-danger');
                        $('#running_task_count').addClass('label-warning');
                    }
                    $('#running_task_count').html(count);


                }
            });

        });
    </script>