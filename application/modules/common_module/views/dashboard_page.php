<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small>
                <?php if ($is_admin == 'admin') echo lang('page_subtitle_admin_dashboard_text') ?>
                <?php if ($is_client == 'client') echo lang('page_subtitle_client_dashboard_text') ?>
                <?php if ($is_staff == 'staff' && $is_admin == 'not_admin') echo lang('page_subtitle_staff_dashboard_text') ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrum_home_text') ?>
                </a>
            </li>
            <li class="active">
                <?php echo lang('breadcrum_page_text') ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <?php if ($is_admin == 'admin') { ?>

            <hr style="border: 3px double #8c8b8b">
            <h4><?php echo lang('admin_section_text') ?></h4>
            <div class="row ">
                <div class="col-lg-6 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-grey">
                        <div class="inner">
                            <h3><?php echo lang('settings_text') ?></h3>

                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/general_settings' ?>"><?php echo lang('general_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/contact_settings' ?>"><?php echo lang('contact_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/image_settings' ?>"><?php echo lang('image_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/file_settings' ?>"><?php echo lang('file_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/currency_settings' ?>"><?php echo lang('currency_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/datetime_settings' ?>"><?php echo lang('datetime_settings_text') ?></a>
                            </p>
                            <p><a style="color: #2b2b2b;border-bottom: 1px #2b2b2b solid"
                                  href="<?php echo base_url() . 'settings_module/email_settings' ?>"><?php echo lang('email_settings_text') ?></a>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-cog"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-6 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3><?php echo $active_user_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('number_of_active_users_text') ?></p>

                            <hr>

                            <p>
                                <?php echo lang('number_of_active_admins_text') ?>
                                &nbsp;(<?php echo $active_admin_count ?>)
                            </p>
                            <p>
                                <?php echo lang('number_of_active_clients_text') ?>
                                &nbsp;(<?php echo $active_client_count ?>)
                            </p>
                            <p>
                                <?php echo lang('number_of_active_staffs_text') ?>&nbsp;
                                (<?php echo $active_staff_count ?>)
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <a href="<?php echo base_url() . 'users/auth' ?>" class="small-box-footer">
                            <?php echo lang('see_all_users_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->


            </div>

            <hr style="border: 1px dotted #8c8b8b">

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php echo $all_running_project_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('all_running_projects_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-gg"></i>
                        </div>
                        <a href="<?php echo base_url() . 'project_module/all_projects' ?>" class="small-box-footer">
                            <?php echo lang('see_all_projects_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo $all_running_task_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('all_running_tasks_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tasks"></i>
                        </div>
                        <a href="<?php echo base_url() . 'task_module/show_all_tasks_from_all_projects' ?>"
                           class="small-box-footer">
                            <?php echo lang('see_all_tasks_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3><?php echo $all_file_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('all_files_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-folder-open"></i>
                        </div>
                        <a href="<?php echo base_url() . 'file_manager_module/all_folders' ?>" class="small-box-footer">
                            <?php echo lang('see_all_files_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-orange">
                        <div class="inner">
                            <h3><?php echo $all_unsolved_ticket_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('all_unsolved_tickets_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-ticket"></i>
                        </div>
                        <a href="<?php echo base_url() . 'support_module/show_ticket_list/all' ?>"
                           class="small-box-footer">
                            <?php echo lang('see_all_tickets_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo $all_unclear_invoice_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('all_unclear_invoice_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>"
                           class="small-box-footer">
                            <?php echo lang('see_all_invoice_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>

            </div>

            <hr style="border: 3px double #8c8b8b">
            <br>

        <?php } ?>

        <!------------------------------------------------------------------------------------------------------------->

        <hr style="border: 3px double #8c8b8b">
        <h4><?php echo lang('my_section_text') ?></h4>

        <div class="row">

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo $my_running_project_count ?></h3>
                        <p style="font-size: larger"><?php echo lang('my_running_projects_text') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url() . 'project_module/my_projects' ?>" class="small-box-footer">
                        <?php echo lang('see_my_projects_text') ?>
                        &nbsp;
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $my_running_task_count ?></h3>
                        <p style="font-size: larger"><?php echo lang('my_running_tasks_text') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tasks"></i>
                    </div>
                    <a href="<?php echo base_url() . 'task_module/show_my_tasks_from_my_projects' ?>"
                       class="small-box-footer">
                        <?php echo lang('see_my_tasks_text') ?>
                        &nbsp;
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3><?php echo lang('files_text') ?></h3>
                        <p style="font-size: larger"><?php echo lang('my_files_text') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-folder-open"></i>
                    </div>
                    <a href="<?php echo base_url() . 'file_manager_module/my_folders' ?>" class="small-box-footer">
                        <?php echo lang('see_my_files_text') ?>
                        &nbsp;
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3><?php echo $my_unsolved_ticket_count ?></h3>
                        <p style="font-size: larger"><?php echo lang('my_unsolved_tickets_text') ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ticket"></i>
                    </div>
                    <a href="<?php echo base_url() . 'support_module/show_ticket_list/my' ?>" class="small-box-footer">
                        <?php echo lang('see_all_tickets_text') ?>
                        &nbsp;
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <?php if ($is_client == 'client') { ?>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo $my_unclear_invoice_count ?></h3>
                            <p style="font-size: larger"><?php echo lang('my_unclear_invoice_text') ?></p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list-alt"></i>
                        </div>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>"
                           class="small-box-footer">
                            <?php echo lang('see_my_invoice_text') ?>
                            &nbsp;
                            <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                <!-- ./col -->
            <?php } ?>
        </div>
        <hr style="border: 3px double #8c8b8b">


</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>