<?php

class Common_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function getDetailedUserData($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getDetailedUserData_asArray($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row_array = $query->row_array();
        return $row_array;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    /* ----- count users starts -----*/
    public function countActiveUsers()
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveAdmins($admin_group_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id=', $admin_group_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveClients($client_group_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id=', $client_group_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveStaffs($staff_group_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id=', $staff_group_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }
    /* ----- count users ends -------*/


    /* ----- count projects starts -----*/
    public function countAllRunningProjects()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countClientRunningProjects($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countStaffRunningProjects($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->where('pas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_project as p', 'pas.project_id=p.project_id');

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);


        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }
    /* ----- count projects ends -------*/

    /* ----- count projects starts -----*/
    public function countAllRunningTasks()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countClientRunningTasks($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countStaffRunningTasks($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_task as t', 't.task_id=tas.task_id');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }
    /* ----- count tasks ends -------*/

    /*---------get tasks starts--------*/
    public function getClientRunningTasks($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');

        $this->db->limit(10);
        $this->db->order_by('t.created_at', 'DESC');

        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getStaffRunningTasks($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_task as t', 't.task_id=tas.task_id');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getClientRunningTasks_asArray($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result_array = $query->result_array();

        return $result_array;
    }

    public function getStaffRunningTasks_asArray($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_task as t', 't.task_id=tas.task_id');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $result_array = $query->result_array();

        return $result_array;
    }
    /*---------get tasks ends---------*/


    /* ----- count files starts ------*/
    public function countAllFiles()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file as f');
        $this->db->where('f.file_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }
    /* ----- count files ends -------*/


    /* ----- count tickets starts ------*/
    public function countAllUnsolvedTickets()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_solution_status!=', 1);
        $this->db->where('t.ticket_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countClientUnsolvedTickets($ticket_created_by)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_created_by', $ticket_created_by);
        $this->db->where('t.ticket_solution_status!=', 1);
        $this->db->where('t.ticket_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countStaffUnsolvedTickets($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_solution_status!=', 1);
        $this->db->where('t.ticket_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id=p.project_id');
        $this->db->join('rspm_tbl_project_assigned_staff as pas', 'p.project_id=pas.project_id');
        $this->db->where('pas.staff_id', $staff_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }
    /* ----- count tickets ends -------*/


    /* ----- count invoices starts ------*/
    public function countAllUnclearInvoices()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice as inv');
        $this->db->where('inv.invoice_payment_clear!=', 1);
        $this->db->where('inv.invoice_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countMyUnclearInvoices($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice as inv');
        $this->db->where('inv.invoice_client_id', $client_id);
        $this->db->where('inv.invoice_payment_clear!=', 1);
        $this->db->where('inv.invoice_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    /* ----- count invoices ends -------*/

}
