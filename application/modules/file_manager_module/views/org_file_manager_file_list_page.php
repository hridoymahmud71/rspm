<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    &nbsp;
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a title="<?php
                if ($which_files == 'without_project') {
                    echo lang('tooltip_upload_without_project_text');
                } else if ($which_files == 'with_project_without_task') {
                    echo lang('tooltip_upload_with_project_without_task_text');
                } else if ($which_files == 'with_project_with_task') {
                    echo lang('tooltip_upload_with_project_with_task_text');
                } else {
                    echo lang('tooltip_upload_text');
                }
                ?>"
                   class="btn btn-primary"
                   href="<?php

                   if ($which_files == 'without_project') {
                       echo base_url() . 'file_manager_module/get_upload_file_form_without_project';
                   } else if ($which_files == 'with_project_without_task') {
                       echo base_url()
                           . 'file_manager_module/get_upload_file_form_with_project_without_task/'
                           . $project_id;
                   } else if ($which_files == 'with_project_with_task') {
                       echo base_url()
                           . 'file_manager_module/get_upload_file_form_with_project_with_task/'
                           . $project_id
                           . '/'
                           . $task_id;
                   } else {
                       echo "#";
                   }

                   ?>">

                    <?php echo lang('go_upload_button_text') ?>
                    &nbsp;
                    <span class="icon"><i class="fa fa-upload"></i></span>
                </a>
            </div>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>                &nbsp;
                <?php if ($is_admin == 'admin') { ?>

                    <a href="<?php echo base_url() . 'file_manager_module/all_folders' ?>">
                        <i class="fa fa-folder-open-o"></i>
                        <?php echo lang('breadcrumb_home_all_folders_text') ?>
                    </a>
                    <?php echo ' | ' ?>
                <?php } ?>

                <a href="<?php echo base_url() . 'file_manager_module/my_folders' ?>">
                    <i class="fa fa-folder-open-o"></i>
                    <?php echo lang('breadcrumb_home_my_folders_text') ?>
                </a>
            </li>
            <li>
                <?php echo lang('breadcrumb_section_main_directory_text') ?>
                <?php echo '/' ?>
                <?php
                if ($which_files == 'without_project') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_without_project/' ?>">
                        <?php echo lang('breadcrumb_section_files_without_project_text') ?>
                    </a>
                <?php } else if ($which_files == 'with_project_without_task') { ?>

                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/' . $project_id ?>">
                        <?php echo $given_project_folder_name ?>
                    </a>

                <?php } else if ($which_files == 'with_project_with_task') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/' . $project_id ?>">
                        <?php echo $given_project_folder_name ?>
                    </a>

                    <?php echo ' / ' ?>

                    <a href="<?php echo base_url() . 'file_manager_module/files_with_project_with_task/'
                        . $project_id
                        . '/'
                        . $task_id ?>">
                        <?php echo $task_number ?>
                    </a>

                <?php } else { ?>
                    <?php echo lang('unknown_text') ?>
                <?php }
                ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!--messages starts-->
    <?php if ($this->session->flashdata('file_delete_success')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap" id="delete-success-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#delete-success-panel" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="panel-body"><?php echo $this->session->flashdata('deleted_file_name') ?>
                    &nbsp;
                    <?php echo lang('file_deleted_text') ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <!--messages ends-->

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-md-4">
                <?php if ($projectroom_menu_section) {
                    echo $projectroom_menu_section;
                } ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo lang('table_title_text') ?>
                            &nbsp;
                            <?php
                            if ($which_files == 'without_project') {
                                echo lang('table_title_files_without_project_text');
                            } else if ($which_files == 'with_project_without_task') {
                                echo '(' . $given_project_folder_name . ')';
                            } else if ($which_files == 'with_project_with_task') {
                                echo '(' . $given_project_folder_name . ' / ' . $task_number . ')';
                            } else {
                                echo lang('unknown_text');
                            }
                            ?>
                        </h3>

                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis" data-column="0"><?php echo lang('column_file_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('column_file_size_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('column_file_ext_text') ?></a>
                            -
                            <a class="toggle-vis"
                               data-column="3"><?php echo lang('column_file_main_directory_text') ?></a>
                            -
                            <a class="toggle-vis"
                               data-column="4"><?php echo lang('column_project_folder_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="5"><?php echo lang('column_task_number_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="6"><?php echo lang('column_file_uploaded_by_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="7"><?php echo lang('column_created_at_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="8"><?php echo lang('column_actions_text') ?></a>
                        </div>
                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                   border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label><?php echo lang('column_file_name_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col2" data-column="2">
                                    <td align="center"><label
                                            for=""><?php echo lang('column_file_ext_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col2_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col2_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col2_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col6" data-column="6">
                                    <td align="center"><label
                                            for=""><?php echo lang('column_file_uploaded_by_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col6_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col6_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col6_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="file-table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_file_name_text') ?></th>
                                <th><?php echo lang('column_file_size_text') ?></th>
                                <th><?php echo lang('column_file_ext_text') ?></th>
                                <th><?php echo lang('column_file_main_directory_text') ?></th>
                                <th><?php echo lang('column_project_folder_name_text') ?></th>
                                <
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_file_uploaded_by_text') ?></th>
                                <th><?php echo lang('column_created_at_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($files) {
                                foreach ($files as $a_file) { ?>
                                    <tr>

                                        <td title="<?php echo $a_file->file_name ?>">
                                            <?php echo $a_file->file_name ?>
                                        </td>
                                        <td data-sort="<?php echo $a_file->file_size ?>">
                                            <?php
                                            echo number_format($a_file->file_size, 3) . ' kB' . ' | '
                                                . number_format($a_file->file_size / 1024, 3) . ' MB'
                                            ?>
                                        </td>
                                        <td><?php echo $a_file->file_ext ?></td>
                                        <td title="<?php echo $a_file->file_main_directory ?>">
                                            <?php echo $a_file->file_main_directory ?>
                                        </td>

                                        <?php
                                        if (($which_files == 'with_project_without_task')
                                            || ($which_files == 'with_project_with_task')
                                        ) { ?>
                                            <td title="<?php echo $given_project_folder_name ?>">
                                                <?php echo $given_project_folder_name ?>
                                            </td>
                                        <?php } else { ?>
                                            <td><?php echo lang('unavailable_text') ?></td>
                                        <?php } ?>

                                        <?php if ($which_files == 'with_project_with_task') { ?>
                                            <td title="<?php echo $task_number ?>">
                                                <?php echo $task_number ?>
                                            </td>
                                        <?php } else { ?>
                                            <td><?php echo lang('unavailable_text') ?></td>
                                        <?php } ?>

                                        <td>
                                            <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/'
                                                . $a_file->file_uploaded_by ?>">
                                                <?php echo $a_file->file_uploaded_by_user_fullname ?>
                                            </a>
                                        </td>
                                        <td data-sort="<?php echo $a_file->created_at_timestamp ?>">
                                            <?php echo $a_file->created_at ?>
                                        </td>

                                        <td>
                                            <a title="<?php echo lang('tooltip_view_text') ?>"
                                               style="color: #2b2b2b"
                                               href="<?php echo base_url() . 'file_manager_module/file_overview/'
                                                   . $a_file->file_id ?>">
                                                <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                            </a>&nbsp;

                                            <a title="<?php echo lang('tooltip_download_text') ?>"
                                               style="color: #2b2b2b"
                                                <?php if ($which_files == 'without_project') { ?>
                                                    href="<?php echo base_url() . $a_file->file_main_directory . '/'
                                                        . $a_file->file_name ?>"
                                                <?php } else if ($which_files == 'with_project_without_task') { ?>
                                                    href="<?php echo base_url() . $a_file->file_main_directory
                                                        . '/'
                                                        . $given_project_folder_name
                                                        . '/'
                                                        . $a_file->file_name ?>"
                                                <?php } else if ($which_files == 'with_project_with_task') { ?>
                                                    href="<?php echo base_url() . $a_file->file_main_directory
                                                        . '/'
                                                        . $given_project_folder_name
                                                        . '/'
                                                        . $a_file->file_name ?>"
                                                <?php } else { ?>
                                                    href="//nothing.com"
                                                <?php } ?>
                                               class="" download>
                                                <i class="fa fa-download fa-lg" aria-hidden="true"></i>
                                            </a>


                                            <?php if ($is_admin == 'admin') { ?>
                                                &nbsp;
                                                <a title="<?php echo lang('tooltip_delete_text') ?>" id=""
                                                   style="color: #2b2b2b"
                                                   href="<?php echo base_url() . 'file_manager_module/'
                                                       . $which_files . '/' . $project_id . '/' . $task_id . '/delete_file/' . $a_file->file_id ?>"
                                                   class="confirmation">
                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                </a>
                                            <?php } ?>
                                        </td>

                                    </tr>
                                <?php }

                            } else { ?>
                                <div style="color: darkred;font-size: larger"><?php echo lang('no_file_found_text') ?></div>
                            <?php } ?>

                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th><?php echo lang('column_file_name_text') ?></th>
                                <th><?php echo lang('column_file_size_text') ?></th>
                                <th><?php echo lang('column_file_ext_text') ?></th>
                                <th><?php echo lang('column_file_main_directory_text') ?></th>
                                <th><?php echo lang('column_project_folder_name_text') ?></th>
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_file_uploaded_by_text') ?></th>
                                <th><?php echo lang('column_created_at_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<!--this css style is holding datatable inside the box-->
<style>
    #file-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #file-table td,
    #file-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#file-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<script>
    /*make column invisile on load*/
    $(function () {
        var tbl = $('#file-table').DataTable();

        tbl.columns([3, 4, 5]).visible(false);

        tbl.columns.adjust().draw(false); // adjust column sizing and redraw
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#file-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        $('#file-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#file-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('sweetalert_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('sweetalert_confirmButtonText')?>",
                cancelButtonText: "<?php echo lang('sweetalert_cancelButtonText')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>