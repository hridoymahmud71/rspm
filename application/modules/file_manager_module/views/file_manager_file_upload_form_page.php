<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

            <?php echo lang('page_title_text'); ?>

            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <?php if($is_admin == 'admin'){ ?>
                    <a href="<?php echo base_url() . 'file_manager_module/all_folders' ?>">
                        <i class="fa fa-folder-o"></i>
                        <?php echo lang('breadcrumb_all_project_folders_text') ?>
                    </a>
                    &nbsp;
                    <?php echo '|' ?>
                    &nbsp;
                <?php } ?>
                <a href="<?php echo base_url() . 'file_manager_module/my_folders' ?>">
                    <i class="fa fa-folder-o"></i>
                    <?php echo lang('breadcrumb_my_project_folders_text') ?>
                </a>
            </li>
            <li>
                <?php if ($what_form == 'without_project') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_without_project' ?>">
                        <?php echo lang('breadcrumb_section_files_without_project_text') ?>
                    </a>
                <?php } else if($what_form == 'with_project_without_task') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'.$project_id ?>">
                        <?php echo $given_project_folder_name ?>
                    </a>
                <?php } else if($what_form == 'with_project_with_task') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'
                        .$project_id ?>">
                        <?php echo $given_project_folder_name ?>
                    </a>
                    <?php echo ' / ' ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_with_project_with_task/'
                        .$project_id.'/'
                        .$task_id ?>">
                        <?php echo  $task_number ?>
                    </a>
                <?php }else{ ?>
                    <?php echo lang('unknown_text') ?>
                <?php } ?>
            </li>
            <li class="active">
                <?php echo lang('breadcrumb_page_text'); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?php if ($projectroom_menu_section) {
                    echo $projectroom_menu_section;
                } ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php echo lang('box_title_text') ?>
                                                        &nbsp;
                            <?php if ($what_form == 'without_project') { ?>
                                <a href="<?php echo base_url() . 'file_manager_module/files_without_project' ?>">
                                    <?php echo lang('breadcrumb_section_files_without_project_text') ?>
                                </a>
                            <?php } else if($what_form == 'with_project_without_task') { ?>
                                <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'.$project_id ?>">
                                    <?php echo $given_project_folder_name ?>
                                </a>
                            <?php } else if($what_form == 'with_project_with_task') { ?>
                                <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'
                                    .$project_id ?>">
                                    <?php echo $given_project_folder_name ?>
                                </a>
                                <?php echo ' / ' ?>
                                <a href="<?php echo base_url() . 'file_manager_module/files_with_project_with_task/'
                                    .$project_id.'/'
                                    .$task_id ?>">
                                    <?php echo  $task_number ?>
                                </a>
                            <?php }else{ ?>
                                <?php echo lang('unknown_text') ?>
                            <?php } ?>
                        </h3>
                        <br><br>
                        <!--messages starts-->
                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php
                            if ($this->session->flashdata('no_file_selected'))
                                echo lang('no_file_selected_text');
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php
                            if ($this->session->flashdata('file_upload_errors'))
                                echo $this->session->flashdata('file_upload_errors');
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php
                            if ($this->session->flashdata('multi_upload_errors')) {
                                foreach ($this->session->flashdata('multi_upload_errors') as $a_upload_error_message) {
                                    echo $a_upload_error_message;
                                }
                            }
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <?php
                            if ($this->session->flashdata('file_upload_success'))
                                echo $this->session->flashdata('file_upload_success')
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <?php
                            if ($this->session->flashdata('multi_upload_success')) {
                                foreach ($this->session->flashdata('multi_upload_success') as $a_upload_success_message) {
                                    echo $a_upload_success_message;
                                }
                            }
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div id="dz_sending" style="display: none" class=" col-md-offset-2 col-md-8" >
                            <?php echo lang('dz_sending_text')?>
                        </div>
                        <div class="col-md-2"></div>

                        <div id="dz_complete" style="display: none" class=" col-md-offset-2 col-md-8" >
                            <?php echo lang('dz_complete_text')?>
                        </div>
                        <div class="col-md-2"></div>
                        <!--messages ends-->

                    </div>
                    <!-- /.box-header -->

                    <!-- single/multiple form starts -->
                    <div></div>
                    <form action="<?php echo base_url() . 'file_manager_module/upload_file' ?>" role="form" id=""
                          method="post" class=""  enctype="multipart/form-data">
                        <div class="box-body">

                            <!--hidden fields -->
                            <div id="hidden_field_wrapper">
                                <input type="hidden"
                                       id="given_project_folder_name"
                                       name="given_project_folder_name"
                                       value="<?php echo $given_project_folder_name ?>">
                                <input type="hidden" name="what_form" value="<?php echo $what_form ?>">
                                <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
                                <input type="hidden" name="task_id" value="<?php echo $task_id ?>">
                            </div>



                            <div class="form-group">
                                <label for="single_multiple_or_dropzone_files"><?php echo lang('label_single_multiple_or_dropzone_files_text') ?></label>

                                <select class="form-control" name="single_multiple_or_dropzone_files"
                                        id="single_multiple_or_dropzone_files">

                                    <option value="single" class=""
                                        <?php if ($this->session->flashdata('single_file_selected')) echo 'selected' ?>
                                    >
                                        <?php echo lang('option_single_file_text') ?>
                                    </option>

                                    <option value="multiple" class=""
                                        <?php if ($this->session->flashdata('multiple_files_selected')) echo 'selected' ?>
                                    >
                                        <?php echo lang('option_multiple_file_text') ?>
                                    </option>

                                    <option value="dropzone" class=""
                                        <?php if ($this->session->flashdata('dropzone_files_selected')) echo 'selected' ?>
                                    >
                                        <?php echo lang('option_dropzone_file_text') ?>
                                    </option>

                                </select>

                            </div>

                            <!--single file starts-->
                            <div id="single_file_wrapper">

                                <hr>
                                <div style="font-size: larger;color:#2b2b2b ">
                                    <?php echo lang('single_file_text') ?>
                                </div>
                                <hr>

                                <div class="form-group">
                                    <label for="single_file_to_upload"><?php echo lang('label_file_to_upload_text') ?></label>
                                    <input type="file" name="single_file_to_upload" class="form-control" id="">
                                </div>

                                <div class="form-group">
                                    <label for="single_file_note"><?php echo lang('label_file_note_text') ?></label>

                                    <input type="text" name="single_file_note" class="form-control"
                                           id="" placeholder="<?php echo lang('placeholder_file_note_text') ?>">
                                </div>
                            </div>
                            <!--single file ends-->

                            <!--multiple file starts-->
                            <div id="multiple_file_wrapper">

                                <hr>
                                <div style="font-size: larger;color:#2b2b2b ">
                                    <?php echo lang('multiple_file_text') ?>
                                </div>
                                <hr>

                                <div id="a_file_container">
                                    <div id="a_file1">
                                        <div class="form-group">
                                            <label for=""><?php echo lang('file_number_text') ?>
                                                &nbsp;
                                                <input class="file_num" value="1" type="text"
                                                       style="border: transparent;background: transparent;color: #2b2b2b"
                                                       readonly>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="multiple_files_to_upload"><?php echo lang('label_file_to_upload_text') ?></label>
                                            <input type="file" name="multiple_files_to_upload1"
                                                   class="form-control multiple_files_to_upload"
                                                   id="">
                                        </div>

                                        <div class="form-group">
                                            <label for="multiple_files_note"><?php echo lang('label_file_note_text') ?></label>

                                            <input type="text" name="multiple_files_note1" multiple
                                                   class="form-control multiple_files_note"

                                                   placeholder="<?php echo lang('placeholder_file_note_text') ?>">
                                        </div>

                                        <div class="form-group" style="font-size: smaller">
                                            <button id="add_file_button1"
                                                    class="btn btn-success .btn-sm add_file_button">
                                                <span><i class="fa fa-plus"></i></span>
                                            </button>
                                            &nbsp;
                                            <button id="remove_file_button1"
                                                    my_attr="a_file1"
                                                    class="btn btn-danger .btn-sm remove_file_button">
                                                <span><i class="fa fa-minus"></i></span>
                                            </button>
                                        </div>
                                        <hr style="border-top: dotted 3px;"/>
                                    </div>
                                </div>

                            </div>
                            <!--multiple file ends-->


                            <!-- /.box-body -->
                            <div class="box-footer" id="single_or_multiple_file_submit_button">
                                <button type="submit" id="" class="btn btn-primary">
                                    <?php echo lang('button_submit_upload_file_text'); ?>
                                </button>
                            </div>
                    </form>
                    <!-- single/multiple form ends -->


                    <!--dropzone form starts-->
                    <div id="dropzone_form_wrapper">
                        <hr>
                        <div style="font-size: larger;color:#2b2b2b ">
                            <?php echo lang('dropzone_file_text') ?>
                        </div>
                        <hr>

                        <form action="<?php echo base_url().'file_manager_module/upload_file_with_dropzone'?>"
                              class="dropzone" id="my-awesome-dropzone" method="" enctype="multipart/form-data">

                            <input type="hidden" id="test" name="test" value="drop zone test value">
                            <input type="hidden"
                                   id="given_project_folder_name"
                                   name="given_project_folder_name"
                                   value="<?php echo $given_project_folder_name ?>">
                            <input type="hidden" id="what_form" name="what_form" value="<?php echo $what_form ?>">
                            <input type="hidden" id="project_id" name="project_id" value="<?php echo $project_id ?>">
                            <input type="hidden" id="task_id" name="task_id" value="<?php echo $task_id ?>">
                        </form>


                        <div class="box-footer" id="">
                            <button type="" id="dropzone_submit_btn" class="btn btn-primary">
                                <?php echo lang('button_submit_upload_with_dropzone_text'); ?>
                            </button>
                            &nbsp;
                            <button id="refresh"
                                    class="btn btn-default"><i class="fa fa-refresh"></i>
                            </button>
                        </div>

                    </div>
                    <!--dropzone  ends-->

                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $("#refresh").on("click", function (e) {
        // Make sure that the form isn't actually being sent.
        e.preventDefault();
        location.reload();
    })
</script>

<!--select single , multiple files or dropzone starts -->
<script>
    $(function () {
        $("#dz_complete").hide();
        $("#dz_sending").hide();

        toggle_between_single_multiple_drop_zone_files();

        $('#single_multiple_or_dropzone_files').change(function () {
            toggle_between_single_multiple_drop_zone_files();
        });

    });

    function toggle_between_single_multiple_drop_zone_files() {

        if ($('#single_multiple_or_dropzone_files').val() == 'single') {

            $('#single_or_multiple_file_submit_button').show();
            $('#single_file_wrapper').show();

            $('.multiple_files_to_upload').val("");
            $('.multiple_files_note').val("");
            $('#multiple_file_wrapper').hide();

            $('#dropzone_form_wrapper').hide();

        }

        else if ($('#single_multiple_or_dropzone_files').val() == 'multiple') {

            $('#single_or_multiple_file_submit_button').show();
            $('#multiple_file_wrapper').show();

            $('#single_file_wrapper').find("input").val("");
            $('#single_file_wrapper').hide();

            $('#dropzone_form_wrapper').hide();

        }

        else {

            $('#dropzone_form_wrapper').show();
            $('#single_or_multiple_file_submit_button').hide();

            $('#single_file_wrapper').find("input").val("");
            $('#single_file_wrapper').hide();

            $('.multiple_files_to_upload').val("");
            $('.multiple_files_note').val("");
            $('#multiple_file_wrapper').hide();

        }

    }
</script>
<!--select single , multiple files or dropzone ends -->


<!--add multiple files-->
<script>
    $(function () {

        file_num = 1;
        file_count = 1;
        max_limit = 5; // not more than 9. More than a digit will desync the file with file note

        $(".add_file_button").on("click", function (e) {
            e.preventDefault();
            var add_file_button_id = $(this).attr('id');

            if (file_count < max_limit) {
                add_a_file(add_file_button_id); //pass add button id to hide it: not used yet
            } else {
                swal("<?php echo lang('swal_upload_limit_reached_text')?>", "<?php echo lang('swal_maximum_limit_text')?>" + ' ' + max_limit)
            }

        });

        $(".remove_file_button").on("click", function (e) {
            e.preventDefault();

            var got_attr = $(this).attr('my_attr');

            var file_to_remove_id = '#' + got_attr;
            if (file_count > 1) {
                $(file_to_remove_id).remove();
                file_count--;
            }

        });


        function add_a_file(add_file_button_id) {
            file_num++;
            $("#remove_file_button1").show();

            var my_clone = $("#a_file1").clone(true);
            my_clone.attr('id', 'a_file' + file_num);

            my_clone.find("input").val("");

            my_clone.find(".file_num").val(file_num);
            my_clone.find(".multiple_files_to_upload").attr('name', 'multiple_files_to_upload' + file_num);
            my_clone.find(".multiple_files_note").attr('name', 'multiple_files_note' + file_num);

            my_clone.find(".remove_file_button").attr('my_attr', 'a_file' + file_num);
            my_clone.find(".remove_file_button").attr('id', 'remove_file_button' + file_num);
            my_clone.find(".add_file_button").attr('id', 'add_file_button' + file_num);-

            $("#a_file_container").append(my_clone);
            $("#remove_file_button1").hide();
            file_count++;
        }


    })
</script>



<!--dropzone js starts-->
<script>
    $(function () {

        var dz_total_file_size_in_MB = 0;

        $("#dz_complete").hide();
        $("#dz_sending").hide();

        Dropzone.options.myAwesomeDropzone  = {

            maxFilesize: <?php echo $dropzone_max_size?>,
            autoDiscover: false,
            addRemoveLinks: true,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: <?php echo $dropzone_max_file_number?>,
            autoProcessQueue: false, // :false Prevents Dropzone from uploading dropped files immediately
            dictResponseError: 'Server not Configured',
            /*acceptedFiles: '.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip',*/
            acceptedFiles: '<?php echo $dropzone_allowed_types ?>',

            accept: function(file, done) {

                done();
            },

            /*success: function(file, response) {
                alert(response);
            },

             */



            /*error: function(file, response) {
                alert(response);

            },*/

            init: function () {

                myDropzone = this; // closure

                $("#dropzone_submit_btn").on("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();

                    var filesInQueue = myDropzone.getQueuedFiles().length;

                    if(filesInQueue == 0){
                        swal('warning','<?php echo lang('swal_no_file_added_text')?>');
                    }else{
                        myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    }

                });


                myDropzone.on("addedfile", function(file) {

                    var php_limit_in_MB = <?php echo (int)(str_replace('M', '', ini_get('post_max_size')));  ?>;
                    var a_file_size_in_MB = Math.ceil(((file.size)/1024)/1024);

                    dz_total_file_size_in_MB += a_file_size_in_MB;

                    if(dz_total_file_size_in_MB >= php_limit_in_MB ){
                        swal('warning','<?php echo lang('swal_file_exeed_php_limit_text')?>');
                        this.removeFile(file);
                    }

                     });

                myDropzone.on("removedfile", function(file) {
                    // perform task //

                    var a_file_size_in_MB = Math.ceil(((file.size)/1024)/1024);

                    dz_total_file_size_in_MB -= a_file_size_in_MB;

                });

                myDropzone.on("sendingmultiple", function(file, xhr, formData) {
                    //alert('sending');
                    $("#dz_sending").css({'display': 'block','color': 'darkgreen','font-size': 'larger'});
                    $("#dz_sending").show().delay(3000).fadeOut();
                });

                myDropzone.on("completemultiple", function() {
                    //alert('complete');
                    $("#dz_complete").css({'display': 'block','color':'darkgreen','font-size': 'larger'});
                    $("#dz_complete").show().delay(3000).fadeOut();
                });

                myDropzone.on("error", function(file, message) {
                    //alert(message);
                    swal(message);
                    this.removeFile(file);
                });

                myDropzone.on("success", function(file, message) {
                    //alert(message);
                    swal('success','<?php echo lang('swal_file_uploaded_text')?>');
                    this.removeFile(file);
                });


            }
        };


    })
</script>
<!--dropzone js ends-->
