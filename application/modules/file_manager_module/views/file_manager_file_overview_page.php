<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php if ($a_file_info) {
                    echo $a_file_info->file_name;
                } ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="file_manager_module/all_folders">
                        <i class="fa fa-folder-open-o"></i>
                        <?php echo lang('breadcrumb_home_all_folders_text') ?>
                    </a>
                <?php } ?>

                <a href="file_manager_module/my_folders">
                    <i class="fa fa-folder-open-o"></i>
                    <?php echo lang('breadcrumb_home_my_folders_text') ?>
                </a>
            </li>
            <li>
                <?php if ($a_file_info->project_id == 0) { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_without_project/' ?>">
                        <?php echo lang('breadcrumb_section_files_without_project_text') ?>
                    </a>
                <?php } else if ($a_file_info->project_id != 0 && $a_file_info->task_id == 0) { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'
                        . $a_file_info->project_id ?>">
                        <?php echo $a_file_info->file_project_folder_name ?>
                    </a>
                <?php } else if ($a_file_info->project_id != 0 && $a_file_info->task_id != 0) { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'
                        . $a_file_info->project_id ?>">
                        <?php echo $a_file_info->file_project_folder_name ?>
                    </a>
                    <?php echo ' / ' ?>
                    <a href="<?php echo base_url() . 'file_manager_module/files_with_project_with_task/'
                        . $a_file_info->task_id ?>"></a>
                <?php } else { ?>
                    <a href="<?php echo '/' ?>">
                        <?php echo lang('unknown_text') ?>
                    </a>
                <?php } ?>

                <a href="">task_number</a>
            </li>
            <li class="active"><?php echo lang('box_title_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!--overview wrap starts-->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <strong><h3 class="box-title"><?php echo lang('box_title_text') ?></h3></strong>

                        <!--only for admin /delete file-->

                        <?php /*if ($is_admin == 'admin') { */?><!--
                            <a title="<?php /*echo lang('tooltip_delete_text') */?>" href=""
                               style="color:#2b2b2b" class="pull-right">
                                <i class="fa fa-trash-o fa-lg"></i>
                            </a>
                        --><?php /*} */?>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!--file details starts-->
                        <strong><i class="fa fa-file-o margin-r-5"></i>
                            <?php echo lang('section_heading_file_details_text') ?>
                        </strong>
                        <p><!--empty space--></p>
                        <div class="box-body table-responsive no-padding ">
                            <table class="table table-hover text-muted table-bordered">
                                <?php if ($a_file_info) { ?>
                                    <tbody>
                                    <tr>
                                        <td><?php echo lang('file_download_text') ?></td>
                                        <td>
                                            <a title="<?php echo lang('tooltip_download_text') ?>"
                                               style="color: #2b2b2b"
                                               href="<?php
                                               if ($a_file_info->project_id == 0) {

                                                   echo base_url()
                                                       . $a_file_info->file_main_directory
                                                       . '/'
                                                       . $a_file_info->file_name;

                                               } else if ($a_file_info->project_id != 0 && $a_file_info->task_id == 0) {

                                                   echo base_url()
                                                       . $a_file_info->file_main_directory
                                                       . '/'
                                                       . $a_file_info->file_project_folder_name
                                                       . '/'
                                                       . $a_file_info->file_name;

                                               } else if ($a_file_info->project_id != 0 && $a_file_info->task_id != 0) {

                                                   echo base_url()
                                                       . $a_file_info->file_main_directory
                                                       . '/'
                                                       . $a_file_info->file_project_folder_name
                                                       . '/'
                                                       . $a_file_info->file_name;

                                               } else {
                                                   echo '/';
                                               }

                                               ?>" download="download">
                                                <i class="fa fa-download fa-lg" area-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_name_text') ?></td>
                                        <td><?php echo $a_file_info->file_name ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_ext_text') ?></td>
                                        <td><?php echo $a_file_info->file_ext ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_size_text') ?></td>
                                        <td>
                                            <?php
                                            echo $a_file_info->file_size;
                                            echo lang('kilobyte_text');
                                            echo ' | ';
                                            echo round($a_file_info->file_size / 1024, 3);
                                            echo lang('megabyte_text');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_uploaded_by_text') ?></td>
                                        <td title="<?php echo lang('tooltip_see_uploader_profile_text') ?>">
                                            <a class="label label-primary" href="<?php echo base_url()
                                                . 'user_profile_module/user_profile_overview/'
                                                . $a_file_info->file_uploaded_by ?>">
                                                <?php echo $a_file_info->file_uploaded_by_user_fullname ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_main_directory_text') ?></td>
                                        <td><?php echo $a_file_info->file_main_directory ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_project_folder_name_text') ?></td>
                                        <td><?php echo $a_file_info->file_project_folder_name ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_project_name_text') ?></td>
                                        <td>
                                            <?php if ($a_file_info->project_id != 0) { ?>
                                                <a title="<?php echo lang('tooltip_go_to_projectroom_text') ?>"
                                                   class=" label label-primary" href="<?php echo base_url()
                                                    . 'projectroom_module/project_overview/'
                                                    . $a_file_info->project_id ?>">
                                                    <?php echo $a_file_info->project_name ?>
                                                </a>
                                            <?php } else { ?>
                                                <!--prints unavailable-->
                                                <?php echo $a_file_info->project_name ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_task_number_text') ?></td>
                                        <td><?php if ($a_file_info->task_id != 0 && $a_file_info->project_id != 0) { ?>
                                                <a title="<?php echo lang('tooltip_go_to_task_overview_text') ?>"
                                                   class="label label-primary" href="<?php echo base_url()
                                                    . 'task_module/task_overview/'
                                                    . $a_file_info->project_id
                                                    . '/'
                                                    . $a_file_info->task_id
                                                ?>">
                                                    <?php echo $a_file_info->task_number ?>
                                                </a>
                                            <?php } else { ?>
                                                <!--prints unavailable-->
                                                <?php echo $a_file_info->task_number ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('file_created_at_text') ?></td>
                                        <td><?php echo $a_file_info->created_at ?></td>
                                    </tr>

                                    <!--only for admin starts-->
                                    <tr>
                                        <td><?php echo lang('file_deletion_status_text') ?></td>
                                        <td>
                                            <?php if ($a_file_info->file_deletion_status == 1) { ?>
                                                <span class="label label-danger">
                                                <?php echo lang('yes_text') ?>
                                            </span>
                                            <?php } else { ?>
                                                <span class="label label-success">
                                                <?php echo lang('no_text') ?>
                                            </span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <!--only for admin ends-->
                                    </tbody>
                                <?php } else { ?>
                                    <div style="color: darkred;font-size: larger">
                                        <?php echo lang('file_unavailable_text') ?>
                                    </div>
                                <?php } ?>
                            </table>
                        </div>
                        <!--file details ends-->

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!--overview wrap ends-->

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script>
    $(function () {
        $(document).tooltip();
    })
</script>