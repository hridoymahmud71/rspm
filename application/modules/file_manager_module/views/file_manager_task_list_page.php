<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    &nbsp;
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a title="<?php echo lang('tooltip_upload_file_without_task_text') ?>" class="btn btn-primary"
                   href="<?php
                   echo base_url()
                       . 'file_manager_module/get_upload_file_form_with_project_without_task/'
                       . $project_id;
                   ?>">

                    <?php echo lang('go_upload_button_text') ?>
                    &nbsp<span class="icon"><i class="fa fa-upload"></i></span>
                </a>
            </div>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'file_manager_module/all_folders' ?>">
                        <i class="fa fa-folder-open-o"></i><?php echo lang('breadcrumb_home_all_folders_text') ?></a>
                    &nbsp;
                    <?php echo '|' ?>
                    &nbsp;
                <?php } ?>

                <a href="<?php echo base_url() . 'file_manager_module/my_folders' ?>">
                    <i class="fa fa-folder-open-o"></i><?php echo lang('breadcrumb_home_my_folders_text') ?>
                </a>
            </li>
            <li>
                <?php echo $project_folder_name ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!--messages starts-->
    <?php if ($this->session->flashdata('success')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap" id="add-success-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#add-success-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body"><?php echo lang('add_successfull_text') ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <!--messages ends-->

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-md-4">
                <?php if ($projectroom_menu_section) {
                    echo $projectroom_menu_section;
                } ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php echo lang('table_title_text') ?>
                            <?php echo '('.$project_name.')' ?>
                        </h3>

                            <div style="padding-top: 1%;padding-bottom: 1%">
                                <?php echo lang('toggle_column_text') ?>
                                <a class="toggle-vis" data-column="0"><?php echo lang('column_task_number_text') ?></a>
                                -
                                <a class="toggle-vis" data-column="1"><?php echo lang('column_task_title_text') ?></a>
                                -
                                <a class="toggle-vis" data-column="2"><?php echo lang('column_total_file_number_text') ?></a>
                                -
                                <a class="toggle-vis" data-column="3"><?php echo lang('column_total_file_size_text') ?></a>
                                -
                                <a class="toggle-vis" data-column="4"><?php echo lang('column_task_status_text') ?></a>
                                -
                                <a class="toggle-vis" data-column="5"><?php echo lang('column_actions_text') ?></a>
                            </div>
                            <div>
                                <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                       border="0">
                                    <tbody>
                                    <tr id="filter_col0" data-column="0">
                                        <td align="center"><label><?php echo lang('column_task_number_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col0_filter" type="text">
                                        </td>

                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                    <tr id="filter_col0" data-column="0">
                                        <td align="center"><label
                                                    for=""><?php echo lang('with_or_without_task_text') ?></label></td>
                                        <td align="center">
                                            <select id="custom_which_task_filter" class="form-control">
                                                <option value="all"><?php echo lang('option_all_text') ?></option>
                                                <option value="files_without_task"><?php echo lang('option_files_without_task_text') ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tr id="filter_col1" data-column="1">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_task_title_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col1_filter" type="text">
                                        </td>

                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col1_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col1_smart" checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                    <tr id="filter_col4" data-column="4">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_task_status_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col4_filter" type="hidden">
                                            <select id="custom_status_filter" class="form-control">
                                                <option value="all"><?php echo lang('option_all_text') ?></option>
                                                <option value="1"><?php echo lang('option_active_text') ?></option>
                                                <option value="0"><?php echo lang('option_inactive_text') ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="task-table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_task_title_text') ?></th>
                                <th><?php echo lang('column_total_file_number_text') ?></th>
                                <th><?php echo lang('column_total_file_size_text') ?></th>
                                <th><?php echo lang('column_task_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-search="files_without_task">
                                    <strong style="color: dimgrey">
                                        <?php echo lang('files_without_task_text') ?>
                                    </strong>
                                </td>
                                <td><?php echo lang('unavailable_text') ?></td>

                                <td data-sort="<?php echo $without_task_file_count ?>">
                                    <?php echo $without_task_file_count ?>
                                </td>
                                <td data-sort="<?php echo $without_task_total_file_size ?>">
                                    <?php echo $without_task_total_file_size.' kB'. ' | '
                                        .$without_task_total_file_size_in_MB.' MB' ?>
                                </td>

                                <td data-search="1"><?php echo lang('unavailable_text') ?></td>
                                <td>
                                    <a title="<?php echo lang('tooltip_view_file_without_task_text') ?>"
                                       style="color: #2b2b2b"
                                       href="<?php echo base_url() . 'file_manager_module/files_with_project_without_task/' . $project_id ?>"
                                       class="">
                                        <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                    </a>
                                    &nbsp;
                                    <a title="<?php echo lang('tooltip_upload_file_without_task_text') ?>"
                                       style="color: #2b2b2b"
                                       href="<?php echo base_url()
                                           . 'file_manager_module/files_with_project_without_task/' . $project_id
                                       ?>"
                                       class="">
                                        <i class="fa fa-upload fa-lg" aria-hidden="true"></i>
                                    </a>
                                </td>

                            </tr>
                            <?php if ($all_tasks) {
                                foreach ($all_tasks as $a_task) { ?>
                                    <tr>
                                        <td title="<?php echo $a_task->task_number ?>">
                                            <a href="<?php echo base_url().'task_module/task_overview/'.$project_id.'/'.$a_task->task_id ?>">
                                                <?php echo $a_task->task_number ?>
                                            </a>

                                        </td>
                                        <td><?php echo $a_task->task_title ?></td>

                                        <td data-sort="<?php echo $a_task->file_count ?>">
                                            <?php echo $a_task->file_count ?>
                                        </td>
                                        <td data-sort="<?php echo $a_task->total_file_size ?>">
                                            <?php
                                                echo $a_task->total_file_size .' kB'.' | '.$a_task->total_file_size_in_MB.' MB';
                                            ?>
                                        </td>

                                        <td data-search="<?php echo $a_task->task_status ?>">
                                            <?php if ($a_task->task_status == 1) { ?>
                                                <span class="label label-primary"><?php echo lang('status_active_text') ?></span>

                                                <?php if ($is_admin == 'admin') { ?>
                                                    &nbsp;
                                                    <a title="<?php echo lang('tooltip_deactivate_text') ?>"
                                                       href="<?php echo base_url() . 'task_module/all_tasks/activate_task/'
                                                           . $a_task->task_id ?>">
                                                    <span class="label label-danger"><i class="fa fa-times"
                                                                                        aria-hidden="true"></i>
                                                    </span>
                                                    </a>
                                                <?php } ?>

                                            <?php } else { ?>
                                                <span class="label label-default"><?php echo lang('status_inactive_text') ?></span>
                                                <?php if ($is_admin == 'admin') { ?>
                                                    &nbsp;
                                                    <a title="<?php echo lang('tooltip_activate_text') ?>"
                                                       href="<?php echo base_url() . 'task_module/all_tasks/activate_task/'
                                                           . $a_task->task_id ?>">
                                                <span class="label label-success"><i class="fa fa-check"
                                                                                     aria-hidden="true"></i></span>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                        <td><?php
                                            if (
                                                $is_admin == 'admin'
                                                || $is_client == 'client'
                                                || ($is_staff == 'staff'
                                                    && $is_admin == 'not_admin'
                                                    && $a_task->is_related_task == 'related_task')
                                            ) { ?>
                                                <?php if ($is_admin == 'admin' || $a_task->task_status == 1) { ?>
                                                    <a title="<?php echo lang('tooltip_view_file_with_task_text') ?>"
                                                       style="color: #2b2b2b"
                                                       href="<?php echo base_url() . 'file_manager_module/files_with_project_with_task/'
                                                           . $a_task->project_id
                                                           . '/'
                                                           . $a_task->task_id
                                                       ?>"
                                                       class="">
                                                        <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                    </a>
                                                    &nbsp;
                                                    <a title="<?php echo lang('tooltip_upload_file_with_task_text') ?>"
                                                       style="color: #2b2b2b"
                                                       href="<?php echo base_url()
                                                           . 'file_manager_module/files_with_project_with_task/'
                                                           . $a_task->project_id
                                                           . '/'
                                                           . $a_task->task_id
                                                       ?>"
                                                       class="">
                                                        <i class="fa fa-upload fa-lg" aria-hidden="true"></i>
                                                    </a>
                                                <?php } else { ?>
                                                    <?php echo lang('unavailable_text') ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php echo lang('not_my_task_text') ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php }

                            } else { ?>
                                <div style="color: darkred;font-size: larger"><?php echo lang('no_task_found_text') ?></div>
                            <?php } ?>

                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th><?php echo lang('column_task_number_text') ?></th>
                                <th><?php echo lang('column_task_title_text') ?></th>
                                <th><?php echo lang('column_total_file_number_text') ?></th>
                                <th><?php echo lang('column_total_file_size_text') ?></th>
                                <th><?php echo lang('column_task_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<!--this css style is holding datatable inside the box-->
<style>
    #task-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #task-table td,
    #task-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#task-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<script>
    /*column toggle*/
    $(function () {

        var table = $('#task-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        $('#task-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#task-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {

        /*-----------------------------*/
        $('#custom_which_task_filter').on('change', function () {

            if ($('#custom_which_task_filter').val() == 'all') {
                $('#col0_filter').val('');
                filterColumn(0);
            } else {
                $('#col0_filter').val($('#custom_which_task_filter').val());
                filterColumn(0);
            }

        });
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>