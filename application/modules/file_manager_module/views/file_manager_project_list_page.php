<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    &nbsp;
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a title="<?php echo lang('tooltip_upload_file_without_project_text') ?>" class="btn btn-primary"
                   href="<?php
                   echo base_url()
                       . 'file_manager_module/get_upload_file_form_without_project'
                   ?>">

                    <?php echo lang('go_upload_button_text') ?>
                    <span class="icon"><i class="fa fa-upload"></i></span>
                </a>
            </div>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <?php echo lang('breadcrumb_section_text') ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!--messages starts-->
    <?php if ($this->session->flashdata('successfull')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap" id="successful-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#successful-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('folder_create_success')) {
                        echo $this->session->flashdata('folder_create_success');
                    } ?>
                    <?php if ($this->session->flashdata('folder_delete_success')) {
                        echo $this->session->flashdata('folder_create_success');
                    } ?>
                    <?php if ($this->session->flashdata('folder_undelete_success')) {
                        echo $this->session->flashdata('folder_create_success');
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('unsuccessfull')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-danger copyright-wrap" id="unsuccessful-panel">
                <div class="panel-heading"><?php echo lang('unsuccessfull_text') ?>
                    <button type="button" class="close" data-target="#unsuccessful-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('directory_exists')) {
                        echo $this->session->flashdata('directory_exists');
                    } ?>
                    <?php if ($this->session->flashdata('folder_create_error')) {
                        echo $this->session->flashdata('folder_create_error');
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <!--messages ends-->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php
                            if ($whose_project == 'all_projects') {
                                echo lang('table_title_all_projects_text');
                            } elseif ($whose_project == 'my_projects') {
                                echo lang('table_title_my_projects_text');
                            } else {
                                echo lang('table_title_text');
                            }
                            ?>
                        </h3>
                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis"
                               data-column="0"><?php echo lang('column_project_folder_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('column_project_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('column_total_file_size_text') ?></a>
                            -
                            <a class="toggle-vis"
                               data-column="3"><?php echo lang('column_total_file_number_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="4"><?php echo lang('column_project_status_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="5"><?php echo lang('column_actions_text') ?></a>
                        </div>
                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                   border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label
                                                for=""><?php echo lang('which_folder_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="hidden">
                                        <select id="custom_folder_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="files_without_project"><?php echo lang('option_files_without_project_text') ?></option>
                                            <option value="no_folder"><?php echo lang('option_no_folder_text') ?></option>
                                            <option value="with_folder"><?php echo lang('option_with_folder_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="filter_col1" data-column="1">
                                    <td align="center">
                                        <label for=""><?php echo lang('column_project_name_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col1_filter" type="text">
                                    </td>

                                </tr>

                                <tr id="filter_col4" data-column="4">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_project_status_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col4_filter" type="hidden">
                                        <select id="custom_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="1"><?php echo lang('option_active_text') ?></option>
                                            <option value="0"><?php echo lang('option_inactive_text') ?></option>
                                        </select>
                                    </td>
                                </tr>

                                <tr id="filter_col5" data-column="5">
                                    <td align="center"><label
                                                for=""><?php echo lang('deleted_or_not_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col5_filter" type="hidden">
                                        <select id="custom_deletion_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="1"><?php echo lang('option_deleted_text') ?></option>
                                            <option value="0"><?php echo lang('option_not_deleted_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="file_manager-project_list-table"
                               class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_project_folder_name_text') ?></th>
                                <th><?php echo lang('column_project_name_text') ?></th>
                                <th><?php echo lang('column_total_file_size_text') ?></th>
                                <th><?php echo lang('column_total_file_number_text') ?></th>
                                <th><?php echo lang('column_project_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-search="<?php echo 'files_without_project' ?>">
                                    <strong style="color: dimgrey">
                                        <?php echo lang('files_without_project_text') ?>
                                    </strong>
                                </td>
                                <td><?php echo lang('unavailable_text') ?></td>

                                <td data-sort="<?php echo $without_project_total_file_size ?>">
                                    <?php echo $without_project_total_file_size . ' kB' . ' | '
                                        . $without_project_total_file_size_in_MB . ' MB' ?>
                                </td>
                                <td data-sort="<?php echo $without_project_file_count ?>">
                                    <?php echo $without_project_file_count ?>
                                </td>
                                <td data-search="1"><?php echo lang('unavailable_text') ?></td>

                                <td data-search ="0">
                                    <a title="<?php echo lang('tooltip_view_file_without_project_text') ?>"
                                       style="color: #2b2b2b"
                                       href="<?php echo base_url() . 'file_manager_module/files_without_project' ?>"
                                       class=""><i class="fa fa-eye fa-lg"
                                                   aria-hidden="true"></i>
                                    </a>
                                    &nbsp;
                                    <a title="<?php echo lang('tooltip_upload_file_without_project_text') ?>"
                                       style="color: #2b2b2b"
                                       href="<?php echo base_url() . 'file_manager_module/get_upload_file_form_without_project' ?>"
                                       class=""><i class="fa fa-upload fa-lg" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>


                            <?php if ($projects) {
                                foreach ($projects as $a_project) { ?>
                                    <tr>
                                        <td data-search="<?php if ($a_project->project_folder_name == '' || $a_project->project_folder_name == null) {
                                            echo 'no_folder';
                                        } else {
                                            echo 'with_folder';
                                        } ?>"
                                            title="<?php if ($a_project->project_folder_name != '') {
                                                echo $a_project->project_folder_name;
                                            } ?>">
                                            <?php
                                            if ($a_project->project_folder_name == '') {
                                                echo lang('no_folder_text');
                                                ?>

                                                &nbsp;
                                                <?php if ($whose_project == 'all_projects') { ?>
                                                    <a title="<?php echo lang('tooltip_create_project_folder_text') ?>"
                                                       href="<?php echo base_url() . 'file_manager_module/all_folders/create_project_folder/'
                                                           . $a_project->project_id ?>">
                                                   <span class="label label-warning"><?php echo lang('create_folder_text') ?>
                                                       &nbsp;<i class="fa fa-folder"
                                                                aria-hidden="true"></i>
                                                   </span>
                                                    </a>
                                                <?php } ?>

                                                <?php if ($whose_project == 'my_projects' && $is_admin == 'admin') { ?>
                                                    <a href="<?php echo base_url() . 'file_manager_module/my_folders/create_project_folder/'
                                                        . $a_project->project_id ?>">
                                                <span class="label label-warning"><?php echo lang('create_folder_text') ?>
                                                    &nbsp;<i class="fa fa-folder"
                                                             aria-hidden="true"></i>
                                                   </span>
                                                    </a>
                                                <?php } ?>

                                            <?php } else { ?>

                                                <span class="label label-default">
                                                    <i class="fa fa-folder" aria-hidden="true"></i>
                                                    &nbsp;
                                                    <?php echo $a_project->project_folder_name; ?>
                                                </span>

                                            <?php } ?>
                                        </td>

                                        <td title="<?php echo $a_project->project_name ?>">
                                            <a href="<?php echo base_url() . 'projectroom_module/project_overview/' . $a_project->project_id ?>">
                                                <?php echo $a_project->project_name ?>
                                            </a>
                                        </td>
                                        <td data-sort="<?php echo $a_project->total_file_size ?>">
                                            <?php
                                            if ($a_project->project_folder_name != '' || $a_project->project_folder_name != null) {
                                                echo $a_project->total_file_size . ' kB' . ' | ' . $a_project->total_file_size_in_MB . ' MB';
                                            } else {
                                                echo lang('unknown_text');
                                            }
                                            ?>
                                        </td>
                                        <td data-sort="<?php echo $a_project->file_count ?>">
                                            <?php
                                            if ($a_project->project_folder_name != '' || $a_project->project_folder_name != null) {
                                                echo $a_project->file_count;
                                            } else {
                                                echo lang('unknown_text');
                                            }
                                            ?>
                                        </td>
                                        <td data-search="<?php echo $a_project->status ?>">
                                            <?php if ($a_project->status != 0) { ?>
                                                <span class="label label-primary"><?php echo lang('status_active_text') ?></span>
                                                &nbsp;
                                                <?php if ($is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_activate_text') ?>"
                                                       href="<?php echo base_url() . 'project_module/deactivate_project/'
                                                           . $a_project->project_id ?>">
                                                    <span class="label label-danger"><i class="fa fa-times"
                                                                                        aria-hidden="true"></i></span>
                                                    </a>
                                                <?php } ?>

                                            <?php } else { ?>
                                                <span class="label label-default"><?php echo lang('status_inactive_text') ?></span>
                                                &nbsp;
                                                <?php if ($is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_activate_text') ?>"
                                                       href="<?php echo base_url() . 'project_module/activate_project/'
                                                           . $a_project->project_id ?>">
                                                <span class="label label-success"><i class="fa fa-check"
                                                                                     aria-hidden="true"></i></span>
                                                    </a>
                                                <?php } ?>


                                            <?php } ?>
                                        </td>

                                        <td data-search="<?php if ($a_project->project_folder_deletion_status != '' || $a_project->project_folder_deletion_status != null) {
                                            echo $a_project->project_folder_deletion_status;
                                        } else {
                                            echo 0;
                                        } ?>">
                                            <?php if ($a_project->project_folder_name != '' && $a_project->project_folder_deletion_status != 1) { ?>
                                                <a title="<?php echo lang('tooltip_view_file_project_text') ?>"
                                                   style="color: #2b2b2b"
                                                   href="<?php echo base_url() . 'file_manager_module/files_in_a_project/'
                                                       . $a_project->project_id ?>"
                                                   class="">
                                                    <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                                </a>

                                                &nbsp;

                                                <a title="<?php echo lang('tooltip_upload_file_in_project_text') ?>"
                                                   style="color: #2b2b2b"
                                                   href="<?php echo base_url()
                                                       . 'file_manager_module/get_upload_file_form_with_project_without_task/'
                                                       . $a_project->project_id ?>"
                                                   class="">
                                                    <i class="fa fa-upload fa-lg" aria-hidden="true"></i>
                                                </a>                                            &nbsp;

                                                <?php if ($whose_project == 'all_projects' && $is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_delete_text') ?>" id=""
                                                       style="color: #2b2b2b" download="downlaod"
                                                       href="<?php echo base_url() . 'file_manager_module/all_folders/delete_project_folder/' . $a_project->project_folder_id ?>"
                                                       class="delete_confirmation" ">
                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                    </a>
                                                <?php } ?>

                                                <?php if ($whose_project == 'my_projects' && $is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_delete_text') ?>" id=""
                                                       style="color: #2b2b2b" download="downlaod"
                                                       href="<?php echo base_url() . 'file_manager_module/my_folders/delete_project_folder/' . $a_project->project_folder_id ?>"
                                                       class="delete_confirmation" ">
                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                    </a>
                                                <?php } ?>

                                            <?php } else if ($a_project->project_folder_deletion_status == 1) { ?>

                                                <?php if ($whose_project == 'all_projects' && $is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_undelete_text') ?>" id=""
                                                       style="color: #2b2b2b" download="downlaod"
                                                       href="<?php echo base_url() . 'file_manager_module/all_folders/undelete_project_folder/' . $a_project->project_folder_id ?>"
                                                       class="undelete_confirmation" ">
                                                    <i id="remove" class="fa fa-rotate-left fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                    </a>
                                                <?php } ?>

                                                <?php if ($whose_project == 'my_projects' && $is_admin == 'admin') { ?>
                                                    <a title="<?php echo lang('tooltip_undelete_text') ?>" id=""
                                                       style="color: #2b2b2b" download="downlaod"
                                                       href="<?php echo base_url() . 'file_manager_module/my_folders/undelete_project_folder/' . $a_project->project_folder_id ?>"
                                                       class="undelete_confirmation" ">
                                                    <i id="remove" class="fa fa-rotate-left fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                    </a>
                                                <?php } ?>

                                                <?php if ($whose_project == 'my_projects' && $is_admin == 'not_admin') { ?>
                                                    <?php echo lang('folder_deleted_text') ?>
                                                <?php } ?>


                                            <?php } else {
                                                echo lang('unavailable_text');
                                            } ?>

                                        </td>


                                    </tr>
                                <?php }

                            } else { ?>
                                <div style="color: darkred;font-size: larger"><?php echo lang('no_project_found_text') ?></div>
                            <?php } ?>

                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th><?php echo lang('column_project_folder_name_text') ?></th>
                                <th><?php echo lang('column_project_name_text') ?></th>
                                <th><?php echo lang('column_total_file_size_text') ?></th>
                                <th><?php echo lang('column_total_file_number_text') ?></th>
                                <th><?php echo lang('column_project_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<!--Upload icon Css-->
<style>
    .element {
        display: inline-flex;
        align-items: flex-start;
    }

    i.fa-upload {
        margin: 5px;
        cursor: pointer;
        font-size: inherit;
    }

    i:hover {
        opacity: 0.6;
    }

    input {
        display: none;
    }
</style>

<!--Upload icon Jquery-->
<script>
    $("i").click(function () {
        $("input[type='file']").trigger('click');
    });

    $('input[type="file"]').on('change', function () {
        var val = $(this).val();
        $(this).siblings('span').text(val);
    })
</script>

<!--this css style is holding datatable inside the box-->
<style>
    #file_manager-project_list-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #file_manager-project_list-table td,
    #file_manager-project_list-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#file_manager-project_list-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<script>
    /*column toggle*/
    $(function () {

        var table = $('#file_manager-project_list-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        $('#file_manager-project_list-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#file_manager-project_list-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {

        /*-----------------------------*/
        $('#custom_folder_status_filter').on('change', function () {

            if ($('#custom_folder_status_filter').val() == 'all') {
                $('#col0_filter').val('');
                filterColumn(0);
            } else {
                $('#col0_filter').val($('#custom_folder_status_filter').val());
                filterColumn(0);
            }

        });
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });

        $('#custom_deletion_status_filter').on('change', function () {

            if ($('#custom_deletion_status_filter').val() == 'all') {
                $('#col5_filter').val('');
                filterColumn(5);
            } else {
                $('#col5_filter').val($('#custom_deletion_status_filter').val());
                filterColumn(5);
            }

        });
        /*-----------------------------*/
    })
</script>

<!--Sweetalaert Css-->
<script>
    $('.undelete_confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_undelete_title_text')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#darkgreen",
                confirmButtonText: "<?php echo lang('swal_undelete_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_undelete_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>

<script>
    $('.delete_confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_delete_title_text')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_delete_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_delete_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>