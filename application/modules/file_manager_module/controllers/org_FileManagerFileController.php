<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileManagerFileController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('settings_module/Settings_model');
        $this->load->model('timezone_module/Timezone_model');
        $this->load->model('auth/Ion_auth_model');


        $this->load->model('project_module/Project_model');
        $this->load->model('file_manager_module/Filemanager_model');


        $this->load->library('form_validation');
        $this->load->library('session');

        //ion auth lib
        $this->load->library('auth/ion_auth');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        //customized lib from libraries
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_file_library');
        $this->load->library('custom_log_library');

        $this->lang->load('file_manager_file_list');
    }

    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    public function showFilesWithoutProject()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        //project_id = 0 ,task_id = 0
        $this->showFiles('without_project', 0, 0);
    }

    public function showFilesWithProjectWithoutTask()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $project_id = $this->uri->segment(3);

        $project_folder_exist = $this->Filemanager_model->checkIfUndeletedProjectFolderExist($project_id);

        if (!$project_folder_exist) {
            redirect('users/auth/does_not_exist');
        } else {
            $this->showFiles('with_project_without_task', $project_id, 0);                         //task_id = 0
        }
    }

    public function showFilesWithProjectWithTask()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $project_id = $this->uri->segment(3);
        $task_id = $this->uri->segment(4);

        $project_folder_exist = $this->Filemanager_model->checkIfUndeletedProjectFolderExist($project_id);

        if (!$project_folder_exist) {
            redirect('users/auth/does_not_exist');
        } else {
            $this->showFiles('with_project_with_task', $project_id, $task_id);
        }
    }

    //set project_id or task_id to zero if not available
    public function showFiles($which_files, $project_id, $task_id)
    {
        $data['which_files'] = $which_files;
        $data['project_id'] = $project_id;
        $data['task_id'] = $task_id;

        if ($which_files == 'without_project') {
            $data['files'] = $this->Filemanager_model->getFilesWithoutProject();
        }
        if ($which_files == 'with_project_without_task') {
            $data['files'] = $this->Filemanager_model->getFilesWithProjectWithoutTask($project_id);
        }
        if ($which_files == 'with_project_with_task') {
            $data['files'] = $this->Filemanager_model->getFilesWithProjectWithTask($project_id, $task_id);
        }

        if ($which_files != 'without_project' && $project_id != 0) {

            $a_project_folder = $this->Filemanager_model->getProjectFolder($project_id);

            if ($a_project_folder && $a_project_folder->project_folder_name != '') {
                $data['given_project_folder_name'] = $a_project_folder->project_folder_name;
            } else {
                $data['given_project_folder_name'] = '';
            }

        } else {
            $data['given_project_folder_name'] = '';
        }

        if ($which_files == 'with_project_with_task' && $task_id != 0) {
            $a_task = $this->Filemanager_model->getTask($task_id);
            if ($a_task) {
                $data['task_number'] = $a_task->task_number;
            } else {
                $data['task_number'] = '';
            }

        } else {
            $data['task_number'] = '';
        }

        $i = 0;
        if ($data['files']) {
            foreach ($data['files'] as $a_file) {

                $data['files'][$i]->created_at_timestamp = $a_file->created_at;
                $a_file->created_at = $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($a_file->created_at);

                $data['files'][$i]->file_uploaded_by_user_fullname = $this->getUser_FullName($a_file->file_uploaded_by);
                $i++;
            }
        }


        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if($project_id != 0){
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view
        }else{
            $data['projectroom_menu_section'] = null;
        }


        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("file_manager_module/file_manager_file_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function showFilesByAjax()
    {

    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getUser_FullName($user_id)
    {
        $a_user_row = $this->Filemanager_model->getUser($user_id);

        if ($a_user_row) {
            return $a_user_row->first_name . ' ' . $a_user_row->last_name;
        } else {
            return $this->lang->line('unknown_text');
        }
    }

    public function deleteFile()
    {
        $which_files = $this->uri->segment(2);
        $project_id = $this->uri->segment(3);
        $task_id = $this->uri->segment(4);
        $file_id = $this->uri->segment(6);

        $is_deleted = $this->Filemanager_model->deleteFile($file_id);

        if ($is_deleted == true) {
            $a_file_info = $this->Filemanager_model->getAFile($file_id);

            $this->session->set_flashdata('file_delete_success', 'file_delete_success');
            $this->session->set_flashdata('deleted_file_name', $a_file_info->file_name);
        }

        if ($which_files = 'without_project') {
            return redirect('file_manager_module/files_without_project');
        }
        if ($which_files = 'with_project_without_task' && $project_id != 0) {
            return redirect('file_manager_module/files_with_project_without_task/' . $project_id);
        }
        if ($which_files = 'with_project_with_task' && $project_id != 0 && $task_id != 0) {
            return redirect('file_manager_module/files_with_project_with_task/' . $project_id . '/' . $task_id);
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getUploadFileForm_WithoutProject()
    {
        $this->getUploadForm('without_project', 0, 0); //no project_id = 0; no task_id = 0
    }

    public function getUploadFileForm_WithProjectWithoutTask()
    {
        $project_id = $this->uri->segment(3);

        $project_folder_exist = $this->Filemanager_model->checkIfUndeletedProjectFolderExist($project_id);

        if (!$project_folder_exist) {
            redirect('users/auth/does_not_exist');
        } else {
            $this->getUploadForm('with_project_without_task', $project_id, 0);                         //task_id = 0
        }
    }

    public function getUploadFileForm_WithProjectWithTask()
    {
        $project_id = $this->uri->segment(3);
        $task_id = $this->uri->segment(4);

        $project_folder_exist = $this->Filemanager_model->checkIfUndeletedProjectFolderExist($project_id);

        if (!$project_folder_exist) {
            redirect('users/auth/does_not_exist');
        } else {
            $this->getUploadForm('with_project_with_task', $project_id, $task_id);                      //task_id = 0
        }

    }

    //set project_id or task_id to zero if not available
    public function getUploadForm($what_form, $project_id, $task_id)
    {
        $this->lang->load('file_manager_file_upload_form');

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        $does_project_folder_exist = false;
        if ($what_form != 'without_project' && $project_id != 0) {

            $a_project_folder = $this->Filemanager_model->getProjectFolder($project_id);

            if ($a_project_folder && $a_project_folder->project_folder_name != '') {
                $data['given_project_folder_name'] = $a_project_folder->project_folder_name;
                $does_project_folder_exist = true;
            } else {
                $data['given_project_folder_name'] = '';
                $does_project_folder_exist = false;
            }

        } else {
            $data['given_project_folder_name'] = '';
            $does_project_folder_exist = false;
        }

        if($what_form != 'without_project' && $project_id != 0 && $does_project_folder_exist == false){
            redirect('users/auth/does_not_exist');
        }

        if ($what_form == 'with_project_with_task' && $task_id != 0) {
            $a_task = $this->Filemanager_model->getTask($task_id);
            if ($a_task) {
                $data['task_number'] = $a_task->task_number;
            } else {
                $data['task_number'] = '';
            }

        } else {
            $data['task_number'] = '';
        }


        $data['dropzone_allowed_types'] = $this->custom_file_library->getDropzoneAllowedTypes();
        $data['dropzone_max_size'] = $this->custom_file_library->getDropzoneMaxFileSize();
        $data['dropzone_max_file_number'] = $this->custom_file_library->getDropzoneMaxNumberOfFiles();


        $data['what_form'] = $what_form;
        $data['project_id'] = $project_id;
        $data['task_id'] = $task_id;

        if($project_id != 0){
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view
        }else{
            $data['projectroom_menu_section'] = null;
        }

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("file_manager_module/file_manager_file_upload_form_page", $data);
        $this->load->view("common_module/footer");
    }

    /*----------------------------File Uploading Starts---------------------------------------------------------------*/

    public function uploadFile()
    {
        $single_multiple_or_dropzone_files = $this->input->post('single_multiple_or_dropzone_files');
        if ($single_multiple_or_dropzone_files == 'single') {
            $this->session->set_flashdata('single_file_selected', 'single_file_selected');
            $this->uploadSingleFile();
        } elseif ($single_multiple_or_dropzone_files == 'multiple') {
            $this->session->set_flashdata('multiple_files_selected', 'multiple_files_selected');
            $this->uploadMultipleFiles();
        } else {
            $this->uploadFileWithDropZone(); // inactive as dropzone hits the url through js
        }

    }


    public function uploadSingleFile()
    {
        $what_form = $this->input->post('what_form');
        $given_project_folder_name = $this->input->post('given_project_folder_name');

        $data['file_note'] = $this->input->post('single_file_note');

        $data['project_id'] = $this->input->post('project_id');
        if ($data['project_id'] != 0) {
            $data['with_project'] = 1;
        } else {
            $data['with_project'] = 0;
        }

        $data['task_id'] = $this->input->post('task_id');
        if ($data['task_id'] != 0) {
            $data['with_task'] = 1;
        } else {
            $data['with_task'] = 0;
        }

        $data['created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data['file_main_directory'] = $this->custom_file_library->getMainDirectory();
        $data['file_uploaded_by'] = $this->session->userdata('user_id');


        if (($_FILES['single_file_to_upload']['name']) != '') {

            $field_name = 'single_file_to_upload';
            $file_details = $_FILES['single_file_to_upload'];


            if ($given_project_folder_name == '') {
                $file_details = $this->custom_file_library->uploadFile($file_details, $field_name); //to main_directory
            } else {
                $data['file_project_folder_name'] = $given_project_folder_name;
                $file_details =
                    $this->custom_file_library
                        ->uploadFile($file_details, $field_name, $given_project_folder_name);        //to main_directory/project_folder
            }


            if ($file_details == false) {
                if ($what_form == 'without_project') {
                    redirect('file_manager_module/get_upload_file_form_without_project');
                }

            } else {
                $data['file_name'] = $file_details['file_name'];
                $data['file_size'] = $file_details['file_size'];
                $data['file_ext'] = $file_details['file_ext'];

                $this->Filemanager_model->insertFile($data);
                $file_id = $this->db->insert_id();
                $array_of_file_id = array();
                $array_of_file_id[] = $file_id;
            }

        } else {
            $this->session->set_flashdata('no_file_selected', 'no_file_selected');
        }

        /*----------------------------------------------*/
        if ($what_form == 'without_project') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }

            return redirect('file_manager_module/get_upload_file_form_without_project');
        } else if ($what_form == 'with_project_without_task') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }

            return redirect('file_manager_module/get_upload_file_form_with_project_without_task/' . $data['project_id']);
        } else if ($what_form == 'with_project_with_task') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }

            return redirect('file_manager_module/get_upload_file_form_with_project_with_task/' . $data['project_id'] . '/' . $data['task_id']);
        } else {
            return redirect('/');
        }


    }

    public function uploadMultipleFiles()
    {

        $what_form = $this->input->post('what_form');
        $given_project_folder_name = $this->input->post('given_project_folder_name');

        $multiple_files_note = $this->input->post('multiple_files_note');

        $data['project_id'] = $this->input->post('project_id');
        if ($data['project_id'] != 0) {
            $data['with_project'] = 1;
        } else {
            $data['with_project'] = 0;
        }

        $data['task_id'] = $this->input->post('task_id');
        if ($data['task_id'] != 0) {
            $data['with_task'] = 1;
        } else {
            $data['with_task'] = 0;
        }

        $data['created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data['file_main_directory'] = $this->custom_file_library->getMainDirectory();
        $data['file_uploaded_by'] = $this->session->userdata('user_id');


        $upload_errors = array();
        $upload_success = array();
        $array_of_file_id = array();


        if ($_FILES != null) {
            foreach ($_FILES as $A_FILE_FIELD_NAME => $A_FILE_DETAILS) {
                if ($A_FILE_DETAILS['name'] != '') {

                    preg_match_all('!\d+!', $A_FILE_FIELD_NAME, $last_part);

                    $data['file_note'] = $this->input->post('multiple_files_note' . $last_part[0][0]);


                    if ($given_project_folder_name == '') {
                        $data['file_project_folder_name'] = '';
                        //to main_directory
                        //actually uploads a single file at a time
                        $returned = $this->custom_file_library->uploadMultipleFiles($A_FILE_DETAILS, $A_FILE_FIELD_NAME);

                    } else {

                        $data['file_project_folder_name'] = $given_project_folder_name;
                        //to main_directory/project_folder
                        //actually uploads a single file at a time
                        $returned = $this->custom_file_library->uploadMultipleFiles($A_FILE_DETAILS, $A_FILE_FIELD_NAME, $given_project_folder_name);

                    }


                    if (array_key_exists('upload_errors', $returned)) {
                        $upload_errors[] = 'For ' . $A_FILE_DETAILS['name'] . ':' . '<br>' . $returned['upload_errors'] . '<br><br>';
                    } else {
                        $upload_success[] = $A_FILE_DETAILS['name'] . ': ' . 'Successfully Uploaded' . '<br>';

                        $data['file_name'] = $returned['file_name'];
                        $data['file_size'] = $returned['file_size'];
                        $data['file_ext'] = $returned['file_ext'];

                        $this->Filemanager_model->insertFile($data);
                        $file_id = $this->db->insert_id();
                        $array_of_file_id[] = $file_id;
                    }

                }
            }
        } else {
            $this->session->set_flashdata('no_file_selected', 'no_file_selected');
        }


        if ($upload_errors) {
            $this->session->set_flashdata('multi_upload_errors', $upload_errors);
        }
        if ($upload_success) {
            $this->session->set_flashdata('multi_upload_success', $upload_success);
        }

        /*------------------------------------*/
        if ($what_form == 'without_project') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);

            }

            return redirect('file_manager_module/get_upload_file_form_without_project');
        } else if ($what_form == 'with_project_without_task') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }

            return redirect('file_manager_module/get_upload_file_form_with_project_without_task/' . $data['project_id']);
        } else if ($what_form == 'with_project_with_task') {

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }

            return redirect('file_manager_module/get_upload_file_form_with_project_with_task/' . $data['project_id'] . '/' . $data['task_id']);
        } else {

            return redirect('/');
        }


    }


    public function uploadFileWithDropZone()
    {
        /*print_r($_POST) . '<br>';
        print_r($_FILES) . '<br>';*/

        $array_of_file_id = array();
        $what_form = $this->input->post('what_form');

        $given_project_folder_name = $this->input->post('given_project_folder_name');


        $data['project_id'] = $this->input->post('project_id');
        if ($data['project_id'] != 0) {
            $data['with_project'] = 1;
        } else {
            $data['with_project'] = 0;
        }

        $data['task_id'] = $this->input->post('task_id');
        if ($data['task_id'] != 0) {
            $data['with_task'] = 1;
        } else {
            $data['with_task'] = 0;
        }


        $data['created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data['file_main_directory'] = $this->custom_file_library->getMainDirectory();
        $data['file_uploaded_by'] = $this->session->userdata('user_id');

        if (!empty($_FILES)) {

            $i = 0;
            foreach ($_FILES['file']['name'] as $A_FILE_NAME) {

                $new_file_name = $this->custom_file_library->getNewFileName($_FILES['file']['name'][$i]);

                $move_from = $_FILES['file']['tmp_name'][$i];
                $main_dir = $this->custom_file_library->getMainDirectory();

                if ($given_project_folder_name == '') {
                    $data['file_project_folder_name'] = '';
                    $move_to = FCPATH . $main_dir . '/' . $new_file_name;
                } else {
                    $data['file_project_folder_name'] = $given_project_folder_name;
                    $move_to = FCPATH . $main_dir . '/' . $given_project_folder_name . '/' . $new_file_name;
                }

                $this->custom_file_library->uploadViaDropzone($move_from, $move_to);

                $exploded_original_filename = explode(".", $_FILES['file']['name'][$i]);
                $ext = end($exploded_original_filename);

                $data['file_name'] = $new_file_name;
                $data['file_size'] = round($_FILES['file']['size'][$i] / 1024, 2);                          //in kB
                $data['file_ext'] = '.' . $ext;

                /*print_r($data);*/

                $this->Filemanager_model->insertFile($data);

                $file_id = $this->db->insert_id();

                $array_of_file_id[] = $file_id;

                $i++;

            }

            if ($array_of_file_id != null) {
                $this->createLogForUploadedFiles($array_of_file_id, $what_form, $data['project_id'], $data['task_id']);
            }


        }

    }

    /*log starts*/
    public function createLogForUploadedFiles($array_of_file_id, $what_form, $project_id, $task_id)
    {

        $created_by = $this->session->userdata('user_id');

        $i = 0;
        $file_list_string = '';
        if ($array_of_file_id) {

            foreach ($array_of_file_id as $a_file_id) {

                if ($i != 0) {
                    $file_list_string .= ',' . $a_file_id;
                } else {
                    $file_list_string .= $a_file_id;
                }

                $i++;
            }
        }

        if ($what_form == 'without_project') {
            $other_information['file_belongs_to_project'] = 0;
        }

        if ($what_form == 'with_project_without_task' && $project_id != 0) {

            // no need to set other information
        }

        if ($what_form == 'with_project_with_task' && $project_id != 0 && $task_id != 0) {

            $other_information['task_belongs_to_project'] = $project_id;

        }

        $other_information['uploaded_file_list'] = $file_list_string;
        $encoded_other_information = json_encode($other_information);


        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('staff') && !$this->ion_auth->is_admin()) {
            $activity_by = 'staff';
        } else if ($this->ion_auth->in_group('client')) {
            $activity_by = 'client';
        } else {
            $activity_by = 'unidentified_group';
        }


        if ($what_form == 'without_project') {
            //acknowledge nobody
            //or maybe acknowledge  admins  . . .

            //admin group_id =1
            $active_admins = $this->Filemanager_model->getActiveAdmins();
            if ($active_admins) {

                foreach ($active_admins as $an_active_admin) {

                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        $created_by,                                                      //1.    $created_by
                        $an_active_admin->id,                                             //2.    $created_for
                        'file',                                                           //3.    $type
                        0,                                                                //4.    $type_id
                        'uploaded_file',                                                  //5.    $activity
                        $activity_by,                                                     //6.    $activity_by
                        'admin',                                                          //7.    $activity_for
                        '',                                                               //8.    $sub_type
                        '',                                                               //9.    $sub_type_id
                        '',                                                               //10.   $super_type
                        '',                                                               //11.   $super_type_id
                        $encoded_other_information,                                       //12.   $other_information
                        ''                                                                //13.   $change_list
                    );
                    /*creating log ends*/

                }
            }
        }

        if ($what_form == 'with_project_without_task' && $project_id != 0) {
            //acknowledge client and assigned staffs to the project

            $project_info = $this->Filemanager_model->getProject($project_id);
            $client_id = $project_info->client_id;

            /*creating log starts*/

            //for client
            $this->custom_log_library->createALog
            (
                $created_by,                                                      //1.    $created_by
                $client_id,                                                       //2.    $created_for
                'file',                                                           //3.    $type
                0,                                                                //4.    $type_id
                'uploaded_file',                                                  //5.    $activity
                $activity_by,                                                     //6.    $activity_by
                'client',                                                         //7.    $activity_for
                '',                                                               //8.    $sub_type
                '',                                                               //9.    $sub_type_id
                'project',                                                        //10.   $super_type
                $project_id,                                                      //11.   $super_type_id
                $encoded_other_information,                                       //12.   $other_information
                ''                                                                //13.   $change_list
            );

            //for assigned staffs to project
            $assigned_staffs_to_project = $this->Filemanager_model->getAssignedStaffsToProject($project_id);

            if ($assigned_staffs_to_project) {
                foreach ($assigned_staffs_to_project as $an_assigned_staff_to_project) {
                    $this->custom_log_library->createALog
                    (
                        $created_by,                                                      //1.    $created_by
                        $an_assigned_staff_to_project->staff_id,                          //2.    $created_for
                        'file',                                                           //3.    $type
                        0,                                                                //4.    $type_id
                        'uploaded_file',                                                  //5.    $activity
                        $activity_by,                                                     //6.    $activity_by
                        'staff',                                                          //7.    $activity_for
                        '',                                                               //8.    $sub_type
                        '',                                                               //9.    $sub_type_id
                        'project',                                                        //10.   $super_type
                        $project_id,                                                      //11.   $super_type_id
                        $encoded_other_information,                                       //12.   $other_information
                        ''                                                                //13.   $change_list
                    );
                }
            }

            /*creating log ends*/

        }

        if ($what_form == 'with_project_with_task' && $project_id != 0 && $task_id != 0) {
            //acknowledge  assigned staffs to the task

            $task_info = $this->Filemanager_model->getTask($task_id);

            $project_info = $this->Filemanager_model->getProject($project_id);
            $client_id = $project_info->client_id;

            /*creating log starts*/

            //for client
            $this->custom_log_library->createALog
            (
                $created_by,                                                      //1.    $created_by
                $client_id,                                                       //2.    $created_for
                'file',                                                           //3.    $type
                0,                                                                //4.    $type_id
                'uploaded_file',                                                  //5.    $activity
                $activity_by,                                                     //6.    $activity_by
                'client',                                                         //7.    $activity_for
                '',                                                               //8.    $sub_type
                '',                                                               //9.    $sub_type_id
                'task',                                                           //10.   $super_type
                $task_id,                                                         //11.   $super_type_id
                $encoded_other_information,                                       //12.   $other_information
                ''                                                                //13.   $change_list
            );

            //for assigned staffs to the task
            $assigned_staffs_to_task = $this->Filemanager_model->getAssignedStaffsToTask($task_id);
            if ($assigned_staffs_to_task) {
                foreach ($assigned_staffs_to_task as $an_assigned_staff_to_task) {
                    $this->custom_log_library->createALog
                    (
                        $created_by,                                                      //1.    $created_by
                        $an_assigned_staff_to_task->staff_id,                             //2.    $created_for
                        'file',                                                           //3.    $type
                        0,                                                                //4.    $type_id
                        'uploaded_file',                                                  //5.    $activity
                        $activity_by,                                                     //6.    $activity_by
                        'staff',                                                          //7.    $activity_for
                        '',                                                               //8.    $sub_type
                        '',                                                               //9.    $sub_type_id
                        'task',                                                           //10.   $super_type
                        $task_id,                                                         //11.   $super_type_id
                        $encoded_other_information,                                       //12.   $other_information
                        ''                                                                //13.   $change_list
                    );
                }
            }

            /*creating log ends*/


        }

    }
    /*log ends*/

    /*----------------------------File Uploading Ends-----------------------------------------------------------------*/

    public function showFileOverview()
    {
        $this->lang->load('file_manager_file_overview');

        $file_id = $this->uri->segment(3);

        $data['a_file_info'] = $this->Filemanager_model->getAFile($file_id);

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($data['a_file_info']) {

            $data['a_file_info']->created_at =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDateAndTime($data['a_file_info']->created_at);

            if ($data['a_file_info']->project_id != 0) {
                $a_project_info = $this->Filemanager_model->getProject($data['a_file_info']->project_id);
                $data['a_file_info']->project_name = $a_project_info->project_name;
            } else {
                $data['a_file_info']->project_name = $this->lang->line('unavailable_text');
            }

            if ($data['a_file_info']->task_id != 0) {
                $a_task_info = $this->Filemanager_model->getTask($data['a_file_info']->task_id);
                $data['a_file_info']->task_number = $a_task_info->task_number;
            } else {
                $data['a_file_info']->task_number = $this->lang->line('unavailable_text');
            }

            if ($data['a_file_info']->file_uploaded_by != 0) {
                $a_user_info = $this->Filemanager_model->getUser($data['a_file_info']->file_uploaded_by);
                $data['a_file_info']->file_uploaded_by_user_fullname = $a_user_info->first_name . ' ' . $a_user_info->last_name;
            } else {
                $data['a_file_info']->file_uploaded_by_user_fullname = $this->lang->line('unavailable_text');
            }
        }

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("file_manager_module/file_manager_file_overview_page", $data);
        $this->load->view("common_module/footer");

    }

}