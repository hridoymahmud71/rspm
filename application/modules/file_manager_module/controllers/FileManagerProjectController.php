<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileManagerProjectController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }
        $this->load->model('settings_module/Settings_model');
        $this->load->model('timezone_module/Timezone_model');
        $this->load->model('auth/Ion_auth_model');


        $this->load->model('project_module/Project_model');
        $this->load->model('file_manager_module/Filemanager_model');


        $this->load->library('form_validation');
        $this->load->library('session');

        //ion auth lib
        $this->load->library('auth/ion_auth');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        //customized lib from libraries
        $this->load->library('custom_datetime_library');

        //$this->lang->load('file_manager_project_list');

    }

    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    public function showAllFolders()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->getProjectList('all_projects');
        }

    }

    public function showMyFolders()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        } else {
            $this->getProjectList('my_projects');

        }
    }

    public function getFolderSuffix($suffix_for)
    {
        if ($suffix_for == 'project') {
            $project_folder_suffix = '_pf';
            return $project_folder_suffix;
        }

        //currently not in use as not creating a separate folder for a task
        if ($suffix_for == 'task') {
            $task_folder_suffix = '_tf';
            return $task_folder_suffix;
        }

    }

    public function getProjectList($whose_project)
    {
        $this->lang->load('file_manager_project_list');

        $data['whose_project'] = $whose_project;


        if ($whose_project == 'all_projects') {

            $data['projects'] = $this->Filemanager_model->getProjectFolders();
        }

        if ($whose_project == 'my_projects') {

            $user_id = $this->session->userdata('user_id');

            if ($this->ion_auth->in_group('staff', $user_id)) {
                $staff_or_client = 'staff';
            } elseif ($this->ion_auth->in_group('client', $user_id)) {
                $staff_or_client = 'client';
            } else {
                $staff_or_client = '';
            }

            if ($staff_or_client == 'staff') {
                $data['projects'] = $this->Filemanager_model->getProjectFoldersByStaff($user_id);

            } elseif ($staff_or_client == 'client') {
                $data['projects'] = $this->Filemanager_model->getProjectFoldersByClient($user_id);

            } else {
                $data['projects'] = null;
            }

        }

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }


        if ($data['projects'] != null) {

            $i = 0;
            foreach ($data['projects'] as $a_project) {

                if ($a_project->project_name && $a_project->project_name != '') {

                    $main_directory = $a_project->project_folder_main_directory;

                    if (!is_dir(FCPATH . $main_directory . '/' . $a_project->project_folder_name)) {
                        $data['projects'][$i]->project_folder_name = '';
                    }

                }

                $i++;

            }
        }

        $data['without_project_file_count'] = $this->Filemanager_model->countFilesOfAProject(0); //param: $project_id = 0
        if (!$data['without_project_file_count']) {
            $data['without_project_file_count'] = 0;
        }
        $data['without_project_total_file_size'] = $this->Filemanager_model->getTotalFileSizeOfAProject(0);     //param: $project_id = 0

        if (!$data['without_project_total_file_size']) {
            $data['without_project_total_file_size'] = 0;
        } else {
            $data['without_project_total_file_size_in_MB']
                = number_format($data['without_project_total_file_size'] / 1024, 3);
            $data['without_project_total_file_size'] = number_format($data['without_project_total_file_size'], 3);
        }


        foreach ($data['projects'] as $a_project) {
            if ($a_project->project_folder_name != '' || $a_project->project_folder_name != null) {
                $a_project->file_count = $this->Filemanager_model->countFilesOfAProject($a_project->project_id);
                if ($a_project->file_count > 0) {
                    $a_project->total_file_size = $this->Filemanager_model->getTotalFileSizeOfAProject($a_project->project_id);

                    if ($a_project->total_file_size == null) {
                        $a_project->total_file_size = 0.000;
                        $a_project->total_file_size_in_MB = 0.000;
                    } else {
                        $a_project->total_file_size_in_MB
                            = number_format($a_project->total_file_size / 1024, 3);
                        $a_project->total_file_size = number_format($a_project->total_file_size, 3);
                    }
                } else {
                    $a_project->total_file_size = 0.000;
                    $a_project->total_file_size_in_MB = 0.000;
                    $a_project->file_count = 0;
                }

            } else {
                $a_project->total_file_size = 0.000;
                $a_project->total_file_size_in_MB = 0.000;
                $a_project->file_count = 0;
            }
        }

        //print_r($data['projects']);

        $data['project_total_file_number'] = $this->lang->line('unknown_text');

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("file_manager_module/file_manager_project_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function createProjectFolder()
    {
        $project_id = $this->uri->segment(4);
        $a_project_info = $this->Filemanager_model->getAProject($project_id);
        $project_name = $a_project_info->project_name;

        $project_folder_suffix = $this->getFolderSuffix('project');
        $project_folder_name = $project_name . $project_folder_suffix;

        $project_folder_created = $this->createProjectDirectory($project_folder_name);

        $data['project_id'] = $project_id;
        $data['project_name'] = $project_name;
        $data['project_folder_main_directory'] = $this->getMainDirectory();
        $data['project_folder_name'] = $project_folder_name;

        if ($project_folder_created == true) {

            $this->session->set_flashdata('successfull', $this->lang->line('successfull_text'));
            $this->session->set_flashdata('folder_create_success', $project_folder_name . $this->lang->line('project_folder_create_success_text'));
            $this->Filemanager_model->createAProjectFolder($data);

        } else {

            $this->session->set_flashdata('unsuccessfull', $this->lang->line('unsuccessfull_text'));
            $this->session->set_flashdata('folder_create_error', $project_folder_name . $this->lang->line('project_folder_create_error_text'));

        }


        if ($this->uri->segment(2) == 'all_folders') {
            redirect('file_manager_module/all_folders');

        } else if ($this->uri->segment(2) == 'my_folders') {
            redirect('file_manager_module/my_folders');
        } else {
            redirect('file_manager_module/my_folders');  //default
        }


    }


    public function getMainDirectory()
    {
        $a_settings = $this->custom_settings_library->getASettings('file_settings', 'upload_path');

        if ($a_settings) {
            $main_directory = $a_settings->settings_value;
        }
        if ($main_directory && $main_directory != '') {

            return $main_directory;

        } else {

            $this->session->set_flashdata('unsuccessfull', $this->lang->line('unsuccessfull_text'));
            $this->session->set_flashdata('create_main_directory', $this->lang->line('create_main_directory_text'));
            redirect('settings_module/file_settings');

        }

    }


    public function createProjectDirectory($project_folder_name)
    {
        $main_directory = $this->getMainDirectory();

        if ($main_directory) {

            if (is_dir(FCPATH . $main_directory . '/' . $project_folder_name . '/')) {

                $this->session->set_flashdata('unsuccessfull', $this->lang->line('unsuccessfull_text'));
                $this->session->set_flashdata('directory_exists', $main_directory . '/' . $project_folder_name . '/' . $this->lang->line('directory_exists_text'));
                return false;

            } else {
                //reomove previous folder from table
                $removed = $this->Filemanager_model->reomoveProjectFolderFrom_DB_Table($project_folder_name);
                if ($removed == true) {
                    if (mkdir(FCPATH . '/' . $main_directory . '/' . $project_folder_name . '/', 0777, TRUE)) {
                        $this->session->set_flashdata('successfull', $this->lang->line('successfull_text'));
                        $this->session->set_flashdata('folder_create_success', $main_directory . '/' . $project_folder_name . '/' . $this->lang->line('project_folder_create_success_text'));
                        return true;
                    } else {
                        return false;
                    }
                }


            }
        }

    }


    public function deleteProjectFolder()
    {

        $project_folder_id = $this->uri->segment(4);

        //all_folders or my_folders
        $which_folder = $this->uri->segment(2);

        $a_project_folder = $this->Filemanager_model->getOnlyProjectFolder($project_folder_id);

        $is_folder_deleted = $this->Filemanager_model->deleteProjectFolder($project_folder_id);

        if ($is_folder_deleted) {

            $this->session->set_flashdata('successfull', $this->lang->line('successfull_text'));
            $delete_success_message =
                sprintf($this->lang->line('project_folder_delete_success_text')
                    , $a_project_folder->project_folder_name);
            $this->session->set_flashdata('folder_delete_success', $delete_success_message);

            redirect('file_manager_module/' . $which_folder);
        }


    }

    public function undeleteProjectFolder()
    {

        $project_folder_id = $this->uri->segment(4);

        //all_folders or my_folders
        $which_folder = $this->uri->segment(2);

        $a_project_folder = $this->Filemanager_model->getOnlyProjectFolder($project_folder_id);

        $is_folder_undeleted = $this->Filemanager_model->undeleteProjectFolder($project_folder_id);

        if ($is_folder_undeleted) {

            $this->session->set_flashdata('successfull', $this->lang->line('successfull_text'));
            $undelete_success_message =
                sprintf($this->lang->line('project_folder_undelete_success_text')
                    , $a_project_folder->project_folder_name);
            $this->session->set_flashdata('folder_undelete_success', $undelete_success_message);

            redirect('file_manager_module/' . $which_folder);
        }


    }


    /*-----------------------------------------------------------------------------------------------------------------*/

    //shows task list
    public function showFilesInAProject()
    {
        $this->lang->load('file_manager_task_list');

        $project_id = $this->uri->segment(3);

        $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view


        $a_project_folder = $this->Filemanager_model->getProjectFolder($project_id);
        if ($a_project_folder) {
            $data['project_folder_name'] = $a_project_folder->project_folder_name;
        } else {
            $data['project_folder_name'] = $this->lang->line('unknown_text');
        }

        $a_project = $this->Filemanager_model->getProject($project_id);
        if ($a_project) {
            $data['project_name'] = $a_project->project_name;
        } else {
            $data['project_name'] = $this->lang->line('unknown_text');
        }


        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        $data['project_id'] = $project_id;
        $data['all_tasks'] = $this->Filemanager_model->getTasks($project_id);

        $data['without_task_file_count'] = $this->Filemanager_model->countFilesOfATask(0); //param: $project_id = 0
        if (!$data['without_task_file_count']) {
            $data['without_task_file_count'] = 0;
        }
        $data['without_task_total_file_size'] = $this->Filemanager_model->getTotalFileSizeOfATask(0);     //param: $project_id = 0

        if (!$data['without_task_total_file_size']) {
            $data['without_task_total_file_size'] = 0;
        } else {
            $data['without_task_total_file_size_in_MB']
                = number_format($data['without_task_total_file_size'] / 1024, 3);
            $data['without_task_total_file_size'] = number_format($data['without_task_total_file_size'], 3);
        }

        $i = 0;
        foreach ($data['all_tasks'] as $a_task) {

            if ($data['is_admin'] == 'admin') {

                $data['all_tasks'][$i]->is_related_task = 'related_task';

            } else if ($data['is_client'] == 'client') {

                $data['all_tasks'][$i]->is_related_task = 'related_task';

            } else if ($data['is_admin'] == 'not_admin' && $data['is_staff'] == 'staff') {

                $is_staff_assigned =
                    $this->FileManagerModel
                        ->checkIfStaffIsAssignedToTask($this->session->userdata('user_id'), $a_task->task_id);

                if ($is_staff_assigned) {

                    $data['all_tasks'][$i]->is_related_task = 'related_task';

                } else {

                    $data['all_tasks'][$i]->is_related_task = 'not_related_task';
                }

            } else {

                $data['all_tasks'][$i]->is_related_task = 'not_related_task';
            }
            $i++;
        }

        foreach ($data['all_tasks'] as $a_task) {

            $a_task->file_count = $this->Filemanager_model->countFilesOfATask($a_task->task_id);

            if ($a_task->file_count > 0) {
                $a_task->total_file_size = $this->Filemanager_model->getTotalFileSizeOfATask($a_task->task_id);

                if ($a_task->total_file_size == null) {
                    $a_task->total_file_size = 0.000;
                    $a_task->total_file_size_in_MB = 0.000;
                } else {
                    $a_task->total_file_size_in_MB
                        = number_format($a_task->total_file_size / 1024, 3);
                    $a_task->total_file_size = number_format($a_task->total_file_size, 3);
                }
            } else {
                $a_task->total_file_size = 0.000;
                $a_task->total_file_size_in_MB = 0.000;
                $a_task->file_count = 0;
            }

        }

        /*need a way to find  no of files*/
        $data['task_total_file_size'] = $this->lang->line('unknown_text');

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("file_manager_module/file_manager_task_list_page", $data);
        $this->load->view("common_module/footer");
    }


}
