<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


/*--------------------------------------------------------------------------------------------------------------------*/
$route['file_manager_module/all_folders'] = 'FileManagerProjectController/showAllFolders';
$route['file_manager_module/my_folders'] = 'FileManagerProjectController/showMyFolders';

$route['file_manager_module/all_folders/create_project_folder/(:any)'] = 'FileManagerProjectController/createProjectFolder';
$route['file_manager_module/my_folders/create_project_folder/(:any)'] = 'FileManagerProjectController/createProjectFolder';

$route['file_manager_module/all_folders/delete_project_folder/(:any)'] = 'FileManagerProjectController/deleteProjectFolder';
$route['file_manager_module/my_folders/delete_project_folder/(:any)'] = 'FileManagerProjectController/deleteProjectFolder';

$route['file_manager_module/all_folders/undelete_project_folder/(:any)'] = 'FileManagerProjectController/undeleteProjectFolder';
$route['file_manager_module/my_folders/undelete_project_folder/(:any)'] = 'FileManagerProjectController/undeleteProjectFolder';
/*--------------------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------------------------------------*/
$route['file_manager_module/files_without_project'] = 'FileManagerFileController/showFilesWithoutProject';
//  (:any) = project_id
$route['file_manager_module/files_with_project_without_task/(:any)'] =
    'FileManagerFileController/showFilesWithProjectWithoutTask';
//  (:any)/(:any) = project_id/task_id
$route['file_manager_module/files_with_project_with_task/(:any)/(:any)'] =
    'FileManagerFileController/showFilesWithProjectWithTask';


$route['file_manager_module/show_files_by_ajax'] =
    'FileManagerFileController/showFilesByAjax';

$route['file_manager_module/get_upload_file_form_without_project'] =
    'FileManagerFileController/getUploadFileForm_WithoutProject';
//  (:any) = project_id
$route['file_manager_module/get_upload_file_form_with_project_without_task/(:any)'] =
    'FileManagerFileController/getUploadFileForm_WithProjectWithoutTask';
//  (:any)/(:any) = project_id/task_id
$route['file_manager_module/get_upload_file_form_with_project_with_task/(:any)/(:any)'] =
    'FileManagerFileController/getUploadFileForm_WithProjectWithTask';
/*--------------------------------------------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------------------------------------------*/
$route['file_manager_module/files_in_a_project/(:any)'] = 'FileManagerProjectController/showFilesInAProject';



/*--------------------------------------------------------------------------------------------------------------------*/
$route['file_manager_module/upload_file'] = 'FileManagerFileController/uploadFile';
$route['file_manager_module/upload_file_with_dropzone'] = 'FileManagerFileController/uploadFileWithDropzone';

// (:any)/(:any) = project_id/task_id
$route['file_manager_module/without_project/(:any)/(:any)/delete_file/(:any)'] = 'FileManagerFileController/deleteFile';
$route['file_manager_module/with_project_without_task/(:any)/(:any)/delete_file/(:any)'] = 'FileManagerFileController/deleteFile';
$route['file_manager_module/with_project_with_task/(:any)/(:any)/delete_file/(:any)'] = 'FileManagerFileController/deleteFile';

//  (:any) = $file_id
$route['file_manager_module/file_overview/(:any)'] = 'FileManagerFileController/showFileOverview';
/*--------------------------------------------------------------------------------------------------------------------*/















