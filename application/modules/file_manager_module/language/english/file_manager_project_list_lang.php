<?php

/*page texts*/
$lang['page_title_text'] = 'File Manager';
$lang['page_subtitle_text'] = 'View Files | Upload  Files';

$lang['go_upload_button_text'] = 'Upload File';

$lang['table_title_text'] = 'Project List';
$lang['table_title_all_projects_text'] = 'All Project Folders List';
$lang['table_title_my_projects_text'] = 'My Project Folders List';


$lang['no_project_found_text'] = 'No Project Is Found !';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Main Directiory';
$lang['breadcrumb_page_text'] = 'Project Files';

/*Column names of the table*/

$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['which_folder_text'] = 'Which folder';
$lang['option_files_without_project_text'] = 'Files without project ';
$lang['option_no_folder_text'] = 'No folder';
$lang['option_with_folder_text'] = 'With folder';

$lang['deleted_or_not_text'] = 'Deleted or not';
$lang['option_deleted_text'] = 'Deleted';
$lang['option_not_deleted_text'] = 'Not Deleted';

$lang['column_project_folder_name_text'] = 'Project Folder';
$lang['column_project_name_text'] = 'Project Name';
$lang['column_total_file_size_text'] = 'Total File size';
$lang['column_total_file_number_text'] = 'Total No. of Files';
$lang['column_project_status_text'] = ' Project Status';
$lang['column_upload_file_text'] = 'Upload File';
$lang['column_actions_text'] = 'Actions';

$lang['files_without_project_text'] = 'Files Without Project';
$lang['create_folder_text'] = 'Create';
$lang['no_folder_text'] = 'No Folder';
$lang['unknown_text'] = 'Unknown';
$lang['unavailable_text'] = 'Unavailable';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

$lang['folder_deleted_text'] = 'Folder Deleted' ;

/*other texts*/
$lang['confirm_delete_text'] = 'Are You Sure To Delete This Project Folder ? ';
$lang['delete_success_text'] = 'Succesfully deleted the project folder.';

/*success messages*/
$lang['successfull_text'] = 'Succesfull';
$lang['project_folder_create_success_text'] = ' Successfully Created';
$lang['file_upload_success_text'] = ' Successfully Uploaded';

$lang['project_folder_delete_success_text'] = '%1$s Successfully deleted';
$lang['project_folder_undelete_success_text'] = '%1$s Successfully undeleted';



/*not success messages */
$lang['unsuccessfull_text'] = 'Unsuccesfull';
$lang['project_folder_create_error_text'] = ' Could not be Created';
$lang['directory_exists_text'] = '  Directory already exists';
$lang['file_upload_error_text'] = ' Not Uploaded';



/*sweetalert lang not working*/
$lang['swal_delete_title_text'] = 'Are you sure to delete this folder?';
$lang['swal_delete_confirm_button_text'] = 'Yes, delete this folder';
$lang['swal_delete_cancel_button_text'] = 'No, keep the folder';

$lang['swal_undelete_title_text'] = 'Are you sure to undelete this folder?';
$lang['swal_undelete_confirm_button_text'] = 'Yes, undelete this folder';
$lang['swal_undelete_cancel_button_text'] = 'No, keep as it is';

/*tooltip text*/
$lang['tooltip_create_project_folder_text'] = 'Create A Folder For The Project' ;

$lang['tooltip_activate_text'] = 'Activate Project' ;
$lang['tooltip_deactivate_text'] = 'Deactivate Project' ;

$lang['tooltip_view_file_without_project_text'] = 'View Files Without Project';
$lang['tooltip_view_file_project_text'] = 'View Files in the folder';
$lang['tooltip_upload_file_without_project_text'] = 'Upload File Without Project';
$lang['tooltip_upload_file_in_project_text'] = 'Upload File In the Project';
$lang['tooltip_delete_text'] = 'Delete Folder';
$lang['tooltip_undelete_text'] = 'Unelete Folder';






