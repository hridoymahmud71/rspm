<?php
/*page texts*/
$lang['page_title_text'] = 'Upload File(s)';
$lang['page_subtitle_text'] = 'Upload Single or Multiple Files';
$lang['box_title_text'] = 'Upload File To:';

$lang['upload_file_main_directory_text'] = 'Main Directory';

$lang['breadcrumb_all_project_folders_text'] = 'All';
$lang['breadcrumb_my_project_folders_text'] = 'My';

$lang['breadcrumb_section_files_without_project_text'] = 'Files Without Project';
$lang['breadcrumb_section_files_with_project_without_task_text'] = 'Files With Project but Without Task';
$lang['breadcrumb_section_files_with_project_with_task_text'] = 'Files With Project and With Task';

$lang['breadcrumb_page_text'] = 'Upload File(s)';


/*upload file form texts*/
$lang['single_file_text'] = 'Single File';
$lang['multiple_file_text'] = 'Multiple Files';
$lang['dropzone_file_text'] = 'DropZone';

$lang['dz_sending_text'] = 'Sending . . .';
$lang['dz_complete_text'] = 'Complete' ;

$lang['label_single_multiple_or_dropzone_files_text'] = 'Single , Mutiple or DropZone';
$lang['option_single_file_text'] = 'Single File';
$lang['option_multiple_file_text'] = 'Multiple Files (For smaller files)';
$lang['option_dropzone_file_text'] = 'Upload with DropZone (For larger files)';

$lang['file_number_text'] = 'File Number';

$lang['label_file_to_upload_text'] = 'File To Upload';
$lang['label_file_note_text'] = 'File Note';

$lang['placeholder_file_note_text'] = 'Add A short note to the file (optional)';

$lang['button_choose_file_text'] = 'Choose File';
$lang['button_add_file_text'] = 'Add Another File';
$lang['button_remove_file_text'] = 'Remove File';
$lang['button_submit_upload_file_text'] = 'Upload File(s)';
$lang['button_submit_upload_with_dropzone_text'] = 'Upload File(s) With DropZone';


/*validation error texts*/
$lang['no_file_selected_text'] = 'No file is selected';

/*sweetalert messages*/
$lang['swal_no_file_added_text'] = 'No File is Added';
$lang['swal_upload_limit_reached_text'] = 'Upload limit reached';
$lang['swal_maximum_limit_text'] = 'max no. file that can be uploaded at a time is ';


