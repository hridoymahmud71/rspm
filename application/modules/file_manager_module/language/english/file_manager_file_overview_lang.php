<?php


/*page texts*/
$lang['page_title_text'] = 'File OverView';

$lang['box_title_text'] = 'Overview';

$lang['breadcrumb_home_all_folders_text'] = 'All';
$lang['breadcrumb_home_my_folders_text'] = 'My';

$lang['breadcrumb_section_main_directory_text'] = 'M.D.';

$lang['breadcrumb_section_files_without_project_text'] = 'Files Without Project';

$lang['breadcrumb_page_text'] = 'File Overview';

/*--------------*/
$lang['unknown_text'] = 'Unknown';

/*section heading*/
$lang['section_heading_file_details_text'] = 'File Details';

/*rows*/
$lang['file_download_text'] = 'Download File';
$lang['file_name_text'] = 'File Name';
$lang['file_ext_text'] = 'File Format';
$lang['file_size_text'] = 'File Size';
$lang['file_uploaded_by_text'] = 'Uploaded By';
$lang['file_main_directory_text'] = 'Main Directory';
$lang['file_project_folder_name_text'] = 'Project Folder';
$lang['file_project_name_text'] = 'Project Name';
$lang['file_task_number_text'] = 'Task Number';
$lang['file_created_at_text'] = 'Uploaded At';
$lang['file_deletion_status_text'] = 'Is File Deleted';

/*other text*/
$lang['file_unavailable_text'] = 'File is Unavailable';

$lang['yes_text'] = 'Yes';
$lang['no_text'] = 'No';

$lang['kilobyte_text'] = 'kB';
$lang['megabyte_text'] = 'MB';

$lang['files_without_project_text'] = 'Files Without Project';
$lang['create_folder_text'] = 'Create';
$lang['no_folder_text'] = 'No Folder';
$lang['unknown_text'] = 'Unknown';
$lang['unavailable_text'] = 'Unavailable';


/*other texts*/
$lang['confirm_delete_text'] = 'Are You Sure To Delete This Project Folder ? ';
$lang['delete_success_text'] = 'Succesfully deleted the project folder.';


/*tooltip text*/
$lang['tooltip_download_text'] = 'Download File';
$lang['tooltip_delete_text'] = 'Delete File';

$lang['tooltip_see_uploader_profile_text'] = 'See Uploader\'s Profile';
$lang['tooltip_go_to_projectroom_text'] = 'Go to Project Room';
$lang['tooltip_go_to_task_overview_text'] = 'See Task Overview';












?>