
<?php

/*page texts*/
$lang['page_title_text'] = 'File(s) In A Project';
$lang['page_subtitle_text'] = 'View Files | Upload  Files';
$lang['go_upload_button_text'] = 'Upload File' ;

$lang['table_title_text'] = 'Task List';

$lang['no_task_found_text'] = 'No Task Is Found !';

$lang['breadcrumb_home_all_folders_text'] = 'All Folders';
$lang['breadcrumb_home_my_folders_text'] = 'My Folders';
$lang['breadcrumb_section_text'] = 'Folder Name';
$lang['breadcrumb_page_text'] = 'Task List';

/*Column names of the table*/

$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['with_or_without_task_text'] = 'With or without task ';
$lang['option_files_without_task_text'] = 'Files without task ';

$lang['column_task_number_text'] = 'Task Number';
$lang['column_task_title_text'] = 'Task Title';
$lang['column_task_status_text'] = 'Task status';
$lang['column_total_file_number_text'] = 'No. of Files';
$lang['column_total_file_size_text'] = 'File(s) size';
$lang['column_actions_text'] = 'Actions';

$lang['files_without_task_text'] = 'Files Without Task';

$lang['unknown_text'] = 'Unknown';
$lang['unavailable_text'] = 'Unavailable';

$lang['not_my_task_text'] = 'Not My Task';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';


/*other texts*/
$lang['confirm_delete_text'] = 'Are You Sure To Delete This Project Folder ? ';
$lang['delete_success_text'] = 'Succesfully deleted the project folder.';

/*success messages*/
$lang['successfull_text'] = 'Succesfull';
$lang['file_upload_success_text'] = ' Successfully Uploaded';

/*not success messages */


/*sweetalert lang not working*/
$lang['swal_delete_title_text'] = 'Are you sure to delete this folder?';
$lang['swal_delete_confirm_button_text'] = 'Yes, delete this folder';
$lang['swal_delete_cancel_button_text'] = 'No, keep the folder';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Activate Task' ;
$lang['tooltip_deactivate_text'] = 'Deactivate Task' ;

$lang['tooltip_view_file_without_task_text'] = 'View Files Without Task';
$lang['tooltip_view_file_with_task_text'] = 'View Files in the Task';
$lang['tooltip_upload_file_without_task_text'] = 'Upload File Without Task';
$lang['tooltip_upload_file_with_task_text'] = 'Upload File with Task';













?>