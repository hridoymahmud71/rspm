<?php

/*page texts*/
$lang['page_title_text'] = 'File Manager';
$lang['page_subtitle_text'] = 'Upload|View  Files';

$lang['table_title_text'] = 'File List';
$lang['table_title_files_without_project_text'] = '(Files Without Project)';

$lang['go_upload_button_text'] = 'Upload File';

$lang['breadcrumb_home_all_folders_text'] = 'All ';
$lang['breadcrumb_home_my_folders_text'] = 'My';

$lang['breadcrumb_section_main_directory_text'] = 'M.D.';

$lang['breadcrumb_section_files_without_project_text'] = 'Files Without Project';

$lang['breadcrumb_page_text'] = 'File List';

/*--------------*/
$lang['unknown_text'] = 'Unknown';

/*Column names of the table*/
$lang['column_file_name_text'] = 'File Name';
$lang['column_file_size_text'] = 'Size';
$lang['column_file_ext_text'] = 'Format';
$lang['column_file_main_directory_text'] = 'Main Directory';
$lang['column_file_uploaded_by_text'] = 'Uploaded BY';
$lang['column_project_folder_name_text'] = 'Project Folder Name';
$lang['column_task_number_text'] = 'Task Number';
$lang['column_created_at_text'] = 'Uploaded At';
$lang['column_actions_text'] = 'Actions';

$lang['files_without_project_text'] = 'Files Without Project';
$lang['create_folder_text'] = 'Create';
$lang['no_folder_text'] = 'No Folder';
$lang['unknown_text'] = 'Unknown';
$lang['unavailable_text'] = 'Unavailable';


$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';


/*other texts*/
$lang['confirm_delete_text'] = 'Are You Sure To Delete This Project Folder ? ';
$lang['delete_success_text'] = 'Succesfully deleted the project folder.';

/*success messages*/
$lang['successfull_text'] = 'Succesfull';
$lang['project_folder_create_success_text'] = ' Successfully Created';
$lang['file_upload_success_text'] = ' Successfully Uploaded';
$lang['file_deleted_text'] = 'is deleted successfully';

/*not success messages */
$lang['unsuccessfull_text'] = 'Unsuccesfull';
$lang['project_folder_create_error_text'] = ' Could not be Created';
$lang['directory_exists_text'] = '  Directory already exists';
$lang['file_upload_error_text'] = ' Not Uploaded';


/*sweetalert lang not working*/
$lang['sweetalert_title'] = 'Are you sure to delete this file ?';
$lang['sweetalert_confirmButtonText'] = 'Yes, delete it!';
$lang['sweetalert_cancelButtonText'] = 'No, cancel please!';

$lang['swal_file_uploaded_text'] = 'File(s) Uploaded';
$lang['swal_file_exeed_php_limit_text'] = 'Added File(s) exeeding php limit ';




/*tooltip text*/
$lang['tooltip_delete_text'] = 'Delete File';
$lang['tooltip_download_text'] = 'Download File';
$lang['tooltip_view_text'] = 'File Overview' ;
$lang['tooltip_upload_text'] = 'Upload a file ' ;
$lang['tooltip_upload_without_project_text'] = 'Upload file without project' ;
$lang['tooltip_upload_with_project_without_task_text'] = 'Upload file  in the project ' ;
$lang['tooltip_upload_with_project_with_task_text'] = 'Upload file in the task' ;

/*------------------*/
$lang['loading_text'] = 'Loading Files . . .';
$lang['no_file_found_text'] = 'No File is Found';
$lang['no_matching_file_found_text'] = 'No Matching File is Found';






