<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filemanager_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $user_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjects()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getProjectsByStaff($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('staff_id', $user_id);
        $this->db->join('rspm_tbl_project', 'rspm_tbl_project_assigned_staff.project_id=rspm_tbl_project.project_id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getProjectsByClient($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('deletion_status!=', 1);
        $this->db->where('client_id', $user_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    //only non deleted projecta
    public function getAProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('deletion_status!=', 1);
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjectFolders()
    {
        $this->db->select('
                            p.project_id,
                            p.project_name,
                            p.status,
                            p.deletion_status,
                            pf.project_folder_id,
                            pf.project_folder_main_directory,
                            pf.project_folder_name,
                            pf.project_folder_file_size,
                            pf.project_folder_file_num,
                            pf.deletion_status as project_folder_deletion_status
                          ');
        $this->db->from('rspm_tbl_project_folder as pf');
        $this->db->join('rspm_tbl_project as p', 'pf.project_id = p.project_id', 'right');
        $this->db->where('p.deletion_status!=', 1);


        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAProjectFolder($project_id)
    {
        $this->db->select('
                            p.project_id,
                            p.project_name,
                            p.status,
                            p.deletion_status,
                            pf.project_folder_id,
                            pf.project_folder_main_directory,
                            pf.project_folder_name,
                            pf.project_folder_file_size,
                            pf.project_folder_file_num,
                            pf.deletion_status as project_folder_deletion_status
                          ');
        $this->db->from('rspm_tbl_project_folder as pf');
        $this->db->where('pf.project_id', $project_id);
        $this->db->join('rspm_tbl_project as p', 'pf.project_id = p.project_id', 'right');
        $this->db->where('pf.deletion_status!=', 1);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getOnlyProjectFolder($project_folder_id)
    {
        $this->db->select('');
        $this->db->from('rspm_tbl_project_folder');
        $this->db->where('project_folder_id', $project_folder_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjectFoldersByClient($user_id)
    {
        $this->db->select('
                            p.project_id,
                            p.project_name,
                            p.status,
                            p.deletion_status,
                            pf.project_folder_id,
                            pf.project_folder_main_directory,
                            pf.project_folder_name,
                            pf.project_folder_file_size,
                            pf.project_folder_file_num,
                            pf.deletion_status as project_folder_deletion_status
                          ');
        $this->db->from('rspm_tbl_project_folder as pf');
        $this->db->join('rspm_tbl_project as p', 'pf.project_id = p.project_id', 'right');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->where('p.client_id', $user_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getProjectFoldersByStaff($user_id)
    {
        $this->db->select('
                            pas.project_id,
                            pas.staff_id,
                            p.project_name,
                            p.status,
                            p.deletion_status,
                            pf.project_folder_id,
                            pf.project_folder_main_directory,
                            pf.project_folder_name,
                            pf.project_folder_file_size,
                            pf.project_folder_file_num,
                            pf.deletion_status as project_folder_deletion_status
                          ');

        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->where('pas.staff_id', $user_id);
        $this->db->join('rspm_tbl_project as p', 'pas.project_id=p.project_id');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('rspm_tbl_project_folder as pf', 'p.project_id=pf.project_id', 'left');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function createAProjectFolder($data)
    {
        $this->db->insert('rspm_tbl_project_folder', $data);
    }

    public function reomoveProjectFolderFrom_DB_Table($project_folder_name)
    {
        $this->db->where('project_folder_name', $project_folder_name);
        $this->db->delete('rspm_tbl_project_folder');
        return true;
    }

    public function deleteProjectFolder($project_folder_id)
    {
        $this->db->set('deletion_status', 1);
        $this->db->where('project_folder_id', $project_folder_id);
        $this->db->update('rspm_tbl_project_folder');

        return true;
    }

    public function undeleteProjectFolder($project_folder_id)
    {
        $this->db->set('deletion_status', 0);
        $this->db->where('project_folder_id', $project_folder_id);
        $this->db->update('rspm_tbl_project_folder');

        return true;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getTasks($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task');
        $this->db->where('project_id', $project_id);
        $this->db->where('task_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function checkIfStaffIsAssignedToTask($staff_id, $task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff');
        $this->db->where('task_id', $task_id);
        $this->db->where('staff_id', $staff_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    //deleted or not doesn't matter
    public function getProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjectFolder($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_folder');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task');
        $this->db->where('task_id', $task_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getAssignedStaffsToProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getAssignedStaffsToTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff');
        $this->db->where('task_id', $task_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function insertFile($data)
    {
        $this->db->insert('rspm_tbl_file', $data);
        return true;
    }

    public function getFilesWithoutProject()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('project_id', 0);
        $this->db->where('file_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getFilesWithProjectWithoutTask($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('project_id', $project_id);
        $this->db->where('file_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getFilesWithProjectWithTask($project_id, $task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('project_id', $project_id);
        $this->db->where('task_id', $task_id);
        $this->db->where('file_deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countFilesByAjax($common_filter_value = false, $specific_filters = false, $limit, $which_files, $project_id = false, $task_id = false)
    {
        
    }

    public function getFilesByAjax($common_filter_value = false, $specific_filters = false, $order, $limit, $which_files, $project_id = false, $task_id = false)
    {
        
    }

    public function deleteFile($file_id)
    {
        $this->db->set('file_deletion_status', 1);
        $this->db->where('file_id', $file_id);
        $this->db->update('rspm_tbl_file');

        return true;
    }

    public function getAFile($file_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('file_id', $file_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function checkIfUndeletedProjectFolderExist($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_folder');
        $this->db->where('project_id', $project_id);
        $this->db->where('project_folder_name!=', '');
        $this->db->where('deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*-----------------------For log purposes-------------------------------------------------------------------------*/

    public function getActiveAdmins()
    {
        $this->db->select('*');
        $this->db->from('users_groups as ug');
        $this->db->where('ug.group_id', 1);
        $this->db->join('users as u', 'ug.user_id = u.id');
        $this->db->where('u.active', 1);
        $this->db->where('u.deletion_status', 0);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countFilesOfAProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('project_id', $project_id);
        $this->db->where('file_deletion_status!=', 1);


        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countFilesOfATask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('task_id', $task_id);
        $this->db->where('file_deletion_status!=', 1);


        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }


    public function getTotalFileSizeOfAProject($project_id)
    {
        $this->db->select_sum('file_size');
        $this->db->from('rspm_tbl_file');
        $this->db->where('project_id', $project_id);
        $this->db->where('file_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {

            $row = $query->row();

            return $row->file_size;
        } else {
            return 0.00;
        }
    }

    public function getTotalFileSizeOfATask($task_id)
    {
        $this->db->select_sum('file_size');
        $this->db->from('rspm_tbl_file');
        $this->db->where('task_id', $task_id);
        $this->db->where('file_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {

            $row = $query->row();

            return $row->file_size;
        } else {
            return 0.00;
        }
    }

}