<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

class Test_library
{

    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*----------------------------------- field starts -------------------------------------------------*/
    public function get_rand_email()
    {
        $username_length = rand(5, 25);

        return $this->generate_email($username_length);
    }


    function generate_email($username_length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $char_count = count($characters);
        $tld = array(".com", ".net", ".biz");

        $k = array_rand($tld);
        $extension = $tld[$k];

        $randomName = '';
        for ($j = 0; $j < $username_length; $j++) {
            $randomName .= $characters[rand(0, strlen($characters) - 1)];
        }


        $lower_alphabets = 'abcdefghijklmnopqrstuvwxyz';
        $random_mail_name = '';

        $mail_name_length = rand(3, 10);

        for ($p = 0; $p < $mail_name_length; $p++) {
            $random_mail_name .= $lower_alphabets[rand(0, strlen($lower_alphabets) - 1)];
        }


        $fullAddress = $randomName . "@" . $random_mail_name . $extension;


        return $fullAddress;

    }

    public function getIP()
    {
        $randIP = "" . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);

        return $randIP;

    }

    function getName()
    {

        $name_init = $this->initial_generate();

        $name_rest = $this->name_generate(rand(3, 15));

        $name = '';

        if (rand(1, 50) != 1) {
            $name = $name_init . $name_rest;
        } else {
            $name = $name_rest;
        }

        return $name;


    }


    function initial_generate()
    {
        $upper_alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $initial = '';

        $initial .= $upper_alphabets[rand(0, strlen($upper_alphabets) - 1)];

        return $initial;
    }

    function name_generate($length)
    {
        $lower_alphabets = 'abcdefghijklmnopqrstuvwxyz';
        $name = '';
        for ($p = 0; $p < $length; $p++) {
            $name .= $lower_alphabets[rand(0, strlen($lower_alphabets) - 1)];
        }

        return $name;
    }

    public function getPhone()
    {
        return $this->phone_generate();
    }


    function phone_generate($length = false)
    {
        $digits = '0123456789';
        $phone_number = '';

        if ($length == false) {
            $length = rand(10, 15);
        }

        for ($p = 0; $p < $length; $p++) {
            $phone_number .= $digits[rand(0, strlen($digits) - 1)];
        }

        return $phone_number;
    }

    public function getLongName_or_Title($length = false)
    {
        $sentence = '';


        if ($length == false) {
            $length = 30;
        }

        while (strlen($sentence) <= $length) {

            $word = $this->word();
            $sentence .= $word . ' ';


        }

        return $sentence;
    }

    public function word()
    {
        $word_init = $this->initial_generate();

        $rand = rand(1, 10);

        if ($rand == 1 || $rand == 2 || $rand == 3) {
            $word_rest = $this->word_generate(rand(1, 3));
        } elseif ($rand == 4 || $rand == 5 || $rand == 6 || $rand == 7) {
            $word_rest = $this->word_generate(rand(4, 8));
        } elseif ($rand == 9 || $rand == 5) {
            $word_rest = $this->word_generate(rand(9, 12));
        } else {
            $word_rest = $this->word_generate(rand(13, 15));
        }

        $word = '';

        if (rand(1, 50) == 1) {
            $word = $word_init . $word_rest;
        } else {
            $word = $word_rest;
        }


        return $word;
    }

    public function word_generate($length)
    {
        $lower_alphabets = 'abcdefghijklmnopqrstuvwxyz';
        $word = '';
        for ($p = 0; $p < $length; $p++) {
            $word .= $lower_alphabets[rand(0, strlen($lower_alphabets) - 1)];
        }


        return $word;
    }

    function rand_decimal($iMin, $iMax, $fSteps)
    {
        return rand(0, 100000000) / 100;
    }

    /*----------------------------------- field ends -------------------------------------------------*/


    /*----------------------------------- user starts -------------------------------------------------*/

    public function getActiveClient($usr_min_lim, $usr_max_lim)
    {
        $cl = false;

        while ($cl == false) {
            $cl = $this->get_act_cl($usr_min_lim, $usr_max_lim);
        }

        return $cl;
    }

    function get_act_cl($usr_min_lim, $usr_max_lim)
    {

        $rand_user = rand($usr_min_lim, $usr_max_lim);

        if ($this->CI->Test_model->this_undeleted_active_UserExists($rand_user, 3)) {

            return $rand_user;
        } else {

            return false;
        }

    }

    public function getActiveStaffs($usr_min_lim, $usr_max_lim, $number_of_assigned_staffs)
    {

        $assigned_staffs = array();
        while (count($assigned_staffs) < $number_of_assigned_staffs) {

            $rand_staff = $this->getAnActiveStaff($usr_min_lim, $usr_max_lim);

            if (!(in_array($rand_staff, $assigned_staffs))) {

                $assigned_staffs[] = $rand_staff;

            }

        }

        return $assigned_staffs;

    }


    function getAnActiveStaff($usr_min_lim, $usr_max_lim)
    {

        $st = false;

        while ($st == false) {
            $st = $this->get_act_st($usr_min_lim, $usr_max_lim);
        }

        return $st;
    }

    function get_act_st($usr_min_lim, $usr_max_lim){

        $rand_user = rand($usr_min_lim, $usr_max_lim);

        if ($this->CI->Test_model->this_undeleted_active_UserExists($rand_user, 4)) {

            return $rand_user;
        } else {

            return false;
        }
    }
    /*----------------------------------- user ends -------------------------------------------------*/


}