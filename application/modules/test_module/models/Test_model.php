<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

class Test_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    /*------------------------------------------------------Users Starts----------------------------------------------*/
    public function insertUser($data)
    {
        $this->db->insert('users', $data);
    }

    public function insertUserDetails($ud_data)
    {
        $this->db->insert('rspm_tbl_user_details', $ud_data);
    }

    public function insertUsersGroup($ug_data)
    {
        $this->db->insert('users_groups', $ug_data);
    }

    public function deleteUser($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }

    public function deleteUserDetails($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('rspm_tbl_user_details');
    }

    public function deleteUserFromGroup($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete('users_groups');
    }

    public function deleteUserFromSpecificGroup($user_id, $group_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $this->db->delete('users_groups');
    }

    public function updateUser($user_id, $data)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    public function thisUserExists($user_id, $group_id)
{

    $this->db->select('*');
    $this->db->from('users as u');
    $this->db->where('u.id', $user_id);
    $this->db->join('users_groups as ug', 'u.id=ug.user_id');
    $this->db->where('ug.group_id', $group_id);

    $query = $this->db->get();
    $num_rows = $query->num_rows();

    if ($num_rows > 0) {
        return true;
    } else {
        return false;
    }

}

    public function this_undeleted_active_UserExists($user_id, $group_id)
    {

        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active', 1);
        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', $group_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    /*------------------------------------------------------Users Ends------------------------------------------------*/

    /*---------------------------------------------- Currency Starts ------------------------------------------------- */
    public function getActiveCurrencies()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency as c');
        $this->db->where('c.currency_status', 1);
        $this->db->where('c.currency_deletion_status!=', 1);

        $query = $this->db->get();
        $result_array = $query->result_array();

        return $result_array;
    }
    /*---------------------------------------------- Currency Ends --------------------------------------------------- */


    /*---------------------------------------------- Project Starts ------------------------------------------------- */
    public function insertProject($data)
    {
        $this->db->insert('rspm_tbl_project', $data);
    }

    public function insertProjectStaffs($data)
    {
        $this->db->insert('rspm_tbl_project_assigned_staff', $data);
    }

    public function deleteProject($project_id){
        $this->db->where('project_id', $project_id);
        $this->db->delete('rspm_tbl_project');
    }

    public function deleteProjectStaffs($project_id){
        $this->db->where('project_id', $project_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }
    /*---------------------------------------------- Project Ends --------------------------------------------------- */


}