<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestController extends MX_Controller
{
    public $password = '';

    function __construct()
    {
        $this->load->model('test_module/Test_model');
        $this->load->model('users/Ion_auth_model');             //for password hashing


        $this->load->library('test_module/test_library');
        $this->load->library('custom_datetime_library');

        set_time_limit(0);
        ini_set('MAX_EXECUTION_TIME', 3600); // 1 hr
        ini_set('memory_limit', '2000M'); // slightly les than 2 GB

        $given_password = '12345678';
        $this->password = $this->Ion_auth_model->hash_password($given_password);
    }

    /*------------------------------------------------------Something Starts------------------------------------------*/
    /*------------------------------------------------------Something Ends--------------------------------------------*/

    /*------------------------------------------------------Users Starts----------------------------------------------*/
    public function insertUsers()
    {
        for ($i = 1; $i <= 20000; $i++) {

            echo '========================================';

            echo '<br>';
            echo 'Inserting #' . $i . ' user';
            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';


            //echo '<pre>';

            $data = array();

            $data['ip_address'] = $this->test_library->getIP();

            $data['username'] = $this->test_library->get_rand_email();


            $data['password'] = $this->password;
            $data['email'] = $this->test_library->get_rand_email();

            $data['first_name'] = $this->test_library->getName();
            $data['last_name'] = $this->test_library->getName();
            $data['company'] = $this->test_library->getLongName_or_Title(rand(6, 40));
            $data['phone'] = $this->test_library->getPhone();

            if (rand(1, 100) == 1) {
                $data['deletion_status'] = 1;
            } else {
                $data['deletion_status'] = 0;
            }

            if ($data['deletion_status'] != 1) {
                $data['active'] = 1;
            } else {
                $data['active'] = 0;
            }


            $data['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();

            /* user details starts*/
            $ud_data['user_show_age'] = rand(0, 1);

            if ($ud_data['user_show_age'] == 1) {
                $ud_data['user_age'] = rand(20, 70);
            }

            if (rand(1, 4) == 1) {
                $ud_data['user_additional_phone'] = $this->test_library->getPhone();
            }

            if (rand(1, 3) == 1) {
                $ud_data['user_additional_email'] = $this->test_library->get_rand_email();
            }

            if (rand(1, 2) == 1) {
                $ud_data['user_position'] = $this->test_library->getLongName_or_Title(rand(6, 15));
            }

            if (rand(1, 5) == 1) {
                $ud_data['user_home_address'] = $this->test_library->getLongName_or_Title(rand(10, 50));
            }

            if (rand(1, 3) == 1) {
                $ud_data['user_office_address'] = $this->test_library->getLongName_or_Title(rand(10, 50));
            }
            /* user details ends*/


            print_r($data);

            /*inserting $data starts*/
            $this->Test_model->insertUser($data);
            echo '<br>';
            echo '#' . $i . ' user inserted';
            echo '<br>';
            $user_id = $this->db->insert_id();
            $ud_data['user_id'] = $user_id;
            /*inserting $data ends*/

            echo '<br>';
            echo '<br>';
            print_r($ud_data);

            /*inserting $ud_data starts*/
            $this->Test_model->insertUserDetails($ud_data);
            echo '<br>';
            echo '#' . $i . ' user details inserted';
            echo '<br>';
            /*inserting $ud_data ends*/

            /*inserting user as member in users_group starts*/
            $ug_data['user_id'] = $user_id;
            $ug_data['group_id'] = 2; // for members

            $this->Test_model->insertUsersGroup($ug_data);

            echo '<br>';
            echo '#' . $i . ' user inserted in ug as member';
            echo '<br>';
            /*inserting user as member in users_group starts*/


            echo '<br>';
            echo '========================================';


            echo '<br>';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'Users and User Details Insertion Ended';
        echo '<br>';

    }

    public function updateUsers()
    {
        $init = 25;
        $max = $init + 20000;

        for ($i = $init; $i <= $max; $i++) {

            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';

            if (rand(1, 10) == 1) {
                $data['deletion_status'] = 1;
                echo '<br>';
                echo '#' . $i . ' user deletion yes';
                echo '<br>';
            } else {
                $data['deletion_status'] = 0;

                echo '<br>';
                echo '#' . $i . ' user deletion no';
                echo '<br>';
            }

            if ($data['deletion_status'] != 1) {
                $data['active'] = 1;

                echo '<br>';
                echo '#' . $i . ' user activation yes';
                echo '<br>';
            } else {
                $data['active'] = 0;

                echo '<br>';
                echo '#' . $i . ' user activation no';
                echo '<br>';
            }

            $this->Test_model->updateUser($i, $data);
            echo '<br>';
            echo '#' . $i . ' user updated';
            echo '<br>';

            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'Users update Ended';
        echo '<br>';

    }

    // set user as admin/client/staff/ etc.
    public
    function setUserGroups()
    {
        $init = 25;
        $max = $init + 20000;

        for ($i = $init; $i <= $max; $i++) {

            $admin = false;
            $client = false;
            $staff = false;

            $rand_adm = rand(1, 15);
            $rand_clt = rand(1, 7);
            $rand_stf = rand(1, 3);

            if ($rand_adm == 1) {
                $admin = true;
            }

            if ($rand_stf == 1) {
                $staff = true;
            }

            if ($rand_clt == 1 && !($admin == true || $staff == true)) {
                $client = true;
            }

            $ug_data['user_id'] = $i;

            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';

            /*insertion starts*/
            if ($admin == true) {
                $ug_data['group_id'] = 1;  //admin group id = 1
                $this->Test_model->insertUsersGroup($ug_data);

                echo '<br>';
                echo '#' . $i . 'insert user in ug as admin';
                echo '<br>';
            }

            if ($client == true) {
                $ug_data['group_id'] = 3; //client group id = 3
                $this->Test_model->insertUsersGroup($ug_data);

                echo '<br>';
                echo '#' . $i . 'insert user in ug as client';
                echo '<br>';
            }

            if ($staff == true) {
                $ug_data['group_id'] = 4; //staff group id = 4
                $this->Test_model->insertUsersGroup($ug_data);

                echo '<br>';
                echo '#' . $i . 'insert user in ug as staff';
                echo '<br>';
            }

            if ($admin == false && $client == false && $staff == false) {
                echo '<br>';
                echo '#' . $i . ' not inserting';
                echo '<br>';
            }

            /*insertion ends*/
            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';


        }
    }

    public
    function deleteUsers()
    {
        $init = 20000;
        $max = 21000;

        for ($i = $init; $i <= $max; $i++) {

            $this->Test_model->deleteUser($i);

            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';

            echo '<br>';
            echo '#' . $i . ' user deleted';
            echo '<br>';

            $this->Test_model->deleteUserDetails($i);
            echo '<br>';
            echo '#' . $i . ' user details deleted';
            echo '<br>';

            $this->Test_model->deleteUserFromGroup($i);
            echo '<br>';
            echo '#' . $i . ' user removed from groups';
            echo '<br>';

            echo '<br>';
            echo '----------------------------------------';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'Users and User Details Deletion Ended';
        echo '<br>';
    }

    public
    function deleteUsersFromAdmin()
    {
        $init = 25;
        $max = $init + 20000;
        $group_id = 1;

        for ($i = $init; $i <= $max; $i++) {
            $this->Test_model->deleteUserFromSpecificGroup($i, $group_id);
            echo '<br>';
            echo '#' . $i . ' user removed from admin group';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'User removal from admin completed';
        echo '<br>';
    }

    public
    function deleteUsersFromClient()
    {
        $init = 25;
        $max = $init + 20000;
        $group_id = 3;

        for ($i = $init; $i <= $max; $i++) {
            $this->Test_model->deleteUserFromSpecificGroup($i, $group_id);
            echo '<br>';
            echo '#' . $i . ' user removed from client group';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'User removal from client completed';
        echo '<br>';
    }

    public
    function deleteUsersFromStaff()
    {
        $init = 25;
        $max = $init + 20000;
        $group_id = 4;

        for ($i = $init; $i <= $max; $i++) {
            $this->Test_model->deleteUserFromSpecificGroup($i, $group_id);
            echo '<br>';
            echo '#' . $i . ' user removed from staff group';
            echo '<br>';
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'User removal from staff completed';
        echo '<br>';
    }

    /*------------------------------------------------------Users Ends------------------------------------------------*/

    /*------------------------------------------------------Project Starts--------------------------------------------*/
    public
    function insertProjects()
    {
        $usr_min_lim = 25;
        $usr_max_lim = 20000;


        for ($i = 0; $i <= 5000; $i++) {

            echo '<br>';
            echo '-----------------------------------------------------------------------------------------------';
            echo '<br>';

            $data['project_name'] = $this->test_library->getLongName_or_Title(rand(10, 30));
            $data['client_id'] = $this->test_library->getActiveClient($usr_min_lim, $usr_max_lim);

            if (rand(1, 100) == 1) {
                $data['deletion_status'] = 1;
            } else {
                $data['deletion_status'] = 0;
            }

            if ($data['deletion_status'] != 1) {
                if (rand(1, 50) == 1) {
                    $data['status'] = 1;
                } else {
                    $data['status'] = 1;
                }

            } else {
                $data['status'] = 0;
            }

            $data['progress'] = rand(0, 100);

            $active_currencies = $this->Test_model->getActiveCurrencies();
            $random_active_currnecy_index = rand(0, count($active_currencies) - 1);
            $data['currency_id'] = $random_active_currnecy_index;

            $data['budget'] = $this->test_library->rand_decimal(0, 100000, $steps = 0.01);

            if (rand(0, 1) == 1) {
                $data['start_date'] =
                    rand($this->custom_datetime_library->getCurrentTimestamp(), $this->custom_datetime_library->getCurrentTimestamp() + 31536000);
            } else {
                $data['start_date'] = 0;
            }

            if (rand(0, 1) == 1) {
                if ($data['start_date'] != 0) {
                    $data['end_date'] =
                        rand($data['start_date'] + 1, $data['start_date'] + 1 + 31536000);
                } else {
                    $data['end_date'] =
                        rand($this->custom_datetime_library->getCurrentTimestamp(), $this->custom_datetime_library->getCurrentTimestamp() + 31536000);
                }

            } else {
                $data['end_date'] = 0;
            }

            $data['created_date'] = $this->custom_datetime_library->getCurrentTimestamp();
            $data['modified_date'] = $this->custom_datetime_library->getCurrentTimestamp();

            echo '<br>';
            print_r($data);
            echo '<br>';

            $this->Test_model->insertProject($data);
            $project_id = $this->db->insert_id();

            echo '<br>';
            echo 'project #' . $project_id . ' just inserted';
            echo '<br>';


            /*staffs starts*/
            $assigned_staffs = array();
            $number_of_assigned_staffs = rand(0, 200);


            if ($number_of_assigned_staffs > 0) {
                $assigned_staffs = $this->test_library->getActiveStaffs($usr_min_lim, $usr_max_lim, $number_of_assigned_staffs);

            } else {
                $assigned_staffs = null;
            }

            $cnt = 0;
            if ($assigned_staffs) {

                for ($inc = 0; $inc < count($assigned_staffs); $inc++) {

                    $p_staff_data = array();
                    $p_staff_data['project_id'] = $project_id;
                    $p_staff_data['staff_id'] = $assigned_staffs[$inc];
                    $this->Test_model->insertProjectStaffs($p_staff_data);

                    echo '<br>';
                    echo 'assigning staff #' . $p_staff_data['staff_id'] . ' to project #' . $p_staff_data['project_id'];
                    echo '<br>';
                    $cnt++;
                }
            } else {
                echo '<br>';
                echo 'no staff is assigned';
                echo '<br>';
            }
            /*staffs ends*/


            echo '<br>';
            echo 'project #' . $project_id . ' inserted and ' . $cnt . ' staff(s) assigned';
            echo '<br>';
            echo '-----------------------------------------------------------------------------------------------';
            echo '<br>';


        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'Project Insertion Complete';
        echo '<br>';
    }

    public function deleteProjects()
    {
        $min_pr_id = 3756;
        $max_pr_id = 3757;

        for ($i = $min_pr_id; $i <= $max_pr_id; $i++) {

            $this->Test_model->deleteProject($i);
            $this->Test_model->deleteProjectStaffs($i);
            echo '<br>';
            echo 'project #' . $i . ' deleted and staff(s) removed ';

            //also have remove tasks and task assigned staffs
            // will have to write code . . .
        }

        echo '<br>';
        echo '======X=====X=====X=====X=====X=====X=====X======';
        echo '<br>';
        echo 'Project Deletion Complete';
        echo '<br>';
    }


    /*------------------------------------------------------Project Ends----------------------------------------------*/


    public
    function insertTasks()
    {

    }

    public
    function insertFiles()
    {

    }

    public
    function insertInvoices()
    {

    }

    public
    function insertTickets()
    {

    }

    public
    function insertTicketResponses()
    {

    }

}