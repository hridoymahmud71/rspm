<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev, RS Soft
 *
 * */

class Custom_auth_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function deleteUser($id)
    {
        $this->db->set('deletion_status', 1);
        $this->db->where('id', $id);
        $this->db->update('users');
    }

    public function countUsers($common_filter_value = false, $specific_filters = false)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'group') {
                    $specific_group_filter_flag = true; // flag logic not written yet

                    if ($filter_value == 'all') {
                        $this->db->where('ug.group_id', 2); // member group_id 2
                    } else {
                        $this->db->where('ug.group_id', $filter_value);
                    }

                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }


        } else {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        if ($specific_group_filter_flag == false) {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getUsers($common_filter_value = false, $specific_filters = false, $order, $limit)
    {

        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');

        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'group') {
                    $specific_group_filter_flag = true; // flag logic not written yet

                    if ($filter_value == 'all') {
                        $this->db->where('ug.group_id', 2); // member group_id 2
                    } else {
                        $this->db->where('ug.group_id', $filter_value);
                    }

                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }


        } else {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        if ($specific_group_filter_flag == false) {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }


        $this->db->order_by('u.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }


    public function getGroups()
    {
        $this->db->select('*');
        $this->db->from('groups');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function removeStaffFromProjects($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function removeStaffFromTasks($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('rspm_tbl_task_assigned_staff');
    }


}