<!DOCTYPE html>
<html>
<head>
    <!--
    Author: Mahmudur Rahman
    Web Dev, RS Soft
    -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo lang('login_heading'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url() ?>project_base_assets/plugins/iCheck/square/blue.css">

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() ?>project_base_assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() ?>project_base_assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url() ?>project_base_assets/plugins/iCheck/icheck.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <h1><?php echo lang('reset_password_heading');?></h1>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        <!--$message contains val_errors-->
        <div class="clearfix" style="color: darkred;font-size: larger"><?php echo $message; ?></div>

        <form action="<?php echo base_url() . 'users/auth/reset_password/' . $code ?>" method="post">
            <div class="form-group has-feedback">
                <label for="new"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
                <input type="password" name="new" class="form-control"
                       placeholder="<?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?>"
                       value="" pattern="<?php echo '^.{' . $this->data['min_password_length'] . '}.*$' ?>">

            </div>
            <div class="form-group has-feedback">
                <label for="new_confirm"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
                <input type="password" name="new_confirm" class="form-control"
                       placeholder="<?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?>"
                       value="" pattern="<?php echo '^.{' . $this->data['min_password_length'] . '}.*$' ?>" >
            </div>

            <input type="hidden" name="user_id" value="<?php print_r($user_id['value']) ?>">

            <?php foreach ($csrf as $k=>$v) { ?>
                <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
            <?php } ?>

            <div class="row">
                <div class="col-xs-4">
                    <button type="submit"
                            class="btn btn-primary btn-block btn-flat"><?php echo lang('reset_password_submit_btn') ?></button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <br>
        <a href="<?php echo base_url() . 'users/auth/login' ?>">
            <?php echo lang('login_text'); ?>
        </a>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

</body>
</html>
