<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li><a href="<?php echo base_url() . 'users/auth' ?>"><?php echo lang('breadcrumb_section_text') ?></a></li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php echo $message; ?>
                            <br>
                            <?php
                            if ($this->session->flashdata('group_selection_error_1')) {
                                echo $this->session->flashdata('group_selection_error_1');
                            }
                            ?>
                            <br>
                            <?php
                            if ($this->session->flashdata('group_selection_error_2')) {
                                echo $this->session->flashdata('group_selection_error_2');
                            }
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'users/auth/edit_user/' . $user->id ?>" role="form" id=""
                          method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $user->id ?>">
                                <?php if ($csrf) foreach ($csrf as $k => $v) { ?>
                                    <input type="hidden" name="<?php echo $k ?>"
                                           value="<?php echo $v ?>">
                                <?php } ?>
                                <div class="form-group">
                                    <label for="first_name"><?php echo lang('label_firstname_text') ?></label>

                                    <input type="text" name="first_name" class="form-control" id="first_name"
                                           value="<?php echo $user->first_name ?>"
                                           placeholder="<?php echo lang('placeholder_firstname_text') ?>">

                                </div>
                                <div class="form-group">
                                    <label for="last_name"><?php echo lang('label_lastame_text') ?></label>

                                    <input type="text" name="last_name" class="form-control" id="last_name"
                                           value="<?php echo $user->last_name ?>"
                                           placeholder="<?php echo lang('placeholder_lastame_text') ?>">
                                </div>
                                <!--<label for="email"><?php /*echo lang('label_email_text') */ ?></label>


                                <input type="email" name="email" class="form-control" id="email"
                                       placeholder="<?php /*echo lang('placeholder_email_text') */ ?>" >-->
                            </div>
                            <div class="form-group">
                                <label for="phone"><?php echo lang('label_phone_text') ?></label>
                                <input type="text" class="form-control" name="phone" id="user_position"
                                       value="<?php echo $user->phone ?>"
                                       placeholder="<?php echo lang('placeholder_phone_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="company"><?php echo lang('label_company_name_text') ?></label>
                                <input type="text" class="form-control" name="company" id="company"
                                       value="<?php echo $user->company ?>"
                                       placeholder="<?php echo lang('placeholder_company_name_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="password"><?php echo lang('label_password_text') . lang('only_if_necessary_text') ?></label>
                                <input type="text" name="password" class="form-control" id="password"
                                       placeholder="<?php echo lang('placeholder_password_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="password_confirm"><?php echo lang('label_confirm_password_text') . lang('only_if_necessary_text') ?></label>
                                <input type="text" name="password_confirm" class="form-control" id="password_confirm"
                                       placeholder="<?php echo lang('placeholder_confirm_password_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="groups">Groups</label>
                                <?php foreach ($groups as $group) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <?php
                                            $gID = $group['id'];
                                            $checked = null;
                                            $item = null;
                                            foreach ($currentGroups as $grp) {
                                                if ($gID == $grp->id) {
                                                    $checked = ' checked="checked"';
                                                    break;
                                                }
                                            }
                                            ?>
                                            <input type="checkbox" class="" name="groups[]"
                                                   value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                            <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>

                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">


                    <button type="submit" id="btnsubmit"
                            class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                </div>
                </form>
            </div>
            <!-- /.box --> </div>


</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<!--write script -->
<!--if group admin or staff is selected , client cannot be selected and vice-versa-->
<script>

</script>