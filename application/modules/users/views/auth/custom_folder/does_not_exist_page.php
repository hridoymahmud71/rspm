<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('not_found_text')?>

        </h1>


        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo lang('not_found_title_text')?></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <p>
                        <?php echo lang('not_found_description_text')?>
                    </p>
                    <p>
                        <a href="<?php echo base_url().'contact_module/show_contact_info' ?>"><?php echo lang('contact_link_text')?></a>
                    </p>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?php echo lang('thank_you_text')?>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
</div>
<!-- /.content-wrapper -->