<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text')?>
            <small><?php echo lang('page_subtitle_text')?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>"><i class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text')?></a></li>
            <li><a href="<?php echo base_url().'users/auth/show_user_groups'?>"><?php echo lang('breadcrumb_section_text')?></a></li>
            <li class="active"><?php echo lang('breadcrumb_page_text')?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text')?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger"><?php echo $message;?></div>
                        <div class="col-md-2"></div>
                    </div>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url().'users/auth/edit_group/'.$group->id ?>" role="form" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="group_name"><?php echo lang('label_group_name_text') ?></label>

                                <input type="text" name="group_name" class="form-control" id="group_name"
                                       value="<?php echo $group->name ?>"
                                    <?php
                                    if($group->name == 'admin' || $group->name == 'staff' || $group->name == 'client' || $group->name == 'member') {
                                        echo ' readonly ';
                                    }
                                        ?>
                                       placeholder="<?php echo lang('placeholder_group_name_text') ?>" >
                            </div>
                            <div class="form-group">
                                <label for="group_description"><?php echo lang('label_group_description_text') ?></label>

                                <input type="text" name="group_description" class="form-control" id="description"
                                       value="<?php echo $group->description ?>"
                                       placeholder="<?php echo lang('placeholder_group_description_text') ?>" >
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->