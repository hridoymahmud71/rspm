<?php

/*page texts*/
$lang['page_title_my_profile_text'] = 'My Profile';
$lang['page_title_user_profile_text'] = 'User\'s Profile';
$lang['page_subtitle_text'] = 'Overview';
$lang['box_title_text'] = 'About Me';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'User Profile';
$lang['breadcrumb_page_text'] = 'Personal Information overview';

$lang['about_me_box_title_text'] = 'About Me';

/*about section*/
$lang['about_section_name_text'] = 'Name';
$lang['about_section_position_text'] = 'Position';
$lang['about_section_company_text'] = 'Company';
$lang['about_section_age_text'] = 'Age';
$lang['about_section_email_text'] = 'Email';
$lang['about_section_phone_text'] = 'Phone';
$lang['about_section_office_address_text'] = 'Office Address';
$lang['about_section_home_address_text'] = 'Home Address';

$lang['default_phone_text'] = 'Default Phone:';
$lang['additional_phone_text'] = 'Additional Phone:';

$lang['default_email_text'] = 'Default Email:';
$lang['additional_email_text'] = 'Additional Email:';

/*not found text */
$lang['not_found_no_position_text'] = 'No Position';
$lang['not_found_no_company_text'] = 'No Company';
$lang['not_found_no_age_text'] = 'Age Unknown';
$lang['not_found_no_email_text'] = 'No Email';
$lang['not_found_no_phone_number_text'] = 'No Phone Number';
$lang['not_found_no_address_text'] = 'Address Unknown';




?>