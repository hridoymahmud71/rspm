<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Userprofile_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }


    //gets called from users/controllers/Auth/create_user()
    public function checkUserIn_UserDetailsTBL($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_details as ud');
        $this->db->where('ud.user_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return false;
        } else {
            return true;
        }
    }

    //gets called from users/controllers/Auth/create_user()
    public function insertUserIdIn_UserDetailsTBL($user_id)
    {
        $this->db->set('user_id', $user_id);
        $this->db->insert('rspm_tbl_user_details');
    }

    /*------------------------------------------------------*/


    public function getRunningProjects_ofClient($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningProjects_ofStaff($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->where('pas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_project as p', 'pas.project_id=p.project_id');

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);


        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningTasks_ofClient($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningTasks_ofStaff($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_task as t', 't.task_id=tas.task_id');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }



    /*----------------------------------------------------------------------------------------------------------------*/


    public function getGroupsByUser($user_id)
    {
        $this->db->select('g.id as group_id');
        $this->db->select('g.name as user_group_name');
        $this->db->from('users_groups as ug');
        $this->db->where('ug.user_id', $user_id);
        $this->db->join('groups as g', 'ug.group_id = g.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUserInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updateUserInfo($user_data, $user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $user_data);

        return true;
    }

    public function updateUserDetailsInfo($user_details_data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $user_details_data);

        return true;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getTimelineElements()
    {
        $data = new stdClass();
        return $data;
    }


}