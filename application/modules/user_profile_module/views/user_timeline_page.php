<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small>
                <?php echo lang('page_subtitle_text') ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_id ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-lg-3 col-md-4">

                <!--userprofile menu section starts-->
                <?php echo $userprofile_menu_section ?>
                <!--userprofile menu section ends-->

            </div>

            <div class="col-lg-9 col-md-8">
                <div class="box box-primary">

                    <div class="box-header with-border">
                        <h3>
                            <?php echo lang('box_title_text') ?>
                        </h3>
                    </div>

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#timeline" data-toggle="tab" aria-expanded="false">
                                    <?php echo lang('navtab_timeline_title_text') ?>
                                </a>
                            </li>
                            <li class="">
                                <a href="#my_activity" data-toggle="tab" aria-expanded="true">
                                    <?php
                                    if ($is_own_timeline == 'own_timeline') {
                                        echo lang('navtab_my_activity_title_text');
                                    } else {
                                        echo lang('navtab_user_activity_title_text');
                                    }
                                    ?>
                                </a>
                            </li>
                            <li class="">
                                <a href="#related_activity" data-toggle="tab" aria-expanded="false">
                                    <?php echo lang('navtab_related_activity_title_text') ?>
                                </a>
                            </li>
                        </ul>

                        <!-- ----------------------------------------Timeline Activity (All)------------------------------------------------ -->

                        <div class="tab-content">
                            <div class="tab-pane active" id="timeline">
                                <div class="box-body">
                                    <!-- The timeline -->
                                    <ul class="timeline timeline-inverse">

                                        <?php if ($timeline_elements) {
                                            foreach ($timeline_elements as $a_timeline_element) { ?>
                                                <!-- timeline time label -->
                                                <li class="time-label">
                                                    <?php if ($a_timeline_element['is_today'] == 'today') { ?>
                                                        <span class="bg-green">
                                                            <?php echo lang('today_text') ?>
                                                        </span>
                                                    <?php } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') { ?>
                                                        <span class="bg-yellow">
                                                                <?php echo lang('yesterday_text') ?>
                                                            </span>
                                                    <?php } else { ?>
                                                        <span class="bg-red">
                                                                <?php echo $a_timeline_element['log_created_at_date']; ?>
                                                        </span>
                                                    <?php } ?>
                                                </li>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <li>
                                                    <i class="<?php echo $a_timeline_element['timeline_element_icon']
                                                        . ' '
                                                        . 'bg-blue' ?>">
                                                    </i>

                                                    <div class="timeline-item">
                                                        <span class="time"><iclass="fa fa-clock-o"></i>
                                                            <?php echo $a_timeline_element['log_created_at_time'] ?>
                                                        </span>

                                                        <!--message header starts-->
                                                        <h3 class="timeline-header">
                                                            <?php echo $a_timeline_element['log_title_message'] ?>

                                                            <!--header extra information starts-->
                                                            <?php
                                                            if (1
                                                                /*$a_timeline_element['is_log_creator'] == 'log_creator'
                                                                &&
                                                                $a_timeline_element['is_log_creator'] != ''*/
                                                            ) { ?>
                                                                <br>

                                                                <small>
                                                                    *   <!--star symbol-->
                                                                    <?php
                                                                    if ($a_timeline_element['log_activity_for_text'] != '') {
                                                                        $log_activity_for_text = $a_timeline_element['log_activity_for_text'];
                                                                    } else {
                                                                        $log_activity_for_text = '';
                                                                    }
                                                                    $log_cr_for_fn = $a_timeline_element['log_created_for_full_name'];
                                                                    $log_cr_for_url = base_url() . $a_timeline_element['log_created_for_route'];
                                                                    if ($log_activity_for_text != '') {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a><strong> ($log_activity_for_text) </strong>";
                                                                    } else {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a>";
                                                                    }

                                                                    if ($a_timeline_element['is_log_receiver'] == 'not_log_receiver') {
                                                                        $log_for_message =
                                                                            sprintf($this->lang->line('log_created_for_string_text'),
                                                                                $log_cr_for_anchor
                                                                            );
                                                                    } else {
                                                                        $log_for_message = $this->lang->line('log_created_for_you_text');
                                                                    }

                                                                    echo $log_for_message;
                                                                    ?>
                                                                </small>
                                                            <?php } ?>
                                                            <!--header extra information starts-->
                                                        </h3>
                                                        <!--message header ends-->

                                                        <!--message body starts-->
                                                        <?php if ($a_timeline_element['log_message'] != '') { ?>
                                                            <div class="timeline-body h5">
                                                                <?php echo $a_timeline_element['log_message'] ?>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message body ends-->

                                                        <!--message footer starts-->
                                                        <?php
                                                        if
                                                        (
                                                            $a_timeline_element['log_type'] == 'file'
                                                            ||
                                                            $a_timeline_element['log_activity'] == 'deleted'
                                                            || ($a_timeline_element['log_activity'] == 'deactivated'
                                                                && $is_viewer_admin == 'viewer_is_not_admin')
                                                        ) { ?>
                                                            <div><!-- empty div : do not show anchor tag --></div>
                                                        <?php } else { ?>
                                                            <div class="timeline-footer">
                                                                <a href="<?php echo base_url() . $a_timeline_element['log_type_route'] ?>"
                                                                   class="btn btn-primary btn-xs">
                                                                    <?php echo lang('go_text') ?>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message footer ends-->

                                                    </div>
                                                </li>
                                                <!-- END timeline item -->
                                                <?php $is_tl_empty = 'no'; ?>

                                            <?php } ?>

                                        <?php } else { ?>
                                            <?php $is_tl_empty = 'yes'; ?>
                                        <?php } ?>

                                        <?php if ($is_tl_empty == 'no') { ?>
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <?php if ($is_tl_empty == 'yes') { ?>
                                        <div style="color: darkred;font-size: larger"><?php echo lang('timeline_empty_text') ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <!-- ---------------------------------------------My/User's Activity------------------------------------------------ -->

                            <div class="tab-pane" id="my_activity">
                                <div class="box-body">
                                    <!-- The timeline -->
                                    <ul class="timeline timeline-inverse">

                                        <?php if ($user_timeline_elements) {
                                            foreach ($user_timeline_elements as $a_user_timeline_element) { ?>
                                                <!-- timeline time label -->
                                                <li class="time-label">
                                                    <?php if ($a_user_timeline_element['is_today'] == 'today') { ?>
                                                        <span class="bg-green">
                                                            <?php echo lang('today_text') ?>
                                                        </span>
                                                    <?php } elseif ($a_user_timeline_element['is_yesterday'] == 'yesterday') { ?>
                                                        <span class="bg-yellow">
                                                                <?php echo lang('yesterday_text') ?>
                                                            </span>
                                                    <?php } else { ?>
                                                        <span class="bg-red">
                                                                <?php echo $a_user_timeline_element['log_created_at_date']; ?>
                                                        </span>
                                                    <?php } ?>
                                                </li>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <li>
                                                    <i class="<?php echo $a_user_timeline_element['timeline_element_icon']
                                                        . ' '
                                                        . 'bg-blue' ?>">
                                                    </i>

                                                    <div class="timeline-item">
                                                        <span class="time"><iclass="fa fa-clock-o"></i>
                                                            <?php echo $a_user_timeline_element['log_created_at_time'] ?>
                                                        </span>

                                                        <!--message header starts-->
                                                        <h3 class="timeline-header">
                                                            <?php echo $a_user_timeline_element['log_title_message'] ?>

                                                            <!--header extra information starts-->
                                                            <?php
                                                            if (1
                                                                /*$a_user_timeline_element['is_log_creator'] == 'log_creator'
                                                                &&
                                                                $a_user_timeline_element['is_log_creator'] != ''*/
                                                            ) { ?>
                                                                <br>

                                                                <small>
                                                                    *   <!--star symbol-->
                                                                    <?php
                                                                    if ($a_user_timeline_element['log_activity_for_text'] != '') {
                                                                        $log_activity_for_text = $a_user_timeline_element['log_activity_for_text'];
                                                                    } else {
                                                                        $log_activity_for_text = '';
                                                                    }
                                                                    $log_cr_for_fn = $a_user_timeline_element['log_created_for_full_name'];
                                                                    $log_cr_for_url = base_url() . $a_user_timeline_element['log_created_for_route'];
                                                                    if ($log_activity_for_text != '') {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a><strong> ($log_activity_for_text) </strong>";
                                                                    } else {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a>";
                                                                    }

                                                                    if ($a_user_timeline_element['is_log_receiver'] == 'not_log_receiver') {
                                                                        $log_for_message =
                                                                            sprintf($this->lang->line('log_created_for_string_text'),
                                                                                $log_cr_for_anchor
                                                                            );
                                                                    } else {
                                                                        $log_for_message = $this->lang->line('log_created_for_you_text');
                                                                    }

                                                                    echo $log_for_message;
                                                                    ?>
                                                                </small>
                                                            <?php } ?>
                                                            <!--header extra information starts-->
                                                        </h3>
                                                        <!--message header ends-->

                                                        <!--message body starts-->
                                                        <?php if ($a_user_timeline_element['log_message'] != '') { ?>
                                                            <div class="timeline-body h5">
                                                                <?php echo $a_user_timeline_element['log_message'] ?>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message body ends-->

                                                        <!--message footer starts-->
                                                        <?php
                                                        if
                                                        (
                                                            $a_user_timeline_element['log_type'] == 'file'
                                                            ||
                                                            $a_user_timeline_element['log_activity'] == 'deleted'
                                                            || ($a_user_timeline_element['log_activity'] == 'deactivated'
                                                                && $is_viewer_admin == 'viewer_is_not_admin')
                                                        ) { ?>
                                                            <div><!-- empty div : do not show anchor tag --></div>
                                                        <?php } else { ?>
                                                            <div class="timeline-footer">
                                                                <a href="<?php echo base_url() . $a_user_timeline_element['log_type_route'] ?>"
                                                                   class="btn btn-primary btn-xs">
                                                                    <?php echo lang('go_text') ?>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message footer ends-->
                                                    </div>
                                                </li>
                                                <!-- END timeline item -->
                                                <?php $is_user_tl_empty = 'no'; ?>

                                            <?php } ?>

                                        <?php } else { ?>
                                            <?php $is_user_tl_empty = 'yes'; ?>
                                        <?php } ?>

                                        <?php if ($is_user_tl_empty == 'no') { ?>
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <?php if ($is_user_tl_empty == 'yes') { ?>
                                        <div style="color: darkred;font-size: larger">
                                            <?php
                                            if ($is_own_timeline == 'not_own_timeline') {
                                                echo lang('user_timeline_empty_text');
                                            } else {
                                                echo lang('my_timeline_empty_text');
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <!-- -----------------------------------------------Related Activity------------------------------------------------ -->

                            <div class="tab-pane" id="related_activity">
                                <div class="box-body">
                                    <!-- The timeline -->
                                    <ul class="timeline timeline-inverse">

                                        <?php if ($related_timeline_elements) {
                                            foreach ($related_timeline_elements as $a_related_timeline_element) { ?>
                                                <!-- timeline time label -->
                                                <li class="time-label">
                                                    <?php if ($a_related_timeline_element['is_today'] == 'today') { ?>
                                                        <span class="bg-green">
                                                            <?php echo lang('today_text') ?>
                                                        </span>
                                                    <?php } elseif ($a_related_timeline_element['is_yesterday'] == 'yesterday') { ?>
                                                        <span class="bg-yellow">
                                                                <?php echo lang('yesterday_text') ?>
                                                            </span>
                                                    <?php } else { ?>
                                                        <span class="bg-red">
                                                                <?php echo $a_related_timeline_element['log_created_at_date']; ?>
                                                        </span>
                                                    <?php } ?>
                                                </li>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <li>
                                                    <i class="<?php echo $a_related_timeline_element['timeline_element_icon']
                                                        . ' '
                                                        . 'bg-blue' ?>">
                                                    </i>

                                                    <div class="timeline-item">
                                                        <span class="time"><iclass="fa fa-clock-o"></i>
                                                            <?php echo $a_related_timeline_element['log_created_at_time'] ?>
                                                        </span>

                                                        <!--message header starts-->
                                                        <h3 class="timeline-header">
                                                            <?php echo $a_related_timeline_element['log_title_message'] ?>

                                                            <!--header extra information starts-->
                                                            <?php
                                                            if (1
                                                                /*$a_related_timeline_element['is_log_creator'] == 'log_creator'
                                                                &&
                                                                $a_related_timeline_element['is_log_creator'] != ''*/
                                                            ) { ?>
                                                                <br>

                                                                <small>
                                                                    *   <!--star symbol-->
                                                                    <?php
                                                                    if ($a_related_timeline_element['log_activity_for_text'] != '') {
                                                                        $log_activity_for_text = $a_related_timeline_element['log_activity_for_text'];
                                                                    } else {
                                                                        $log_activity_for_text = '';
                                                                    }
                                                                    $log_cr_for_fn = $a_related_timeline_element['log_created_for_full_name'];
                                                                    $log_cr_for_url = base_url() . $a_related_timeline_element['log_created_for_route'];
                                                                    if ($log_activity_for_text != '') {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a><strong> ($log_activity_for_text) </strong>";
                                                                    } else {
                                                                        $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a>";
                                                                    }

                                                                    if ($a_related_timeline_element['is_log_receiver'] == 'not_log_receiver') {
                                                                        $log_for_message =
                                                                            sprintf($this->lang->line('log_created_for_string_text'),
                                                                                $log_cr_for_anchor
                                                                            );
                                                                    } else {
                                                                        $log_for_message = $this->lang->line('log_created_for_you_text');
                                                                    }

                                                                    echo $log_for_message;
                                                                    ?>
                                                                </small>
                                                            <?php } ?>
                                                            <!--header extra information starts-->
                                                        </h3>
                                                        <!--message header ends-->

                                                        <!--message body starts-->
                                                        <?php if ($a_related_timeline_element['log_message'] != '') { ?>
                                                            <div class="timeline-body h5">
                                                                <?php echo $a_related_timeline_element['log_message'] ?>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message body ends-->

                                                        <!--message footer starts-->
                                                        <?php
                                                        if
                                                        (
                                                            $a_related_timeline_element['log_type'] == 'file'
                                                            ||
                                                            $a_related_timeline_element['log_activity'] == 'deleted'
                                                            || ($a_related_timeline_element['log_activity'] == 'deactivated'
                                                                && $is_viewer_admin == 'viewer_is_not_admin')
                                                        ) { ?>
                                                            <div><!-- empty div : do not show anchor tag --></div>
                                                        <?php } else { ?>
                                                            <div class="timeline-footer">
                                                                <a href="<?php echo base_url() . $a_related_timeline_element['log_type_route'] ?>"
                                                                   class="btn btn-primary btn-xs">
                                                                    <?php echo lang('go_text') ?>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                        <!--message footer ends-->
                                                    </div>
                                                </li>
                                                <!-- END timeline item -->
                                                <?php $is_related_tl_empty = 'no'; ?>

                                            <?php } ?>

                                        <?php } else { ?>
                                            <?php $is_related_tl_empty = 'yes'; ?>
                                        <?php } ?>

                                        <?php if ($is_related_tl_empty == 'no') { ?>
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <?php if ($is_related_tl_empty == 'yes') { ?>
                                        <div style="color: darkred;font-size: larger"><?php echo lang('related_timeline_empty_text') ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>

                    <div class="box-footer with-border">

                    </div>

                </div>
                <!--/.box-primary-->


            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>