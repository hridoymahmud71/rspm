<!-- Content Wrapper. Contains page content -->

<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    body {
        font: 12px 'Open Sans';
    }

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #user_profile_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #user_cover_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .user_profile_image-preview {
        max-width: 25%;
    }

    .user_cover_image-preview {
        max-width: 40%;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_id ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-3 col-md-4">

                <!--userprofile menu section starts-->
                <?php echo $userprofile_menu_section ?>
                <!--userprofile menu section ends-->

            </div>

            <div class="col-lg-offset-1 col-lg-7  col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php if ($validation_errors) {
                                echo $validation_errors;
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('profile_image_upload_error')) {
                                echo $this->session->flashdata('profile_image_upload_error');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('profile_image_resize_error')) {
                                echo $this->session->flashdata('profile_image_resize_error');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('cover_image_upload_error')) {
                                echo $this->session->flashdata('cover_image_upload_error');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('cover_image_resize_error')) {
                                echo $this->session->flashdata('cover_image_resize_error');
                                echo '<br>';
                            }
                            ?>

                            <!--demo-->
                            <?php if ($this->session->flashdata('file_upload_errors'))
                                echo $this->session->flashdata('file_upload_errors');
                            ?>
                            <!--demo-->
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <!--demo-->
                            <?php if ($this->session->flashdata('file_upload_success')) {
                                echo $this->session->flashdata('file_upload_success');
                                echo '<br>';
                            }
                            ?>
                            <!--demo-->

                            <?php if ($this->session->flashdata('update_success')) {
                                echo $this->session->flashdata('update_success');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('profile_image_upload_success')) {
                                echo $this->session->flashdata('profile_image_upload_success');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('profile_image_resize_success')) {
                                echo $this->session->flashdata('profile_image_resize_success');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('cover_image_upload_success')) {
                                echo $this->session->flashdata('cover_image_upload_success');
                                echo '<br>';
                            }
                            ?>

                            <?php if ($this->session->flashdata('cover_image_resize_success')) {
                                echo $this->session->flashdata('cover_image_resize_success');
                                echo '<br>';
                            }
                            ?>

                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form action="<?php echo base_url() . 'user_profile_module/edit_user_info/' . $user_id ?>"
                          role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <input type="hidden" name="user_id" value="<?php echo $user_id ?>">

                            <div class="form-group">
                                <label for="first_name"><?php echo lang('label_first_name_text') ?></label>

                                <input type="text" name="first_name" class="form-control" id="first_name"
                                       value="<?php echo $user_info->first_name ?>"
                                       placeholder="<?php echo lang('placeholder_first_name_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="last_name"><?php echo lang('label_last_name_text') ?></label>

                                <input type="text" name="last_name" class="form-control" id="last_name"
                                       value="<?php echo $user_info->last_name ?>"
                                       placeholder="<?php echo lang('placeholder_last_name_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="email"><?php echo lang('label_email_text') ?></label>

                                <input type="text" name="email" class="form-control" id="email"
                                       value="<?php echo $user_info->email ?>"
                                       placeholder="<?php echo lang('placeholder_email_text') ?>" readonly>
                            </div>

                            <div class="form-group">
                                <label for="user_additional_email"><?php echo lang('label_user_additional_email_text') ?></label>

                                <input type="text" name="user_additional_email" class="form-control"
                                       id="user_additional_email"
                                       value="<?php echo $user_info->user_additional_email ?>"
                                       placeholder="<?php echo lang('placeholder_user_additional_email_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="change_password"><?php echo lang('label_change_password_text') ?></label>

                                <div class="radio">
                                    <label>
                                        <input class="" name="change_password" id=""
                                               value="1" type="radio"
                                            <?php if ($this->input->post('change_password') == 1) {
                                                echo ' checked ';
                                            } ?>
                                        >
                                        <?php echo lang('option_change_password_yes_text') ?>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input class="" name="change_password" id=""
                                               value="0" type="radio" checked>
                                        <?php echo lang('option_change_password_no_text') ?>
                                    </label>
                                </div>

                            </div>

                            <div id="password_wrapper">
                                <div class="form-group">
                                    <label for="password"><?php echo lang('label_password_text') ?></label>

                                    <input type="text" name="password" class="form-control"
                                           id="password"
                                           value=""
                                           placeholder="<?php echo lang('placeholder_password_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="confirm_password"><?php echo lang('label_confirm_password_text') ?></label>

                                    <input type="text" name="confirm_password" class="form-control"
                                           id="confirm_password"
                                           value=""
                                           placeholder="<?php echo lang('placeholder_confirm_password_text') ?>">
                                </div>
                            </div>


                            <!--if true image from database, else static image //currently not working -->
                            <?php $image_found = false; ?>

                            <!--profile image upload snippet starts-->
                            <div class="form-group">
                                <label for="user_profile_image"><?php echo lang('label_user_profile_image_text') ?></label>

                                <div class="main-img-preview">
                                    <?php $image_found = false ?>
                                    <?php if ($user_info->user_profile_image) { ?>
                                        <div><?php $image_found = true ?></div>
                                        <img class="thumbnail user_profile_image-preview"
                                             src="<?php echo base_url() . $image_directory . '/' . $user_info->user_profile_image ?>"
                                             title="<?php echo lang('img_title_user_profile_image_text') ?>" alt="">

                                    <?php } ?>

                                    <?php if ($image_found == false) { ?>

                                        <img class="thumbnail user_profile_image-preview"
                                             src="<?php echo base_url()
                                                 . 'project_base_assets/base_demo_images/user_profile_image_demo.png' ?>"
                                             title="<?php echo lang('img_title_user_profile_image_text') ?>">

                                    <?php } ?>
                                </div>

                                <div class="input-group">
                                    <input id="fake_upload_user_profile_image-id" class="form-control fake-shadow"
                                           placeholder="Choose File" disabled="disabled">
                                    <div class="input-group-btn">
                                        <div class="fileUpload btn btn-primary fake-shadow">
                                            <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_user_profile_image_text') ?></span>
                                            <input id="user_profile_image-id" name="user_profile_image" type="file"
                                                   value=""
                                                   class="attachment_upload">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block"><?php echo lang('help_user_profile_image_text') ?></p>
                            </div>
                            <!--profile image upload snippet ends-->

                            <!--cover image upload snippet starts-->
                            <div class="form-group">
                                <label for="user_cover_image"><?php echo lang('label_user_cover_image_text') ?></label>

                                <div class="main-img-preview">
                                    <?php $image_found = false ?>
                                    <?php if ($user_info->user_cover_image) { ?>
                                        <div><?php $image_found = true ?></div>
                                        <img class="thumbnail user_cover_image-preview"
                                             src="<?php echo base_url() . $image_directory . '/' . $user_info->user_cover_image ?>"
                                             title="<?php echo lang('') ?>" alt="">

                                    <?php } ?>

                                    <?php if ($image_found == false) { ?>

                                        <img class="thumbnail user_cover_image-preview"
                                             src="<?php echo base_url()
                                                 . 'project_base_assets/base_demo_images/placeholder_image_demo.jpg' ?>"
                                             title="Preview Logo">

                                    <?php } ?>
                                </div>

                                <div class="input-group">
                                    <input id="fake_upload_user_cover_image-id" class="form-control fake-shadow"
                                           placeholder="Choose File" disabled="disabled">
                                    <div class="input-group-btn">
                                        <div class="fileUpload btn btn-primary fake-shadow">
                                            <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_user_cover_image_text') ?></span>
                                            <input id="user_cover_image-id" name="user_cover_image" type="file"
                                                   value=""
                                                   class="attachment_upload">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block"><?php echo lang('help_user_cover_image_text') ?></p>
                            </div>
                            <!--cover image upload snippet ends-->

                            <div class="form-group">
                                <label for="phone"><?php echo lang('label_phone_text') ?></label>

                                <input type="text" name="phone" class="form-control" id="phone"
                                       value="<?php echo $user_info->phone ?>"
                                       placeholder="<?php echo lang('placeholder_user_additional_phone_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_additional_phone"><?php echo lang('label_user_additional_phone_text') ?></label>

                                <input type="text" name="user_additional_phone" class="form-control"
                                       id="user_additional_phone"
                                       value="<?php echo $user_info->user_additional_phone ?>"
                                       placeholder="<?php echo lang('placeholder_user_additional_phone_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_show_age"><?php echo lang('label_user_show_age_text') ?></label>

                                <div class="radio">
                                    <label>
                                        <input class="radio_user_show_age" name="user_show_age" id="user_show_age_1"
                                               value="1"
                                               type="radio" <?php if ($user_info->user_show_age == 1) {
                                            echo "checked ='checked'";
                                        } ?>
                                        >
                                        <?php echo lang('option_show_age_yes_text') ?>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input class="radio_user_show_age" name="user_show_age" id="user_show_age_0"
                                               value="0"
                                               type="radio" <?php if ($user_info->user_show_age == 0) {
                                            echo "checked ='checked'";
                                        } ?>
                                        >
                                        <?php echo lang('option_show_age_no_text') ?>
                                    </label>
                                </div>

                            </div>

                            <div id="user_age_wrapper">
                                <div class="form-group">
                                    <label for="user_age"><?php echo lang('label_user_age_text') ?></label>

                                    <input type="number" name="user_age" class="form-control" id="user_age"
                                           value="<?php echo $user_info->user_age ?>"
                                           placeholder="<?php echo lang('placeholder_user_age_text') ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="user_position"><?php echo lang('label_user_position_text') ?></label>

                                <input type="text" name="user_position" class="form-control"
                                       id="user_position"
                                       value="<?php echo $user_info->user_position ?>"
                                       placeholder="<?php echo lang('placeholder_user_position_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="user_home_address"><?php echo lang('label_user_home_address_text') ?></label>

                                <textarea class="form-control" name="user_home_address" id="user_home_address"
                                          placeholder="<?php echo lang('placeholder_user_office_address_text') ?>"
                                          style="width: 100%; height: 100px;"
                                ><?php echo $user_info->user_home_address ?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="user_office_address"><?php echo lang('label_user_office_address_text') ?></label>

                                <textarea class="form-control" name="user_office_address" id="user_office_address"
                                          placeholder="<?php echo lang('placeholder_user_office_address_text') ?>"
                                          style="width: 100%; height: 100px;"
                                ><?php echo $user_info->user_office_address ?></textarea>
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" id="btnsubmit" class="btn btn-primary">
                                <?php echo lang('button_submit_text') ?>
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.box --> </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--------------------------------------------------------------------------------------------------------------------->
<script>
    $(function () {
        $('#user_age_wrapper').hide();
        show_hide_user_age_wrapper();


        $("input[name$='user_show_age']").click(function () {
            var if_show_user_age = $(this).val();
            // alert(if_show_user_age)
            if (if_show_user_age == 1) {
                // alert('show');
                $('#user_age_wrapper').show();
            } else {
                // alert('hide');
                $('#user_age_wrapper').hide();
            }
        });

    });

    function show_hide_user_age_wrapper() {

        var if_show_user_age = $("input[name$='user_show_age']").val();
        //alert(if_show_user_age)
        if (if_show_user_age == 1) {
            //alert('show');
            $('#user_age_wrapper').show();
        } else {
            //alert('hide');
            $('#user_age_wrapper').hide();
        }

    }
</script>

<script>
    $(document).ready(function () {
        var brand = document.getElementById('user_profile_image-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fake_upload_user_profile_image-id').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.user_profile_image-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#user_profile_image-id").change(function () {
            readURL(this);
        });
    });

</script>


<script>
    $(function () {
        $('#password_wrapper').hide();
        show_hide_user_age_wrapper();


        $("input[name$='change_password']").click(function () {
            var change_password = $(this).val();
            // alert(if_show_user_age)
            if (change_password == 1) {
                // alert('show');
                $('#password_wrapper').show();
            } else {
                // alert('hide');
                $('#password_wrapper').hide();
            }
        });

    });

    function show_hide_password_wrapper() {

        var if_show_user_age = $("input[name$='change_password']").val();
        //alert(if_show_user_age)
        if (if_show_user_age == 1) {
            //alert('show');
            $('#password_wrapper').show();
        } else {
            //alert('hide');
            $('#password_wrapper').hide();
        }

    }
</script>

<script>
    $(document).ready(function () {
        var brand = document.getElementById('user_cover_image-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fake_upload_user_cover_image-id').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.user_cover_image-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#user_cover_image-id").change(function () {
            readURL(this);
        });
    });

</script>