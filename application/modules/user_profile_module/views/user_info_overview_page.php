<div class="content-wrapper" style="">
    &nbsp;
    <?php if ($is_own_profile == 'own_profile') { ?>
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <a class="btn btn-primary"
                       href="<?php echo base_url() . 'user_profile_module/edit_user_info/' . $user_id ?>">Edit Info
                        &nbsp<span class="icon"><i class="fa fa-pencil-square-o"></i></span>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php
            if ($is_own_profile == 'own_profile') {
                echo lang('page_title_my_profile_text');
            } else {
                echo lang('page_title_user_profile_text');
            }
            ?>

            <small>
                <?php echo lang('page_subtitle_text'); ?>
            </small>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_id ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-lg-3 col-md-4">

                <!--userprofile menu section starts-->
                <?php echo $userprofile_menu_section ?>
                <!--userprofile menu section ends-->

            </div>
            <!-- /.col -->
            <div class="col-lg-9 col-md-8">

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('about_me_box_title_text') ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <strong><i class="fa fa-user margin-r-5"></i><?php echo lang('about_section_name_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                        </p>

                        <hr>

                        <strong><i class="fa fa-hand-o-right margin-r-5"></i><?php echo lang('about_section_position_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->user_position != '') {
                                echo $user_info->user_position;
                            } else {
                                echo lang('not_found_no_position_text');
                            }
                            ?>
                        </p>

                        <hr>

                        <strong><i class="fa fa-briefcase margin-r-5"></i><?php echo lang('about_section_company_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->company != '') {
                                echo $user_info->company;
                            } else {
                                echo lang('not_found_no_company_text');
                            }
                            ?>
                        </p>

                        <hr>

                        <strong><i class="fa fa-clock-o margin-r-5"></i><?php echo lang('about_section_age_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->user_age != '' && $user_info->user_show_age == 1) {
                                echo $user_info->user_age;
                            } else {
                                echo lang('not_found_no_age_text');
                            }
                            ?>
                        </p>

                        <hr>

                        <strong><i class="fa fa-envelope-square margin-r-5"></i><?php echo lang('about_section_email_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->email != '') {
                                echo lang('default_email_text') . ' ' . $user_info->email;
                            } else {
                                echo lang('default_email_text') . ' ' .lang('not_found_no_email_text');
                            }
                            ?>
                            <br>
                            <?php
                            if ($user_info->user_additional_email != '') {
                                echo lang('additional_email_text') . ' ' . $user_info->user_additional_email;
                            } else {
                                echo lang('additional_email_text') . ' ' . lang('not_found_no_email_text');
                            }
                            ?>

                        </p>

                        <hr>

                        <strong><i class="fa fa-mobile-phone margin-r-5"></i><?php echo lang('about_section_phone_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->phone != '') {
                                echo lang('default_phone_text') . ' ' . $user_info->phone;
                            } else {
                               echo lang('default_phone_text') . ' ' .lang('not_found_no_phone_number_text');
                            }
                            ?>
                            <br>
                            <?php
                            if ($user_info->user_additional_phone != '') {
                                echo lang('additional_phone_text') . ' ' . $user_info->user_additional_phone;
                            } else {
                                echo lang('additional_phone_text') . ' ' .lang('not_found_no_phone_number_text');
                            }
                            ?>

                        </p>

                        <hr>

                        <strong><i class="fa fa-building margin-r-5"></i><?php echo lang('about_section_office_address_text') ?>
                        </strong>

                        <p class="text-muted">
                            <?php
                            if ($user_info->user_office_address != '' && !empty($user_info->user_office_address)) {
                                echo $user_info->user_office_address;
                            } else {
                                echo lang('not_found_no_address_text');
                            }
                            ?>
                        </p>

                        <?php if ($is_own_profile == 'own_profile' || $is_admin == 'admin') { ?>
                            <hr>

                            <strong><i class="fa fa-home margin-r-5"></i><?php echo lang('about_section_home_address_text'); ?>
                            </strong>

                            <p class="text-muted">
                                <?php
                                if ($user_info->user_home_address != '' && !empty($user_info->user_home_address)) {
                                    echo $user_info->user_home_address;

                                } else {
                                    echo lang('not_found_no_address_text');
                                }
                                ?>
                            </p>
                        <?php } ?>

                        <div class="box-footer with-border">

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>