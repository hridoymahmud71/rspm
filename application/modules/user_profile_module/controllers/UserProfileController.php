<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfileController extends MX_Controller
{

    function __construct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Userprofile_model');
        $this->load->model('log_module/Log_model');
        $this->load->model('user/Ion_auth_model');                                      //for password hashing purposes

        $this->load->library('form_validation');
        $this->load->library('custom_log_library');

        // application/libraries
        $this->load->library('custom_image_library');
        $this->load->library('custom_datetime_library');
    }

    public function countRunningProjects($user_id)
    {
        $count = 0;

        if ($this->ion_auth->in_group('client',$user_id)) {
            $count = $this->Userprofile_model->getRunningProjects_ofClient($user_id);
        }

        if ($this->ion_auth->in_group('staff',$user_id)) {
            $count = $this->Userprofile_model->getRunningProjects_ofStaff($user_id);
        }

        return $count;
    }

    public function countRunningTasks($user_id)
    {
        $count = 0;

        if ($this->ion_auth->in_group('client',$user_id)) {
            $count = $this->Userprofile_model->getRunningTasks_ofClient($user_id);
        }

        if ($this->ion_auth->in_group('staff',$user_id)) {
            $count = $this->Userprofile_model->getRunningTasks_ofStaff($user_id);
        }

        return $count;
    }

    public function getUserProfileMenuSection($user_id)
    {
        $this->lang->load('userprofile_menu_section');

        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        $data['groups_of_a_user'] = $this->getGroupsByUser($user_id);

        $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);
        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();

        $data['running_projects_count'] = $this->countRunningProjects($user_id);
        $data['running_tasks_count'] =  $this->countRunningTasks($user_id);

        $userprofile_menu_section = $this->load->view("user_profile_module/userprofile_menu_section", $data, true);
        return $userprofile_menu_section;
    }

    public function checkIfOwnProfile($user_id)
    {
        if ($this->session->userdata('user_id') == $user_id) {
            return true;
        } else {
            return false;
        }
    }


    /*------------------INFO OVERVIEW STARTS--------------------------------------------------------------------------*/

    public function showUserProfile()
    {
        $this->lang->load('user_info_overview');

        $user_id = $this->uri->segment(3);

        $data['user_id'] = $user_id;

        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }

        $data['userprofile_menu_section'] = $this->getUserProfileMenuSection($user_id);

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_info_overview_page", $data);
        $this->load->view("common_module/footer");

    }

    public function getGroupsByUser($user_id)
    {
        $groups_of_a_user = $this->Userprofile_model->getGroupsByUser($user_id);
        return $groups_of_a_user;
    }


    /*------------------INFO OVERVIEW ENDS----------------------------------------------------------------------------*/

    /*------------------INFO EDIT STARTS------------------------------------------------------------------------------*/

    /*form load view func */
    public function editUserInfo()
    {
        $user_id = $this->uri->segment(3);


        /*
         rule : an admin can edit any non-admin-user's info

        there is no anchor or button to get access to another
            user's edit-info-form even for the admin.

        the 'if-else' segment below is given in case any user try to
            load another user's form by changing the route directly
        */
        if (($user_id != $this->session->userdata('user_id'))) {
            if ($this->ion_auth->is_admin() && !$this->ion_auth->is_admin($user_id)) {
                //do nothing
            } else {
                redirect('users/auth/need_permission');
            }

        }

        if ($this->input->post()) {

            $this->postedEditUserInfo();

        } else {

            $data['user_id'] = $user_id;
            $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);

            $data['validation_errors'] = null;

            $this->loadUserInfoForm($data);
        }

    }

    public function postedEditUserInfo()
    {

        $data['user_info'] = new stdClass();


        $data['user_id'] = $this->input->post('user_id');

        // to get images which can not be loaded from post
        $data['user_info'] = $this->Userprofile_model->getUserInfo($this->input->post('user_id'));

        $data['user_info']->first_name = $this->input->post('first_name');
        $data['user_info']->last_name = $this->input->post('last_name');

        $data['user_info']->email = $this->input->post('email');
        $data['user_info']->user_additional_email = $this->input->post('user_additional_email');

        $data['user_info']->password = $this->input->post('password');
        $data['user_info']->confirm_password = $this->input->post('confirm_password');

        $data['user_info']->phone = $this->input->post('phone');
        $data['user_info']->user_additional_phone = $this->input->post('user_additional_phone');

        $data['user_info']->user_show_age = $this->input->post('user_show_age');
        $data['user_info']->user_age = $this->input->post('user_age');

        $data['user_info']->position = $this->input->post('position');
        $data['user_info']->user_home_address = $this->input->post('user_home_address');
        $data['user_info']->user_office_address = $this->input->post('user_office_address');

        if ($this->validateForm() == 'validated') {

            /*uploading image files starts : error checking*/
            if (($_FILES['user_profile_image']['name']) != '') {

                $field_name = 'user_profile_image';
                $file_details = $_FILES['user_profile_image'];

                $user_profile_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);
                //print_r($user_profile_image_upload_returns['image_upload_error'][0]);die();

                if ($user_profile_image_upload_returns['image_upload_error'][0] != 'no_upload_error') {

                    $this->session->set_flashdata('profile_image_upload_error',
                        $this->lang->line('for_profile_image_text')
                        . ' ' .
                        $user_profile_image_upload_returns['image_upload_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

                if ($user_profile_image_upload_returns['image_resize_error'] [0] != 'no_resize_error') {

                    $this->session->set_flashdata('profile_image_resize_error',
                        $this->lang->line('for_profile_image_text')
                        . ' ' .
                        $user_profile_image_upload_returns['image_resize_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

            }

            if (($_FILES['user_cover_image']['name']) != '') {

                $field_name = 'user_cover_image';
                $file_details = $_FILES['user_cover_image'];

                $user_cover_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);

                if ($user_cover_image_upload_returns['image_upload_error'][0] != 'no_upload_error') {

                    $this->session->set_flashdata('cover_image_upload_error',
                        $this->lang->line('for_cover_image_text')
                        . ' ' .
                        $user_cover_image_upload_returns['image_upload_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

                if ($user_cover_image_upload_returns['image_resize_error'] [0] != 'no_resize_error') {

                    $this->session->set_flashdata('cover_image_resize_error',
                        $this->lang->line('for_cover_image_text')
                        . ' ' .
                        $user_cover_image_upload_returns['image_resize_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

            }
            /*uploading image files ends :error ckecking*/
            echo print_r($data);
            $this->updateUserInfo();
        } else {
            $data['validation_errors'] = $this->validateForm();
            $this->loadUserInfoForm($data);
        }
    }

    public function loadUserInfoForm($data)
    {
        $this->lang->load('user_info_form');

        $data['userprofile_menu_section'] = $this->getUserProfileMenuSection($data['user_id']);
        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_info_form_page", $data);
        $this->load->view("common_module/footer");

    }

    public function validateForm()
    {
        $this->lang->load('user_info_form');

        $this->form_validation->set_rules('first_name', 'first_name', 'required', array(
                'required' => $this->lang->line('first_name_required_text')
            )
        );

        $this->form_validation->set_rules('last_name', 'last_name', 'required', array(
                'required' => $this->lang->line('last_name_required_text')
            )
        );

        $this->form_validation->set_rules('email', 'email', 'required|valid_email', array(
                'required' => $this->lang->line('email_required_text'),
                'valid_email' => $this->lang->line('email_valid_email_text'),
            )
        );

        $this->form_validation->set_rules('user_additional_email', 'user_additional_email', 'valid_email', array(
                'valid_email' => $this->lang->line('user_additional_email_valid_email_text')
            )
        );

        $this->form_validation->set_rules('user_show_age', 'user_show_age', 'required', array(
                'required' => $this->lang->line('user_show_age_required_text')
            )
        );

        if ($this->input->post('user_show_age')) {
            $this->form_validation->set_rules('user_age', 'user_age', 'required|integer|greater_than_equal_to[1]', array(
                    'required' => $this->lang->line('user_age_required_text'),
                    'integer' => $this->lang->line('user_age_integer_text'),
                    'greater_than_equal_to' => $this->lang->line('user_age_greater_than_equal_to_text'),
                )
            );
        }

        if($this->input->post('change_password') == 1) {
            $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]', array(
                    'required' => $this->lang->line('password_required_text'),
                    'min_length' => sprintf($this->lang->line('password_min_length_text'),$this->config->item('min_password_length', 'ion_auth')),
                    'max_length' => sprintf($this->lang->line('password_max_length_text'),$this->config->item('max_password_length', 'ion_auth')),
                    'matches' => $this->lang->line('password_and_confirm_password_not_match_text')
                )
            );

            $this->form_validation->set_rules('confirm_password', 'confirm_password', 'required', array(
                    'required' => $this->lang->line('confirm_password_required_text')
                )
            );
        }

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('validation_errors', validation_errors());

            return validation_errors();
        } else {
            return 'validated';
        }


    }

    public function updateUserInfo()
    {
        //echo "update";        die();

        $this->lang->load('user_info_form');


        $user_id = $this->input->post('user_id');

        $user_data['first_name'] = $this->input->post('first_name');
        $user_data['last_name'] = $this->input->post('last_name');

        if($this->input->post('password')){
            $hashed_password = $this->Ion_auth_model->hash_password($this->input->post('password'));
            $user_data['password'] = $hashed_password ;
        }

        //$data['email'] = $this->input->post('email');
        $user_details_data['user_additional_email'] = $this->input->post('user_additional_email');

        $user_data['phone'] = $this->input->post('phone');
        $data['user_additional_phone'] = $this->input->post('user_additional_phone');

        $user_details_data['user_show_age'] = $this->input->post('user_show_age');
        if ($this->input->post('user_age')) {
            $user_details_data['user_age'] = $this->input->post('user_age');
        }

        $user_details_data['user_position'] = $this->input->post('user_position');
        $user_details_data['user_home_address'] = $this->input->post('user_home_address');
        $user_details_data['user_office_address'] = $this->input->post('user_office_address');


        /*uploading image files starts : error already checked before*/
        if (($_FILES['user_profile_image']['name']) != '') {

            $field_name = 'user_profile_image';
            $file_details = $_FILES['user_profile_image'];

            $user_profile_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);


            if ($user_profile_image_upload_returns['image_resize_success'][0] != 'no_resize_success') {

                $this->session->set_flashdata('profile_image_resize_success',
                    $this->lang->line('for_profile_image_text')
                    . ' ' .
                    $user_profile_image_upload_returns['image_resize_success'][0]);

            }

            if (($user_profile_image_upload_returns['image_resize_success'][0] != 'no_resize_success')
                &&
                ($user_profile_image_upload_returns['image_upload_success'][0] != 'no_upload_success')
            ) {

                $this->session->set_flashdata('profile_image_upload_success',
                    $this->lang->line('for_profile_image_text')
                    . ' ' .
                    $user_profile_image_upload_returns['image_upload_success'][0]);

                $image_name = $user_profile_image_upload_returns['image_details'][0]['file_name'];

                $user_details_data['user_profile_image'] = $image_name;

            }

        }

        if (($_FILES['user_cover_image']['name']) != '') {

            $field_name = 'user_cover_image';
            $file_details = $_FILES['user_cover_image'];

            $user_cover_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);

            if ($user_cover_image_upload_returns['image_resize_success'] != 'no_resize_success') {

                $this->session->set_flashdata('cover_image_resize_success',
                    $this->lang->line('for_cover_image_text')
                    . ' ' .
                    $user_cover_image_upload_returns['image_resize_success'][0]);

            }

            if (($user_cover_image_upload_returns['image_resize_success'] != 'no_resize_success')
                &&
                ($user_cover_image_upload_returns['image_upload_success'] != 'no_resize_success')
            ) {

                $this->session->set_flashdata('cover_image_upload_success',
                    $this->lang->line('for_cover_image_text')
                    . ' ' .
                    $user_cover_image_upload_returns['image_upload_success'][0]);

                $image_name = $user_cover_image_upload_returns['image_details'][0]['file_name'];

                $user_details_data['user_cover_image'] = $image_name;

            }

        }
        /*uploading image files ends  : error already checked before*/

        /*print_r($user_details_data);
        echo "<br>";
        print_r($user_details_data);*/

        $is_userdata_updated = $this->Userprofile_model->updateUserInfo($user_data, $user_id);
        $is_user_details_data_updated = $this->Userprofile_model->updateUserDetailsInfo($user_details_data, $user_id);

        if ($is_userdata_updated && $is_user_details_data_updated) {
            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('user_profile_module/edit_user_info/' . $user_id);

    }
    /*------------------INFO EDIT ENDS--------------------------------------------------------------------------------*/


    /*------------------TIMELINE STARTS-------------------------------------------------------------------------------*/

    public function showUserTimeline()
    {
        $this->lang->load('user_timeline');

        $user_id = $this->uri->segment(3);
        $data['user_id'] = $user_id;

        /*
         rule : an admin can see any non-admin-user's timeline
        */
        if (($user_id != $this->session->userdata('user_id'))) {
            if ($this->ion_auth->is_admin() && !$this->ion_auth->is_admin($user_id)) {
                //do nothing
            } else {
                redirect('users/auth/need_permission');
            }
        }

        if ($this->session->userdata('user_id') == $user_id) {
            $data['is_own_timeline'] = 'own_timeline';
        } else {
            $data['is_own_timeline'] = 'not_own_timeline';
        }

        if ($this->ion_auth->is_admin()) {
            $data['is_viewer_admin'] = 'viewer_is_admin';
        } else {
            $data['is_viewer_admin'] = 'viewer_is_not_admin';
        }

        $data['userprofile_menu_section'] = $this->getUserProfileMenuSection($user_id);

        $data['timeline_elements'] = $this->getTimelineElements($user_id);
        $data['user_timeline_elements'] = $this->getUserCreatedElements($user_id);
        $data['related_timeline_elements'] = $this->getUserRelatedElements($user_id);


        /*print_r($data['timeline_elements']);
        print_r($data['user_timeline_elements']);
        print_r($data['related_timeline_elements']);*/
        //die();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_timeline_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getTimelineElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {


                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                /*checking is today or yesterday  starts*/

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];


                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getUserCreatedElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getUserCreatedLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {


                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];

                /*checking is today or yesterday starts*/
                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getUserRelatedElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getUserRelatedLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {

                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];

                /*checking is today or yesterday starts*/
                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getTimelineElementIcon($log_type)
    {
        if ($log_type == '') {
            return 'fa fa-hand-o-right';
        }
        if ($log_type == 'project') {
            return 'fa fa-gg';
        }

        if($log_type == 'task'){
            return 'fa fa-tasks';
        }

        if($log_type == 'file'){
            return 'fa fa-file-o';
        }

        if($log_type == 'invoice'){
            return 'fa fa-list-alt';
        }

        if($log_type == 'ticket' || $log_type == 'ticket_response'){
            return 'fa fa-ticket';
        }

        return 'fa fa-hand-o-right';


    }


}
