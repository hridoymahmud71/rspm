<?php

/*page texts*/
$lang['page_title_text'] = 'Add Ticket';

$lang['page_subtitle_text'] = 'Ticket\'s information';
$lang['box_title_text'] = 'Ticket Information Form';

$lang['breadcrumb_home_text'] = 'Home';

$lang['breadcrumb_section_my_tickets_text'] = 'My Tickets';
$lang['breadcrumb_section_project_tickets_text'] = 'Project Tickets';

$lang['breadcrumb_page_text'] = 'Add Ticket';

$lang['button_submit_text'] = 'Submit';

/*Add group form texts*/

$lang['label_ticket_project_name_text'] = 'Project Name';
$lang['label_ticket_title_text'] = 'Ticket Title';
$lang['label_ticket_number_text'] = 'Ticket Number';
$lang['label_ticket_description_text'] = 'Ticket Description';
$lang['label_upload_files_with_dropzone_text'] = 'Upload Files With Dropzone';

$lang['placeholder_ticket_title_text'] = 'Enter Ticket Title';
$lang['placeholder_ticket_description_text'] = 'Enter Ticket Description';
$lang['placeholder_select_project_text'] = 'Select Project';

$lang['option_invoice_status_open_text'] = 'Open';
$lang['option_invoice_status_close_text'] = 'Close';


/*-other text-*/
$lang['select_a_project_text'] = 'Select A Project';
$lang['see_ticket_text'] = 'See Ticket';

$lang['dz_sending_text'] = 'Sending . . .';
$lang['dz_complete_text'] = 'Complete' ;

/*success messages*/
$lang['ticket_add_success_text'] = 'Ticket Added Succesfully';


/*js validation error texts*/
$lang['js_val_error_cant_be_empty_text'] = 'Cannot be empty';
$lang['js_val_error_select_a_project_text'] = 'Select A Project';

/*swal texts*/
$lang['swal_file_exeed_php_limit_text'] = 'PHP Limit Exeeds';
$lang['swal_upload_limit_reached_text'] = 'Upload limit reached';
$lang['swal_maximum_limit_text'] = 'max no. file that can be uploaded at a time is ';
$lang['swal_file_uploaded_text'] = 'File(s) Uploaded';

/*-------------------------------------------------------*/
$lang['no_project_found_text'] = 'No Project Found';


?>
