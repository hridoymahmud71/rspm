<?php

/*page texts*/
$lang['page_title_text'] = 'Tickets';

$lang['page_subtitle_all_ticket_text'] = 'All Tickets';
$lang['page_subtitle_my_ticket_text'] = 'My Tickets';
$lang['page_subtitle_project_ticket_text'] = 'Project Tickets';

$lang['table_title_text'] = 'Ticket List';


$lang['no_ticket_found_text'] = 'No Ticket Is Found !';

$lang['breadcrumb_home_text'] = 'Home';

$lang['breadcrumb_section_all_ticket_text'] = 'All Tickets';
$lang['breadcrumb_section_my_ticket_text'] = 'My Tickets';
$lang['breadcrumb_section_project_ticket_text'] = 'Project Tickets';

$lang['breadcrumb_page_text'] = 'Ticket List';

$lang['add_button_text'] = 'Add A Ticket';

/*Column names of the table*/

$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_solved_text'] = 'Solved';
$lang['option_unsolved_text'] = 'Unsolved';

$lang['column_ticket_number_text'] = 'Ticket Number';
$lang['column_ticket_title_text'] = 'Ticket Title';
$lang['column_ticket_project_name_text'] = 'Project Name';
$lang['column_ticket_created_by_text'] = 'Created By';
$lang['column_ticket_created_at_text'] = 'Created At';
$lang['column_ticket_solution_status_text'] = 'Solved or Unsolved';
$lang['column_actions_text'] = 'Actions';


$lang['status_solved_text'] = 'Solved';
$lang['status_unsolved_text'] = 'Unsolved';



/*other texts*/
$lang['see_ticket_text'] = 'See Ticket';

/*success messages*/
$lang['successful_text'] = 'Successful';

$lang['ticket_solved_text'] = 'Ticket %1$s is marked as solved ';
$lang['ticket_unsolved_text'] = 'Ticket %1$s is marked as unsolved';
$lang['ticket_deleted_text'] = 'Ticket %1$s is deleted';


/*sweetalert lang */
$lang['swal_title'] = 'Are you sure to delete this Ticket ?';
$lang['swal_confirm_button_text'] = 'Yes, delete it!';
$lang['swal_cancel_button_text'] = 'No, cancel please!';



/*tooltip text*/
$lang['tooltip_make_ticket_solved_text'] = 'Mark Ticket as Solved ';
$lang['tooltip_make_ticket_unsolved_text'] = 'Mark Ticket as Unsolved ';

$lang['tooltip_view_text'] = 'View Ticket ';
$lang['tooltip_delete_text'] = 'Delete Ticket ';






