<?php

/*page texts*/
$lang['page_title_text'] = 'Ticket View';


$lang['table_title_text'] = 'Ticket view';

$lang['breadcrumb_home_text'] = 'Home';

$lang['breadcrumb_section_all_ticket_text'] = 'All Tickets';
$lang['breadcrumb_section_my_ticket_text'] = 'My Tickets';
$lang['breadcrumb_section_project_ticket_text'] = 'Project Tickets';

$lang['breadcrumb_page_text'] = 'Ticket Ticket View';


$lang['box_title_ticket_number_text'] = 'Ticket No. :';

/*ticket body text*/
$lang['project_text'] = 'Project';
$lang['written_by_text'] = 'Written By';
$lang['no_ticket_description_text'] = 'No description';
$lang['ticket_file_count_text'] = '%1$s file(s)';
$lang['kB_text'] = 'kB';
$lang['MB_text'] = 'MB';
$lang['or_text'] = 'or';

$lang['no_attachment_text'] = 'No Attachment';

$lang['delete_text'] = 'Delete';
$lang['reply_text'] = 'Reply';
$lang['mark_solved_text'] = 'Mark as solved';
$lang['mark_unsolved_text'] = 'Mark as unsolved';


/*responses list texts*/
$lang['response_list_box_title_text'] = 'Responses';
$lang['no_responses_yet_text'] = 'No responses yet';
$lang['created_on_text'] = 'Created On';
$lang['ticket_response_file_count_text'] = '%1$s file(s)';

/*response form texts*/
$lang['response_form_box_title_text'] = 'Your Response';
$lang['label_upload_response_files_with_dropzone_text'] = 'Upload Files';

$lang['placeholder_ticket_response_description_text'] = 'Enter a response for the ticket';
$lang['button_submit_text'] = 'Submit Response';

$lang['dz_sending_text'] = 'Sending . . .';
$lang['dz_complete_text'] = 'Complete';

$lang['js_val_error_cant_be_empty_text'] = 'Cannot be Empty';


/*swal texts*/
$lang['swal_file_exeed_php_limit_text'] = 'PHP Limit Exeeds';
$lang['swal_upload_limit_reached_text'] = 'Upload limit reached';
$lang['swal_maximum_limit_text'] = 'max no. file that can be uploaded at a time is';

$lang['swal_response_submitted_text'] = 'Response Submitted';
$lang['swal_response_submitted_and_file_uploaded_text'] = 'Response Submitted and File(s) Uploaded';

/*swal delete button text*/
$lang['swal_title'] = 'Are you sure to delete this Ticket ?';
$lang['swal_confirm_button_text'] = 'Yes, delete it!';
$lang['swal_cancel_button_text'] = 'No, cancel please!';
?>