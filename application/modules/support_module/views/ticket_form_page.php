<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrumb_home_text') ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() . 'support_module/show_ticket_list/my' ?>">
                    <?php echo lang('breadcrumb_section_my_tickets_text') ?>
                </a>
                <?php if ($which_form == 'add_ticket_by_project') { ?>
                    |
                    <a href="<?php echo base_url() . 'support_module/show_project_ticket_list/' . $project_id ?>">
                        <?php echo lang('breadcrumb_section_project_tickets_text') ?>
                    </a>
                <?php } ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?php
                if ($projectroom_menu_section && $which_form == 'add_ticket_by_project') {
                    echo $projectroom_menu_section;
                }
                ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <button id="refresh"
                                class="btn btn-default pull-right"><i class="fa fa-refresh"></i>
                        </button>
                        <br>

                        <div id="dz_sending" style="display: none" class=" col-md-offset-2 col-md-8">
                            <?php echo lang('dz_sending_text') ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div id="dz_complete" style="display: none" class=" col-md-offset-2 col-md-8">
                            <?php echo lang('dz_complete_text') ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div id="ticket_add_success" style="color: darkgreen;font-size: larger"
                             class=" col-md-offset-2 col-md-8"></div>
                        <div class="col-md-2"></div>
                        <div id="see_ticket" style="color: darkgreen;font-size: larger"
                             class=" col-md-offset-2 col-md-8"></div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <?php if ($which_form == 'add_ticket') { ?>
                                <div class="form-group">
                                    <label><?php echo lang('label_ticket_project_name_text') ?></label>
                                    <select id="select_project" name=""
                                            class=" select_project form-control select2" style="width: 100%;"
                                            data-placeholder="<?php echo lang('placeholder_select_project_text') ?>">
                                    </select>
                                    <span style="color: darkred" class=""
                                          id="error_select_project"></span>
                                </div>
                            <?php } else { ?>
                                <!--$which_form == 'add_ticket_by_project'-->
                                <div class="form-group">
                                    <label for=""><?php echo lang('label_project_name_text') ?></label>

                                    <input type="text" name="" class="form-control"
                                           id="" readonly
                                           value="<?php if ($a_project) {
                                               echo $a_project->project_name;
                                           } ?>"
                                           placeholder="">
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="ticket_number"><?php echo lang('label_ticket_number_text') ?></label>

                                <input type="text" readonly name="" class="form-control"
                                       id="ticket_number" value="<?php echo $ticket_number ?>"
                                       placeholder="<?php echo lang('placeholder_ticket_number_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="ticket_title"><?php echo lang('label_ticket_title_text') ?></label>

                                <input type="text" name="" class="form-control"
                                       id="ticket_title"
                                       value=""
                                       placeholder="<?php echo lang('placeholder_ticket_title_text') ?>">
                                <span id="error_ticket_title" style="color: darkred"></span>
                            </div>

                            <div class="form-group">
                                <label for="ticket_description"><?php echo lang('label_ticket_description_text') ?></label>

                                <textarea class="form-control" name="" id="ticket_description" rows="3"
                                          placeholder="<?php echo lang('placeholder_ticket_description_text') ?>"
                                          style="width: 100%; height: 300px;"
                                ></textarea>
                            </div>


                        </div>
                        <!-- /.box-body -->
                    </form>


                    <div class="box-body">
                        <label for=""><?php echo lang('label_upload_files_with_dropzone_text') ?></label>
                        <form action="<?php echo base_url() . 'support_module/create_ticket_with_dropzone' ?>"
                              class="dropzone"
                              id="my-awesome-dropzone" method="post" enctype="multipart/form-data">


                            <input type="hidden" id="test" name="test" value="drop zone test value">

                            <input type="hidden" id="what_form" name="what_form" value="<?php echo $which_form ?>">
                            <input type="hidden" id="project_id_in_dz" name="project_id"
                                   value="<?php echo $project_id ?>">

                            <input type="hidden" id="ticket_number_in_dz" name="ticket_number"
                                   value="<?php echo $ticket_number ?>">
                            <input type="hidden" id="ticket_title_in_dz" name="ticket_title">
                            <input type="hidden" id="ticket_description_in_dz" name="ticket_description">
                        </form>
                    </div>

                    <div class="box-footer">
                        <button id="submit_btn"
                                class="btn btn-primary"><?php echo lang('button_submit_text') ?>
                        </button>
                    </div>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $("#refresh").on("click", function (e) {
        // Make sure that the form isn't actually being sent.
        e.preventDefault();
        location.reload();
    })
</script>

<!--select2-->
<script>
    $(function () {

        $(".select_project").select2({
            ajax: {
                url: '<?php echo base_url() . 'support_module/get_active_projects_of_a_client_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,
                allowClear: true,

                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data);
                     console.log(params);
                     console.log(data.more_pages);*/

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work

            }

        });

    });
</script>

<!--tiny mce-->
<script>
    tinymce.init({
        selector: '#ticket_description',
        toolbar: 'fontsizeselect',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt'
    });
</script>

<script>

    $('#ticket_title').keyup(function () {
        var ticket_title = $('#ticket_title').val();
        if (ticket_title == '') {
            $('#error_ticket_title').text('<?php echo lang('js_val_error_cant_be_empty_text') ?>');
        } else {
            $('#error_ticket_title').text('');
        }
    });
</script>

<script>
    $(function () {
        $('.select_project').on("select2:select", function (e) {

            var project_id = $('#select_project').val();

            if (project_id == '' || project_id == null || project_id == 0) {
                $('#error_select_project').text('<?php echo lang('js_val_error_select_a_project_text') ?>');
                $("#project_id_in_dz").val(0);
            } else {
                $('#error_select_project').text('');
                $("#project_id_in_dz").val(project_id);
            }
        });
    })
</script>

<script>
    function validate_form_fields() {

        var ticket_title_val = false;
        var select_project_val = false;

        if ($('#ticket_title').val() == '') {
            $('#error_ticket_title').text('<?php echo lang('js_val_error_cant_be_empty_text') ?>');
            ticket_title_val = false;
        } else {
            $('#error_ticket_title').text('');
            ticket_title_val = true;
        }

        if ($('#select_project').val() == '' || $('#select_project').val() == null || $('#select_project').val() == 0) {
            $('#error_select_project').text('<?php echo lang('js_val_error_select_a_project_text') ?>');
            select_project_val = false;
        } else {
            $('#error_select_project').text('');
            select_project_val = true;
        }

        <?php if($which_form != 'add_ticket') { ?>
        select_project_val = true;  //do not need selectbox value
        <?php }?>

        if (ticket_title_val == false || select_project_val == false) {

            return false;
        } else {

            return true;
        }
    }
</script>

<script>
    function reset_fields() {

        $('.select_project').val('').trigger('change.select2');
        $('#ticket_number').val('');
        $('#ticket_title').val('');
        tinymce.get('ticket_description').setContent('');

        <?php if($which_form == 'add_ticket') { ?>
        $("#project_id_in_dz").val(0);
        $('#select_project').val(0);
        <?php } ?>

        <?php if($which_form == 'add_ticket_by_project') { ?>
        var pj_id = parseInt(<?php echo $project_id?>);
        $("#project_id_in_dz").val(pj_id);
        <?php } ?>

        $("#ticket_number_in_dz").val('');
        $("#ticket_title_in_dz").val('');
        $("#ticket_description_in_dz").val('');
    }
</script>

<script>
    function get_ticket_number() {
        $.ajax({
            url: '<?php echo base_url() . 'support_module/get_generated_ticket_number_by_ajax'?>',
            type: 'GET',
            success: function (ticket_number) {
                $('#ticket_number').val(ticket_number);
                $("#ticket_number_in_dz").val(ticket_number);
            }
        })
    }


</script>

<script>
    function show_ticket_add_success_text() {
        $('#ticket_add_success').show().delay(100).fadeOut();
        $('#ticket_add_success').text('<?php echo lang('ticket_add_success_text')?>').delay(1000).fadeIn();
    }
</script>

<!--dropzone js starts-->
<script>
    $(function () {

        var dz_total_file_size_in_MB = 0;

        $("#dz_complete").hide();
        $("#dz_sending").hide();

        Dropzone.options.myAwesomeDropzone = {

            maxFilesize: <?php echo $dropzone_max_size?>,
            autoDiscover: false,
            addRemoveLinks: true,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: <?php echo $dropzone_max_file_number?>,
            autoProcessQueue: false, // :false Prevents Dropzone from uploading dropped files immediately
            dictResponseError: 'Server not Configured',
            /*acceptedFiles: '.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip',*/
            acceptedFiles: '<?php echo $dropzone_allowed_types ?>',

            accept: function (file, done) {

                done();
            },

            /*success: function(file, response) {
             alert(response);
             },

             */



            /*error: function(file, response) {
             alert(response);

             },*/

            init: function () {

                myDropzone = this; // closure

                $("#submit_btn").on("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();


                    var filesInQueue = myDropzone.getQueuedFiles().length;

                    if (filesInQueue == 0) {

                        var validation_result = validate_form_fields();

                        if (validation_result == true) {

                            var ticket_title = $('#ticket_title').val();
                            $('#ticket_title_in_dz').val(ticket_title);

                            var ticket_number = $('#ticket_number').val();
                            $('#ticket_number_in_dz').val(ticket_number);

                            var ticket_description = tinymce.get('ticket_description').getContent();
                            $('#ticket_description_in_dz').val(ticket_description);

                            $.post("<?php  echo base_url() . 'support_module/create_ticket_with_dropzone'?>", $('#my-awesome-dropzone').serialize())
                                .done(function (response) {
                                    /*console.log(response);*/
                                    var parsed_response = JSON.parse(response);
                                    /*console.log(parsed_response);
                                     console.log(parsed_response.ticket_anchor);*/

                                    $('#see_ticket').show().delay(100).fadeOut();
                                    $('#see_ticket').html(parsed_response.ticket_anchor).delay(3000).fadeIn();
                                });
                            reset_fields();
                            show_ticket_add_success_text();
                            get_ticket_number();

                        }
                    } else {
                        var validation_result = validate_form_fields();

                        if (validation_result == true) {

                            var ticket_title = $('#ticket_title').val();
                            $('#ticket_title_in_dz').val(ticket_title);

                            var ticket_description = tinymce.get('ticket_description').getContent();
                            $('#ticket_description_in_dz').val(ticket_description);

                            myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                            reset_fields();
                            show_ticket_add_success_text();
                            get_ticket_number();
                        }
                    }

                });


                myDropzone.on("addedfile", function (file) {

                    var php_limit_in_MB = <?php echo (int)(str_replace('M', '', ini_get('post_max_size'))); ?>;
                    var a_file_size_in_MB = Math.ceil(((file.size) / 1024) / 1024);

                    dz_total_file_size_in_MB += a_file_size_in_MB;

                    if (dz_total_file_size_in_MB >= php_limit_in_MB) {
                        swal('warning', '<?php echo lang('swal_file_exeed_php_limit_text')?>');
                        this.removeFile(file);
                    }

                });

                myDropzone.on("removedfile", function (file) {
                    // perform task //

                    var a_file_size_in_MB = Math.ceil(((file.size) / 1024) / 1024);

                    dz_total_file_size_in_MB -= a_file_size_in_MB;

                });

                myDropzone.on("sendingmultiple", function (file, xhr, formData) {
                    //alert('sending');
                    $("#dz_sending").css({'display': 'block', 'color': 'darkgreen', 'font-size': 'larger'});
                    $("#dz_sending").show().delay(3000).fadeOut();
                });

                myDropzone.on("completemultiple", function (file, response) {
                    //alert('complete');
                    $("#dz_complete").css({'display': 'block', 'color': 'darkgreen', 'font-size': 'larger'});
                    $("#dz_complete").show().delay(3000).fadeOut();

                });

                myDropzone.on("error", function (file, message) {
                    //alert(message);
                    swal(message);
                    this.removeFile(file);
                });

                myDropzone.on("success", function (file, response) {

                    this.removeFile(file);
                });

                myDropzone.on("successmultiple", function (file, response) {

                    swal('success', '<?php echo lang('swal_file_uploaded_text')?>');

                    /*alert(response);
                     console.log(response);*/
                    var parsed_response = JSON.parse(response);
                    /*console.log(parsed_response);
                     console.log(parsed_response.ticket_anchor);*/
                    $('#see_ticket').show().delay(100).fadeOut();
                    $('#see_ticket').html(parsed_response.ticket_anchor).delay(3000).fadeIn();

                });


            }
        };


    })
</script>
<!--dropzone js ends-->





