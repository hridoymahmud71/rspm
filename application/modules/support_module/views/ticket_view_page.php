<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrumb_home_text') ?>
                </a>
            </li>

            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'support_module/show_ticket_list/all' ?>">
                        <?php echo lang('breadcrumb_section_all_ticket_text') ?>
                    </a>
                    |
                <?php } ?>

                <!--for staffs and client's-->
                <a href="<?php echo base_url() . 'support_module/show_ticket_list/my' ?>">
                    <?php echo lang('breadcrumb_section_my_ticket_text') ?>
                </a>
                |
                <a href="<?php echo base_url() . 'support_module/show_project_ticket_list/'
                    . $a_tkt_info_with_pr_with_cr->project_id ?>">
                    <?php echo lang('breadcrumb_section_project_ticket_text') ?>
                </a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-md-offset-1 col-md-5">

                <?php echo $projectroom_menu_section; ?>

            </div class="col-md-offset-1 col-md-5">
            <div></div>
        </div>
        <!--projectroom menu ends-->

        <!--ticket details starts-->
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <div style="font-size: larger">
                                <label for=""><?php echo lang('project_text') ?></label>
                                &nbsp;
                                :
                                <a href="<?php echo base_url().'projectroom_module/project_overview/'
                                    . $a_tkt_info_with_pr_with_cr->project_id ?>">
                                    <?php echo $a_tkt_info_with_pr_with_cr->project_name ?>
                                </a>
                            </div>
                            <br>
                            <?php echo lang('box_title_ticket_number_text') ?>
                            &nbsp;
                            <?php echo $a_tkt_info_with_pr_with_cr->ticket_number ?>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-info">
                            <h3><?php echo $a_tkt_info_with_pr_with_cr->ticket_title ?></h3>
                            <h5><?php echo lang('written_by_text') ?>
                                &nbsp;
                                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/'
                                    . $a_tkt_info_with_pr_with_cr->id ?>">
                                    <span class=" label label-primary">
                                        <?php
                                        echo $a_tkt_info_with_pr_with_cr->first_name
                                            . ' '
                                            . $a_tkt_info_with_pr_with_cr->last_name;
                                        ?>
                                    </span>
                                </a>
                                <span class="mailbox-read-time pull-right">
                                    <?php echo $a_tkt_info_with_pr_with_cr->ticket_created_at_datetime_string ?>
                                </span>
                            </h5>
                        </div>

                        <?php
                        if ($ticket_file_count > 0) { ?>
                            <div class="mailbox-read-info">
                                <?php
                                $ticket_file_count_text
                                    = sprintf($this->lang->line('ticket_response_file_count_text'), $ticket_file_count);
                                echo $ticket_file_count_text;
                                ?>
                            </div>
                        <?php } ?>

                        <div class="mailbox-read-message">
                            <?php
                            if ($a_tkt_info_with_pr_with_cr->ticket_description != '') {
                                echo $a_tkt_info_with_pr_with_cr->ticket_description;
                            } else {
                                echo '<i>' . lang('no_ticket_description_text') . '</i>';
                            }
                            ?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?php if ($files_of_a_ticket) { ?>
                            <ul class="mailbox-attachments clearfix">
                                <?php foreach ($files_of_a_ticket as $a_tkt_file) { ?>
                                    <li class="an-attachment" style="width: auto;">
                                        <span class="mailbox-attachment-icon <?php if ($a_tkt_file->ticket_file_type == 'image') {
                                            echo 'has-image an-attachment-img';
                                        } ?>">
                                            <?php if (/*$a_tkt_file->ticket_file_type == 'image'*/
                                            0
                                            ) { ?>
                                                <img class="" src="<?php echo base_url()
                                                    . $a_tkt_file->ticket_file_main_directory
                                                    . '/'
                                                    . $a_tkt_file->ticket_file_name

                                                ?>" alt="Attachment">
                                            <?php } else { ?>
                                                <?php if ($a_tkt_file->ticket_file_type == 'image') { ?>
                                                    <i class="fa fa-file-image-o"></i>
                                                <?php } else if ($a_tkt_file->ticket_file_type == 'compressed') { ?>
                                                    <i class="fa fa-file-zip-o"></i>
                                                <?php } else if ($a_tkt_file->ticket_file_type == 'doc') { ?>
                                                    <i class="fa fa-file-word-o "></i>
                                                <?php } else if ($a_tkt_file->ticket_file_type == 'ppt') { ?>
                                                    <i class="fa fa-file-powerpoint-o "></i>
                                                <?php } else if ($a_tkt_file->ticket_file_type == 'pdf') { ?>
                                                    <i class="fa fa-file-pdf-o"></i>
                                                <?php } else { ?>
                                                    <i class="fa fa-file-o"></i>
                                                <?php } ?>

                                            <?php } ?>


                                        </span>

                                        <div class="mailbox-attachment-info">
                                            <a href="#" class="mailbox-attachment-name">
                                                <i class="<?php if ($a_tkt_file->ticket_file_type == 'image') {
                                                    echo 'fa fa-camera';
                                                } else {
                                                    echo 'fa fa-paperclip';
                                                }
                                                ?>"></i>
                                                <?php echo $a_tkt_file->ticket_file_name ?>
                                            </a>
                                            <span class="mailbox-attachment-size">
                                                <?php echo $a_tkt_file->ticket_file_size ?>
                                                <?php echo lang('kB_text') ?>
                                                <br>
                                                <?php echo lang('or_text') ?>
                                                <br>
                                                <?php echo $a_tkt_file->ticket_file_size_in_MB ?>
                                                <?php echo lang('MB_text') ?>
                                                <a href="<?php echo base_url()
                                                    . $a_tkt_file->ticket_file_main_directory
                                                    . '/'
                                                    . $a_tkt_file->ticket_file_name

                                                ?> " download class="btn btn-default btn-xs pull-right">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <div> <?php echo lang('no_attachment_text') ?></div>
                        <?php } ?>
                    </div>
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button id="reply_btn" class="btn btn-default"><i class="fa fa-reply"></i>
                                &nbsp;
                                <?php echo lang('reply_text') ?>
                            </button>
                        </div>
                        <?php if ($is_admin == 'admin') { ?>
                            <a href="<?php echo base_url() . 'support_module/ticket_view/delete_ticket/'
                                . $a_tkt_info_with_pr_with_cr->ticket_id ?>"
                               class="btn btn-default delete_confirmation">
                                <i class="fa fa-trash-o"></i>
                                &nbsp;
                                <?php echo lang('delete_text') ?>
                            </a>
                        <?php } ?>
                        <?php if ($is_client == 'client') { ?>
                            <?php if ($a_tkt_info_with_pr_with_cr->ticket_solution_status == 0) { ?>
                                <a href="<?php echo base_url() . 'support_module/ticket_view/make_ticket_solved/'
                                    . $a_tkt_info_with_pr_with_cr->ticket_id ?>"
                                   class="btn btn-default">
                                    <i class="fa fa-thumbs-o-up"></i>
                                    &nbsp;
                                    <?php echo lang('mark_solved_text') ?>
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo base_url() . 'support_module/ticket_view/make_ticket_unsolved/'
                                    . $a_tkt_info_with_pr_with_cr->ticket_id ?>"
                                   class="btn btn-default">
                                    <i class="fa fa-thumbs-o-down"></i>
                                    &nbsp;
                                    <?php echo lang('mark_unsolved_text') ?>
                                </a>
                            <?php } ?>

                        <?php } ?>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!--ticket details ends-->

        <!--responses starts-->
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box box-primary">
                    <div class="box-body" id="">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <?php echo lang('response_list_box_title_text') ?>
                            </h3>
                        </div>
                        <?php if ($ticket_responses_with_creator) { ?>
                            <?php foreach ($ticket_responses_with_creator as $a_ticket_response_with_creator) { ?>
                                <div class="post"
                                    <?php if ($a_ticket_response_with_creator->ticket_response_creator_type == 'client') { ?>
                                        style="background-color: #fef9e7"
                                    <?php } else if ($a_ticket_response_with_creator->ticket_response_creator_type == 'only_admin') { ?>
                                        style="background-color: #fdedec"
                                    <?php } else if ($a_ticket_response_with_creator->ticket_response_creator_type == 'both_admin_and_staff') { ?>
                                        style="background-color: #ebf5fb"
                                    <?php } else if ($a_ticket_response_with_creator->ticket_response_creator_type == 'not_admin_but_staff') { ?>
                                        style="background-color: #e9f7ef"
                                    <?php } else { ?>
                                        style="background-color: inherit"
                                    <?php } ?>
                                >
                                    <div class="" style="padding: 2%">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm"
                                                <?php
                                                if
                                                ($a_ticket_response_with_creator->user_profile_image == ''
                                                    ||
                                                    $a_ticket_response_with_creator->user_profile_image == null
                                                ) { ?>
                                                    src="<?php echo base_url() . 'project_base_assets/base_demo_images/user_profile_image_demo.png' ?>"
                                                <?php } else { ?>
                                                    src="<?php echo base_url() . $image_directory . '/' . $a_ticket_response_with_creator->user_profile_image ?>"
                                                <?php } ?>
                                                 alt="user image">
                                            <span class="username">
                                            <a href="<?php echo 'user_profile_module/user_profile_overview/'
                                                . $a_ticket_response_with_creator->id
                                            ?>">
                                                <?php echo $a_ticket_response_with_creator->first_name
                                                    . ' '
                                                    . $a_ticket_response_with_creator->last_name
                                                ?>
                                            </a>
                                        </span>
                                            <span class="description">
                                            <?php echo lang('created_on_text') ?>
                                                &nbsp;
                                                <?php echo $a_ticket_response_with_creator->ticket_response_created_at_datetime_string ?>
                                        </span>
                                        </div>
                                        <!-- /.user-block -->
                                        <?php echo $a_ticket_response_with_creator->ticket_response_description ?>
                                        <br>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="" class="link-black text-sm reply_btn">
                                                    <i class="fa fa fa-reply margin-r-5"></i>
                                                    <?php echo lang('reply_text') ?>
                                                </a>
                                            </li>
                                            <li>
                                                <?php
                                                if ($a_ticket_response_with_creator->ticket_response_file_count > 0) {
                                                    $ticket_response_file_count_text
                                                        = sprintf
                                                    (
                                                        $this->lang->line('ticket_response_file_count_text')
                                                        , $a_ticket_response_with_creator->ticket_response_file_count
                                                    );
                                                    echo $ticket_response_file_count_text;
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>



                                    <?php if ($a_ticket_response_with_creator->ticket_response_files) { ?>

                                        <?php foreach ($a_ticket_response_with_creator->ticket_response_files as $a_tkt_resp_file) { ?>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <a style="height: 80px" href="<?php echo base_url()
                                                        . $a_tkt_resp_file->ticket_response_file_main_directory
                                                        . '/'
                                                        . $a_tkt_resp_file->ticket_response_file_name
                                                    ?>"
                                                       class="btn btn-app" download>
                                                        <?php if ($a_tkt_resp_file->ticket_response_file_type == 'image') { ?>
                                                            <i class="fa fa-file-image-o"></i>
                                                        <?php } else if ($a_tkt_resp_file->ticket_response_file_type == 'compressed') { ?>
                                                            <i class="fa fa-file-zip-o"></i>
                                                        <?php } else if ($a_tkt_resp_file->ticket_response_file_type == 'doc') { ?>
                                                            <i class="fa fa-file-word-o "></i>
                                                        <?php } else if ($a_tkt_resp_file->ticket_response_file_type == 'ppt') { ?>
                                                            <i class="fa fa-file-powerpoint-o "></i>
                                                        <?php } else if ($a_tkt_resp_file->ticket_response_file_type == 'pdf') { ?>
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        <?php } else { ?>
                                                            <i class="fa fa-file-o"></i>
                                                        <?php } ?>

                                                        <?php echo $a_tkt_resp_file->ticket_response_file_name ?>
                                                        <br>
                                                        <?php echo $a_tkt_resp_file->ticket_response_file_size ?>
                                                        &nbsp;
                                                        <?php echo lang('kB_text') ?>
                                                        &nbsp;
                                                        <?php echo lang('or_text') ?>
                                                        &nbsp;
                                                        <?php echo $a_tkt_resp_file->ticket_response_file_size_in_MB ?>
                                                        &nbsp;
                                                        <?php echo lang('MB_text') ?>
                                                    </a>
                                                </div>

                                            </div>

                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div>
                                <?php echo lang('no_responses_yet_text') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!--responses ends-->

        <!--response form starts-->
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <?php echo lang('response_form_box_title_text') ?>
                            </h3>
                        </div>
                        <div class="row">
                            <div id="dz_sending" style="display: none" class=" col-md-offset-2 col-md-8">
                                <?php echo lang('dz_sending_text') ?>
                            </div>
                            <div class="col-md-2"></div>

                            <div id="dz_complete" style="display: none" class=" col-md-offset-2 col-md-8">
                                <?php echo lang('dz_complete_text') ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="form-group">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm"
                                    <?php
                                    if
                                    ($user_data_with_details->user_profile_image == ''
                                        ||
                                        $user_data_with_details->user_profile_image == null
                                    ) { ?>
                                        src="<?php echo base_url() . 'project_base_assets/base_demo_images/user_profile_image_demo.png' ?>"
                                    <?php } else { ?>
                                        src="<?php echo base_url() . $image_directory . '/' . $user_data_with_details->user_profile_image ?>"
                                    <?php } ?>
                                     alt="User Image">

                                <span class="username">
                                    <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/'
                                        . $user_data_with_details->user_id ?>">
                                        <?php echo $user_data_with_details->first_name
                                            . ' '
                                            . $user_data_with_details->last_name
                                        ?>
                                    </a>
                                </span>
                            </div>
                            &nbsp;
                            <textarea class="form-control" name="ticket_response_description"
                                      id="ticket_response_description"
                                      rows="1"
                                      placeholder="<?php echo lang('placeholder_ticket_response_description_text') ?>"
                                      style="width: 100%;"
                            ></textarea>
                            <span id="error_ticket_response_description" style="color: darkred"></span>
                        </div>

                        <label for=""><?php echo lang('label_upload_response_files_with_dropzone_text') ?></label>
                        <form action="<?php echo base_url() . 'support_module/create_ticket_response_with_dropzone' ?>"
                              class="dropzone"
                              id="my-awesome-dropzone" method="post" enctype="multipart/form-data">

                            <input type="hidden" id="test" name="test" value="drop zone test value">
                            <input type="hidden" id="ticket_id_in_dz" name="ticket_id"
                                   value="<?php echo $a_tkt_info_with_pr_with_cr->ticket_id ?>"
                                   value="<?php echo $user_data_with_details->id ?>">
                            <input type="hidden" id="ticket_response_description_in_dz"
                                   name="ticket_response_description">
                        </form>
                    </div>

                    <div class="box-footer">
                        <button id="submit_btn"
                                class="btn btn-primary"><?php echo lang('button_submit_text') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--response form starts-->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $('.delete_confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>

<script>
    $("#reply_btn").click(function () {
        $('html, body').animate({
            scrollTop: $("#ticket_response_description").offset().top
        }, 1000);
    });
</script>

<script>
    $(".reply_btn").on("click", function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#ticket_response_description").offset().top
        }, 1000);
    });
</script>

<script>
    function validate_form_fields() {
        var ticket_response_description_val = false;

        if ($('#ticket_response_description').val() == '') {
            $('#error_ticket_response_description').text('<?php echo lang('js_val_error_cant_be_empty_text') ?>');
            ticket_response_description_val = false;
        } else {
            $('#error_ticket_response_description').text('');
            ticket_response_description_val = true;
        }

        if (ticket_response_description_val == false) {
            return false;
        } else {
            return true;
        }
    }
</script>

<!--dropzone js starts-->
<script>
    $(function () {

        var dz_total_file_size_in_MB = 0;

        $("#dz_complete").hide();
        $("#dz_sending").hide();

        Dropzone.options.myAwesomeDropzone = {

            maxFilesize: <?php echo $dropzone_max_size?>,
            autoDiscover: false,
            addRemoveLinks: true,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: <?php echo $dropzone_max_file_number?>,
            autoProcessQueue: false, // :false Prevents Dropzone from uploading dropped files immediately
            dictResponseError: 'Server not Configured',
            /*acceptedFiles: '.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip',*/
            acceptedFiles: '<?php echo $dropzone_allowed_types ?>',

            accept: function (file, done) {

                done();
            },

            /*success: function(file, response) {
             alert(response);
             },

             */

            /*error: function(file, response) {
             alert(response);

             },*/

            init: function () {

                myDropzone = this; // closure

                $("#submit_btn").on("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();

                    var filesInQueue = myDropzone.getQueuedFiles().length;

                    if (filesInQueue == 0) {

                        var validation_result = validate_form_fields();

                        if (validation_result == true) {

                            var ticket_response_description = $('#ticket_response_description').val();
                            $('#ticket_response_description_in_dz').val(ticket_response_description);

                            $.post("<?php  echo base_url() . 'support_module/create_ticket_response_with_dropzone'?>", $('#my-awesome-dropzone').serialize());
                            swal('success', '<?php echo lang('swal_response_submitted_text')?>');
                            location.reload();
                        }
                    } else {
                        var validation_result = validate_form_fields();

                        if (validation_result == true) {

                            var ticket_response_description = $('#ticket_response_description').val();
                            $('#ticket_response_description_in_dz').val(ticket_response_description);

                            myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                        }
                    }

                });


                myDropzone.on("addedfile", function (file) {

                    var php_limit_in_MB = <?php echo (int)(str_replace('M', '', ini_get('post_max_size'))); ?>;
                    var a_file_size_in_MB = Math.ceil(((file.size) / 1024) / 1024);

                    dz_total_file_size_in_MB += a_file_size_in_MB;

                    if (dz_total_file_size_in_MB >= php_limit_in_MB) {
                        swal('warning', '<?php echo lang('swal_file_exeed_php_limit_text')?>');
                        this.removeFile(file);
                    }
                });

                myDropzone.on("removedfile", function (file) {
                    // perform task //

                    var a_file_size_in_MB = Math.ceil(((file.size) / 1024) / 1024);

                    dz_total_file_size_in_MB -= a_file_size_in_MB;

                });

                myDropzone.on("sendingmultiple", function (file, xhr, formData) {
                    //alert('sending');
                    $("#dz_sending").css({'display': 'block', 'color': 'darkgreen', 'font-size': 'larger'});
                    $("#dz_sending").show().delay(3000).fadeOut();
                });

                myDropzone.on("completemultiple", function (file, response) {
                    //alert('complete');
                    $("#dz_complete").css({'display': 'block', 'color': 'darkgreen', 'font-size': 'larger'});
                    $("#dz_complete").show().delay(3000).fadeOut();

                });

                myDropzone.on("error", function (file, message) {
                    //alert(message);
                    swal(message);
                    this.removeFile(file);
                });

                myDropzone.on("success", function (file, response) {

                    this.removeFile(file);
                });

                myDropzone.on("successmultiple", function (file, response) {

                    swal('success', '<?php echo lang('swal_response_submitted_and_file_uploaded_text')?>');
                    location.reload();
                });

            }
        };


    })
</script>
<!--dropzone js ends-->