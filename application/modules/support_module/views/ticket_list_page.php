<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php if ($is_client == 'client') { ?>
        &nbsp;
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">

                    <a class="btn btn-primary"
                       href="<?php
                       if ($which_list == 'project_ticket') {
                           echo base_url() . 'support_module/add_ticket_by_project/' . $project_id;
                       } else {
                           echo base_url() . 'support_module/add_ticket';
                       }
                       ?>">
                        <?php echo lang('add_button_text')
                        ?>
                        &nbsp;
                        <span class="icon"><i class="fa fa-plus"></i></span>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>

            <?php if ($which_list == 'all_ticket') { ?>
                <small><?php echo lang('page_subtitle_all_ticket_text') ?></small>
            <?php } ?>

            <?php if ($which_list == 'ticket_by_client' || $which_list == 'ticket_for_staff') { ?>
                <small><?php echo lang('page_subtitle_my_ticket_text') ?></small>
            <?php } ?>

            <?php if ($which_list == 'project_ticket') { ?>
                <small><?php echo lang('page_subtitle_project_ticket_text') ?></small>
            <?php } ?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?>
                </a>
            </li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'support_module/show_ticket_list/all' ?>">
                        <?php echo lang('breadcrumb_section_all_ticket_text') ?>
                    </a>
                    |
                <?php } ?>

                <?php if ($is_client == 'client' || $is_staff == 'staff') { ?>
                    <a href="<?php echo base_url() . 'support_module/show_ticket_list/my' ?>">
                        <?php echo lang('breadcrumb_section_my_ticket_text') ?>
                    </a>

                <?php } ?>

                <?php if ($which_list == 'project_ticket') { ?>
                    |
                    <a href="#"><?php echo lang('breadcrumb_section_project_ticket_text') ?></a>
                <?php } ?>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!--messages starts-->
    <?php if ($this->session->flashdata('success')) { ?>
        <br>
        <div class="clearfix">
            <div class="col-md-6">
                <div class="panel panel-success copyright-wrap" id="success-panel">
                    <div class="panel-heading"><?php echo lang('successful_text') ?>
                        <button type="button" class="close" data-target="#success-panel" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php if ($this->session->flashdata('ticket_deleted')) {
                            echo $this->session->flashdata('ticket_deleted');
                        } ?>

                        <?php if ($this->session->flashdata('ticket_solved')) {
                            echo $this->session->flashdata('ticket_solved');
                        } ?>

                        <?php if ($this->session->flashdata('ticket_unsolved')) {
                            echo $this->session->flashdata('ticket_unsolved');
                        } ?>


                        <?php if (!$this->session->flashdata('ticket_deleted') && $this->session->flashdata('see_ticket')) {
                            echo '<br>';
                            echo $this->session->flashdata('see_ticket');
                        } ?>

                    </div>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>

    <?php } ?>

    <!--messages ends-->

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-md-4">
                <?php echo $projectroom_menu_section ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo lang('table_title_text') ?></h3>
                        &nbsp;
                        <?php if ($which_list == 'project_ticket') { ?>
                            <a href="<?php echo base_url() . 'projectroom_module/project_overview/' . $project_id ?>">
                                <?php echo $a_project_info->project_name ?>
                            </a>
                        <?php } ?>

                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis" data-column="0"><?php echo lang('column_ticket_number_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('column_ticket_title_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('column_ticket_project_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="3"><?php echo lang('column_ticket_created_by_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="4"><?php echo lang('column_ticket_created_at_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="5"><?php echo lang('column_ticket_solution_status_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="6"><?php echo lang('column_actions_text') ?></a>
                        </div>
                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                   border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label><?php echo lang('column_ticket_number_text') ?></label>
                                    </td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <tr id="filter_col1" data-column="1">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_ticket_title_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col1_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col1_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col1_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>

                                <?php if($which_list != 'project_ticket') { ?>
                                <tr id="filter_col2" data-column="2">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_ticket_project_name_text') ?></label>
                                    </td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col2_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col2_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col2_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <?php } ?>

                                <?php if($is_client == 'not_client') { ?>
                                <tr id="filter_col3" data-column="3">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_ticket_created_by_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col3_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col3_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col3_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>
                                <?php } ?>
                                <tr id="filter_col5" data-column="5">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_ticket_solution_status_text') ?></label>
                                    </td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col5_filter" type="hidden">
                                        <select id="custom_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="1"><?php echo lang('option_solved_text') ?></option>
                                            <option value="0"><?php echo lang('option_unsolved_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="ticket-table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_ticket_number_text') ?></th>
                                <th><?php echo lang('column_ticket_title_text') ?></th>
                                <th><?php echo lang('column_ticket_project_name_text') ?></th>
                                <th><?php echo lang('column_ticket_created_by_text') ?></th>
                                <th><?php echo lang('column_ticket_created_at_text') ?></th>
                                <th><?php echo lang('column_ticket_solution_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($tickets_with_creator_with_project) {
                                foreach ($tickets_with_creator_with_project as $a_tkt_w_cr_w_pj) { ?>
                                    <tr>
                                        <td title="<?php echo $a_tkt_w_cr_w_pj->ticket_number ?>">
                                            <?php echo $a_tkt_w_cr_w_pj->ticket_number ?>
                                        </td>
                                        <td><?php echo $a_tkt_w_cr_w_pj->ticket_title ?></td>
                                        <td>
                                            <a href="<?php echo base_url() . 'projectroom_module/project_overview/'
                                                . $a_tkt_w_cr_w_pj->project_id ?>">
                                                <?php echo $a_tkt_w_cr_w_pj->project_name ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/'
                                                . $a_tkt_w_cr_w_pj->id ?>">
                                                <?php echo $a_tkt_w_cr_w_pj->first_name . ' ' . $a_tkt_w_cr_w_pj->last_name ?>
                                            </a>
                                        </td>
                                        <td data-sort="<?php echo $a_tkt_w_cr_w_pj->ticket_created_at ?>">
                                            <?php echo $a_tkt_w_cr_w_pj->ticket_created_at_datetime_string ?>
                                        </td>
                                        <td data-search="<?php echo $a_tkt_w_cr_w_pj->ticket_solution_status ?>">
                                            <?php if ($a_tkt_w_cr_w_pj->ticket_solution_status == 1) { ?>
                                                <span class="label label-success"><?php echo lang('status_solved_text') ?></span>
                                                <?php if ($is_client == 'client') { ?>
                                                    &nbsp;
                                                    <a title="<?php echo lang('tooltip_make_ticket_unsolved_text') ?>"
                                                       href="<?php
                                                       if ($which_list == 'ticket_by_client' || $which_list == 'ticket_for_staff') {
                                                           echo base_url() . 'support_module/my_ticket/make_ticket_unsolved/'
                                                               . $a_tkt_w_cr_w_pj->ticket_id;
                                                       } else {
                                                           echo base_url() . 'support_module/' . $which_list . '/make_ticket_unsolved/'
                                                               . $a_tkt_w_cr_w_pj->ticket_id;
                                                       }
                                                       ?>">
                                                        <i style="color: #2b2b2b" class="fa fa-thumbs-o-down"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                <?php } ?>

                                            <?php } else { ?>
                                                <span class="label label-danger"><?php echo lang('status_unsolved_text') ?></span>
                                                <?php if ($is_client == 'client') { ?>
                                                    &nbsp;
                                                    <a title="<?php echo lang('tooltip_make_ticket_solved_text') ?>"
                                                       href="<?php
                                                       if ($which_list == 'ticket_by_client' || $which_list == 'ticket_for_staff') {
                                                           echo base_url() . 'support_module/my_ticket/make_ticket_solved/'
                                                               . $a_tkt_w_cr_w_pj->ticket_id;
                                                       } else {
                                                           echo base_url() . 'support_module/' . $which_list . '/make_ticket_solved/'
                                                               . $a_tkt_w_cr_w_pj->ticket_id;
                                                       }
                                                       ?>">
                                                        <i style="color: #2b2b2b" class="fa fa-thumbs-o-up"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a title="<?php echo lang('tooltip_view_text') ?>"
                                               style="color: #2b2b2b"
                                               href="<?php echo base_url() . 'support_module/view_ticket/'
                                                   . $a_tkt_w_cr_w_pj->ticket_id ?>"
                                               class=""><i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                                            </a>
                                            <?php if ($is_admin == 'admin') { ?>
                                                &nbsp;
                                                <a title="<?php echo lang('tooltip_delete_text') ?>" id=""
                                                   style="color: #2b2b2b"
                                                   href="<?php echo base_url() . 'support_module/' . $which_list . '/delete_ticket/'
                                                       . $a_tkt_w_cr_w_pj->ticket_id ?>"
                                                   class="delete_confirmation">
                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                       aria-hidden="true">
                                                    </i>
                                                </a>
                                            <?php } ?>
                                        </td>

                                    </tr>
                                <?php }

                            } else { ?>
                                <div style="color: darkred;font-size: larger"><?php echo lang('no_ticket_found_text') ?></div>
                            <?php } ?>

                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th><?php echo lang('column_ticket_number_text') ?></th>
                                <th><?php echo lang('column_ticket_title_text') ?></th>
                                <th><?php echo lang('column_ticket_project_name_text') ?></th>
                                <th><?php echo lang('column_ticket_created_by_text') ?></th>
                                <th><?php echo lang('column_ticket_created_at_text') ?></th>
                                <th><?php echo lang('column_ticket_solution_status_text') ?></th>
                                <th><?php echo lang('column_actions_text') ?></th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<!--this css style is holding datatable inside the box-->
<style>
    #ticket-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #ticket-table td,
    #ticket-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#ticket-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<?php if ($which_list == 'project_ticket' && $is_client == 'client') { ?>
    <script>
        /*make column invisile on load*/
        $(function () {
            var tbl = $('#ticket-table').DataTable();

            tbl.columns([2,3]).visible(false);

            tbl.columns.adjust().draw(false); // adjust column sizing and redraw
        });
    </script>
<?php } ?>

<?php if ($which_list == 'ticket_by_client') { ?>
    <script>
        /*make column invisile on load*/
        $(function () {
            var tbl = $('#ticket-table').DataTable();

            tbl.columns([3]).visible(false);

            tbl.columns.adjust().draw(false); // adjust column sizing and redraw
        });
    </script>
<?php } ?>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#ticket-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        $('#ticket-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#ticket-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col5_filter').val('');
                filterColumn(5);
            } else {
                $('#col5_filter').val($('#custom_status_filter').val());
                filterColumn(5);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    $('.delete_confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>