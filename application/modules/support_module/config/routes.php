<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['support_module/get_generated_ticket_number_by_ajax'] = 'SupportController/getGeneratedTicketNumberByAjax';

$route['support_module/show_ticket_list/all'] = 'SupportController/showAllTicketList';
$route['support_module/show_ticket_list/my'] = 'SupportController/showMyTicketList';
//(:any) = $project_id
$route['support_module/show_project_ticket_list/(:any)'] = 'SupportController/showProjectTicketList';

$route['support_module/add_ticket'] = 'SupportController/addTicket';

//(:any) = $project_id
$route['support_module/add_ticket_by_project/(:any)'] = 'SupportController/addTicket_ByProject';

$route['support_module/get_active_projects_of_a_client_by_ajax'] = 'SupportController/getActiveProjects_OfAClient_byAjax';

$route['support_module/create_ticket_with_dropzone'] = 'SupportController/createTicketWithDropzone';


//(:any) = $ticket_id
$route['support_module/edit_ticket/(:any)'] = 'SupportController/editTicket';

//(:any) = $ticket_id
$route['support_module/all_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
$route['support_module/my_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
$route['support_module/project_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';

//(:any) = $ticket_id
$route['support_module/view_ticket/(:any)'] = 'SupportController/showTicket';
$route['support_module/create_ticket_response_with_dropzone'] = 'SupportController/createTicketResponseWithDropzone';

$route['support_module/all_ticket/make_ticket_solved/(:any)'] = 'SupportController/makeTicketSolved';
$route['support_module/my_ticket/make_ticket_solved/(:any)'] = 'SupportController/makeTicketSolved';
$route['support_module/project_ticket/make_ticket_solved/(:any)'] = 'SupportController/makeTicketSolved';
$route['support_module/ticket_view/make_ticket_solved/(:any)'] = 'SupportController/makeTicketSolved';

$route['support_module/all_ticket/make_ticket_unsolved/(:any)'] = 'SupportController/makeTicketUnsolved';
$route['support_module/my_ticket/make_ticket_unsolved/(:any)'] = 'SupportController/makeTicketUnsolved';
$route['support_module/project_ticket/make_ticket_unsolved/(:any)'] = 'SupportController/makeTicketUnsolved';
$route['support_module/ticket_view/make_ticket_unsolved/(:any)'] = 'SupportController/makeTicketUnsolved';

$route['support_module/all_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
$route['support_module/my_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
$route['support_module/project_ticket/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
$route['support_module/ticket_view/delete_ticket/(:any)'] = 'SupportController/deleteTicket';
