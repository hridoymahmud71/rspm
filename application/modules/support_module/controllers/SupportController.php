<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Created By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class SupportController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Support_model');

        // application/libraries
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');
        $this->load->library('custom_file_library');


    }

    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    //for admin
    public function showAllTicketList()
    {
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        $this->showTicketList('all_ticket', 0);
    }

    //for client or staff
    public function showMyTicketList()
    {
        if ($this->ion_auth->in_group('client')) {
            $this->showTicketList('ticket_by_client', 0);
        }

        if ($this->ion_auth->in_group('staff')) {
            $this->showTicketList('ticket_for_staff', 0);
        }


    }

    //from projectroom
    public function showProjectTicketList()
    {
        $project_id = $this->uri->segment(3);

        $this->showTicketList('project_ticket', $project_id);
    }

    public function showTicketList($which_list, $project_id)
    {
        $this->lang->load('ticket_list');
        $data['a_project_info'] = null;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        $data['which_list'] = $which_list;

        if ($which_list == 'all_ticket') {
            $data['tickets_with_creator_with_project'] = $this->Support_model->getAllTickets_withCreator_withProject();
        }

        if ($which_list == 'ticket_by_client') {
            $client_id = $this->session->userdata('user_id');
            $data['tickets_with_creator_with_project'] = $this->Support_model->getClientTickets_withCreator_withProject($client_id);
        }

        if ($which_list == 'ticket_for_staff') {
            $staff_id = $this->session->userdata('user_id');
            $data['tickets_with_creator_with_project'] = $this->Support_model->getStaffTickets_withCreator_withProject($staff_id);
        }

        if ($which_list == 'project_ticket' && $project_id != 0) {
            $data['tickets_with_creator_with_project'] = $this->Support_model->getProjectTickets_withCreator_withProject($project_id);
            $data['a_project_info'] = $this->Support_model->getProject($project_id);
        }


        $i = 0;
        foreach ($data['tickets_with_creator_with_project'] as $a_tkt_w_cr_w_pj) {
            $data['tickets_with_creator_with_project'][$i]->ticket_created_at_datetime_string =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDateAndTime($a_tkt_w_cr_w_pj->ticket_created_at);

            $i++;
        }

        if ($which_list == 'project_ticket' && $project_id != 0) {
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id);
        } else {
            $data['projectroom_menu_section'] = null;
        }

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("support_module/ticket_list_page", $data);
        $this->load->view("common_module/footer");
    }

    /*----------------------------------------------------------------------------------------------------------------*/


    public function get_generated_ticket_number()
    {
        $ticket_prefix = 'tkt_';

        $time_zone = $this->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('YmdHis');

        $ticket_number = $ticket_prefix . $datetime . '_r' . rand(10000, 99999);

        return $ticket_number;
    }

    public function getGeneratedTicketNumberByAjax()
    {
        $ticket_prefix = 'tkt_';

        $time_zone = $this->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('YmdHis');

        $ticket_number = $ticket_prefix . $datetime . '_r' . rand(10000, 99999);

        echo $ticket_number;
    }

    public function addTicket()
    {
        $data = array();
        $data['ticket_number'] = $this->get_generated_ticket_number();

        if ($this->input->post()) {
            /*
            # no php validation . all exeptions are checked by javascript.
            # if needed, write checkValidation();
            */
            $this->createTicketWithDropzone();
        } else {
            $this->getTicketForm('add_ticket', 0, $data);
        }
    }

    public function addTicket_ByProject()
    {
        $project_id = $this->uri->segment(3);
        $data = array();
        $data['ticket_number'] = $this->get_generated_ticket_number();

        if ($this->input->post()) {
            /*
            # no php validation . all exeptions are checked by javascript.
            # if needed, write checkValidation();
            */
            $this->createTicketWithDropzone();
        } else {
            $this->getTicketForm('add_ticket_by_project', $project_id, $data);
        }
    }

    public function getTicketForm($which_form, $project_id, $data)
    {
        $this->lang->load('ticket_form');

        $data['which_form'] = $which_form;
        $data['project_id'] = $project_id;

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
            redirect('users/auth/need_permission');
        }

        /*if ($which_form == 'add_ticket' && $data['is_client'] == 'client') {
            $data['all_active_projects_of_a_client'] =
                $this->Support_model->getActiveProjects_OfAClient($this->session->userdata('user_id'));
        } else {
            $data['all_active_projects_of_a_client'] = null;
        }*/

        if ($which_form == 'add_ticket_by_project' && $project_id != 0) {
            $data['a_project'] =
                $this->Support_model->getProject($project_id);
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id);

            $does_pr_belongs_to_cl =
                $this->checkIfActiveProject_BelongsToClient($project_id, $this->session->userdata('user_id'));

            if ($does_pr_belongs_to_cl == false) {
                redirect('users/auth/need_permission');
            }

        } else {
            $data['a_project'] = null;
            $data['projectroom_menu_section'] = null;
        }


        $data['dropzone_allowed_types'] = $this->custom_file_library->getDropzoneAllowedTypes();
        $data['dropzone_max_size'] = $this->custom_file_library->getDropzoneMaxFileSize();
        $data['dropzone_max_file_number'] = $this->custom_file_library->getDropzoneMaxNumberOfFiles();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("support_module/ticket_form_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getActiveProjects_OfAClient_byAjax()
    {
        $this->lang->load('ticket_form');

        $client_id = $this->session->userdata('user_id'); //only a client can create ticket . be careful

        $this->lang->load('ticket_form');

        $search_filter = false;
        if (!empty($this->input->get('q'))) {
            $search_filter = $this->input->get('q');
        }

        $search_filter = trim($search_filter);

        $page = $this->input->get('page');
        if (!$this->input->get('page')) {
            $page = 1;
        }

        $limit = 10;

        $total_count = $this->Support_model->countActiveProjects_OfAClient($client_id,$search_filter);
        $offset = ($page - 1) * $limit;

        //echo 'page:'.$page.' total_count:'.$total_count.' offset:'.$offset;die();
        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count; //bool

        $all_active_projects_of_a_client = $this->Support_model->getActiveProjects_OfAClient($client_id,$search_filter, $limit, $offset);
        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();

        if ($all_active_projects_of_a_client) {


            foreach ($all_active_projects_of_a_client as $an_active_projects_of_a_client) {
                $ap = array();
                $ap['id'] = $an_active_projects_of_a_client->project_id;
                $ap['text'] = $an_active_projects_of_a_client->project_name;

                $items = $ap;
                $json_data['items'][] = $items;
            }
        } else {
            $ap = array();
            $ap['id'] = '0';
            $ap['text'] = $this->lang->line('no_project_found_text');

            $items = $ap;
            $json_data['items'][] = $items;
        }

        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        //$json_data['last_query'] = $last_query;
        echo json_encode($json_data);
    }

    public function checkIfActiveProject_BelongsToClient($project_id, $client_id)
    {
        return $this->Support_model->checkIfActiveProject_BelongsToClient($project_id, $client_id);
    }

    public function createTicketWithDropzone()
    {
       /* print_r($_POST);
        print_r($_FILES);
        die();*/
        $this->lang->load('ticket_form');

        $data['ticket_project_id'] = $this->input->post('project_id');
        $data['ticket_title'] = $this->input->post('ticket_title');
        $data['ticket_number'] = $this->input->post('ticket_number');
        $data['ticket_description'] = $this->input->post('ticket_description');

        $data['ticket_created_by'] = $this->session->userdata('user_id');
        $data['ticket_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        $this->Support_model->insertTicket($data);
        $ticket_id = $this->db->insert_id();

        if (!empty($_FILES)) {

            $i = 0;
            foreach ($_FILES['file']['name'] as $A_FILE_NAME) {

                $new_file_name = $this->custom_file_library->getNewFileName($_FILES['file']['name'][$i]);

                $move_from = $_FILES['file']['tmp_name'][$i];
                $main_dir = $this->custom_file_library->getMainDirectory();

                $move_to = FCPATH . $main_dir . '/' . $new_file_name;

                $this->custom_file_library->uploadViaDropzone($move_from, $move_to);

                $exploded_original_filename = explode(".", $_FILES['file']['name'][$i]);
                $ext = end($exploded_original_filename);

                $file_data['ticket_id'] = $ticket_id;
                $file_data['ticket_file_name'] = $new_file_name;
                $file_data['ticket_file_size'] = round($_FILES['file']['size'][$i] / 1024, 2);              //in kB
                $file_data['ticket_file_ext'] = '.' . $ext;
                $file_data['ticket_file_main_directory'] = $main_dir;

                $this->Support_model->insertTicketFile($file_data);

                $i++;
            }
        }

        /*creating log starts*/
        $project_assigned_staffs = $this->Support_model->getProjectAssignedStaffs($this->input->post('project_id'));

        //for assigned staffs
        if ($project_assigned_staffs) {
            foreach ($project_assigned_staffs as $an_assigned_staff) {
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $an_assigned_staff->staff_id,                                           //2.    $created_for
                    'ticket',                                                               //3.    $type
                    $ticket_id,                                                             //4.    $type_id
                    'created',                                                              //5.    $activity
                    'client',                                                               //6.    $activity_by
                    'staff',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    'project',                                                              //10.   $super_type
                    $this->input->post('project_id'),                                       //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
            }
        }
        /*creating log ends*/

        $ticket_route = base_url() . 'support_module/view_ticket/' . $ticket_id;
        $see_ticket_text = $this->lang->line('see_ticket_text');
        $ticket_anchor = "<a href='$ticket_route'>$see_ticket_text</a>";
        $response['ticket_anchor'] = $ticket_anchor;
        $encoded_response = json_encode($response);

        //necessary
        echo $encoded_response;

    }

    /*--------------------------------------------------------------------------------------------------*/

    public function makeTicketSolved()
    {
        $this->lang->load('ticket_list');

        $ticket_id = $this->uri->segment(4);
        $from_where = $this->uri->segment(2);

        $is_made_solved = $this->Support_model->makeTicketSolved($ticket_id);

        if ($is_made_solved == true) {
            $ticket_info = $this->Support_model->getTicket($ticket_id);

            $this->session->set_flashdata('success', $this->lang->line('successful_text'));
            $this->session->set_flashdata('ticket_id', $ticket_id);


            $ticket_solved_text = sprintf($this->lang->line('ticket_solved_text'), $ticket_info->ticket_number);
            $this->session->set_flashdata('ticket_solved', ($ticket_solved_text));

            $ticket_route = base_url() . 'support_module/view_ticket/' . $ticket_id;
            $see_ticket_text = $this->lang->line('see_ticket_text');
            $see_ticket_anchor = "<a href='$ticket_route'>$see_ticket_text</a>";
            $this->session->set_flashdata('see_ticket', ($see_ticket_anchor));

            /*creating log starts*/
            $project_assigned_staffs = $this->Support_model->getProjectAssignedStaffs($ticket_info->ticket_project_id);

            //for assigned staffs
            if ($project_assigned_staffs) {
                foreach ($project_assigned_staffs as $an_assignd_staff) {
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        $an_assignd_staff->staff_id,                                            //2.    $created_for
                        'ticket',                                                               //3.    $type
                        $ticket_id,                                                             //4.    $type_id
                        'marked_solved',                                                        //5.    $activity
                        'client',                                                               //6.    $activity_by
                        'staff',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        'project',                                                              //10.   $super_type
                        $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                }
            }
            /*creating log ends*/

            if ($from_where == 'all_ticket') {
                redirect('support_module/show_ticket_list/all');
            }

            if ($from_where == 'my_ticket') {
                redirect('support_module/show_ticket_list/my');
            }

            if ($from_where == 'project_ticket') {
                redirect('support_module/show_project_ticket_list/' . $ticket_info->ticket_project_id);
            }

            if ($from_where == 'ticket_view') {
                redirect('support_module/view_ticket/' . $ticket_id);
            }
        }
    }

    public function makeTicketUnsolved()
    {
        $this->lang->load('ticket_list');

        $ticket_id = $this->uri->segment(4);
        $from_where = $this->uri->segment(2);

        $is_made_unsolved = $this->Support_model->makeTicketUnsolved($ticket_id);

        if ($is_made_unsolved == true) {
            $ticket_info = $this->Support_model->getTicket($ticket_id);

            $this->session->set_flashdata('success', $this->lang->line('successful_text'));
            $this->session->set_flashdata('ticket_id', $ticket_id);

            $ticket_unsolved_text = sprintf($this->lang->line('ticket_unsolved_text'), $ticket_info->ticket_number);
            $this->session->set_flashdata('ticket_unsolved', $ticket_unsolved_text);

            $ticket_route = base_url() . 'support_module/view_ticket/' . $ticket_id;
            $see_ticket_text = $this->lang->line('see_ticket_text');
            $see_ticket_anchor = "<a href='$ticket_route'>$see_ticket_text</a>";
            $this->session->set_flashdata('see_ticket', $see_ticket_anchor);

            /*creating log starts*/
            $project_assigned_staffs = $this->Support_model->getProjectAssignedStaffs($ticket_info->ticket_project_id);

            //for assigned staffs
            if ($project_assigned_staffs) {
                foreach ($project_assigned_staffs as $an_assignd_staff) {
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        $an_assignd_staff->staff_id,                                            //2.    $created_for
                        'ticket',                                                               //3.    $type
                        $ticket_id,                                                             //4.    $type_id
                        'marked_unsolved',                                                      //5.    $activity
                        'client',                                                               //6.    $activity_by
                        'staff',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        'project',                                                              //10.   $super_type
                        $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                }
            }
            /*creating log ends*/

            if ($from_where == 'all_ticket') {
                redirect('support_module/show_ticket_list/all');
            }

            if ($from_where == 'my_ticket') {
                redirect('support_module/show_ticket_list/my');
            }

            if ($from_where == 'project_ticket') {
                redirect('support_module/show_project_ticket_list/' . $ticket_info->ticket_project_id);
            }

            if ($from_where == 'ticket_view') {
                redirect('support_module/view_ticket/' . $ticket_id);
            }
        }
    }

    public function deleteTicket()
    {
        $this->lang->load('ticket_list');

        $ticket_id = $this->uri->segment(4);
        $from_where = $this->uri->segment(2);

        $is_deleted = $this->Support_model->deleteTicket($ticket_id);

        if ($is_deleted == true) {
            $ticket_info = $this->Support_model->getTicket($ticket_id);

            $this->session->set_flashdata('success', $this->lang->line('successful_text'));

            $ticket_deleted_text = sprintf($this->lang->line('ticket_deleted_text'), $ticket_info->ticket_number);
            $this->session->set_flashdata('ticket_deleted', $ticket_deleted_text);

            /*creating log starts*/

            $a_tkt_info_w_pr_w_cr =
                $this->Support_model->getATicketInfo_withProject_withCreator($ticket_id);
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $a_tkt_info_w_pr_w_cr->id,                                              //2.    $created_for
                'ticket',                                                               //3.    $type
                $ticket_id,                                                             //4.    $type_id
                'deleted',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );

            $project_assigned_staffs = $this->Support_model->getProjectAssignedStaffs($ticket_info->ticket_project_id);

            //for assigned staffs
            if ($project_assigned_staffs) {
                foreach ($project_assigned_staffs as $an_assignd_staff) {
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        $an_assignd_staff->staff_id,                                            //2.    $created_for
                        'ticket',                                                               //3.    $type
                        $ticket_id,                                                             //4.    $type_id
                        'deleted',                                                              //5.    $activity
                        'client',                                                               //6.    $activity_by
                        'staff',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        'project',                                                              //10.   $super_type
                        $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                }
            }
            /*creating log ends*/


            if ($from_where == 'all_ticket') {
                redirect('support_module/show_ticket_list/all');
            }

            if ($from_where == 'my_ticket') {
                redirect('support_module/show_ticket_list/my');
            }

            if ($from_where == 'project_ticket') {
                redirect('support_module/show_project_ticket_list/' . $ticket_info->ticket_project_id);
            }

            if ($from_where == 'ticket_view') {
                redirect('support_module/show_ticket_list/all');
            }
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/


    public function ifStaffRelatedToProject($staff_id, $project_id)
    {
        return $this->Support_model->ifStaffRelatedToProject($staff_id, $project_id);
    }


    public function showTicket()
    {
        $this->lang->load('ticket_view');

        $ticket_id = $this->uri->segment(3);
        $data = array();

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        $data['a_tkt_info_with_pr_with_cr'] =
            $this->Support_model->getATicketInfo_withProject_withCreator($ticket_id);

        if ($data['a_tkt_info_with_pr_with_cr']->ticket_deletion_status == 1) {
            redirect('users/auth/does_not_exist');
        }

        $data['a_tkt_info_with_pr_with_cr']->ticket_created_at_datetime_string =
            $this->custom_datetime_library
                ->convert_and_return_TimestampToDateAndTime($data['a_tkt_info_with_pr_with_cr']->ticket_created_at);

        $data['files_of_a_ticket'] = $this->Support_model->getFilesOfATicket($ticket_id);

        $data['ticket_responses_with_creator'] = $this->Support_model->getTicketResponses_withCreator($ticket_id);


        /*if ticket creator not the client (may be the project got reallocated to anyone else) */
        if (
            $data['is_client'] == 'client'
            &&
            $data['a_tkt_info_with_pr_with_cr']->client_id != $data['a_tkt_info_with_pr_with_cr']->ticket_created_by
        ) {
            redirect('users/auth/need_permission');
        }

        /*non admin staffs shouldn't get acces if not belongs to project*/
        if ($data['is_staff'] == 'staff' && $data['is_admin'] == 'not_admin') {
            if (
                $this->
                ifStaffRelatedToProject
                (
                    $this->session->userdata('user_id'),
                    $data['a_tkt_info_with_pr_with_cr']->project_id
                ) == false
            ) {
                redirect('users/auth/need_permission');
            }
        }

        $data['ticket_file_count'] = 0;
        if ($data['files_of_a_ticket']) {
            $i = 0;
            foreach ($data['files_of_a_ticket'] as $a_file_of_a_ticket) {

                /*convert file size in MB*/
                $data['files_of_a_ticket'][$i]->ticket_file_size_in_MB =
                    number_format($a_file_of_a_ticket->ticket_file_size / 1024, 2);

                /*find formats starts*/
                if (
                    $a_file_of_a_ticket->ticket_file_ext == '.doc'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.docx'
                ) {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'doc';
                } else if (
                    $a_file_of_a_ticket->ticket_file_ext == '.jpg'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.jpeg'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.png'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.gif'
                ) {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'image';
                } else if ($a_file_of_a_ticket->ticket_file_ext == '.pdf') {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'pdf';
                } else if (
                    $a_file_of_a_ticket->ticket_file_ext == '.zip'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.rar'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.7z'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.tar'
                ) {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'compressed';
                } else if (
                    $a_file_of_a_ticket->ticket_file_ext == '.ppt'
                    ||
                    $a_file_of_a_ticket->ticket_file_ext == '.pptx'
                ) {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'ppt';
                } else {
                    $data['files_of_a_ticket'][$i]->ticket_file_type = 'unknown';
                }
                /*find formats ends*/

                $data['ticket_file_count']++;
                $i++;
            }
        }

        if ($data['ticket_responses_with_creator']) {

            $incr = 0;

            foreach ($data['ticket_responses_with_creator'] as $a_ticket_response_with_creator) {

                $data['ticket_responses_with_creator'][$incr]->ticket_response_created_at_datetime_string =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDateAndTime($a_ticket_response_with_creator->ticket_response_created_at);

                $data['ticket_responses_with_creator'][$incr]->ticket_response_creator_type = '';

                if
                (
                    $this->ion_auth->in_group('admin', $a_ticket_response_with_creator->ticket_response_created_by)
                    &&
                    !$this->ion_auth->in_group('staff', $a_ticket_response_with_creator->ticket_response_created_by)

                ) {
                    $data['ticket_responses_with_creator'][$incr]->ticket_response_creator_type = 'only_admin';
                }

                if
                ($this->ion_auth->in_group('client', $a_ticket_response_with_creator->ticket_response_created_by)
                ) {
                    $data['ticket_responses_with_creator'][$incr]->ticket_response_creator_type = 'client';
                }

                if
                (
                    $this->ion_auth->in_group('admin', $a_ticket_response_with_creator->ticket_response_created_by)
                    &&
                    $this->ion_auth->in_group('staff', $a_ticket_response_with_creator->ticket_response_created_by)

                ) {
                    $data['ticket_responses_with_creator'][$incr]->ticket_response_creator_type = 'both_admin_and_staff';
                }

                if
                (
                    !$this->ion_auth->in_group('admin', $a_ticket_response_with_creator->ticket_response_created_by)
                    &&
                    $this->ion_auth->in_group('staff', $a_ticket_response_with_creator->ticket_response_created_by)
                ) {
                    $data['ticket_responses_with_creator'][$incr]->ticket_response_creator_type = 'not_admin_but_staff';
                }


                $data['ticket_responses_with_creator'][$incr]->ticket_response_files = array();
                $data['ticket_responses_with_creator'][$incr]->ticket_response_files =
                    $this->Support_model->getFilesOfATicketResponse($a_ticket_response_with_creator->ticket_response_id);


                $data['ticket_responses_with_creator'][$incr]->ticket_response_file_count = 0;
                if ($data['ticket_responses_with_creator'][$incr]->ticket_response_files) {
                    $jncr = 0;

                    foreach ($data['ticket_responses_with_creator'][$incr]->ticket_response_files as $a_ticket_response_file) {

                        $a_ticket_response_file->ticket_response_file_size_in_MB =
                            number_format($a_ticket_response_file->ticket_response_file_size / 1024, 2);

                        /*find formats starts*/
                        if (
                            $a_ticket_response_file->ticket_response_file_ext == '.doc'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.docx'
                        ) {
                            $a_ticket_response_file->ticket_response_file_type = 'doc';
                        } else if (
                            $a_ticket_response_file->ticket_response_file_ext == '.jpg'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.jpeg'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.png'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.gif'
                        ) {
                            $a_ticket_response_file->ticket_response_file_type = 'image';
                        } else if ($a_ticket_response_file->ticket_response_file_ext == '.pdf') {
                            $a_ticket_response_file->ticket_response_file_type = 'pdf';
                        } else if (
                            $a_ticket_response_file->ticket_response_file_ext == '.zip'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.rar'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.7z'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.tar'
                        ) {
                            $a_ticket_response_file->ticket_response_file_type = 'compressed';
                        } else if (
                            $a_ticket_response_file->ticket_response_file_ext == '.ppt'
                            ||
                            $a_ticket_response_file->ticket_response_file_ext == '.pptx'
                        ) {
                            $a_ticket_response_file->ticket_response_file_type = 'ppt';
                        } else {
                            $a_ticket_response_file->ticket_response_file_type = 'unknown';
                        }
                        /*find formats ends*/

                        $data['ticket_responses_with_creator'][$incr]->ticket_response_file_count++;
                        $jncr++;
                    }
                }


                $data['ticket_responses_with_creator'][$incr]->ticket_response_created_at_datetime_string =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDateAndTime($a_ticket_response_with_creator->ticket_response_created_at);

                $incr++;
            }

        }

        //print_r($data['ticket_responses_with_creator']);die();

        $data['user_data_with_details'] =
            $this->Support_model->getUserdata_withDetails($this->session->userdata('user_id'));
        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();        //for user profile picture

        $data['dropzone_allowed_types'] = $this->custom_file_library->getDropzoneAllowedTypes();
        $data['dropzone_max_size'] = $this->custom_file_library->getDropzoneMaxFileSize();
        $data['dropzone_max_file_number'] = $this->custom_file_library->getDropzoneMaxNumberOfFiles();

        $data['projectroom_menu_section'] =
            $this->getProjectroom_MenuSection($data['a_tkt_info_with_pr_with_cr']->project_id);

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("support_module/ticket_view_page", $data);
        $this->load->view("common_module/footer");
    }

    public function createTicketResponseWithDropzone()
    {
        /*no validation is needed . validated through javascript*/
        /*print_r($_POST);
        print_r($_FILES);*/

        $this->lang->load('ticket_view');

        $data['ticket_id'] = $this->input->post('ticket_id');
        $data['ticket_response_description'] = $this->input->post('ticket_response_description');

        $data['ticket_response_created_by'] = $this->session->userdata('user_id');
        $data['ticket_response_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        $this->Support_model->insertTicketResponse($data);
        $ticket_response_id = $this->db->insert_id();

        if (!empty($_FILES)) {

            $i = 0;
            foreach ($_FILES['file']['name'] as $A_FILE_NAME) {

                $new_file_name = $this->custom_file_library->getNewFileName($_FILES['file']['name'][$i]);

                $move_from = $_FILES['file']['tmp_name'][$i];
                $main_dir = $this->custom_file_library->getMainDirectory();

                $move_to = FCPATH . $main_dir . '/' . $new_file_name;

                $this->custom_file_library->uploadViaDropzone($move_from, $move_to);

                $exploded_original_filename = explode(".", $_FILES['file']['name'][$i]);
                $ext = end($exploded_original_filename);

                $file_data['ticket_response_id'] = $ticket_response_id;
                $file_data['ticket_response_file_name'] = $new_file_name;
                $file_data['ticket_response_file_size'] = round($_FILES['file']['size'][$i] / 1024, 2);         //in kB
                $file_data['ticket_response_file_ext'] = '.' . $ext;
                $file_data['ticket_response_file_main_directory'] = $main_dir;

                $this->Support_model->insertTicketResponseFile($file_data);

                $i++;
            }
        }

        /*creating log starts*/
        $ticket_info = $this->Support_model->getTicket($this->input->post('ticket_id'));
        $project_assigned_staffs = $this->Support_model->getProjectAssignedStaffs($ticket_info->ticket_project_id);

        if ($this->ion_auth->is_admin()) {
            $is_admin = 'admin';
        } else {
            $is_admin = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $is_client = 'client';
        } else {
            $is_client = 'not_client';
        }

        if ($this->ion_auth->in_group('staff')) {
            $is_staff = 'staff';
        } else {
            $is_staff = 'not_staff';
        }

        if ($is_client == 'not_client') {

            $activity_by = '';

            /*if response creator is not client then make a log for client also*/
            //for client
            if ($is_admin == 'admin' && $is_staff == 'staff') {
                $activity_by = 'staff';
            } else if ($is_admin == 'not_admin' && $is_staff == 'staff') {
                $activity_by = 'staff';
            } else if ($is_admin == 'admin' && $is_staff == 'not_staff') {
                $activity_by = 'admin';
            } else {
                $activity_by = '';
            }


            $a_tkt_info_w_pr_w_cr =
                $this->Support_model->getATicketInfo_withProject_withCreator($this->input->post('ticket_id'));
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                $a_tkt_info_w_pr_w_cr->id,                                              //2.    $created_for
                'ticket_response',                                                      //3.    $type
                $this->input->post('ticket_id'),                                        //4.    $type_id
                'created_response_for_ticket',                                          //5.    $activity
                $activity_by,                                                           //6.    $activity_by
                'client',                                                               //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'project',                                                              //10.   $super_type
                $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
        }

        //for assigned staffs
        if ($project_assigned_staffs) {
            foreach ($project_assigned_staffs as $an_assignd_staff) {
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    $an_assignd_staff->staff_id,                                            //2.    $created_for
                    'ticket_response',                                                      //3.    $type
                    $this->input->post('ticket_id'),                                        //4.    $type_id
                    'created_response_for_ticket',                                          //5.    $activity
                    'client',                                                               //6.    $activity_by
                    'staff',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    'project',                                                              //10.   $super_type
                    $ticket_info->ticket_project_id,                                        //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
            }
        }
        /*creating log ends*/

    }


}