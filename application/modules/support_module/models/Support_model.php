<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Support_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function getProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function countActiveProjects_OfAClient($client_id,$search_filter)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.client_id', $client_id);
        $this->db->where('p.status', 1);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getActiveProjects_OfAClient($client_id,$search_filter,$limit,$offset)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.client_id', $client_id);
        $this->db->where('p.status', 1);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function checkIfActiveProject_BelongsToClient($project_id, $client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id', $project_id);
        $this->db->where('client_id', $client_id);
        $this->db->where('status', 1);
        $this->db->where('deletion_status', 0);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    /*--------------------------------------------------------------------*/
    public function insertTicket($data)
    {
        $this->db->insert('rspm_tbl_ticket', $data);
    }

    public function insertTicketFile($file_data)
    {
        $this->db->insert('rspm_tbl_ticket_file', $file_data);
    }

    /*---------------------------------------------------------------------*/

    public function getAllTickets_withCreator_withProject()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_deletion_status!=', 1);
        $this->db->join('users as u', 't.ticket_created_by = u.id');
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id = p.project_id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getClientTickets_withCreator_withProject($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_deletion_status!=', 1);
        $this->db->join('users as u', 't.ticket_created_by = u.id');
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id = p.project_id');
        $this->db->where('p.client_id', $client_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getStaffTickets_withCreator_withProject($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_deletion_status!=', 1);
        $this->db->join('users as u', 't.ticket_created_by = u.id');
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id = p.project_id');
        $this->db->join('rspm_tbl_project_assigned_staff as pas', 'p.project_id = pas.project_id');
        $this->db->where('pas.staff_id', $staff_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getProjectTickets_withCreator_withProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_project_id', $project_id);
        $this->db->where('t.ticket_deletion_status!=', 1);
        $this->db->join('users as u', 't.ticket_created_by = u.id');
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id = p.project_id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*--------------------------------------------------------------------*/

    public function makeTicketSolved($ticket_id)
    {
        $this->db->set('ticket_solution_status', 1);
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('rspm_tbl_ticket');

        return true;
    }

    public function makeTicketUnsolved($ticket_id)
    {
        $this->db->set('ticket_solution_status', 0);
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('rspm_tbl_ticket');

        return true;
    }

    public function deleteTicket($ticket_id)
    {
        $this->db->set('ticket_deletion_status', 1);
        $this->db->where('ticket_id', $ticket_id);
        $this->db->update('rspm_tbl_ticket');

        return true;
    }

    /*-------------------------------------------------------------*/

    public function getTicket($ticket_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket');
        $this->db->where('ticket_id', $ticket_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getATicketInfo_withProject_withCreator($ticket_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket as t');
        $this->db->where('t.ticket_id', $ticket_id);

        $this->db->join('users as u', 't.ticket_created_by = u.id');
        $this->db->join('rspm_tbl_project as p', 't.ticket_project_id = p.project_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    public function getFilesOfATicket($ticket_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket_file');
        $this->db->where('ticket_id', $ticket_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getUserdata_withDetails($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);

        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function insertTicketResponse($data)
    {
        $this->db->insert('rspm_tbl_ticket_response', $data);
    }

    public function insertTicketResponseFile($file_data)
    {
        $this->db->insert('rspm_tbl_ticket_response_file', $file_data);
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function ifStaffRelatedToProject($staff_id, $project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('staff_id', $staff_id);
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getTicketResponses_withCreator($ticket_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket_response as tr');
        $this->db->where('tr.ticket_id', $ticket_id);
        $this->db->join('rspm_tbl_ticket as t', 'tr.ticket_id = t.ticket_id');
        $this->db->join('users as u', 'tr.ticket_response_created_by = u.id');
        $this->db->join('rspm_tbl_user_details as ud', 'tr.ticket_response_created_by = ud.user_id');
        $this->db->order_by('tr.ticket_response_created_at','asc');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getFilesOfATicketResponse($ticket_response_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket_response_file');
        $this->db->where('ticket_response_id', $ticket_response_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    /*--------For Log Purposes Starts-------*/
    public function getProjectAssignedStaffs($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    /*--------For Log Purposes Starts-------*/


}