<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                        class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li><a href="<?php echo base_url() . 'settings_module' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php if ($this->session->flashdata('validation_errors')) echo
                            $this->session->flashdata('validation_errors');
                            ?>

                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <br>
                            <?php if ($this->session->flashdata('update_success_text')) echo
                            $this->session->flashdata('update_success_text');
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'settings_module/update_file_settings' ?>" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="upload_path"><?php echo lang('label_upload_path_text') ?></label>

                                <input type="text" name="upload_path" class="form-control" id="upload_path"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'upload_path')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_upload_path_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="allowed_types"><?php echo lang('label_allowed_types_text') ?></label>

                                <input type="text" name="allowed_types" class="form-control" id="allowed_types"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'allowed_types')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_allowed_types_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="max_size"><?php echo lang('label_max_size_text') ?></label>

                                <input type="text" name="max_size" class="form-control" id="max_size"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'max_size')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_max_size_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="dropzone_allowed_types"><?php echo lang('label_dropzone_allowed_types_text') ?></label>

                                <input type="text" name="dropzone_allowed_types" class="form-control" id="dropzone_allowed_types"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'dropzone_allowed_types')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_dropzone_allowed_types_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="dropzone_max_size"><?php echo lang('label_dropzone_max_size_text') ?></label>

                                <input type="text" name="dropzone_max_size" class="form-control" id="dropzone_max_size"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'dropzone_max_size')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_dropzone_max_size_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="dropzone_max_file_number"><?php echo lang('label_dropzone_max_file_number_text') ?></label>

                                <input type="text" name="dropzone_max_file_number" class="form-control" id="dropzone_max_file_number"
                                       value="<?php
                                       if ($all_file_settings) {
                                           foreach ($all_file_settings as $a_file_settings) {
                                               if (($a_file_settings->settings_key) == 'dropzone_max_file_number')
                                                   echo $a_file_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_dropzone_max_file_number_text') ?>">
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




