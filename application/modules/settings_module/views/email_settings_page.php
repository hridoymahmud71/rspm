<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrumb_home_text') ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() . 'settings_module' ?>">
                    <?php echo lang('breadcrumb_section_text') ?>
                </a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                            <?php if ($this->session->flashdata('validation_errors')) echo
                            $this->session->flashdata('validation_errors');
                            ?>

                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <br>
                            <?php if ($this->session->flashdata('update_success_text')) echo
                            $this->session->flashdata('update_success_text');
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'settings_module/update_email_settings' ?>" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="send_email_notification"><?php echo lang('label_send_email_notification_text') ?></label>

                                <select class="form-control" name="send_email_notification" id="protocol">
                                    <option value="1"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'send_email_notification')
                                                    && ($a_image_settings->settings_value == '1')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_send_email_notification_yes_text') ?>
                                    </option>

                                    <option value="0"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'send_email_notification')
                                                    && ($a_image_settings->settings_value == '0')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_send_email_notification_no_text') ?>
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="protocol"><?php echo lang('label_protocol_text') ?></label>

                                <select class="form-control" name="protocol" id="protocol">
                                    <option value="php"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'protocol')
                                                    && ($a_image_settings->settings_value == 'php')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_protocol_php_text') ?>
                                    </option>

                                    <option value="smtp"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'protocol')
                                                    && ($a_image_settings->settings_value == 'smtp')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_protocol_smtp_text') ?>
                                    </option>
                                </select>
                            </div>

                            <div id="smtp_wrapper">

                                <div class="form-group">
                                    <label for="smtp_host"><?php echo lang('label_smtp_host_text') ?></label>

                                    <input type="text" name="smtp_host" class="form-control" id="smtp_host"
                                           value="<?php
                                           if ($all_email_settings) {
                                               foreach ($all_email_settings as $a_file_settings) {
                                                   if (($a_file_settings->settings_key) == 'smtp_host')
                                                       echo $a_file_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_smtp_host_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="smtp_port"><?php echo lang('label_smtp_port_text') ?></label>

                                    <input type="text" name="smtp_port" class="form-control" id="smtp_port"
                                           value="<?php
                                           if ($all_email_settings) {
                                               foreach ($all_email_settings as $a_file_settings) {
                                                   if (($a_file_settings->settings_key) == 'smtp_port')
                                                       echo $a_file_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_smtp_port_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="smtp_timeout"><?php echo lang('label_smtp_timeout_text') ?></label>
                                    <input type="text" name="smtp_timeout" class="form-control" id="smtp_timeout"
                                           value="<?php
                                           if ($all_email_settings) {
                                               foreach ($all_email_settings as $a_file_settings) {
                                                   if (($a_file_settings->settings_key) == 'smtp_timeout')
                                                       echo $a_file_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_smtp_timeout_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="smtp_user"><?php echo lang('label_smtp_user_text') ?></label>

                                    <input type="text" name="smtp_user" class="form-control" id="smtp_user"
                                           value="<?php
                                           if ($all_email_settings) {
                                               foreach ($all_email_settings as $a_file_settings) {
                                                   if (($a_file_settings->settings_key) == 'smtp_user')
                                                       echo $a_file_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_smtp_user_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="smtp_pass"><?php echo lang('label_smtp_pass_text') ?></label>

                                    <input type="text" name="smtp_pass" class="form-control" id="smtp_pass"
                                           value="<?php
                                           if ($all_email_settings) {
                                               foreach ($all_email_settings as $a_file_settings) {
                                                   if (($a_file_settings->settings_key) == 'smtp_pass')
                                                       echo $a_file_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_smtp_pass_text') ?>">
                                </div>

                            </div>


                            <div class="form-group">
                                <label for="mailtype"><?php echo lang('label_mailtype_text') ?></label>

                                <select class="form-control" name="mailtype" id="">
                                    <option value="html"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'mailtype')
                                                    && ($a_image_settings->settings_value == 'html')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_mailtype_html_text') ?>
                                    </option>

                                    <option value="text"
                                        <?php
                                        if ($all_email_settings) {
                                            foreach ($all_email_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'mailtype')
                                                    && ($a_image_settings->settings_value == 'text')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_mailtype_text_text') ?>
                                    </option>
                                </select>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id=""
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $('#smtp_wrapper').hide();
        show_hide_smtp_wrapper();

        $('#protocol').change(function () {
            show_hide_smtp_wrapper();
        });

    });

    function show_hide_smtp_wrapper() {
        if ($('#protocol').val() == 'smtp') {
            $('#smtp_wrapper').show();
        } else {
            $('#smtp_wrapper').hide();
        }
    }
</script>




