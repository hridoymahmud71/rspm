<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li><a href="<?php echo base_url() . 'settings_module' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php if ($this->session->flashdata('validation_errors')) echo
                            $this->session->flashdata('validation_errors');
                            ?>

                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <br>
                            <?php if ($this->session->flashdata('update_success_text')) echo
                            $this->session->flashdata('update_success_text');
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'settings_module/update_image_settings' ?>" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <br>

                            <!--Original Settings Starts-->

                            <div style="font-size: larger"><?php echo lang('image_settings_text') ?></div>
                            <hr>

                            <div class="form-group">
                                <label for="encrypt_name"><?php echo lang('label_encrypt_name_text') ?></label>

                                <select class="form-control" name="encrypt_name" id="">
                                    <option value="1"
                                        <?php
                                        if ($all_image_settings) {
                                            foreach ($all_image_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'encrypt_name')
                                                    && ($a_image_settings->settings_value == '1')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_encrypt_name_yes_text') ?>
                                    </option>

                                    <option value="0"
                                        <?php
                                        if ($all_image_settings) {
                                            foreach ($all_image_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'encrypt_name')
                                                    && ($a_image_settings->settings_value == '0')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_encrypt_name_no_text') ?>
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="upload_path"><?php echo lang('label_upload_path_text') ?></label>

                                <input type="text" name="upload_path" class="form-control" id="upload_path"
                                       value="<?php
                                       if ($all_image_settings) {
                                           foreach ($all_image_settings as $a_image_settings) {
                                               if (($a_image_settings->settings_key) == 'upload_path')
                                                   echo $a_image_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_upload_path_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="allowed_types"><?php echo lang('label_allowed_types_text') ?></label>

                                <input type="text" name="allowed_types" class="form-control" id="allowed_types"
                                       value="<?php
                                       if ($all_image_settings) {
                                           foreach ($all_image_settings as $a_image_settings) {
                                               if (($a_image_settings->settings_key) == 'allowed_types')
                                                   echo $a_image_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_allowed_types_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="max_size"><?php echo lang('label_max_size_text') ?></label>

                                <input type="text" name="max_size" class="form-control" id="max_size"
                                       value="<?php
                                       if ($all_image_settings) {
                                           foreach ($all_image_settings as $a_image_settings) {
                                               if (($a_image_settings->settings_key) == 'max_size')
                                                   echo $a_image_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_max_size_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="max_width"><?php echo lang('label_max_width_text') ?></label>

                                <input type="text" name="max_width" class="form-control" id="max_width"
                                       value="<?php
                                       if ($all_image_settings) {
                                           foreach ($all_image_settings as $a_image_settings) {
                                               if (($a_image_settings->settings_key) == 'max_width')
                                                   echo $a_image_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_max_width_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="max_height"><?php echo lang('label_max_height_text') ?></label>

                                <input type="text" name="max_height" class="form-control" id="max_height"
                                       value="<?php
                                       if ($all_image_settings) {
                                           foreach ($all_image_settings as $a_image_settings) {
                                               if (($a_image_settings->settings_key) == 'max_height')
                                                   echo $a_image_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_max_height_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="create_thumb"><?php echo lang('label_create_thumb_text') ?></label>

                                <select class="form-control" name="create_thumb" id="create_thumb">
                                    <option value="1"
                                        <?php
                                        if ($all_image_settings) {
                                            foreach ($all_image_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'create_thumb')
                                                    && ($a_image_settings->settings_value == '1')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_create_thumb_yes_text') ?>
                                    </option>

                                    <option value="0"
                                        <?php
                                        if ($all_image_settings) {
                                            foreach ($all_image_settings as $a_image_settings) {
                                                if (($a_image_settings->settings_key == 'create_thumb')
                                                    && ($a_image_settings->settings_value == '0')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_create_thumb_no_text') ?>
                                    </option>
                                </select>
                            </div>

                            <!--Original Settings Ends-->

                            <br><br>

                            <!--Thumb Settings Starts-->
                            <div id="thumb_settings_wrapper">
                                <div style="font-size: larger"><?php echo lang('thumb_settings_text') ?></div>
                                <hr>

                                <div class="form-group">
                                    <label for="thumb_source"><?php echo lang('label_thumb_source_text') ?></label>

                                    <input type="text" name="thumb_source" class="form-control" id="thumb_source"
                                           value="<?php
                                           if ($all_image_settings) {
                                               foreach ($all_image_settings as $a_image_settings) {
                                                   if (($a_image_settings->settings_key) == 'thumb_source')
                                                       echo $a_image_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_thumb_source_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="thumb_marker"><?php echo lang('label_thumb_marker_text') ?></label>

                                    <input type="text" name="thumb_marker" class="form-control" id="thumb_marker"
                                           value="<?php
                                           if ($all_image_settings) {
                                               foreach ($all_image_settings as $a_image_settings) {
                                                   if (($a_image_settings->settings_key) == 'thumb_marker')
                                                       echo $a_image_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_thumb_marker_text') ?>">
                                </div>

                                <div class="form-group">
                                    <label for="maintain_ratio"><?php echo lang('label_maintain_thumb_ratio_text') ?></label>

                                    <select class="form-control" name="maintain_ratio" id="maintain_ratio">
                                        <option value="1"
                                            <?php
                                            if ($all_image_settings) {
                                                foreach ($all_image_settings as $a_image_settings) {
                                                    if (($a_image_settings->settings_key == 'maintain_ratio')
                                                        && ($a_image_settings->settings_value == '1')
                                                    )
                                                        echo 'selected';
                                                }
                                            }
                                            ?>
                                                class="">
                                            <?php echo lang('option_maintain_thumb_ratio_yes_text') ?>
                                        </option>

                                        <option value="0"
                                            <?php
                                            if ($all_image_settings) {
                                                foreach ($all_image_settings as $a_image_settings) {
                                                    if (($a_image_settings->settings_key == 'maintain_ratio')
                                                        && ($a_image_settings->settings_value == '0')
                                                    )
                                                        echo 'selected';
                                                }
                                            }
                                            ?>
                                                class="">
                                            <?php echo lang('option_maintain_thumb_ratio_no_text') ?>
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="thumb_width"><?php echo lang('label_thumb_width_text') ?></label>

                                    <input type="text" name="thumb_width" class="form-control" id="thumb_width"
                                           value="<?php
                                           if ($all_image_settings) {
                                               foreach ($all_image_settings as $a_image_settings) {
                                                   if (($a_image_settings->settings_key) == 'thumb_width')
                                                       echo $a_image_settings->settings_value;
                                               }
                                           }
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_thumb_width_text') ?>">
                                </div>

                                <div id="thumb_height_wrapper">
                                    <div class="form-group">
                                        <label for="thumb_height"><?php echo lang('label_thumb_height_text') ?></label>

                                        <input type="text" name="thumb_height" class="form-control" id="thumb_height"
                                               value="<?php
                                               if ($all_image_settings) {
                                                   foreach ($all_image_settings as $a_image_settings) {
                                                       if (($a_image_settings->settings_key) == 'thumb_width')
                                                           echo $a_image_settings->settings_value;
                                                   }
                                               }
                                               ?>"
                                               placeholder="<?php echo lang('placeholder_thumb_height_text') ?>">
                                    </div>
                                </div>

                            </div>

                            <!--Thumb Settings Ends-->

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $(function () {
        $('#thumb_settings_wrapper').hide();
        show_hide_thumb_settings();

        $('#create_thumb').change(function () {
            show_hide_thumb_settings();
        });

    });

    function show_hide_thumb_settings() {
        if ($('#create_thumb').val() == 1) {
            $('#thumb_settings_wrapper').show();
        } else {
            $('#thumb_settings_wrapper').hide();
        }
    }
</script>

<script>
    $(function () {
        $('#thumb_height_wrapper').hide();
        show_hide_thumb_height();

        $('#maintain_ratio').change(function () {
            show_hide_thumb_height();
        });


    });
    
    function show_hide_thumb_height() {
        if ($('#maintain_ratio').val() == 0) {
            $('#thumb_height_wrapper').show();
        } else {
            $('#thumb_height_wrapper').hide();
        }
    }
</script>
