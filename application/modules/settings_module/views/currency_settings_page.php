<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    &nbsp;
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a class="btn btn-primary"
                   href="<?php echo base_url() . 'currency_module/' ?>"><?php echo lang('view_currency_text') ?>
                    &nbsp<span class="icon"><i class="fa fa-eye"></i></span></a>

                <a class="btn btn-primary"
                   href="<?php echo base_url() . 'currency_module/add_currency' ?>"><?php echo lang('add_currency_text') ?>
                    &nbsp<span class="icon"><i class="fa fa-plus"></i></span></a>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li><a href="<?php echo base_url() . 'settings_module' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php if ($this->session->flashdata('validation_errors')) echo
                            $this->session->flashdata('validation_errors');
                            ?>
                            <br>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <br>
                            <?php if ($this->session->flashdata('update_success_text')) echo
                            $this->session->flashdata('update_success_text');
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'settings_module/update_currency_settings' ?>" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <?php if (!empty($all_currencies)) { ?>

                            <div class="form-group">
                                <label for="currency_id"><?php echo lang('label_currency_text') ?></label>

                                <select class="form-control" name="currency_id" id="currency_id">
                                    <option value="0"
                                            currency_name="<?php echo lang('empty_text') ?>"
                                            conversion_rate="<?php echo lang('empty_text') ?>"
                                            currency_sign="<?php echo lang('empty_text') ?>">
                                        <?php echo lang('currency_choose_text') ?>
                                    </option>

                                    <?php foreach ($all_currencies as $a_currency) { ?>
                                        <option
                                                currency_name="<?php echo $a_currency->currency_name ?>"
                                                conversion_rate="<?php echo $a_currency->conversion_rate ?>"
                                                currency_sign="<?php echo $a_currency->currency_sign ?>"

                                                value="<?php echo $a_currency->currency_id ?>"

                                            <?php
                                            if ($all_currency_settings) {
                                                foreach ($all_currency_settings as $a_currency_settings) {
                                                    if (($a_currency_settings->settings_key == 'currency_id')
                                                        && ($a_currency_settings->settings_value == $a_currency->currency_id)
                                                    )
                                                        echo 'selected';
                                                }
                                            }
                                            ?>
                                        >
                                            <?php echo $a_currency->currency_short_name ?>

                                        </option>
                                    <?php } ?>
                                </select>

                            </div>


                            <div class="form-group">
                                <label for="currency_name"><?php echo lang('label_currency_name_text') ?></label>

                                <input type="text" name="currency_name" class="form-control" id="currency_name"
                                       value="<?php
                                       if ($all_currency_settings) {
                                           foreach ($all_currency_settings as $a_currency_settings) {
                                               if (($a_currency_settings->settings_key) == 'currency_name')
                                                   echo $a_currency_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="" readonly>
                            </div>

                            <div class="form-group">
                                <label for="conversion_rate"><?php echo lang('label_conversion_rate_text') ?>
                                    <small><?php echo lang('label_help_usd_to_your_currenct_text') ?></small>
                                </label>

                                <input type="text" name="conversion_rate" class="form-control" id="conversion_rate"
                                       value="<?php
                                       if ($all_currency_settings) {
                                           foreach ($all_currency_settings as $a_currency_settings) {
                                               if (($a_currency_settings->settings_key) == 'conversion_rate')
                                                   echo $a_currency_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="" readonly>
                            </div>

                            <div class="form-group">
                                <label for="currency_sign"><?php echo lang('label_currency_sign_text') ?></label>

                                <input type="text" name="currency_sign" class="form-control" id="currency_sign"
                                       value="<?php
                                       if ($all_currency_settings) {
                                           foreach ($all_currency_settings as $a_currency_settings) {
                                               if (($a_currency_settings->settings_key) == 'currency_sign')
                                                   echo $a_currency_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="" readonly>
                            </div>

                            <div class="form-group">
                                <label for="currency_position"><?php echo lang('label_currency_position_text_text') ?></label>

                                <select class="form-control" name="currency_position" id="currency_position">

                                    <option value="left_along"
                                        <?php
                                        if ($all_currency_settings) {
                                            foreach ($all_currency_settings as $a_currency_settings) {
                                                if (($a_currency_settings->settings_key == 'currency_position')
                                                    && ($a_currency_settings->settings_value == "left_along")
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_currency_position_left_along_text') ?>
                                    </option>

                                    <option value="right_along"
                                        <?php
                                        if ($all_currency_settings) {
                                            foreach ($all_currency_settings as $a_currency_settings) {
                                                if (($a_currency_settings->settings_key == 'currency_position')
                                                    && ($a_currency_settings->settings_value == "right_along")
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_currency_position_right_along_text') ?>
                                    </option>

                                    <option value="left_far"
                                        <?php
                                        if ($all_currency_settings) {
                                            foreach ($all_currency_settings as $a_currency_settings) {
                                                if (($a_currency_settings->settings_key == 'currency_position')
                                                    && ($a_currency_settings->settings_value == "left_far")
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_currency_position_left_far_text') ?>
                                    </option>

                                    <option value="right_far"
                                        <?php
                                        if ($all_currency_settings) {
                                            foreach ($all_currency_settings as $a_currency_settings) {
                                                if (($a_currency_settings->settings_key == 'currency_position')
                                                    && ($a_currency_settings->settings_value == "right_far")
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                    >
                                        <?php echo lang('option_currency_position_right_far_text') ?>
                                    </option>

                                </select>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>

                        <?php } else { ?>
                            <div class="form-group" style="font-size: larger;color: darkred">
                                <?php echo lang('no_currency_found_text') ?>
                                &nbsp;
                                <a href="<?php echo base_url().'currency_module'?>">
                                    <?php echo lang('add_or_activate_currency_text') ?>
                                </a>
                            </div>
                        <?php } ?>

                    </form>
                </div>
                <!-- /.box --> </div>



        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>

    $('#currency_id').change(function () {
        var options = $(this.options[this.selectedIndex]);
        var currency_name = options.attr('currency_name');
        var conversion_rate = options.attr('conversion_rate');
        var currency_sign = options.attr('currency_sign');

        $("#currency_name").val(currency_name);
        $("#conversion_rate").val(conversion_rate);
        $("#currency_sign").val(currency_sign);

    });
</script>