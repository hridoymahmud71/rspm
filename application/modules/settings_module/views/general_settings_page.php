<!-- Content Wrapper. Contains page content -->

<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    body {
        font: 12px 'Open Sans';
    }

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .img-preview {
        max-width: 50%;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i
                            class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li><a href="<?php echo base_url() . 'settings_module' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active"><?php echo lang('breadcrumb_page_text') ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>

                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php if ($this->session->flashdata('validation_errors')) {
                                echo '<br>';
                                echo $this->session->flashdata('validation_errors');
                            }
                            ?>

                            <?php if ($this->session->flashdata('image_upload_errors')) {
                                echo '<br>';
                                echo $this->session->flashdata('image_upload_errors');
                            }
                            ?>
                            <?php if ($this->session->flashdata('image_resize_errors')) {
                                echo '<br>';
                                echo $this->session->flashdata('image_resize_errors');
                            }
                            ?>

                            <!--demo-->
                            <?php if ($this->session->flashdata('file_upload_errors')) {
                                echo '<br>';
                                echo $this->session->flashdata('file_upload_errors');
                            }
                            ?>
                            <!--demo-->

                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                            <!--demo-->
                            <?php if ($this->session->flashdata('file_upload_success')) {
                                echo '<br>';
                                echo $this->session->flashdata('file_upload_success');
                            }
                            ?>
                            <!--demo-->
                            <br>
                            <?php if ($this->session->flashdata('update_success_text')) {
                                echo '<br>';
                                echo $this->session->flashdata('update_success_text');
                            }
                            ?>

                            <?php if ($this->session->flashdata('image_upload_success')) {
                                echo '<br>';
                                echo $this->session->flashdata('image_upload_success');
                            }
                            ?>
                            <br>
                            <?php if ($this->session->flashdata('image_resize_success')) {
                                echo '<br>';
                                echo $this->session->flashdata('image_resize_success');
                            }
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . 'settings_module/update_general_settings' ?>" role="form"
                          id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <!--demo-->
                            <!--<div class="form-group">
                                <input type="file" class="" name="demo">
                            </div>-->
                            <!--demo-->

                            <div class="form-group">
                                <label for="site_name"><?php echo lang('label_site_name_text') ?></label>

                                <input type="text" name="site_name" class="form-control" id="site_name"
                                       value="<?php
                                       if ($all_general_settings) {
                                           foreach ($all_general_settings as $a_general_settings) {
                                               if (($a_general_settings->settings_key) == 'site_name')
                                                   echo $a_general_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="site_short_name"><?php echo lang('label_site_short_name_text') ?></label>

                                <input type="text" name="site_short_name" class="form-control" id="site_short_name"
                                       value="<?php
                                       if ($all_general_settings) {
                                           foreach ($all_general_settings as $a_general_settings) {
                                               if (($a_general_settings->settings_key) == 'site_short_name')
                                                   echo $a_general_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_site_short_name_text') ?>">
                            </div>

                            <div class="form-group">
                                <label for="site_email"><?php echo lang('label_site_email_text') ?></label>

                                <input type="text" name="site_email" class="form-control" id="site_email"
                                       value="<?php
                                       if ($all_general_settings) {
                                           foreach ($all_general_settings as $a_general_settings) {
                                               if (($a_general_settings->settings_key) == 'site_email')
                                                   echo $a_general_settings->settings_value;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_site_email_text') ?>">
                            </div>

                            <!--if true image from database, else static image //currently not working -->
                            <?php $image_found = false; ?>

                            <!--image upload snippet starts-->
                            <div class="form-group">
                                <label for="site_logo"><?php echo lang('label_site_logo_text') ?></label>
                                <div class="main-img-preview">

                                    <?php if ($all_general_settings) {
                                        foreach ($all_general_settings as $a_general_settings) {
                                            if (($a_general_settings->settings_key == 'site_logo')
                                                && ($a_general_settings->settings_value != '')
                                            ) {
                                                ?>
                                                <div><?php $image_found = true ?></div>
                                                <img class="thumbnail img-preview"
                                                     src="<?php echo base_url() . $image_path . '/' . $a_general_settings->settings_value ?>"
                                                     title="Preview Logo" alt="">
                                            <?php }
                                        }
                                    } ?>

                                    <?php if ($image_found == false) { ?>

                                        <img class="thumbnail img-preview"
                                             src="<?php echo base_url()
                                                 . 'project_base_assets/base_demo_images/placeholder_image_demo.jpg' ?>"
                                             title="Preview Logo">

                                    <?php } ?>

                                </div>

                                <div class="input-group">
                                    <input id="fakeUploadLogo" class="form-control fake-shadow"
                                           placeholder="Choose File" disabled="disabled">
                                    <div class="input-group-btn">
                                        <div class="fileUpload btn btn-primary fake-shadow">
                                            <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_site_logo_text') ?></span>
                                            <input id="logo-id" name="site_logo" type="file"
                                                   value=""
                                                   class="attachment_upload">
                                        </div>
                                    </div>
                                </div>
                                <p class="help-block"><?php echo lang('help_site_logo_text') ?></p>
                            </div>
                            <!--image upload snippet ends-->


                            <div class="form-group">
                                <label for="logo_or_sitename"><?php echo lang('label_logo_or_site_name_text') ?></label>

                                <select class="form-control" name="logo_or_sitename" id="">
                                    <option value="sitename"
                                        <?php
                                        if ($all_general_settings) {
                                            foreach ($all_general_settings as $a_general_settings) {
                                                if (($a_general_settings->settings_key == 'logo_or_sitename')
                                                    && ($a_general_settings->settings_value == 'sitename')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_site_name_text') ?>
                                    </option>

                                    <option value="logo"
                                        <?php
                                        if ($all_general_settings) {
                                            foreach ($all_general_settings as $a_general_settings) {
                                                if (($a_general_settings->settings_key == 'logo_or_sitename')
                                                    && ($a_general_settings->settings_value == 'logo')
                                                )
                                                    echo 'selected';
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_logo_text') ?>
                                    </option>
                                </select>
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.box --> </div>


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $(document).ready(function () {
        var brand = document.getElementById('logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#logo-id").change(function () {
            readURL(this);
        });
    });


</script>