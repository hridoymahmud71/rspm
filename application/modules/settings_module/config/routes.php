<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['settings_module'] = 'SettingsController';

$route['settings_module/general_settings'] = 'GeneralSettingsController';
$route['settings_module/update_general_settings'] = 'GeneralSettingsController/updateGeneralSettings';

$route['settings_module/contact_settings'] = 'ContactSettingsController';
$route['settings_module/update_contact_settings'] = 'ContactSettingsController/updateContactSettings';

$route['settings_module/image_settings'] = 'ImageSettingsController';
$route['settings_module/update_image_settings'] = 'ImageSettingsController/updateImageSettings';

$route['settings_module/currency_settings'] = 'CurrencySettingsController';
$route['settings_module/update_currency_settings'] = 'CurrencySettingsController/updateCurrencySettings';

$route['settings_module/datetime_settings'] = 'DatetimeSettingsController';
$route['settings_module/update_datetime_settings'] = 'DatetimeSettingsController/updateDatetimeSettings';

$route['settings_module/file_settings'] = 'FileSettingsController';
$route['settings_module/update_file_settings'] = 'FileSettingsController/updateFileSettings';

$route['settings_module/email_settings'] = 'EmailSettingsController';
$route['settings_module/update_email_settings'] = 'EmailSettingsController/updateEmailSettings';