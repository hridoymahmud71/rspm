<?php

/*page texts*/
$lang['page_title_text'] = 'General Settings';
$lang['page_subtitle_text'] = 'Edit and update general settings here';
$lang['box_title_text'] = 'General Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'General Settings';



/*General settings form texts*/
$lang['label_site_name_text'] = 'Site\'s Name';
$lang['label_site_short_name_text'] = 'Site\'s Short Name';
$lang['label_site_email_text'] = 'Site\'s Email';
$lang['label_site_logo_text'] = 'Site\'s Logo';
$lang['label_logo_or_site_name_text'] = 'Show Logo or Site\'s Name';

$lang['option_site_name_text'] = 'Show Site\'s Name';
$lang['option_logo_text'] = 'Show Logo';



$lang['placeholder_site_name_text'] = 'Enter Site\'s Name';
$lang['placeholder_site_short_name_text'] = 'Enter Site\'s Short Name';
$lang['placeholder_site_email_text'] = 'Enter Site\'s Short Name';

$lang['help_site_logo_text'] = 'Upload Site\'s Logo';
$lang['upload_site_logo_text'] = 'Upload Logo';


$lang['button_submit_text'] = 'Update General Settings';

/*validation error texts*/
$lang['site_name_required'] = 'Site\'s Name is Required';
$lang['site_short_name_required'] = 'Site\'s Short Name is Required';
$lang['site_short_name_maxlength'] = 'Site\'s Short Name cannot be longer than %s characters';

$lang['site_email_required'] = 'Site\'s Email is Required';
$lang['site_email_not_valid'] = 'Site\'s Email is not Valid';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated General Settings';

