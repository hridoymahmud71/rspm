<?php

/*page texts*/
$lang['page_title_text'] = 'Email Settings';
$lang['page_subtitle_text'] = 'Edit and update email settings here';
$lang['box_title_text'] = 'Email Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Email Settings';

$lang['button_submit_text'] = 'Update Email Settings';

/*File settings form texts*/
$lang['label_send_email_notification_text'] = 'Send Email Notification';
$lang['label_protocol_text'] = 'Email Protocol';
$lang['label_smtp_host_text'] = 'SMTP Host';
$lang['label_smtp_port_text'] = 'SMTP Port';
$lang['label_smtp_timeout_text'] = 'SMTP Timeout';
$lang['label_smtp_user_text'] = 'SMTP User Email Address';
$lang['label_smtp_pass_text'] = 'SMTP User Email Password';
$lang['label_mailtype_text'] = 'Mail Type';

$lang['placeholder_smtp_host_text'] = 'Example: ssl://smtp.googlemail.com';
$lang['placeholder_smtp_port_text'] = 'Example:  for gmail-ssl enter: 465';
$lang['placeholder_smtp_timeout_text'] = 'Enter 7 if unsure';
$lang['placeholder_smtp_user_text'] = 'Enter your email id';
$lang['placeholder_smtp_pass_text'] = 'Enter your email password';

$lang['option_send_email_notification_yes_text'] = 'Yes, send email notification';
$lang['option_send_email_notification_no_text'] = 'No, do not send notification ';

$lang['option_protocol_php_text'] = 'PHP';
$lang['option_protocol_smtp_text'] = 'SMTP';

$lang['option_mailtype_html_text'] = 'HTML';
$lang['option_mailtype_text_text'] = 'Text';


/*validation error texts*/
$lang['smtp_host_required_text'] = 'SMTP Host is required';

$lang['smtp_port_required_text'] = 'SMTP Port  Is Required';
$lang['smtp_port_integer_text'] = 'SMTP Port  Must Be Integer';
$lang['smtp_port_max_length_text'] = 'SMTP Port should be less than or equal to %1$d Digits';

$lang['smtp_timeout_required_text'] = 'SMTP Timeout  Is Required';
$lang['smtp_timeout_integer_text'] = 'SMTP Timeout Must Be Integer';
$lang['smtp_timeout_less_than_equal_to_text'] = 'SMTP Timeout should be less than or equal to %1$d';


$lang['smtp_user_required_text'] = 'SMTP User Email is required';
$lang['smtp_user_valid_email_text'] = 'SMTP User Email is not valid';

$lang['smtp_pass_required_text'] = 'SMTP User Password is required';


/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Email Settings';

