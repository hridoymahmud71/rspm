<?php

/*page texts*/
$lang['page_title_text'] = 'File Settings';
$lang['page_subtitle_text'] = 'Edit and update file settings here';
$lang['box_title_text'] = 'File Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'File Settings';


/*File settings form texts*/
$lang['label_encrypt_name_text'] = 'Encrypt Name ?';
$lang['label_upload_path_text'] = 'File Upload Path';
$lang['label_allowed_types_text'] = 'Allowed File Types';
$lang['label_max_size_text'] = 'Maximum File Size (kB)';
$lang['label_dropzone_allowed_types_text'] = 'Allowed File Types For Dropzone';
$lang['label_dropzone_max_size_text'] = 'Maximum File Size (MB) For Dropzone';
$lang['label_dropzone_max_file_number_text'] = 'Maximum No. of Files For Dropzone';

$lang['placeholder_upload_path_text'] = 'Example: project_files';
$lang['placeholder_allowed_types_text'] = 'Example:  doc | jpg | pdf | ppt';
$lang['placeholder_max_size_text'] = 'Enter Maximum Size (do not enter decimal values)';
$lang['placeholder_dropzone_allowed_types_text'] = 'Example:  doc | jpg | pdf | ppt';
$lang['placeholder_dropzone_max_size_text'] = 'Enter Maximum Size (do not enter decimal values)';
$lang['placeholder_dropzone_max_file_number_text'] = 'How many files can be uploaded at a time';

$lang['button_submit_text'] = 'Update File Settings';

/*validation error texts*/
$lang['upload_path_required_text'] = 'Upload Path For File Is Required';
$lang['allowed_types_required_text'] = 'File Types Is Required';
$lang['dropzone_allowed_types_required_text'] = 'File Types Is Required For Dropzone';

$lang['max_size_required_text'] = 'Maximum File Size Is Required';
$lang['max_size_integer_text'] = 'Maximum File Size Should Be Integer';

$lang['dropzone_max_size_required_text'] = 'Maximum File Size for Dropzone Is Required';
$lang['dropzone_max_size_integer_text'] = 'Maximum File Size for Dropzone Should Be Integer';

$lang['dropzone_max_file_number_required_text'] = 'Maximum File Number for Dropzone Is Required';
$lang['dropzone_max_file_number_integer_text'] = 'Maximum File Number for Dropzone Should Be Integer';
$lang['dropzone_max_file_number_greater_than_equal_to_text'] =
    'Maximum File Size for Dropzone Should Be Greater Than Or Equal To %1$d';
$lang['dropzone_max_file_number_less_than_equal_to_text'] =
    'Maximum File Size for Dropzone Should Be Less Than Than Or Equal To %1$d';


/*success messages*/
$lang['update_success_text'] = 'Successfully Updated File Settings';

