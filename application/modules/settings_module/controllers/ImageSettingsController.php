<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImageSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->lang->load('image_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'image_settings';
            $data['all_image_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/image_settings_page", $data);
            } else {
                $this->load->view("settings_module/image_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateImageSettings()
    {
        $data['encrypt_name'] = $this->input->post('encrypt_name');
        $data['upload_path'] = $this->input->post('upload_path');
        $data['allowed_types'] = $this->input->post('allowed_types');
        $data['max_size'] = $this->input->post('max_size');
        $data['max_width'] = $this->input->post('max_width');
        $data['max_height'] = $this->input->post('max_height');
        $data['create_thumb'] = $this->input->post('create_thumb');

        /*for thumbs*/
        $data['thumb_source'] = $this->input->post('thumb_source');
        $data['thumb_marker'] = $this->input->post('thumb_marker');
        $data['maintain_ratio'] = $this->input->post('maintain_ratio');
        $data['thumb_width'] = $this->input->post('thumb_width');
        $data['thumb_height'] = $this->input->post('thumb_height');

        /*set rules starts*/
        $this->form_validation->set_rules('upload_path', 'upload_path', 'required',
            array(
                'required' => $this->lang->line('upload_path_required_text')
            )
        );

        $this->form_validation->set_rules('allowed_types', 'allowed_types', 'required',
            array(
                'required' => $this->lang->line('allowed_types_required_text')
            )
        );

        $this->form_validation->set_rules('max_size', 'max_size', 'required|integer',
            array(
                'required' => $this->lang->line('max_size_required_text'),
                'integer' => $this->lang->line('max_size_integer_text')
            )
        );

        $this->form_validation->set_rules('max_width', 'max_width', 'required|integer',
            array(
                'required' => $this->lang->line('max_width_required_text'),
                'integer' => $this->lang->line('max_width_integer_text')
            )
        );

        $this->form_validation->set_rules('max_height', 'max_height', 'required|integer',
            array(
                'required' => $this->lang->line('max_height_required_text'),
                'integer' => $this->lang->line('max_height_integer_text')
            )
        );

        /*rules for thumb*/
        if($this->input->post('create_thumb') == 1){
        //if (isset($_POST['create_thumb']) && ($_POST['create_thumb'] == TRUE) ){

            $this->form_validation->set_rules('thumb_source', 'thumb_source', 'required',
                array(
                    'required' => $this->lang->line('thumb_source_required_text')
                )
            );

            $this->form_validation->set_rules('thumb_width', 'thumb_width', 'required|integer',
                array(
                    'required' => $this->lang->line('thumb_width_required_text'),
                    'integer' => $this->lang->line('thumb_width_integer_text')
                )
            );

            if ($this->input->post('maintain_ratio') == 0) {

                $this->form_validation->set_rules('thumb_height', 'thumb_height', 'required|integer',
                    array(
                        'required' => $this->lang->line('thumb_height_required_text'),
                        'integer' => $this->lang->line('thumb_height_integer_text')
                    )
                );
            }



        }

        /*set rules ends*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());

        }else{

            if($this->input->post('upload_path') != ''){
                $upload_path = $this->input->post('upload_path');
                if(!is_dir($upload_path)){
                    mkdir($upload_path , 0777, TRUE);
                }
            }

            if($this->input->post('thumb_source') != ''){
                $thumb_source = $this->input->post('thumb_source');
                if(!is_dir($upload_path.'/'.$thumb_source)){
                    mkdir($upload_path.'/'.$thumb_source , 0777, TRUE);
                }
            }

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'image_settings';
            foreach ($data as $a_settings_key => $a_settings_value){

                if(($this->custom_settings_library->ifSettingsExist($a_settings_code,$a_settings_key)) == true){

                    $this->custom_settings_library->updateSettings($a_settings_code,$a_settings_key,$a_settings_value);

                }else{

                    $this->custom_settings_library->addSettings($a_settings_code,$a_settings_key,$a_settings_value);

                }

            }

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
        }


        redirect('settings_module/image_settings');

    }


}