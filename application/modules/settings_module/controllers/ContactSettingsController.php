<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->lang->load('contact_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'contact_settings';
            $data['all_contact_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/contact_settings_page", $data);
            } else {
                $this->load->view("settings_module/contact_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateContactSettings()
    {
        $data = array();

        if ($this->input->post('admin_contact_email')) {
            $data['admin_contact_email'] = $this->input->post('admin_contact_email');
        }
        if ($this->input->post('admin_contact_phone')) {
            $data['admin_contact_phone'] = $this->input->post('admin_contact_phone');
        }

        if ($this->input->post('company_contact_email')) {
            $data['company_contact_email'] = $this->input->post('company_contact_email');
        }
        if ($this->input->post('company_contact_phone')) {
            $data['company_contact_phone'] = $this->input->post('company_contact_phone');
        }
        if ($this->input->post('company_contact_address')) {
            $data['company_contact_address'] = $this->input->post('company_contact_address');
        }

        if ($this->input->post('company_facebook_id')) {
            $data['company_facebook_id'] = $this->input->post('company_facebook_id');
        }
        if ($this->input->post('company_twitter_id')) {
            $data['company_twitter_id'] = $this->input->post('company_twitter_id');
        }
        if ($this->input->post('company_youtube_id')) {
            $data['company_youtube_id'] = $this->input->post('company_youtube_id');
        }

        if ($data == null) {
            $this->session->set_flashdata('noting_to_update', $this->lang->line('noting_to_update_text'));
        }

        /*set rules starts*/
        if ($this->input->post('admin_contact_email')) {

            $this->form_validation->set_rules('admin_contact_email', 'admin_contact_email', 'valid_email',
                array(
                    'valid_email' => $this->lang->line('admin_contact_email_valid_email_text')
                )
            );
        }

        if ($this->input->post('company_contact_email')) {

            $this->form_validation->set_rules('company_contact_email', 'company_contact_email', 'valid_email',
                array(
                    'valid_email' => $this->lang->line('company_contact_email_valid_email_text')
                )
            );
        }
        /*set rules ends*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());

        } else {

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'contact_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('settings_module/contact_settings');

    }


}