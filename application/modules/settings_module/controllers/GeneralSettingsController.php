<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->library('image_lib');

        //customized lib from application/libraries
        $this->load->library('custom_image_library');
        $this->load->library('custom_file_library');


        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->helper(array('form', 'url'));

        $this->lang->load('general_settings');


    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {

            $a_settings_code = 'general_settings';
            $data['all_general_settings'] = $this->custom_settings_library->getSettings($a_settings_code);


            $image_path = $this->custom_image_library->getMainImageDirectory();

            $data['image_path'] = $image_path;


            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/general_settings_page", $data);
            } else {
                $this->load->view("settings_module/general_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateGeneralSettings()
    {
        $data['site_name'] = $this->input->post('site_name');
        $data['site_short_name'] = $this->input->post('site_short_name');
        $data['site_email'] = $this->input->post('site_email');
        $data['logo_or_sitename'] = $this->input->post('logo_or_sitename');


        $this->form_validation->set_rules('site_name', 'site_name', 'required',
            array(
                'required' => $this->lang->line('site_name_required')
            )
        );
        $this->form_validation->set_rules('site_short_name', $this->lang->line('label_site_short_name_text'), 'required|max_length[4]',
            array(
                'required' => $this->lang->line('site_short_name_required'),
            )
        );


        $this->form_validation->set_rules('site_email', 'site_email', 'required|valid_email',
            array(
                'required' => $this->lang->line('site_email_required'),
                'valid_email' => $this->lang->line('site_email_not_valid')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('settings_module/general_settings');
        } else {

            /*uploading image files starts*/
            if (($_FILES['site_logo']['name']) != '') {

                $field_name = 'site_logo';
                $file_details = $_FILES['site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['site_logo'] = $site_logo_image_details['file_name'];
            }
            /*uploading image files ends*/

            /* demo*/
            /*if (($_FILES['demo']['name']) != '') {

                $field_name = 'demo';
                $file_details = $_FILES['demo'];

                $uploaded_file_details = $this->custom_file_library->uploadFile($file_details, $field_name);
                if ($uploaded_file_details == false) {
                    redirect('settings_module/general_settings');
                }
            }*/
            /* demo*/


            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'general_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
            redirect('settings_module/general_settings');
        }

    }


}