<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->lang->load('file_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'file_settings';
            $data['all_file_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/file_settings_page", $data);
            } else {
                $this->load->view("settings_module/file_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateFileSettings()
    {
        $data['upload_path'] = $this->input->post('upload_path');
        $data['allowed_types'] = $this->input->post('allowed_types');
        $data['max_size'] = $this->input->post('max_size');
        $data['dropzone_allowed_types'] = $this->input->post('dropzone_allowed_types');
        $data['dropzone_max_size'] = $this->input->post('dropzone_max_size');
        $data['dropzone_max_file_number'] = $this->input->post('dropzone_max_file_number');

        /*set rules starts*/
        $this->form_validation->set_rules('upload_path', 'upload_path', 'required',
            array(
                'required' => $this->lang->line('upload_path_required_text')
            )
        );

        $this->form_validation->set_rules('allowed_types', 'allowed_types', 'required',
            array(
                'required' => $this->lang->line('allowed_types_required_text')
            )
        );

        $this->form_validation->set_rules('max_size', 'max_size', 'required|integer',
            array(
                'required' => $this->lang->line('max_size_required_text'),
                'integer' => $this->lang->line('max_size_integer_text')
            )
        );

        $this->form_validation->set_rules('dropzone_allowed_types', 'dropzone_allowed_types', 'required',
            array(
                'required' => $this->lang->line('dropzone_allowed_types_required_text')
            )
        );

        $this->form_validation->set_rules('dropzone_max_size', 'dropzone_max_size', 'required|integer',
            array(
                'required' => $this->lang->line('dropzone_max_size_required_text'),
                'integer' => $this->lang->line('dropzone_max_size_integer_text')
            )
        );

        $this->form_validation->set_rules('dropzone_max_file_number', 'dropzone_max_file_number',
            'required|integer|greater_than_equal_to[1]|less_than_equal_to[100]',
            array(
                'required' => $this->lang->line('dropzone_max_file_number_required_text'),
                'integer' => $this->lang->line('dropzone_max_file_number_integer_text'),
                'greater_than_equal_to'
                => sprintf($this->lang->line('dropzone_max_file_number_greater_than_equal_to_text'), 1),
                'less_than_equal_to'
                => sprintf($this->lang->line('dropzone_max_file_number_less_than_equal_to_text'), 100)
            )
        );


        /*set rules ends*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());

        } else {

            if ($this->input->post('upload_path') != '') {
                $upload_path = $this->input->post('upload_path');
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777, TRUE);
                }
            }

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'file_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
        }

        redirect('settings_module/file_settings');

    }


}