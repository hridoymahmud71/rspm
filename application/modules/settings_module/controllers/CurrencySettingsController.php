<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CurrencySettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('currency_module/Currency_model');

        $this->load->library('form_validation');
        $this->load->library('session');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->lang->load('currency_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'currency_settings';
            $data['all_currency_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $data['all_currencies'] = $this->Currency_model->getActivatedCurrencies();

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");
            $this->load->view("settings_module/currency_settings_page", $data);
            $this->load->view("common_module/footer");

        }
    }

    public function updateCurrencySettings()
    {
        $data['currency_id'] = $this->input->post('currency_id');
        $data['currency_name'] = $this->input->post('currency_name');
        $data['currency_sign'] = $this->input->post('currency_sign');
        $data['conversion_rate'] = $this->input->post('conversion_rate');
        $data['currency_position'] = $this->input->post('currency_position');



        if ($this->input->post('currency_id') == 0) {
            $this->session->set_flashdata('validation_errors', $this->lang->line('no_currency_chosen_text'));
            redirect('settings_module/currency_settings');
        } else {

            $a_settings_code = 'currency_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);
                }

            }

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
            redirect('settings_module/currency_settings');

        }

    }


}