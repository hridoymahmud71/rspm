<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        //$this->load->library('form_validation');
        $this->load->library('form_validation');
        $this->load->library('session');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->lang->load('email_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'email_settings';
            $data['all_email_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/email_settings_page", $data);
            } else {
                $this->load->view("settings_module/email_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateEmailSettings()
    {
        $data['protocol'] = $this->input->post('protocol');
        $data['send_email_notification'] = $this->input->post('send_email_notification');

        if($this->input->post('protocol') == 'smtp'){
            $data['smtp_host'] = $this->input->post('smtp_host');
            $data['smtp_port'] = $this->input->post('smtp_port');
            $data['smtp_timeout'] = $this->input->post('smtp_timeout');
            $data['smtp_user'] = $this->input->post('smtp_user');
            $data['smtp_pass'] = $this->input->post('smtp_pass');
        }

        $data['mailtype'] = $this->input->post('mailtype');

        /*set rules starts*/

        if($this->input->post('protocol') == 'smtp'){

            $this->form_validation->set_rules('smtp_host', 'smtp_host', 'required',
                array(
                    'required' => $this->lang->line('smtp_host_required_text')
                )
            );

            $this->form_validation->set_rules('smtp_port', 'smtp_port', 'required|integer|max_length[4]',
                array(
                    'required' => $this->lang->line('smtp_port_required_text'),
                    'integer' => $this->lang->line('smtp_port_integer_text'),
                    'max_length' => sprintf($this->lang->line('smtp_port_max_length_text'), 4)
                )
            );

            $this->form_validation->set_rules('smtp_timeout', 'smtp_timeout', 'required|integer|less_than_equal_to[10]',
                array(
                    'required' => $this->lang->line('smtp_timeout_required_text'),
                    'integer' => $this->lang->line('smtp_timeout_integer_text'),
                    'less_than_equal_to' => sprintf($this->lang->line('smtp_timeout_less_than_equal_to_text'), 10)
                )
            );

            $this->form_validation->set_rules('smtp_user', 'smtp_user', 'required|valid_email',
                array(
                    'required' => $this->lang->line('smtp_user_required_text'),
                    'valid_email' => $this->lang->line('smtp_user_valid_email_text')
                )
            );

            $this->form_validation->set_rules('smtp_pass', 'smtp_pass', 'required',
                array(
                    'required' => $this->lang->line('smtp_pass_required_text')
                )
            );
        }

        /*set rules ends*/

        if ($this->input->post('protocol') == 'smtp' && $this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());

        } else {

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'email_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
        }

        redirect('settings_module/email_settings');

    }


}