<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['invoice_module/show_invoice_list/all'] = 'InvoiceController/showAllInvoiceList';
$route['invoice_module/show_invoice_list/my'] = 'InvoiceController/showMyInvoiceList';
$route['invoice_module/show_project_invoice_list/(:any)'] = 'InvoiceController/showProjectInvoiceList';
$route['invoice_module/get_invoice_list_with_ajax'] = 'InvoiceController/getInvoiceListWithAjax';

$route['invoice_module/add_invoice'] = 'InvoiceController/addInvoice';

$route['invoice_module/get_all_non_deleted_projects_with_client_with_currency_by_ajax'] =
    'InvoiceController/getAllNonDeletedProjectsWithClientWithCurrencyByAjax';

//
//(:any) = $project_id
$route['invoice_module/add_invoice_by_project/(:any)'] = 'InvoiceController/addInvoice_ByProject';

//(:any) = $invoice_id
$route['invoice_module/edit_invoice/(:any)'] = 'InvoiceController/editInvoice';

//(:any) = $invoice_id
$route['invoice_module/all_invoice/delete_invoice/(:any)'] = 'InvoiceController/deleteInvoice';
$route['invoice_module/my_invoice/delete_invoice/(:any)'] = 'InvoiceController/deleteInvoice';
$route['invoice_module/project_invoice/delete_invoice/(:any)'] = 'InvoiceController/deleteInvoice';

//(:any) = $invoice_id
$route['invoice_module/view_invoice/(:any)'] = 'InvoiceController/showInvoice';
$route['invoice_module/print_invoice/(:any)'] = 'InvoiceController/printInvoice';
$route['invoice_module/generate_invoice_pdf/(:any)'] = 'InvoiceController/generateInvoicePdf';

$route['invoice_module/all_invoice/open_invoice/(:any)'] = 'InvoiceController/openInvoice';
$route['invoice_module/my_invoice/open_invoice/(:any)'] = 'InvoiceController/openInvoice';
$route['invoice_module/project_invoice/open_invoice/(:any)'] = 'InvoiceController/openInvoice';

$route['invoice_module/all_invoice/close_invoice/(:any)'] = 'InvoiceController/closeInvoice';
$route['invoice_module/my_invoice/close_invoice/(:any)'] = 'InvoiceController/closeInvoice';
$route['invoice_module/project_invoice/close_invoice/(:any)'] = 'InvoiceController/closeInvoice';

$route['invoice_module/all_invoice/clear_invoice/(:any)'] = 'InvoiceController/clearInvoice';
$route['invoice_module/my_invoice/clear_invoice/(:any)'] = 'InvoiceController/clearInvoice';
$route['invoice_module/project_invoice/clear_invoice/(:any)'] = 'InvoiceController/clearInvoice';

$route['invoice_module/all_invoice/unclear_invoice/(:any)'] = 'InvoiceController/unclearInvoice';
$route['invoice_module/my_invoice/unclear_invoice/(:any)'] = 'InvoiceController/unclearInvoice';
$route['invoice_module/project_invoice/unclear_invoice/(:any)'] = 'InvoiceController/unclearInvoice';

$route['invoice_module/all_invoice/email_invoice/(:any)'] = 'InvoiceController/emailInvoice';
$route['invoice_module/my_invoice/email_invoice/(:any)'] = 'InvoiceController/emailInvoice';
$route['invoice_module/project_invoice/email_invoice/(:any)'] = 'InvoiceController/emailInvoice';