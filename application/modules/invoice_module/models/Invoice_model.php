<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Invoice_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function getProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id', $project_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjectWithClient($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('project_id', $project_id);
        $this->db->join('users as u', 'p.client_id = u.id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProjectWithClientWithCurrency($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('project_id', $project_id);
        $this->db->join('users as u', 'p.client_id = u.id');
        $this->db->join('rspm_tbl_currency as c', 'p.currency_id = c.currency_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getInvoice($invoice_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice');
        $this->db->where('invoice_id', $invoice_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getClient($client_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $client_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getDetailedClientInfo($client_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('id', $client_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getCurrency($currency_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_id', $currency_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getAllNonDeletedProjectsWithClient()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('users as u', 'p.client_id = u.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countAllNonDeletedProjectsWithWithCurrency($search_filter)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('users as u', 'p.client_id = u.id');
        $this->db->join('rspm_tbl_currency as c', 'p.currency_id = c.currency_id');

        if ($search_filter != false) {
            $this->db->like('p.project_name', $search_filter);
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getAllNonDeletedProjectsWithWithCurrency($search_filter, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.deletion_status!=', 1);
        $this->db->join('users as u', 'p.client_id = u.id');
        $this->db->join('rspm_tbl_currency as c', 'p.currency_id = c.currency_id');

        if ($search_filter != false) {
            $this->db->like('p.project_name', $search_filter);
        }

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countInvoicesByAjax(
        $common_filter_value = false,
        $specific_filters = false,
        $project_id = false,
        $client_id = false,
        $which_list
    )
    {
        $this->db->select('
                             i.invoice_id as invoice_id,
                             i.invoice_project_id as invoice_project_id,
                             i.invoice_client_id as invoice_client_id,
                             i.invoice_number as invoice_number,
                             i.invoice_total as invoice_total, 
                             i.invoice_currency_id as invoice_currency_id, 
                             i.invoice_status as invoice_status,
                             i.invoice_payment_clear as invoice_payment_clear,
                             i.invoice_deletion_status as invoice_deletion_status,
                             i.invoice_deadline as invoice_deadline,
                             i.invoice_clear_date as invoice_clear_date,
                             i.invoice_created_at as invoice_created_at,
                             i.invoice_modified_at as invoice_modified_at,
                             
                             p.project_id as project_id,
                             p.project_name as project_name,
                             p.client_id as project_client_id,
                                                         
                             u.id as client_user_id,
                             u.first_name as client_first_name,
                             u.last_name as client_last_name,
                             
                             c.currency_id as currency_id,
                             c.currency_sign as currency_sign
                        ');
        $this->db->from('rspm_tbl_invoice as i');
        $this->db->where('i.invoice_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 'i.invoice_project_id = p.project_id');
        $this->db->join('rspm_tbl_currency as c', 'i.invoice_currency_id = c.currency_id');
        $this->db->join('users as u', 'i.invoice_client_id = u.id');

        //logic starts
        //no need to write condition for all_invoice as it's the default condition

        if ($which_list == 'my_invoice' && !$project_id && $client_id) {
            $this->db->where('u.id', $client_id);
            //loading invoices of a client
        }

        if ($which_list == 'project_invoice' && $project_id && !$client_id) {
            $this->db->where('p.project_id', $project_id);
            //loading invoices of a project

        }
        //logic ends


        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('i.invoice_number', $common_filter_value);
            $this->db->or_like('p.project_name', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);

            $this->db->group_end();
        }


        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'invoice_number') {
                    $this->db->like('i.' . $column_name, $filter_value);
                }

                if ($column_name == 'invoice_project') {
                    $this->db->like('p.project_name', $filter_value);
                }

                if ($column_name == 'invoice_client') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'invoice_status') {
                    if ($filter_value == 'open') {
                        $this->db->where('i.invoice_status', 1);
                    } else {
                        $this->db->where('i.invoice_status!=', 1);
                    }
                }

                if ($column_name == 'invoice_payment_clear') {
                    if ($filter_value == 'clear') {
                        $this->db->where('i.invoice_payment_clear', 1);
                    } else {
                        $this->db->where('i.invoice_payment_clear!=', 1);
                    }
                }
            }
        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();



        return $num_rows;
    }

    public function getInvoicesByAjax(
        $common_filter_value = false,
        $specific_filters = false,
        $order,
        $limit,
        $project_id = false,
        $client_id = false,
        $which_list
    )

    {
        $this->db->select('
                             i.invoice_id as invoice_id,
                             i.invoice_project_id as invoice_project_id,
                             i.invoice_client_id as invoice_client_id,
                             i.invoice_number as invoice_number,
                             i.invoice_total as invoice_total, 
                             i.invoice_currency_id as invoice_currency_id, 
                             i.invoice_status as invoice_status,
                             i.invoice_payment_clear as invoice_payment_clear,
                             i.invoice_deletion_status as invoice_deletion_status,
                             i.invoice_deadline as invoice_deadline,
                             i.invoice_clear_date as invoice_clear_date,
                             i.invoice_created_at as invoice_created_at,
                             i.invoice_modified_at as invoice_modified_at,
                             
                             p.project_id as project_id,
                             p.project_name as project_name,
                             p.client_id as project_client_id,
                                                         
                             u.id as client_user_id,
                             u.first_name as client_first_name,
                             u.last_name as client_last_name,
                             
                             c.currency_id as currency_id,
                             c.currency_sign as currency_sign
                        ');
        $this->db->from('rspm_tbl_invoice as i');
        $this->db->where('i.invoice_deletion_status!=', 1);
        $this->db->join('rspm_tbl_project as p', 'i.invoice_project_id = p.project_id');
        $this->db->join('rspm_tbl_currency as c', 'i.invoice_currency_id = c.currency_id');
        $this->db->join('users as u', 'i.invoice_client_id = u.id');

        //logic starts
        //no need to write condition for all_invoice as it's the default condition

        if ($which_list == 'my_invoice' && !$project_id && $client_id) {
            $this->db->where('u.id', $client_id);
            //loading invoices of a client
        }

        if ($which_list == 'project_invoice' && $project_id && !$client_id) {
            $this->db->where('p.project_id', $project_id);
            //loading invoices of a project
        }
        //logic ends


        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('i.invoice_number', $common_filter_value);
            $this->db->or_like('p.project_name', $common_filter_value);

            $this->db->or_like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $common_filter_value);

            $this->db->group_end();
        }


        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'invoice_number') {
                    $this->db->like('i.' . $column_name, $filter_value);
                }

                if ($column_name == 'invoice_project') {
                    $this->db->like('p.project_name', $filter_value);
                }

                if ($column_name == 'invoice_client') {
                    $this->db->group_start();
                    $this->db->like('u.first_name', $filter_value);
                    $this->db->or_like('u.last_name', $filter_value);
                    $this->db->or_like("CONCAT((u.first_name),(' '),(u.last_name))", $filter_value);
                    $this->db->group_end();
                }

                if ($column_name == 'invoice_status') {
                    if ($filter_value == 'open') {
                        $this->db->where('i.invoice_status', 1);
                    } else {
                        $this->db->where('i.invoice_status!=', 1);
                    }
                }

                if ($column_name == 'invoice_payment_clear') {
                    if ($filter_value == 'clear') {
                        $this->db->where('i.invoice_payment_clear', 1);
                    } else {
                        $this->db->where('i.invoice_payment_clear!=', 1);
                    }
                }
            }
        }


        if ($order['column'] == 'invoice_client') {
            $this->db->order_by('u.first_name');
        } else if ($order['column'] == 'invoice_project') {
            $this->db->order_by('p.project_name');
        } else {
            $this->db->order_by('i.' . $order['column'], $order['by']);
        }

        $this->db->limit($limit['length'], $limit['start']);


        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function getAllInvoice()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getMyInvoice($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice');
        $this->db->where('invoice_client_id', $client_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getProjectInvoice($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice');
        $this->db->where('invoice_project_id', $project_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function insertInvoice($data)
    {
        $this->db->insert('rspm_tbl_invoice', $data);
    }

    public function insertInvoiceItems($item_data)
    {
        $this->db->insert('rspm_tbl_invoice_item', $item_data);
    }

    public function getInvoiceItems($invoice_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice_item');
        $this->db->where('item_invoice_id', $invoice_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function updateInvoice($invoice_id, $data)
    {
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice', $data);
    }

    public function deleteInvoiceItems($invoice_id)
    {
        $this->db->where('item_invoice_id', $invoice_id);
        $this->db->delete('rspm_tbl_invoice_item');
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function makeInvoiceOpen($invoice_id)
    {
        $this->db->set('invoice_status', 1);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    public function makeInvoiceClose($invoice_id)
    {
        $this->db->set('invoice_status', 0);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    public function makeInvoiceClear($invoice_id)
    {
        $this->db->set('invoice_payment_clear', 1);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    public function makeInvoiceUnclear($invoice_id)
    {
        $this->db->set('invoice_payment_clear', 0);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    public function updateClearDate($invoice_id, $timestamp)
    {
        $this->db->set('invoice_clear_date', $timestamp);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    public function deleteInvoice($invoice_id)
    {
        $this->db->set('invoice_deletion_status', 1);
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('rspm_tbl_invoice');
    }

    /*---------------------------------------------*/

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}