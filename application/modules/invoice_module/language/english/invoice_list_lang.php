<?php

/*page text*/
$lang['add_button_text'] = 'Add Invoice';

$lang['page_title_text'] = 'Invoice';
$lang['page_subtitle_all_invoice_text'] = 'All Invoices';
$lang['page_subtitle_my_invoice_text'] = 'My Invoices';
$lang['page_subtitle_project_invoice_text'] = 'Project Invoices';

$lang['breadcrum_home_text'] = 'Home';

$lang['breadcrum_section_all_invoices_text'] = 'All Invoices';
$lang['breadcrum_section_my_invoices_text'] = 'My Invoice';
$lang['breadcrum_section_project_invoices_text'] = 'Project Invoices';

$lang['breadcrum_page_text'] = 'Invoice List';

$lang['not_found_text'] = 'No Invoice is found';

/*column lang*/

$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';

$lang['option_clear_text'] = 'Clear';
$lang['option_unclear_text'] = 'Unclear';

$lang['option_open_text'] = 'Open';
$lang['option_close_text'] = 'Close';

$lang['column_invoice_number_text'] = 'Invoice No.';
$lang['column_project_name_text'] = 'Project Name';
$lang['column_client_name_text'] = 'Client Name';
$lang['column_invoice_total_text'] = 'Invoice Total';
$lang['column_invoice_status_text'] = 'Status';
$lang['column_invoice_payment_cleared_text'] = 'Cleared ?';
$lang['column_invoice_created_at_text'] = 'Created On';
$lang['column_invoice_deadline_text'] = 'Deadline';
$lang['column_action_text'] = 'Action';

/*other texts*/
$lang['clear_text'] = 'Clear';
$lang['unclear_text'] = 'Unclear';
$lang['open_text'] = 'Open';
$lang['close_text'] = 'Close';

$lang['successfull_text'] = 'Successfull';
$lang['unccessfull_text'] = 'Unsuccessfull';
/*success messages*/

$lang['open_success_message_text'] = 'Invoice: %1$s Opened SuccessFully';
$lang['close_success_message_text'] = 'Invoice: %1$s Closed SuccessFully';
$lang['clear_success_message_text'] = 'Invoice: %1$s Cleared SuccessFully';
$lang['unclear_success_message_text'] = 'Invoice: %1$s Uncleared SuccessFully';
$lang['delete_success_message_text'] = 'Invoice: %1$s Deleted SuccessFully';

/*swal text*/
$lang['swal_delete_title_text'] = 'Delete Invoice';
$lang['swal_delete_confirm_button_text'] = 'Yes, delete This Invoice';
$lang['swal_delete_cancel_button_text'] = 'No, do not delete';

/*tooollip text*/
$lang['tooltip_make_open_text'] = 'Make Invoice Open';
$lang['tooltip_make_close_text'] = 'Make Invoice Close';
$lang['tooltip_make_clear_text'] = 'Make Invoice Clear';
$lang['tooltip_make_unclear_text'] = 'Make Invoice Unclear';

$lang['tooltip_view_text'] = 'View Invoice';
$lang['tooltip_edit_text'] = 'Edit Invoice';
$lang['tooltip_delete_text'] = 'Delete Invoice';

$lang['tooltip_generate_pdf_text'] = 'Generate Pdf';
$lang['tooltip_mail_to_client_text'] = 'Mail To Client';
$lang['tooltip_print_text'] = 'Print Invoice';

/*-------------------------*/
$lang['loading_text'] = 'Loading Invoices . . .';
$lang['no_date_text'] = 'No Date Available';

$lang['no_invoice_found_text'] = 'No Invoice Is Found';
$lang['no_matching_invoice_found_text'] = 'No Matching Is Found';

/*for log purposes*/
$lang['log_field_invoice_clear_date_text'] = 'Invoice Clear Date';
?>