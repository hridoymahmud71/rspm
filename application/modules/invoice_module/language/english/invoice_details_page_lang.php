<?php

/*page texts*/
$lang['invoice_text'] = 'Invoice';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrumb_section_all_invoice_text'] = 'All Invoice';
$lang['breadcrumb_section_my_invoice_text'] = 'My Invoice';
$lang['breadcrumb_section_project_invoice_text'] = 'Project Invoice';

$lang['breadcrum_page_text'] = 'Invoice Details';

$lang['print_date_text'] = 'Print Date:';

/*other texts*/
$lang['unavailable_text'] = 'Unavailable';
$lang['print_text'] = 'Print';
$lang['submit_payment_text'] = 'Submit';
$lang['generate_pdf_text'] = 'Generate Pdf';
$lang['email_to_client_text'] = 'Email to Client';

/*contact texts*/
$lang['from_text'] = 'From,';
$lang['to_text'] = 'To,';
$lang['address_text'] = 'Address:';
$lang['phone_text'] = 'Phone:';
$lang['email_text'] = 'Email:';

/*invoice texts*/
$lang['invoice_number_text'] = 'Invoice No.:';
$lang['invoice_deadline_text'] = 'Invoice Deadline:';
$lang['invoice_clear_date_text'] = 'Invoice Clear Date:';
$lang['invoice_total_text'] = 'Invoice Total:';

$lang['invoice_description_text'] = 'Invoice Description:';

$lang['invoice_is_clear_text'] = 'Invoice Cleared?:';
$lang['invoice_clear_yes_text'] = 'Yes';
$lang['invoice_clear_no_text'] = 'No';

$lang['invoice_is_open_text'] = 'Invoice Open?:';
$lang['invoice_open_yes_text'] = 'Yes';
$lang['invoice_open_no_text'] = 'No';

/*project_text*/
$lang['project_name_text'] = 'Project Name:';

/*item_column_texts*/

$lang['item_column_serial_number_text'] = '#';
$lang['item_column_item_name_text'] = 'Item Name';
$lang['item_column_item_description_text'] = 'Item Description';
$lang['item_column_item_price_text'] = 'Price';
$lang['item_column_item_quantity_text'] = 'Qty';
$lang['item_column_item_tax_rate_text'] = 'Tx rt.(%)';
$lang['item_column_item_tax_amount_text'] = 'Tx Amt.';
$lang['item_column_item_discount_rate_text'] = 'Dsc. Rt.(%)';
$lang['item_column_item_discount_amount_text'] = 'Dsc Amt.';
$lang['item_column_item_net_price_text'] = 'Net Price';
$lang['item_column_item_total_text'] = 'Item Total';
?>