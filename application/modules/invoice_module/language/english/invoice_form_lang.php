<?php

/*page texts*/
$lang['page_title_add_text'] = 'Add Invoice';
$lang['page_title_edit_text'] = 'Edit Invoice';

$lang['page_subtitle_text'] = 'Invoice\'s information';
$lang['box_title_text'] = 'Invoice Information Form';

$lang['breadcrumb_home_text'] = 'Home';

$lang['breadcrumb_section_all_invoice_text'] = 'All Invoices';
$lang['breadcrumb_section_my_invoice_text'] = 'My Invoices';
$lang['breadcrumb_section_project_invoice_text'] = 'Project Invoices';

$lang['breadcrumb_add_page_text'] = 'Add Invoice';
$lang['breadcrumb_edit_page_text'] = 'Edit Invoice';


/*Add group form texts*/
$lang['label_invoice_number_text'] = 'Invoice No.';
$lang['label_invoice_project_name_text'] = 'Project Name';
$lang['label_invoice_client_name_text'] = 'Client Name';
$lang['label_invoice_currency_short_name_text'] = 'Currency';
$lang['label_invoice_description_text'] = 'Invoice Description.';
$lang['label_invoice_status_text'] = 'Status';
$lang['label_invoice_payment_clear_text'] = 'Payment Cleared ?';
$lang['label_invoice_clear_date_text'] = 'Clear Date';
$lang['label_invoice_deadline_text'] = 'Deadline';
$lang['label_invoice_total_text'] = 'Invoice Total';

$lang['placeholder_invoice_number_text'] = 'Enter Invoice No.';
$lang['placeholder_invoice_project_name_text'] = 'Select Project';
$lang['placeholder_invoice_client_name_text'] = 'Project\'s client ';
$lang['placeholder_currency_short_name_text'] = 'Project\'s Currency';
$lang['placeholder_invoice_description_text'] = 'Enter Invoice Description.';
$lang['placeholder_invoice_deadline_text'] = 'Invoice Deadline';
$lang['placeholder_invoice_clear_date_text'] = 'Clear Date';
$lang['placeholder_invoice_total_text'] = 'Invoice Total';

$lang['option_invoice_status_open_text'] = 'Open';
$lang['option_invoice_status_close_text'] = 'Close';

$lang['option_invoice_payment_cleared_yes_text'] = 'Yes, payment cleared';
$lang['option_invoice_payment_cleared_no_text'] = 'No, payment not cleared';

/*-other text-*/
$lang['select_a_project_text'] = '--- Select A Project ---';
$lang['no_project_text'] = 'No Project is found !';
$lang['no_client_text'] = 'No Client';
$lang['no_currency_text'] = 'No Currency';
$lang['add_a_project_text'] = 'Add A project';
$lang['items_text'] = 'Items';
$lang['see_invoice_text'] = 'See Invoice';
$lang['unavailable_text'] = 'Unavailable';
$lang['change_project_currency_text'] = 'Change Project\'s Currency';


/*items*/
$lang['label_item_number_text'] = 'Item no.';

$lang['label_item_name_text'] = 'Item Name.';
$lang['label_item_description_text'] = 'Item Description';

$lang['label_item_price_text'] = ' Price';
$lang['label_item_tax_rate_text'] = ' Tax Rate.';
$lang['label_item_tax_amount_text'] = ' Tax Amt.';
$lang['label_item_discount_rate_text'] = ' Disc. Rt.';
$lang['label_item_discount_amount_text'] = ' Disc. Amt.';

$lang['label_item_net_price_text'] = 'Net Price';
$lang['label_item_quantity_text'] = 'Quantity.';
$lang['label_item_total_text'] = 'Total.';

$lang['placeholder_item_name_text'] = 'Item Name.';
$lang['placeholder_item_description_text'] = 'Item Description';

$lang['placeholder_item_price_text'] = 'Price';
$lang['placeholder_item_tax_rate_text'] = 'Tax Rate.';
$lang['placeholder_item_tax_amount_text'] = ' Tax Amt.';
$lang['placeholder_item_discount_rate_text'] = 'Disc. Rate.';
$lang['placeholder_item_discount_amount_text'] = 'Disc. Amt.';

$lang['placeholder_item_net_price_text'] = 'Net Price';
$lang['placeholder_item_quantity_text'] = 'Quantity.';
$lang['placeholder_item_total_text'] = 'Net Price &#10005; Quantity';

$lang['label_add_or_remove_item_text'] = 'Add/Remove';

/*submit button*/
$lang['button_submit_create_text'] = 'Create Invoice';
$lang['button_submit_update_text'] = 'Update Invoice';


/*success messages*/
$lang['add_success_message_text'] = 'Invoice: %1$s Added SuccessFully ';
$lang['update_success_message_text'] = 'Invoice: %1$s Updated SuccessFully ';

/*js validation error texts*/
$lang['js_val_error_cant_be_empty_text'] = 'Cannot be empty';
$lang['js_val_error_cant_be_more_than_hundred_empty_text'] = 'Cannot be more than 100';
$lang['js_val_error_not_a_number_text'] = 'Not a number';
$lang['js_val_error_not_an_integer_text'] = 'Not a whole number';
$lang['js_val_error_not_negative_text'] = 'Cannot be negative';
$lang['js_val_error_invoice_deadline_not_empty_text'] = 'Deadline Needed' ;
$lang['js_val_error_invoice_clear_date_not_empty_text'] = 'Invoice Clear Date Needed' ;
$lang['js_val_error_select_a_project_text']   = 'Select A Project';


/*swal texts*/
$lang['swal_item_limit_reached_text'] = 'Maximum Limit Reached';
$lang['swal_maximum_limit_text'] = 'Maximumum no. of items is ';


/*for log purposes*/
$lang['log_field_invoice_description_text'] = 'Invoice Description';
$lang['log_field_invoice_status_text'] = 'Invoice Status';
$lang['invoice_payment_clear'] = 'Invoice Payment Clear Status';
$lang['log_field_invoice_clear_date_text'] = 'Invoice Clear Date';
$lang['log_field_invoice_deadline_text'] = 'Invoice Deadline';
$lang['log_field_invoice_total_text'] = 'Invoice Total';


?>