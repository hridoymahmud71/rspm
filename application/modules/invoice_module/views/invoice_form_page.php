<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            <?php
            if ($which_form == 'add_invoice' || $which_form == 'add_invoice_by_project') {
                echo lang('page_title_add_text');
            }
            if ($which_form == 'edit_invoice') {
                echo lang('page_title_edit_text');
            }
            ?>


            <small>
                <?php
                if ($which_form == 'edit_invoice' || $which_form == 'add_invoice_by_project') {

                    if ($a_project_info_with_client_with_currency) {
                        echo $a_project_info_with_client_with_currency->project_name;
                    }
                } else {
                    echo lang('page_subtitle_text');
                } ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'common_module' ?>">
                    <i class="fa fa-dashboard"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>">
                        <?php echo lang('breadcrumb_section_all_invoice_text') ?>
                    </a>
                <?php } ?>

                <?php if ($is_client == 'client') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>">
                        <?php echo lang('breadcrumb_section_my_invoice_text') ?>
                    </a>
                <?php } ?>
                <?php if ($which_form != 'add_invoice') { ?>
                    |
                    <a href="<?php echo base_url() . 'invoice_module/show_project_invoice_list/' . $project_id ?>">
                        <?php echo lang('breadcrumb_section_project_invoice_text') ?>
                    </a>
                <?php } ?>
            </li>
            <li class="active">
                <?php
                if ($which_form == 'add_invoice' || $which_form == 'add_invoice_by_project') {
                    echo lang('breadcrumb_add_page_text');
                }
                if ($which_form == 'edit_invoice') {
                    echo lang('breadcrumb_edit_page_text');
                }
                ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!--projectroom menu starts-->
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?php
                if ($projectroom_menu_section &&
                    ($which_form == 'add_invoice_by_project' || $which_form == 'edit_invoice')
                ) {
                    echo $projectroom_menu_section;
                }
                ?>
            </div>
        </div>
        <!--projectroom menu ends-->

        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <br>
                            <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                                <?php if ($this->session->flashdata('add_success')) {
                                    echo $this->session->flashdata('add_success');
                                } ?>
                                <?php if ($this->session->flashdata('update_success')) {
                                    echo $this->session->flashdata('update_success');
                                } ?>

                                &nbsp;
                                <?php echo $this->session->flashdata('see_invoice') ?>
                            </div>
                            <div class="col-lg-2 col-md-1"></div>
                        <?php } ?>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo $form_action ?>" role="form"
                          id="form_invoice" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <?php if ($which_form == 'edit_invoice') { ?>
                                <input type="hidden" name="invoice_id"
                                       value="<?php if ($an_invoice_info->invoice_id) {
                                           echo $an_invoice_info->invoice_id;
                                       } ?>">
                            <?php } ?>

                            <div class="form-group">
                                <label for="invoice_number"><?php echo lang('label_invoice_number_text') ?></label>
                                <input name="invoice_number" class="form-control" id="currency_name"
                                       value="<?php echo $an_invoice_info->invoice_number ?>"
                                       placeholder="<?php echo lang('placeholder_invoice_number_text') ?>" type="text"
                                       readonly>
                            </div>


                            <?php if (
                                $which_form == 'add_invoice_by_project'
                                ||
                                $which_form == 'project_invoice'
                                ||
                                $which_form == 'edit_invoice'
                            ) { ?>
                                <input type="hidden" name="invoice_project_id"
                                       value="<?php echo $a_project_info_with_client_with_currency->project_id ?>">

                                <div class="form-group">
                                    <label for=""><?php echo lang('label_invoice_project_name_text') ?></label>
                                    <input name="" class="form-control" id=""
                                           value="<?php echo $a_project_info_with_client_with_currency->project_name ?>"
                                           placeholder="<?php echo lang('placeholder_invoice_project_name_text') ?>"
                                           type="text" readonly>
                                </div>

                            <?php } else { ?>

                                <!-- $which_form == 'add_invoice' -->
                                <div class="form-group" id="add_a_project_text_wrapper"
                                     style="font-size: larger;color: darkred">
                                    <?php echo lang('no_project_text') ?>
                                    &nbsp;
                                    <?php if ($is_admin == 'admin') { ?>
                                        <a href="<?php echo base_url() . 'project_module/add_project' ?>">
                                            <?php echo lang('add_a_project_text') ?>
                                        </a>
                                    <?php } ?>
                                </div>

                                <div class="form-group">
                                    <label><?php echo lang('label_invoice_project_name_text') ?></label>
                                    <select id="select_project" name="invoice_project_id"
                                            class="select_project form-control select2" style="width: 100%;"
                                            data-placeholder="<?php echo lang('placeholder_invoice_project_name_text') ?>">
                                    </select>
                                    <span style="color: darkred" class=""
                                          id="error_select_project"></span>
                                </div>


                            <?php } ?>


                            <?php if (
                                $which_form == 'add_invoice_by_project'
                                ||
                                $which_form == 'project_invoice'
                                ||
                                $which_form == 'edit_invoice'
                            ) { ?>
                                <input id="client_id" type="hidden" name="invoice_client_id"
                                       value="<?php echo $a_project_info_with_client_with_currency->client_id ?>">
                                <input id="currency_id" type="hidden" name="invoice_currency_id"
                                       value="<?php echo $a_project_info_with_client_with_currency->currency_id ?>">

                                <div class="form-group">
                                    <label for=""><?php echo lang('label_invoice_client_name_text') ?></label>
                                    <input name="" class="form-control" id="client_fullname"
                                           value="<?php echo $a_project_info_with_client_with_currency->first_name
                                               . ' '
                                               . $a_project_info_with_client_with_currency->last_name ?>"
                                           placeholder="<?php echo lang('placeholder_invoice_client_name_text') ?>"
                                           type="text" readonly>
                                </div>

                                <div class="form-group">
                                    <label for=""><?php echo lang('label_invoice_currency_short_name_text') ?>
                                        <?php if ($which_form != 'edit_invoice') { ?>
                                            &nbsp;
                                            <a href="<?php echo base_url() . 'project_module/edit_project/'
                                                . $a_project_info_with_client_with_currency->project_id ?>">
                                                <?php echo lang('change_project_currency_text') ?>
                                            </a>
                                        <?php } ?>
                                    </label>
                                    <input name="" class="form-control" id="currency_short_name"
                                           value="<?php echo $a_project_info_with_client_with_currency->currency_short_name ?>"
                                           placeholder="<?php echo lang('placeholder_currency_short_name_text') ?>"
                                           type="text" readonly>
                                </div>
                            <?php } else { ?>
                                <!-- $which_form == 'add_invoice' -->
                                <input id="client_id" type="hidden" name="invoice_client_id"
                                       value="">
                                <input id="currency_id" type="hidden" name="invoice_currency_id"
                                       value="">

                                <div class="form-group">
                                    <label for=""><?php echo lang('label_invoice_client_name_text') ?></label>
                                    <input name="" class="form-control" id="client_fullname"
                                           value=""
                                           placeholder="<?php echo lang('placeholder_invoice_client_name_text') ?>"
                                           type="text" readonly>
                                </div>

                                <div class="form-group">
                                    <label for=""><?php echo lang('label_invoice_currency_short_name_text') ?>
                                        &nbsp;
                                        <?php if ($is_admin == 'admin') { ?>
                                            <div id="change_project_currency_wrapper">
                                                <a id="project_edit_anchor"
                                                   href="#"><?php echo lang('change_project_currency_text') ?></a>
                                            </div>
                                        <?php } ?>
                                    </label>
                                    <input name="" class="form-control" id="currency_short_name"
                                           value=""
                                           placeholder="<?php echo lang('placeholder_currency_short_name_text') ?>"
                                           type="text" readonly>
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="invoice_description"><?php echo lang('label_invoice_description_text') ?></label>
                                <textarea class="form-control" name="invoice_description" id="" cols="30" rows="5"
                                          placeholder="<?php echo lang('placeholder_invoice_description_text') ?>"
                                ><?php if ($an_invoice_info) {
                                        echo $an_invoice_info->invoice_description;
                                    } ?></textarea>
                            </div>

                            <?php if ($which_form == 'edit_invoice') { ?>
                                <div class="form-group">
                                    <label for="invoice_status"><?php echo lang('label_invoice_status_text') ?></label>

                                    <select class="form-control" name="invoice_status" id="invoice_status">

                                        <option value="1"
                                            <?php
                                            if ($an_invoice_info) {
                                                if ($an_invoice_info->invoice_status == 1) {
                                                    echo ' selected ';
                                                }
                                            } ?>
                                                class="">
                                            <?php echo lang('option_invoice_status_open_text') ?>
                                        </option>

                                        <option value="0"
                                            <?php
                                            if ($an_invoice_info) {
                                                if ($an_invoice_info->invoice_status == 0) {
                                                    echo ' selected ';
                                                }
                                            } ?>
                                                class="">
                                            <?php echo lang('option_invoice_status_close_text') ?>
                                        </option>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="invoice_payment_clear"><?php echo lang('label_invoice_payment_clear_text') ?></label>

                                    <select class="form-control" name="invoice_payment_clear"
                                            id="invoice_payment_clear">

                                        <option value="1"
                                            <?php
                                            if ($an_invoice_info) {
                                                if ($an_invoice_info->invoice_payment_clear == 1) {
                                                    echo 'selected ';
                                                }
                                            } ?>
                                                class="">
                                            <?php echo lang('option_invoice_payment_cleared_yes_text') ?>
                                        </option>

                                        <option value="0"
                                            <?php
                                            if ($an_invoice_info) {
                                                if ($an_invoice_info->invoice_payment_clear == 0) {
                                                    echo ' selected ';
                                                }
                                            } ?>
                                                class="">
                                            <?php echo lang('option_invoice_payment_cleared_no_text') ?>
                                        </option>

                                    </select>
                                </div>

                                <div id="invoice_clear_date_wrapper" class="form-group">
                                    <label><?php echo lang('label_invoice_clear_date_text') ?></label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="invoice_clear_date" class="form-control pull-right"
                                               id="invoice_clear_date"
                                               placeholder="<?php echo lang('placeholder_invoice_clear_date_text') ?>"
                                               readonly style="background-color: inherit"
                                               value="<?php
                                               if ($an_invoice_info) {
                                                   echo $an_invoice_info->invoice_clear_date_datestring;
                                               } else {
                                                   echo '';
                                               }
                                               ?>"
                                        >
                                    </div>
                                    <span style="color: darkred" class=""
                                          id="error_invoice_clear_date">
                                </span>
                                    <!-- /.input group -->
                                </div>

                            <?php } else { ?>

                                <!-- in add form(s) invoice status is open and payment is not cleared -->
                                <input type="hidden" name="invoice_status" value="1">
                                <input type="hidden" name="invoice_payment_clear" value="0">
                            <?php } ?>

                            <div class="form-group">
                                <label><?php echo lang('label_invoice_deadline_text') ?></label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="invoice_deadline" class="form-control pull-right"
                                           id="invoice_deadline"
                                           placeholder="<?php echo lang('placeholder_invoice_deadline_text') ?>"
                                           readonly style="background-color: inherit"
                                           value="<?php
                                           if ($an_invoice_info) {
                                               echo $an_invoice_info->invoice_deadline_datestring;
                                           } else {
                                               echo '';
                                           }
                                           ?>"
                                    >
                                </div>
                                <span style="color: darkred" class=""
                                      id="error_invoice_deadline">
                                </span>
                                <!-- /.input group -->
                            </div>

                            <hr>
                            <h3 style="color: dimgrey"><?php echo lang('items_text') ?></h3>
                            <hr>

                            <!-- item container starts-->
                            <div id="item_container">
                                <?php if ($which_form != 'edit_invoice') { ?>
                                    <!-- ---------- an item starts ---------- -->
                                    <div id="an_item0" style="display: none" class="box box-success">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <?php echo lang('label_item_number_text') ?>
                                                &nbsp; &nbsp;
                                                <input class="item_num" value="0" type="text"
                                                       style="border: transparent;background: transparent;color: #2b2b2b"
                                                       readonly>
                                            </h3>

                                            <div class="pull-right">
                                                <button id="add_item_button0"
                                                        class="btn btn-success .btn-sm add_item_button">
                                                    <span><i class="fa fa-plus"></i></span>
                                                </button>
                                                &nbsp;
                                                <button id="remove_item_button0"
                                                        my_attr="an_item0"
                                                        class="btn btn-danger .btn-sm remove_item_button">
                                                    <span><i class="fa fa-minus"></i></span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_name_text') ?></label>
                                                    <input id="item_name0" class="form-control item_name"
                                                           placeholder="<?php echo lang('placeholder_item_name_text') ?>"
                                                           type="text"
                                                           name="item_name[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_name"
                                                          id="error_item_name0">
                                                </span>
                                                </div>
                                                <div class="col-xs-8">
                                                    <label><?php echo lang('label_item_description_text') ?></label>
                                                    <textarea id="item_description0" class="form-control"
                                                              name="item_description[]"
                                                              cols="30" rows="2"
                                                              placeholder="<?php echo lang('placeholder_item_description_text') ?>"></textarea>
                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_price_text') ?></label>
                                                    <input id="item_price0" class="form-control item_price"
                                                           placeholder="<?php echo lang('placeholder_item_price_text') ?>"
                                                           type="text"
                                                           name="item_price[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_price"
                                                          id="error_item_price0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_tax_rate_text') ?></label>
                                                    <input id="item_tax_rate0" class="form-control item_tax_rate"
                                                           placeholder="<?php echo lang('placeholder_item_tax_rate_text') ?>"
                                                           type="text"
                                                           name="item_tax_rate[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_tax_rate"
                                                          id="error_item_tax_rate0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_tax_amount_text') ?></label>
                                                    <input id="item_tax_amount0" class="form-control item_tax_amount"
                                                           name="item_tax_amount[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_tax_amount_text') ?>"
                                                           type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_discount_rate_text') ?></label>
                                                    <input id="item_discount_rate0"
                                                           class="form-control item_discount_rate"
                                                           placeholder="<?php echo lang('placeholder_item_discount_rate_text') ?>"
                                                           type="text"
                                                           name="item_discount_rate[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_discount_rate"
                                                          id="error_item_discount_rate0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_discount_amount_text') ?></label>
                                                    <input id="item_discount_amount0"
                                                           class="form-control item_discount_amount"
                                                           name="item_discount_amount[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_discount_amount_text') ?>"
                                                           type="text" required>
                                                    <span style="color: darkred" class="error_item_discount_amount"
                                                          id="error_item_discount_amount0">
                                                </span>
                                                </div>


                                            </div>
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_net_price_text') ?></label>
                                                    <input id="item_net_price0" class="form-control item_net_price"
                                                           name="item_net_price[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_net_price_text') ?>"
                                                           type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_quantity_text') ?></label>
                                                    <input id="item_quantity0" class="form-control item_quantity"
                                                           name="item_quantity[]"
                                                           placeholder="<?php echo lang('placeholder_item_quantity_text') ?>"

                                                           type="number" required>
                                                    <span style="color: darkred" class="error_item_quantity"
                                                          id="error_item_quantity0">
                                                </span>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_total_text') ?></label>
                                                    <input id="item_total0" class="form-control item_total"
                                                           name="item_total[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_total_text') ?>"
                                                           type="text" required>
                                                    <span style="color: darkred" class="error_item_total"
                                                          id="error_item_total0">
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- ---------- an item ends ---------- -->
                                <?php } else { ?>
                                    <!-- ---------- an item starts ---------- -->
                                    <div id="an_item0" style="display: none" class="box box-success">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <?php echo lang('label_item_number_text') ?>
                                                &nbsp; &nbsp;
                                                <input class="item_num" value="0" type="text"
                                                       style="border: transparent;background: transparent;color: #2b2b2b"
                                                       readonly>
                                            </h3>

                                            <div class="pull-right">
                                                <button id="add_item_button0"
                                                        class="btn btn-success .btn-sm add_item_button">
                                                    <span><i class="fa fa-plus"></i></span>
                                                </button>
                                                &nbsp;
                                                <button id="remove_item_button0"
                                                        my_attr="an_item0"
                                                        class="btn btn-danger .btn-sm remove_item_button">
                                                    <span><i class="fa fa-minus"></i></span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_name_text') ?></label>
                                                    <input id="item_name0" class="form-control item_name"
                                                           placeholder="<?php echo lang('placeholder_item_name_text') ?>"
                                                           type="text"
                                                           name="item_name[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_name"
                                                          id="error_item_name0">
                                                </span>
                                                </div>
                                                <div class="col-xs-8">
                                                    <label><?php echo lang('label_item_description_text') ?></label>
                                                    <textarea id="item_description0" class="form-control"
                                                              name="item_description[]"
                                                              cols="30" rows="2"
                                                              placeholder="<?php echo lang('placeholder_item_description_text') ?>"></textarea>
                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_price_text') ?></label>
                                                    <input id="item_price0" class="form-control item_price"
                                                           placeholder="<?php echo lang('placeholder_item_price_text') ?>"
                                                           type="text"
                                                           name="item_price[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_price"
                                                          id="error_item_price0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_tax_rate_text') ?></label>
                                                    <input id="item_tax_rate0" class="form-control item_tax_rate"
                                                           placeholder="<?php echo lang('placeholder_item_tax_rate_text') ?>"
                                                           type="text"
                                                           name="item_tax_rate[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_tax_rate"
                                                          id="error_item_tax_rate0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_tax_amount_text') ?></label>
                                                    <input id="item_tax_amount0" class="form-control item_tax_amount"
                                                           name="item_tax_amount[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_tax_amount_text') ?>"
                                                           type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_discount_rate_text') ?></label>
                                                    <input id="item_discount_rate0"
                                                           class="form-control item_discount_rate"
                                                           placeholder="<?php echo lang('placeholder_item_discount_rate_text') ?>"
                                                           type="text"
                                                           name="item_discount_rate[]"
                                                           required>
                                                    <span style="color: darkred" class="error_item_discount_rate"
                                                          id="error_item_discount_rate0">
                                                </span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label><?php echo lang('label_item_discount_amount_text') ?></label>
                                                    <input id="item_discount_amount0"
                                                           class="form-control item_discount_amount"
                                                           name="item_discount_amount[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_discount_amount_text') ?>"
                                                           type="text" required>
                                                    <span style="color: darkred" class="error_item_discount_amount"
                                                          id="error_item_discount_amount0">
                                                </span>
                                                </div>


                                            </div>
                                            <div class="row form-group">
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_net_price_text') ?></label>
                                                    <input id="item_net_price0" class="form-control item_net_price"
                                                           name="item_net_price[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_net_price_text') ?>"
                                                           type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_quantity_text') ?></label>
                                                    <input id="item_quantity0" class="form-control item_quantity"
                                                           name="item_quantity[]"
                                                           placeholder="<?php echo lang('placeholder_item_quantity_text') ?>"

                                                           type="number" required>
                                                    <span style="color: darkred" class="error_item_quantity"
                                                          id="error_item_quantity0">
                                                </span>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo lang('label_item_total_text') ?></label>
                                                    <input id="item_total0" class="form-control item_total"
                                                           name="item_total[]"
                                                           readonly
                                                           placeholder="<?php echo lang('placeholder_item_total_text') ?>"
                                                           type="text" required>
                                                    <span style="color: darkred" class="error_item_total"
                                                          id="error_item_total0">
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- ---------- an item ends ---------- -->
                                    <?php if ($invoice_items) { ?>
                                        <?php $i = 0 ?>
                                        <?php foreach ($invoice_items as $an_invoice_item) { ?>
                                            <?php $i++ ?>

                                            <!-- ---------- an item starts ---------- -->
                                            <div id="<?php echo 'an_item' . $i ?>" class="box box-success">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">
                                                        <?php echo lang('label_item_number_text') ?>
                                                        &nbsp; &nbsp;
                                                        <input class="item_num" value="<?php echo $i ?>" type="text"
                                                               style="border: transparent;background: transparent;color: #2b2b2b"
                                                               readonly>
                                                    </h3>

                                                    <div class="pull-right">
                                                        <button id="<?php echo 'add_item_button' . $i ?>"
                                                                class="btn btn-success .btn-sm add_item_button">
                                                            <span><i class="fa fa-plus"></i></span>
                                                        </button>
                                                        &nbsp;
                                                        <button id="<?php echo 'remove_item_button' . $i ?>"
                                                                my_attr="<?php echo 'an_item' . $i ?>"
                                                                class="btn btn-danger .btn-sm remove_item_button">
                                                            <span><i class="fa fa-minus"></i></span>
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="box-body">
                                                    <div class="row form-group">
                                                        <div class="col-xs-4">
                                                            <label><?php echo lang('label_item_name_text') ?></label>
                                                            <input id="<?php echo 'item_name' . $i ?>"
                                                                   class="form-control item_name"
                                                                   placeholder="<?php echo lang('placeholder_item_name_text') ?>"
                                                                   type="text"
                                                                   name="item_name[]"
                                                                   value="<?php echo $an_invoice_item->item_name ?>"
                                                                   required>
                                                            <span style="color: darkred" class="error_item_name"
                                                                  id="<?php echo 'error_item_name' . $i ?>">
                                                </span>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <label><?php echo lang('label_item_description_text') ?></label>
                                                            <textarea id="<?php echo 'item_description' . $i ?>"
                                                                      class="form-control"
                                                                      name="item_description[]"
                                                                      cols="30" rows="2"
                                                                      placeholder="<?php echo lang('placeholder_item_description_text') ?>"
                                                            ><?php echo $an_invoice_item->item_name ?></textarea>
                                                        </div>

                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-xs-4">
                                                            <label><?php echo lang('label_item_price_text') ?></label>
                                                            <input id="<?php echo 'item_price' . $i ?>"
                                                                   class="form-control item_price"
                                                                   placeholder="<?php echo lang('placeholder_item_price_text') ?>"
                                                                   type="text"
                                                                   name="item_price[]"
                                                                   value="<?php echo $an_invoice_item->item_price ?>"
                                                                   required>
                                                            <span style="color: darkred" class="error_item_price"
                                                                  id="<?php echo 'error_item_price' . $i ?>">
                                                </span>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label><?php echo lang('label_item_tax_rate_text') ?></label>
                                                            <input id="<?php echo 'item_tax_rate' . $i ?>"
                                                                   class="form-control item_tax_rate"
                                                                   placeholder="<?php echo lang('placeholder_item_tax_rate_text') ?>"
                                                                   type="text"
                                                                   name="item_tax_rate[]"
                                                                   value="<?php echo $an_invoice_item->item_tax_rate ?>"
                                                                   required>
                                                            <span style="color: darkred" class="error_item_tax_rate"
                                                                  id="<?php echo 'error_item_tax_rate' . $i ?>">
                                                </span>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label><?php echo lang('label_item_tax_amount_text') ?></label>
                                                            <input id="<?php echo 'item_tax_amount' . $i ?>"
                                                                   class="form-control item_tax_amount"
                                                                   name="item_tax_amount[]"
                                                                   value="<?php echo $an_invoice_item->item_tax_amount ?>"
                                                                   readonly
                                                                   placeholder="<?php echo lang('placeholder_item_tax_amount_text') ?>"
                                                                   type="text" required>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label><?php echo lang('label_item_discount_rate_text') ?></label>
                                                            <input id="<?php echo 'item_discount_rate' . $i ?>"
                                                                   class="form-control item_discount_rate"
                                                                   placeholder="<?php echo lang('placeholder_item_discount_rate_text') ?>"
                                                                   type="text"
                                                                   name="item_discount_rate[]"
                                                                   value="<?php echo $an_invoice_item->item_discount_rate ?>"
                                                                   required>
                                                            <span style="color: darkred"
                                                                  class="error_item_discount_rate"
                                                                  id="<?php echo 'error_item_discount_rate' . $i ?>">
                                                </span>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label><?php echo lang('label_item_discount_amount_text') ?></label>
                                                            <input id="<?php echo 'item_discount_amount' . $i ?>"
                                                                   class="form-control item_discount_amount"
                                                                   name="item_discount_amount[]"
                                                                   value="<?php echo $an_invoice_item->item_discount_amount ?>"
                                                                   readonly
                                                                   placeholder="<?php echo lang('placeholder_item_discount_amount_text') ?>"
                                                                   type="text" required>
                                                        </div>


                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-xs-4">
                                                            <label><?php echo lang('label_item_net_price_text') ?></label>
                                                            <input id="<?php echo 'item_net_price' . $i ?>"
                                                                   class="form-control item_net_price"
                                                                   name="item_net_price[]"
                                                                   value="<?php echo $an_invoice_item->item_net_price ?>"
                                                                   readonly
                                                                   placeholder="<?php echo lang('placeholder_item_net_price_text') ?>"
                                                                   type="text" required>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label><?php echo lang('label_item_quantity_text') ?></label>
                                                            <input id="<?php echo 'item_quantity' . $i ?>"
                                                                   class="form-control item_quantity"
                                                                   name="item_quantity[]"
                                                                   value="<?php echo $an_invoice_item->item_quantity ?>"
                                                                   placeholder="<?php echo lang('placeholder_item_quantity_text') ?>"

                                                                   type="number" required>
                                                            <span style="color: darkred" class="error_item_quantity"
                                                                  id="<?php echo 'error_item_quantity' . $i ?>">
                                                </span>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label><?php echo lang('label_item_total_text') ?></label>
                                                            <input id="<?php echo 'item_total' . $i ?>"
                                                                   class="form-control item_total"
                                                                   name="item_total[]"
                                                                   value="<?php echo $an_invoice_item->item_total ?>"
                                                                   readonly
                                                                   placeholder="<?php echo lang('placeholder_item_total_text') ?>"
                                                                   type="text" required>
                                                            <span style="color: darkred" class="error_item_total"
                                                                  id="<?php echo 'error_item_total' . $i ?>">
                                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- ---------- an item ends ---------- -->

                                        <?php } ?>

                                    <?php } ?>

                                <?php } ?>
                            </div>
                            <!-- item container ends-->

                            <hr style="border: 1px dashed black;"
                            ">

                            <div class="form-group">
                                <label><?php echo lang('label_invoice_total_text') ?></label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-money"></i>
                                    </div>
                                    <?php if ($which_form != 'edit_invoice') { ?>
                                        <input type="text" name="invoice_total" class="form-control pull-right"
                                               id="invoice_total"
                                               placeholder="<?php echo lang('placeholder_invoice_total_text') ?>"
                                               readonly
                                               value=""
                                               required
                                        >
                                    <?php } else { ?>
                                        <input type="text" name="invoice_total" class="form-control pull-right"
                                               id="invoice_total"
                                               placeholder="<?php echo lang('placeholder_invoice_total_text') ?>"
                                               readonly
                                               value="<?php if ($an_invoice_info) {
                                                   echo $an_invoice_info->invoice_total;
                                               } ?>"
                                               required
                                        >
                                    <?php } ?>
                                </div>
                                <!-- /.input group -->
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="submit_invoice" class="btn btn-primary">
                                <?php
                                if ($which_form == 'add_invoice' || $which_form == 'add_invoice_by_project') {
                                    echo lang('button_submit_create_text');
                                }
                                if ($which_form == 'edit_invoice') {
                                    echo lang('button_submit_update_text');
                                }
                                ?>
                            </button>
                        </div>

                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!--------------------------------------------------------------------------------------------------------------------->

<script>
    $('#add_a_project_text_wrapper').hide();
</script>

<!--------------------------------------------------------------------------------------------------------------------->


<script>

    $(function () {

        $(".select_project").select2({

            ajax: {
                url: '<?php echo base_url() . 'invoice_module/get_all_non_deleted_projects_with_client_with_currency_by_ajax' ?>',
                dataType: 'json',
                cache: true,
                delay: 500,
                allowClear: true,

                data: function (params) {

                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    /*console.log(data.items[0].id);
                     console.log(params);
                     console.log(data.more_pages);*/

                    //if no project is found
                    if (data.items[0].id == 0) {
                        $('#add_a_project_text_wrapper').show();
                    } else {
                        $('#add_a_project_text_wrapper').hide();
                    }

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                minimumInputLength: 3,
                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work

            }

        });

    });


</script>

<script>
    $(function () {
        <?php if($which_form == 'add_invoice') { ?>
        $('#change_project_currency_wrapper').hide();
        <?php } ?>

        $('.select_project').on("select2:select", function (e) {
            $('#change_project_currency_wrapper').show();

            var value = $(e.currentTarget).find("option:selected").val();
            //alert(value);

            var data = $('#select_project').select2('data');
            $('#client_id').val(data[0].client_id);
            $('#client_fullname').val(data[0].client_fullname);
            $('#currency_id').val(data[0].client_id);
            $('#currency_short_name').val(data[0].currency_short_name);
        });
    })
</script>

<!-------------------------------------------------------------------------------------->

<script>
    $('#invoice_deadline').datepicker({
        autoclose: true,
        numberOfMonths: 2,
        minDate: 0,
        dateFormat: '<?php echo $dateformat?>'
    });
    <?php if($which_form == 'edit_invoice') { ?>
    $('#invoice_clear_date').datepicker({
        autoclose: true,
        numberOfMonths: 2,
        minDate: new Date(<?php if ($an_invoice_info) {
            echo $an_invoice_info->invoice_created_at * 1000;
        } else {
            echo 0;
        }?>),
        maxDate: 0,
        dateFormat: '<?php echo $dateformat?>'
    });
    <?php } ?>
</script>


<!--------------------------------------------------------------------------------------------------------------------->
<!--add remove items-->
<script>
    $(function () {

        <?php if($which_form != 'edit_invoice') { ?>
        item_num = 1;
        item_count = 1;
        <?php } else { ?>
        item_num = parseInt(<?php echo $number_of_items ?>);
        item_count = parseInt(<?php echo $number_of_items ?>);
        <?php  } ?>

        max_limit = parseInt(<?php echo $maximum_number_of_item_row ?>);

        $(".add_item_button").on("click", function (e) {
            e.preventDefault();
            var add_item_button_id = $(this).attr('id');

            if (item_count < max_limit) {
                add_an_item(add_item_button_id); //pass add button id to hide it: not used yet
            } else {
                swal("<?php echo lang('swal_item_limit_reached_text')?>",
                    "<?php echo lang('swal_maximum_limit_text')?>" + ' ' + max_limit)
            }

        });

        $(".remove_item_button").on("click", function (e) {
            e.preventDefault();

            var got_attr = $(this).attr('my_attr');

            var item_to_remove_id = '#' + got_attr;

            if (item_count > 1) {
                $(item_to_remove_id).remove();
                item_count--;
                calculate_invoiceTotal();
            }

        });

        $("#item_name0").val('name0');
        $("#item_price0").val(0.00);
        $("#item_discount_rate0").val(0.00);
        $("#item_discount_amount0").val(0.00);
        $("#item_tax_rate0").val(0.00);
        $("#item_tax_amount0").val(0.00);
        $("#item_net_price0").val(0.00);
        $("#item_quantity0").val(1);
        $("#item_total0").val(0.00);

        <?php if($which_form != 'edit_invoice') { ?>
        append_first_item_row();
        $("#invoice_total").val(0.00);
        <?php } ?>

        function append_first_item_row() {
            $("#an_item0").css('display', 'block');
            var my_clone = $("#an_item0").clone(true);
            $("#an_item0").css('display', 'none');
            my_clone.attr('id', 'an_item1');

            my_clone.find("input").val("");
            my_clone.find("textarea").val("");

            my_clone.find(".item_num").val(1);

            my_clone.find(".remove_item_button").attr('my_attr', 'an_item1');
            my_clone.find(".remove_item_button").attr('id', 'remove_item_button1');
            my_clone.find(".add_item_button").attr('id', 'add_item_button1');


            /*-----------------------------------*/
            my_clone.find("#item_name0").attr('id', 'item_name1');
            my_clone.find("#error_item_name0").attr('id', 'error_item_name1');
            my_clone.find(".error_item_name").text('');

            my_clone.find("#item_description0").attr('id', 'item_description1');

            my_clone.find("#item_price0").attr('id', 'item_price1');
            my_clone.find("#error_item_price0").attr('id', 'error_item_price1');
            my_clone.find("#item_price1").val(0.00);
            my_clone.find(".error_item_price").text('');

            my_clone.find("#item_net_price0").attr('id', 'item_net_price1');
            my_clone.find("#error_item_net_price0").attr('id', 'error_item_net_price1');
            my_clone.find("#item_net_price1").val(0.00);
            my_clone.find(".error_item_net_price").text('');


            my_clone.find("#item_quantity0").attr('id', 'item_quantity1');
            my_clone.find("#error_item_quantity0").attr('id', 'error_item_quantity1');
            my_clone.find("#item_quantity1").val(1);
            my_clone.find(".error_item_quantity").text('');

            my_clone.find("#item_discount_rate0").attr('id', 'item_discount_rate1');
            my_clone.find("#error_item_discount_rate0").attr('id', 'error_item_discount_rate1');
            my_clone.find("#item_discount_rate1").val(0.00);
            my_clone.find(".error_item_discount_rate").text('');

            my_clone.find("#item_discount_amount0").attr('id', 'item_discount_amount1');
            my_clone.find("#error_item_discount_amount0").attr('id', 'error_item_discount_amount1');
            my_clone.find("#error_item_discount_amount1").val(0.00);
            my_clone.find(".error_item_discount_amount").text('');

            my_clone.find("#item_tax_rate0").attr('id', 'item_tax_rate1');
            my_clone.find("#error_item_tax_rate0").attr('id', 'error_item_tax_rate1');
            my_clone.find("#item_tax_rate1").val(0.00);
            my_clone.find(".error_item_tax_rate").text('');

            my_clone.find("#item_tax_amount0").attr('id', 'item_tax_amount1');
            my_clone.find("#error_item_tax_amount0").attr('id', 'error_item_tax_amount1');
            my_clone.find("#item_tax_amount1").val(0.00);
            my_clone.find(".error_item_tax_amount").text('');

            my_clone.find("#item_total0").attr('id', 'item_total1');
            my_clone.find("#error_item_total0").attr('id', 'error_item_total1');
            my_clone.find("#item_total1").val(0.00);
            my_clone.find(".error_item_total").text('');
            /*-----------------------------------*/

            $("#item_container").append(my_clone);
        }


        function add_an_item(add_item_button_id) {
            item_num++;

            $("#an_item0").css('display', 'block');
            var my_clone = $("#an_item0").clone(true);
            $("#an_item0").css('display', 'none');
            my_clone.attr('id', 'an_item' + item_num);

            my_clone.find("input").val("");
            my_clone.find("textarea").val("");

            my_clone.find(".item_num").val(item_num);
            /*my_clone.find(".multiple_files_to_upload").attr('name', 'multiple_files_to_upload' + item_num);
             my_clone.find(".multiple_files_note").attr('name', 'multiple_files_note' + item_num);*/

            my_clone.find(".remove_item_button").attr('my_attr', 'an_item' + item_num);
            my_clone.find(".remove_item_button").attr('id', 'remove_item_button' + item_num);
            my_clone.find(".add_item_button").attr('id', 'add_item_button' + item_num);


            /*-----------------------------------*/
            my_clone.find("#item_name0").attr('id', 'item_name' + item_num);
            my_clone.find("#error_item_name0").attr('id', 'error_item_name' + item_num);
            my_clone.find(".error_item_name").text('');

            my_clone.find("#item_description0").attr('id', 'item_description' + item_num);

            my_clone.find("#item_price0").attr('id', 'item_price' + item_num);
            my_clone.find("#error_item_price0").attr('id', 'error_item_price' + item_num);
            my_clone.find("#item_price" + item_num).val(0.00);
            my_clone.find(".error_item_price").text('');

            my_clone.find("#item_net_price0").attr('id', 'item_net_price' + item_num);
            my_clone.find("#error_item_net_price0").attr('id', 'error_item_net_price' + item_num);
            my_clone.find("#item_net_price" + item_num).val(0.00);
            my_clone.find(".error_item_net_price").text('');


            my_clone.find("#item_quantity0").attr('id', 'item_quantity' + item_num);
            my_clone.find("#error_item_quantity0").attr('id', 'error_item_quantity' + item_num);
            my_clone.find("#item_quantity" + item_num).val(1);
            my_clone.find(".error_item_quantity").text('');

            my_clone.find("#item_discount_rate0").attr('id', 'item_discount_rate' + item_num);
            my_clone.find("#error_item_discount_rate0").attr('id', 'error_item_discount_rate' + item_num);
            my_clone.find("#item_discount_rate" + item_num).val(0.00);
            my_clone.find(".error_item_discount_rate").text('');

            my_clone.find("#item_discount_amount0").attr('id', 'item_discount_amount' + item_num);
            my_clone.find("#error_item_discount_amount0").attr('id', 'error_item_discount_amount' + item_num);
            my_clone.find("#error_item_discount_amount" + item_num).val(0.00);
            my_clone.find(".error_item_discount_amount").text('');

            my_clone.find("#item_tax_rate0").attr('id', 'item_tax_rate' + item_num);
            my_clone.find("#error_item_tax_rate0").attr('id', 'error_item_tax_rate' + item_num);
            my_clone.find("#item_tax_rate" + item_num).val(0.00);
            my_clone.find(".error_item_tax_rate").text('');

            my_clone.find("#item_tax_amount0").attr('id', 'item_tax_amount' + item_num);
            my_clone.find("#error_item_tax_amount0").attr('id', 'error_item_tax_amount' + item_num);
            my_clone.find("#item_tax_amount" + item_num).val(0.00);
            my_clone.find(".error_item_tax_amount").text('');

            my_clone.find("#item_total0").attr('id', 'item_total' + item_num);
            my_clone.find("#error_item_total0").attr('id', 'error_item_total' + item_num);
            my_clone.find("#item_total" + item_num).val(0.00);
            my_clone.find(".error_item_total").text('');
            /*-----------------------------------*/

            calculate_invoiceTotal();

            $("#item_container").append(my_clone);

            item_count++;
        }


    });
</script>


<script>
    $(function () {
        $(".item_name").focusout(function () {
            var item_name_id = $(this).attr('id');
            validate_itemName(item_name_id);
        });

        $(".item_price").focusout(function () {
            var item_price_id = $(this).attr('id');
            validate_itemPrice(item_price_id);
        });

        $(".item_quantity").focusout(function () {
            var item_quantity_id = $(this).attr('id');
            validate_itemQuantity(item_quantity_id);
        });


        $(".item_discount_rate").focusout(function () {
            var item_discount_rate_id = $(this).attr('id');
            validate_itemDiscountRate(item_discount_rate_id);
        });

        $(".item_tax_rate").focusout(function () {
            var item_tax_rate_id = $(this).attr('id');
            validate_itemTaxRate(item_tax_rate_id);
        });


    });


</script>

<script>
    $(function () {

        $(".item_quantity").mouseup(function () {
            var item_quantity_id = $(this).attr('id');
            validate_itemQuantity(item_quantity_id);
        });

        $(".item_quantity").bind('input', function () {

            var item_quantity_id = $(this).attr('id');
            validate_itemQuantity(item_quantity_id);
        });

    })

</script>

<script>

    function validate_itemName(item_name_id) {
        var error_item_name_id = '#error_' + item_name_id;
        $(error_item_name_id).text('');

        var item_name_id = '#' + item_name_id;

        if ($(item_name_id).val() == '') {

            $(error_item_name_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        }
        return true;
    }

    function validate_itemPrice(item_price_id) {
        var error_item_price_id = '#error_' + item_price_id;
        $(error_item_price_id).text('');

        var item_price_id = '#' + item_price_id;

        if ($(item_price_id).val() == '') {
            $(error_item_price_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        }

        if (isNaN($(item_price_id).val())) {
            $(error_item_price_id).text('<?php echo lang('js_val_error_not_a_number_text')?>');
            return false;
        }
        if ($(item_price_id).val() < 0) {
            $(error_item_price_id).text('<?php echo lang('js_val_error_not_negative_text')?>');
            return false;
        }
        return true;
    }

    function validate_itemQuantity(item_quantity_id) {

        var error_item_quantity_id = '#error_' + item_quantity_id;
        $(error_item_quantity_id).text('');

        var item_quantity_id = '#' + item_quantity_id;

        if ($(item_quantity_id).val() == '') {
            $(error_item_quantity_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        }
        else {
            if (isNaN($(item_quantity_id).val())) {
                $(error_item_quantity_id).text('<?php echo lang('js_val_error_not_a_number_text')?>');
                return false;
            }
            if (!(($(item_quantity_id).val() % 1 ) === 0)) {
                $(error_item_quantity_id).text('<?php echo lang('js_val_error_not_an_integer_text')?>');
                return false;
            }
            if ($(item_quantity_id).val() < 0) {
                $(error_item_quantity_id).text('<?php echo lang('js_val_error_not_negative_text')?>');
                return false;
            }
        }
        return true;
    }

    function validate_itemDiscountRate(item_discount_rate_id) {
        var error_item_discount_rate_id = '#error_' + item_discount_rate_id;
        $(error_item_discount_rate_id).text('');

        var item_discount_rate_id = '#' + item_discount_rate_id;

        if ($(item_discount_rate_id).val() == '') {
            $(error_item_discount_rate_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        } else {
            if (isNaN($(item_discount_rate_id).val())) {
                $(error_item_discount_rate_id).text('<?php echo lang('js_val_error_not_a_number_text')?>');
                return false;
            }

            if ($(item_discount_rate_id).val() > 100) {
                $(error_item_discount_rate_id).text('<?php echo lang('js_val_error_cant_be_more_than_hundred_empty_text')?>');
                return false;
            }

            if ($(item_discount_rate_id).val() < 0) {
                $(error_item_discount_rate_id).text('<?php echo lang('js_val_error_not_negative_text')?>');
                return false;
            }
        }


        return true;
    }

    function validate_itemTaxRate(item_tax_rate_id) {
        var error_item_tax_rate_id = '#error_' + item_tax_rate_id;
        $(error_item_tax_rate_id).text('');

        var item_tax_rate_id = '#' + item_tax_rate_id;

        if ($(item_tax_rate_id).val() == '') {
            $(error_item_tax_rate_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        }

        if (isNaN($(item_tax_rate_id).val())) {
            $(error_item_tax_rate_id).text('<?php echo lang('js_val_error_not_a_number_text')?>');
            return false;
        }

        if ($(item_tax_rate_id).val() > 100) {
            $(error_item_tax_rate_id).text('<?php echo lang('js_val_error_cant_be_more_than_hundred_empty_text')?>');
            return false;
        }

        if ($(item_tax_rate_id).val() < 0) {
            $(error_item_tax_rate_id).text('<?php echo lang('js_val_error_not_negative_text')?>');
            return false;
        }

        return true;
    }

    function validate_itemTotal(item_total_id) {
        var error_item_total_id = '#error_' + item_total_id;
        $(error_item_total_id).text('');

        var item_total_id = '#' + item_total_id;

        if ($(item_total_id).val() == '') {
            $(error_item_total_id).text('<?php echo lang('js_val_error_cant_be_empty_text')?>');
            return false;
        }
        return true;
    }
</script>

<script>
    $(function () {

        $(".item_quantity").bind('mouseup input', function () {

            var item_quantity_id = $(this).attr('id');
            validate_itemQuantity(item_quantity_id);
            var row_id = $(this).parent().parent().parent().parent().attr('id');

            var item_price_id = $('#' + row_id).find(".item_price").attr('id');
            var item_discount_rate_id = $('#' + row_id).find(".item_discount_rate").attr('id');
            var item_tax_rate_id = $('#' + row_id).find(".item_tax_rate").attr('id');
            var item_discount_amount_id = $('#' + row_id).find(".item_discount_amount").attr('id');
            var item_tax_amount_id = $('#' + row_id).find(".item_tax_amount").attr('id');
            var item_net_price_id = $('#' + row_id).find(".item_net_price").attr('id');
            var item_total_id = $('#' + row_id).find(".item_total").attr('id');

            calculate_itemTotal(
                row_id,
                item_quantity_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_total_id
            );

            calculate_itemNetPrice(
                row_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_net_price_id
            )
        });

    });
</script>

<script>
    $(function () {

        $('.item_name').keyup(function () {
            var item_name_id = $(this).attr('id');
            validate_itemName(item_name_id);
        });

        $(".item_price").keyup(function () {

            var item_price_id = $(this).attr('id');
            validate_itemPrice(item_price_id);
            var row_id = $(this).parent().parent().parent().parent().attr('id');

            var item_quantity_id = $('#' + row_id).find(".item_quantity").attr('id');
            var item_discount_rate_id = $('#' + row_id).find(".item_discount_rate").attr('id');
            var item_tax_rate_id = $('#' + row_id).find(".item_tax_rate").attr('id');
            var item_discount_amount_id = $('#' + row_id).find(".item_discount_amount").attr('id');
            var item_tax_amount_id = $('#' + row_id).find(".item_tax_amount").attr('id');
            var item_net_price_id = $('#' + row_id).find(".item_net_price").attr('id');
            var item_total_id = $('#' + row_id).find(".item_total").attr('id');

            calculateDiscountAmount(row_id, item_price_id, item_discount_rate_id, item_discount_amount_id);
            calculateTaxAmount(row_id, item_price_id, item_tax_rate_id, item_tax_amount_id);

            calculate_itemTotal(
                row_id,
                item_quantity_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_total_id
            );

            calculate_itemNetPrice(
                row_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_net_price_id
            )

        });

        $(".item_quantity").keyup(function () {

            var item_quantity_id = $(this).attr('id');
            validate_itemQuantity(item_quantity_id);
            var row_id = $(this).parent().parent().parent().parent().attr('id');

            var item_price_id = $('#' + row_id).find(".item_price").attr('id');
            var item_discount_rate_id = $('#' + row_id).find(".item_discount_rate").attr('id');
            var item_tax_rate_id = $('#' + row_id).find(".item_tax_rate").attr('id');
            var item_discount_amount_id = $('#' + row_id).find(".item_discount_amount").attr('id');
            var item_tax_amount_id = $('#' + row_id).find(".item_tax_amount").attr('id');
            var item_net_price_id = $('#' + row_id).find(".item_net_price").attr('id');
            var item_total_id = $('#' + row_id).find(".item_total").attr('id');

            calculate_itemTotal(
                row_id,
                item_quantity_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_total_id
            );

            calculate_itemNetPrice(
                row_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_net_price_id
            )

        });

        $(".item_discount_rate").keyup(function () {

            var item_discount_rate_id = $(this).attr('id');
            validate_itemDiscountRate(item_discount_rate_id);
            var row_id = $(this).parent().parent().parent().parent().attr('id');

            var item_price_id = $('#' + row_id).find(".item_price").attr('id');
            var item_quantity_id = $('#' + row_id).find(".item_quantity").attr('id');
            var item_discount_rate_id = $('#' + row_id).find(".item_discount_rate").attr('id');
            var item_tax_rate_id = $('#' + row_id).find(".item_tax_rate").attr('id');
            var item_discount_amount_id = $('#' + row_id).find(".item_discount_amount").attr('id');
            var item_tax_amount_id = $('#' + row_id).find(".item_tax_amount").attr('id');
            var item_net_price_id = $('#' + row_id).find(".item_net_price").attr('id');
            var item_total_id = $('#' + row_id).find(".item_total").attr('id');

            calculateDiscountAmount(row_id, item_price_id, item_discount_rate_id, item_discount_amount_id);
            calculate_itemTotal(
                row_id,
                item_quantity_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_total_id
            );

            calculate_itemNetPrice(
                row_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_net_price_id
            )
        });

        $(".item_tax_rate").keyup(function () {

            var item_tax_rate_id = $(this).attr('id');
            validate_itemTaxRate(item_tax_rate_id);
            var row_id = $(this).parent().parent().parent().parent().attr('id');

            var item_price_id = $('#' + row_id).find(".item_price").attr('id');
            var item_quantity_id = $('#' + row_id).find(".item_quantity").attr('id');
            var item_discount_rate_id = $('#' + row_id).find(".item_discount_rate").attr('id');
            var item_discount_amount_id = $('#' + row_id).find(".item_discount_amount").attr('id');
            var item_tax_amount_id = $('#' + row_id).find(".item_tax_amount").attr('id');
            var item_net_price_id = $('#' + row_id).find(".item_net_price").attr('id');
            var item_total_id = $('#' + row_id).find(".item_total").attr('id');

            calculateTaxAmount(row_id, item_price_id, item_tax_rate_id, item_tax_amount_id);
            calculate_itemTotal(
                row_id,
                item_quantity_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_total_id
            );

            calculate_itemNetPrice(
                row_id,
                item_price_id,
                item_discount_rate_id,
                item_discount_amount_id,
                item_tax_rate_id,
                item_tax_amount_id,
                item_net_price_id
            )

        });

    })
</script>

<script>
    function round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }
</script>

<script>

    function calculateDiscountAmount(row_id, item_price_id, item_discount_rate_id, item_discount_amount_id) {
        var item_price = parseFloat($('#' + item_price_id).val());
        var item_discount_rate = parseFloat($('#' + item_discount_rate_id).val()) || 0;

        var discount = item_price * (item_discount_rate / 100);
        discount = round(discount, 2);

        $('#' + item_discount_amount_id).val(discount);

    }

    function calculateTaxAmount(row_id, item_price_id, item_tax_rate_id, item_tax_amount_id) {
        var item_price = parseFloat($('#' + item_price_id).val());
        var item_tax_rate = parseFloat($('#' + item_tax_rate_id).val()) || 0;

        var tax = item_price * (item_tax_rate / 100);
        tax = round(tax, 2);

        $('#' + item_tax_amount_id).val(tax);
    }


</script>

<script>
    function calculate_itemNetPrice(row_id,
                                    item_price_id,
                                    item_discount_rate_id,
                                    item_discount_amount_id,
                                    item_tax_rate_id,
                                    item_tax_amount_id,
                                    item_net_price_id) {

        var item_price = parseFloat($('#' + item_price_id).val());
        var item_discount_rate = parseFloat($('#' + item_discount_rate_id).val()) || 0;
        var item_tax_rate = parseFloat($('#' + item_tax_rate_id).val()) || 0;


        var discount = item_price * (item_discount_rate / 100);
        var tax = item_price * (item_tax_rate / 100);

        if (
                /*validate_itemPrice(item_price_id) == true &&
                 validate_itemQuantity(item_quantity_id) == true &&
                 validate_itemDiscount(item_discount_id) == true*/
            true

        ) {

            var item_net_price = (item_price + tax - discount);
            item_net_price = round(item_net_price, 2);
            $('#' + item_net_price_id).val(item_net_price);
        }

    }
</script>

<script>
    function calculate_itemTotal(row_id,
                                 item_quantity_id,
                                 item_price_id,
                                 item_discount_rate_id,
                                 item_discount_amount_id,
                                 item_tax_rate_id,
                                 item_tax_amount_id,
                                 item_total_id) {

        var item_price = parseFloat($('#' + item_price_id).val());
        var item_quantity = parseInt($('#' + item_quantity_id).val());
        var item_discount_rate = parseFloat($('#' + item_discount_rate_id).val()) || 0;
        var item_tax_rate = parseFloat($('#' + item_tax_rate_id).val()) || 0;


        var discount = item_price * (item_discount_rate / 100);
        var tax = item_price * (item_tax_rate / 100);

        if (
                /*validate_itemPrice(item_price_id) == true &&
                 validate_itemQuantity(item_quantity_id) == true &&
                 validate_itemDiscount(item_discount_id) == true*/
            true

        ) {


            var item_total = (item_price + tax - discount) * item_quantity;
            item_total = round(item_total, 2);
            $('#' + item_total_id).val(item_total);
        }


        calculate_invoiceTotal();

    }
</script>

<script>
    $(function () {
        $('#invoice_clear_date_wrapper').hide();

        if ($('#invoice_payment_clear').val() == 1) {
            $('#invoice_clear_date_wrapper').show();
        } else {
            $('#invoice_clear_date_wrapper').hide();

        }

        $('#invoice_payment_clear').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if (valueSelected == 1) {
                $('#invoice_clear_date_wrapper').show();
            } else {
                $('#invoice_clear_date_wrapper').hide();

            }
        });


    });
</script>

<script>
    $(function () {
        $("#invoice_deadline").datepicker({
            onSelect: function () {
                alert('date seleced');
                $('#error_invoice_deadline').text('');

            }
        });

    });


</script>

<script>
    function calculate_invoiceTotal() {
        //DO TOTAL CALCULATION, TAKE ALL CLASSES
        var total = 0;

        $('.item_total').each(function () {
            var val = parseFloat($(this).val());
            total += val;
            total = round(total, 2);
            $('#invoice_total').val(total);
        });
    }
</script>

<script>
    function isEveryElemTrueOrNot(element) {
        if (element == true) {
            return true;
        } else {
            return false;
        }
    }
</script>

<script>
    $('#submit_invoice').on('click', function (e) {
        e.preventDefault();

        /*-------------------------------*/
        item_name_val_array = new Array();
        $('.item_name').each(function () {
            var item_name_id = $(this).attr('id');
            var item_name_result = validate_itemName(item_name_id);
            item_name_val_array.push(item_name_result);
        });
        item_name_validation = item_name_val_array.every(isEveryElemTrueOrNot);
        /*-------------------------------*/


        /*-------------------------------*/
        item_price_val_array = new Array();
        $('.item_price').each(function () {
            var item_price_id = $(this).attr('id');
            var item_price_result = validate_itemPrice(item_price_id);
            item_price_val_array.push(item_price_result);
        });
        item_price_validation = item_price_val_array.every(isEveryElemTrueOrNot);
        /*-------------------------------*/


        /*-------------------------------*/
        item_tax_rate_val_array = new Array();
        $('.item_tax_rate').each(function () {
            var item_tax_rate_id = $(this).attr('id');
            var item_tax_rate_result = validate_itemTaxRate(item_tax_rate_id);
            item_name_val_array.push(item_tax_rate_result);
        });
        item_tax_rate_validation = item_tax_rate_val_array.every(isEveryElemTrueOrNot);
        /*-------------------------------*/


        /*-------------------------------*/
        item_discount_rate_val_array = new Array();
        $('.item_discount_rate').each(function () {
            var item_discount_rate_id = $(this).attr('id');
            var item_discount_rate_result = validate_itemDiscountRate(item_discount_rate_id);
            item_discount_rate_val_array.push(item_discount_rate_result);

        });
        item_discount_rate_validation = item_discount_rate_val_array.every(isEveryElemTrueOrNot);
        /*-------------------------------*/


        /*-------------------------------*/
        item_quantity_val_array = new Array();
        $('.item_quantity').each(function () {
            var item_quantity_id = $(this).attr('id');
            var item_quantity_result = validate_itemQuantity(item_quantity_id);
            item_quantity_val_array.push(item_quantity_result);

        });
        item_quantity_validation = item_quantity_val_array.every(isEveryElemTrueOrNot);
        /*-------------------------------*/

        if ($("#invoice_clear_date").datepicker("getDate") === null && $("#invoice_payment_clear").val() == 1) {
            var invoice_clear_date_validation = false;
            $('#error_invoice_clear_date').text('<?php echo lang('js_val_error_invoice_clear_date_not_empty_text')?>');
        } else {
            var invoice_clear_date_validation = true;
            $('#error_invoice_clear_date').text('');
        }

        if ($("#invoice_deadline").datepicker("getDate") === null) {
            var invoice_deadline_validation = false;
            $('#error_invoice_deadline').text('<?php echo lang('js_val_error_invoice_deadline_not_empty_text')?>');
        } else {
            var invoice_deadline_validation = true;
            $('#error_invoice_deadline').text('');
        }


        var select_project_validation = true;

        <?php if($which_form == 'add_invoice') { ?>
        var select_project_validation = false;
        if ($('#select_project').val() == 'select_a_project') {
            var select_project_validation = false;
            $('#error_select_project').text('<?php echo lang('js_val_error_select_a_project_text')?>');

        } else {
            var select_project_validation = true;
        }

        <?php } ?>

        console.log(select_project_validation);
        console.log(invoice_clear_date_validation);
        console.log(invoice_deadline_validation);
        console.log(item_name_validation);
        console.log(item_price_validation);
        console.log(item_tax_rate_validation);
        console.log(item_discount_rate_validation);
        console.log(item_quantity_validation);

        /*--------------------------------------------------------------------------------------------*/
        if (
            select_project_validation
            &&
            invoice_clear_date_validation
            &&
            invoice_deadline_validation
            &&
            item_name_validation
            &&
            item_price_validation
            &&
            item_tax_rate_validation
            &&
            item_discount_rate_validation
            &&
            item_quantity_validation
        ) {
            $("#an_item0").remove();
            if ($('#invoice_payment_clear').val() != 1) {
                $('#invoice_clear_date').remove();
            }
            $("#form_invoice").submit();
        }

    });
</script>