<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('invoice_text') ?>
            <small><?php echo $invoice_info->invoice_number ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() . 'common_module' ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrum_home_text') ?>
                </a>
            </li>
            <li>
                <?php if ($is_admin == 'admin') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>">
                        <?php echo lang('breadcrumb_section_all_invoice_text') ?>
                    </a>
                <?php } ?>

                <?php if ($is_client == 'client') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>">
                        <?php echo lang('breadcrumb_section_my_invoice_text') ?>
                    </a>
                <?php } ?>

                |
                <a href="<?php echo base_url() . 'invoice_module/show_project_invoice_list/'
                    . $invoice_project_info->project_id ?>">
                    <?php echo lang('breadcrumb_section_project_invoice_text') ?>
                </a>

            </li>
            <li class="active"><?php echo lang('breadcrum_page_text') ?></li>
        </ol>
    </section>
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa  fa-building-o"></i>
                    <?php echo $system_contact_info->system_contact_company_name ?>
                    <small class="pull-right"><?php echo lang('print_date_text') ?>
                        <strong><?php echo $print_date_datestring ?></strong>
                    </small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <?php echo lang('from_text') ?>
                <address>
                    <strong>
                        <?php echo $system_contact_info->system_contact_company_name ?>
                    </strong>
                    <br>

                    <?php echo lang('address_text') ?><br>
                    &nbsp;
                    <?php
                    if ($system_contact_info->system_contact_address != '') {
                        echo $system_contact_info->system_contact_address;
                    } else {
                        echo lang('unavailable_text');
                    }
                    ?>
                    <br>

                    <?php echo lang('phone_text') ?>
                    &nbsp;
                    <?php echo $system_contact_info->system_contact_phone ?><br>

                    <?php echo lang('email_text') ?>
                    &nbsp;
                    <?php echo $system_contact_info->system_contact_email ?><br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <?php echo lang('to_text') ?>
                <address>
                    <strong>
                        <?php echo $invoice_client_detail_info->first_name
                            . ' '
                            . $invoice_client_detail_info->last_name
                        ?>
                    </strong>
                    <br>
                    <?php echo lang('address_text') ?><br>
                    &nbsp;
                    <?php
                    if ($invoice_client_detail_info->user_office_address != '') {
                        echo $invoice_client_detail_info->user_office_address;
                    } else {
                        echo lang('unavailable_text');
                    }
                    ?>
                    <br>
                    <?php echo lang('phone_text') ?>
                    &nbsp;
                    <?php echo $invoice_client_detail_info->phone ?>
                    <br>
                    <?php echo lang('email_text') ?>
                    &nbsp;
                    <?php echo $invoice_client_detail_info->email ?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b><?php echo lang('invoice_number_text') ?></b>
                <?php echo $invoice_info->invoice_number ?>
                <br>

                <b><?php echo lang('project_name_text') ?></b>
                <?php echo $invoice_project_info->project_name ?>
                <br><br>

                <b><?php echo lang('invoice_deadline_text') ?>
                </b>
                <?php echo $invoice_info->invoice_deadline_datestring ?>
                <br>

                <b><?php echo lang('invoice_clear_date_text') ?>
                </b>
                <?php echo $invoice_info->invoice_clear_date_datestring ?>
                <br>

                <b><?php echo lang('invoice_total_text') ?></b>
                <?php
                if ($currency_position == 'left_far') {
                    echo $invoice_currency_info->currency_sign;
                    echo '  ';
                    echo $invoice_info->invoice_total;

                } else if ($currency_position == 'right_far') {
                    echo $invoice_info->invoice_total;
                    echo ' ';
                    echo $invoice_currency_info->currency_sign;

                } else if ($currency_position == 'left_along') {
                    echo $invoice_currency_info->currency_sign;
                    echo ' ';
                    echo $invoice_info->invoice_total;

                } else {
                    echo $invoice_info->invoice_total;
                    echo ' ';
                    echo $invoice_currency_info->currency_sign;
                }
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-------------------------------------------------------------->
        <hr>
        <div>
            <strong><?php echo lang('invoice_is_clear_text') ?></strong>
            &nbsp;
            <?php
            echo ' ';
            if ($invoice_info->invoice_payment_clear == 1) {
                echo lang('invoice_clear_yes_text');
            } else {
                echo lang('invoice_clear_no_text');
            }
            ?>
            <br>

            <strong><?php echo lang('invoice_is_open_text') ?></strong>
            &nbsp;
            <?php
            echo ' ';
            if ($invoice_info->invoice_status == 1) {
                echo lang('invoice_open_yes_text');
            } else {
                echo lang('invoice_open_no_text');
            }
            ?>
            <br>
        </div>
        <hr>

        <hr>
        <div>
            <strong><?php echo lang('invoice_description_text') ?></strong><br>
            <?php echo $invoice_info->invoice_description ?>
        </div>
        <hr>
        <!-------------------------------------------------------------------->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?php echo lang('item_column_serial_number_text') ?></th>
                        <th><?php echo lang('item_column_item_name_text') ?></th>
                        <th><?php echo lang('item_column_item_description_text') ?></th>
                        <th><?php echo lang('item_column_item_price_text') ?></th>
                        <th><?php echo lang('item_column_item_tax_rate_text') ?></th>
                        <th><?php echo lang('item_column_item_tax_amount_text') ?></th>
                        <th><?php echo lang('item_column_item_discount_rate_text') ?></th>
                        <th><?php echo lang('item_column_item_discount_amount_text') ?></th>
                        <th><?php echo lang('item_column_item_net_price_text') ?></th>
                        <th><?php echo lang('item_column_item_quantity_text') ?></th>
                        <th><?php echo lang('item_column_item_total_text') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($invoice_items) {
                        $i = 0; ?>

                        <?php foreach ($invoice_items as $an_invoice_item) {
                            $i++; ?>

                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $an_invoice_item->item_name ?></td>
                                <td><?php echo $an_invoice_item->item_description ?></td>
                                <td>
                                    <?php
                                    if ($currency_position == 'left_far') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo '  ';
                                        echo $an_invoice_item->item_price;

                                    } else if ($currency_position == 'right_far') {
                                        echo $an_invoice_item->item_price;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;

                                    } else if ($currency_position == 'left_along') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo ' ';
                                        echo $an_invoice_item->item_price;

                                    } else {
                                        echo $an_invoice_item->item_price;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $an_invoice_item->item_tax_rate ?></td>
                                <td>
                                    <?php
                                    if ($currency_position == 'left_far') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo '  ';
                                        echo $an_invoice_item->item_tax_amount;

                                    } else if ($currency_position == 'right_far') {
                                        echo $an_invoice_item->item_tax_amount;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;

                                    } else if ($currency_position == 'left_along') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo ' ';
                                        echo $an_invoice_item->item_tax_amount;

                                    } else {
                                        echo $an_invoice_item->item_tax_amount;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $an_invoice_item->item_discount_rate ?></td>
                                <td>
                                    <?php
                                    if ($currency_position == 'left_far') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo '  ';
                                        echo $an_invoice_item->item_discount_amount;

                                    } else if ($currency_position == 'right_far') {
                                        echo $an_invoice_item->item_discount_amount;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;

                                    } else if ($currency_position == 'left_along') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo ' ';
                                        echo $an_invoice_item->item_discount_amount;

                                    } else {
                                        echo $an_invoice_item->item_discount_amount;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($currency_position == 'left_far') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo '  ';
                                        echo $an_invoice_item->item_net_price;

                                    } else if ($currency_position == 'right_far') {
                                        echo $an_invoice_item->item_net_price;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;

                                    } else if ($currency_position == 'left_along') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo ' ';
                                        echo $an_invoice_item->item_net_price;

                                    } else {
                                        echo $an_invoice_item->item_net_price;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $an_invoice_item->item_quantity ?></td>
                                <td>
                                    <?php
                                    if ($currency_position == 'left_far') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo '  ';
                                        echo $an_invoice_item->item_total;

                                    } else if ($currency_position == 'right_far') {
                                        echo $an_invoice_item->item_total;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;

                                    } else if ($currency_position == 'left_along') {
                                        echo $invoice_currency_info->currency_sign;
                                        echo ' ';
                                        echo $an_invoice_item->item_total;

                                    } else {
                                        echo $an_invoice_item->item_total;
                                        echo ' ';
                                        echo $invoice_currency_info->currency_sign;
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <?php if($invoice_info->invoice_payment_clear != 1 ) { ?>
            <!-- accepted payments column -->
            <div class="col-xs-5">
                <p class="lead">Payment Methods:</p>
                <img src="<?php echo base_url() . 'project_base_assets/dist/img/credit/visa.png' ?>" alt="Visa">
                <img src="<?php echo base_url() . 'project_base_assets/dist/img/credit/mastercard.png' ?>"
                     alt="Mastercard">
                <img src="<?php echo base_url() . 'project_base_assets/dist/img/credit/american-express.png' ?>"
                     alt="American Express">
                <img src="<?php echo base_url() . 'project_base_assets/dist/img/credit/paypal2.png' ?>" alt="Paypal">

                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    <?php echo $payment_method_help_text ?>
                </p>
            </div>
            <!-- /.col -->
            <?php }else { ?>
                <div class="col-xs-5" ></div>
            <?php } ?>

            <div class="col-xs-offset-2 col-xs-5">
                <p class="lead">
                    <?php
                    if ($invoice_info->invoice_payment_clear == 1) {
                        echo lang('invoice_clear_date_text')
                            . ' '
                            . $invoice_info->invoice_clear_date_datestring;
                    } else {
                        echo lang('invoice_deadline_text')
                            . ' '
                            . $invoice_info->invoice_deadline_datestring;
                    }
                    ?>
                </p>

                <div class="table-responsive">
                    <table class="table">

                        <tr>
                            <th><?php echo lang('invoice_total_text') ?></th>
                            <td>
                                <?php
                                if ($currency_position == 'left_far') {
                                    echo $invoice_currency_info->currency_sign;
                                    echo '  ';
                                    echo $invoice_info->invoice_total;

                                } else if ($currency_position == 'right_far') {
                                    echo $invoice_info->invoice_total;
                                    echo ' ';
                                    echo $invoice_currency_info->currency_sign;

                                } else if ($currency_position == 'left_along') {
                                    echo $invoice_currency_info->currency_sign;
                                    echo ' ';
                                    echo $invoice_info->invoice_total;

                                } else {
                                    echo $invoice_info->invoice_total;
                                    echo ' ';
                                    echo $invoice_currency_info->currency_sign;
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="<?php echo base_url().'invoice_module/print_invoice/'.$invoice_id ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i>
                    <?php echo lang('print_text') ?>
                </a>
                &nbsp;
                <a href="" target="_blank" class="btn btn-warning"><i class="fa fa-envelope-o"></i>
                    <?php echo lang('email_to_client_text') ?>
                </a>

                <div class="pull-right">
                    <a href="<?php echo base_url().'invoice_module/generate_invoice_pdf/'.$invoice_id ?>" target="_blank" download="" class="btn btn-danger ">
                        <i class="fa fa-file-pdf-o"></i>
                        <?php echo lang('generate_pdf_text') ?>
                    </a>
                    <?php if($invoice_info->invoice_payment_clear != 1) { ?>
                    &nbsp;
                    <a disabled="" href="" target="_blank" class="btn btn-success">
                        <i class="fa fa-credit-card"></i>
                        <?php echo lang('submit_payment_text') ?>
                    </a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
<!-- /.content-wrapper -->