<div class="content-wrapper">

    <?php if ($is_admin == 'admin') { ?>
        &nbsp;
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">

                    <a class="btn btn-primary"
                       href="<?php
                       if ($which_list == 'all_invoice') {
                           echo base_url() . 'invoice_module/add_invoice';
                       }
                       if ($which_list == 'project_invoice' && $project_id != 0) {
                           echo base_url() . 'invoice_module/add_invoice_by_project/' . $project_id;
                       }

                       ?>"
                    >
                        <?php echo lang('add_button_text') ?>
                        &nbsp;
                        <span class="icon"><i class="fa fa-plus"></i></span>
                    </a>

                </div>
            </div>
        </div>
    <?php } ?>

    <section class="content-header">
        <h1>
            <?php echo lang('page_title_text') ?>
            <small>
                <?php
                if ($which_list == 'all_invoice') {
                    echo lang('page_subtitle_all_invoice_text');
                }

                if ($which_list == 'my_invoice') {
                    echo lang('page_subtitle_my_invoice_text');
                }

                if ($which_list == 'project_invoice') {
                    echo lang('page_subtitle_project_invoice_text');
                }

                ?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() . 'common_module' ?>"><i class="fa fa-dashboard"></i>
                    <?php echo lang('breadcrum_home_text') ?>
                </a>
            </li>
            <li>
                <?php if ($which_list == 'all_invoice') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>">
                        <?php echo lang('breadcrum_section_all_invoices_text') ?>
                    </a>
                <?php } ?>

                <?php if ($which_list == 'my_invoice') { ?>
                    <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>">
                        <?php echo lang('breadcrum_section_my_invoices_text') ?>
                    </a>
                <?php } ?>

                <?php if ($which_list == 'project_invoice') { ?>
                    <?php if ($is_admin == 'admin') { ?>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/all' ?>">
                            <?php echo lang('breadcrum_section_all_invoices_text') ?>
                        </a>
                    <?php } ?>
                    <?php if ($is_client == 'client') { ?>
                        <a href="<?php echo base_url() . 'invoice_module/show_invoice_list/my' ?>">
                            <?php echo lang('breadcrum_section_my_invoices_text') ?>
                        </a>
                    <?php } ?>
                    |
                    <a href="<?php echo base_url() . 'invoice_module/show_project_invoice_list/' . $project_id ?>">
                        <?php echo lang('breadcrum_section_project_invoices_text') ?>
                    </a>
                <?php } ?>

            </li>
            <li class="active"><?php echo lang('breadcrum_page_text') ?></li>
        </ol>
    </section>

    <!-- Content Header (Page header) -->

    <?php if ($this->session->flashdata('success')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-success copyright-wrap" id="success-panel">
                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                    <button type="button" class="close" data-target="#success-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('open_success')) {
                        echo $this->session->flashdata('open_success');
                    } ?>

                    <?php if ($this->session->flashdata('close_success')) {
                        echo $this->session->flashdata('close_success');
                    } ?>

                    <?php if ($this->session->flashdata('clear_success')) {
                        echo $this->session->flashdata('clear_success');
                    } ?>

                    <?php if ($this->session->flashdata('unclear_success')) {
                        echo $this->session->flashdata('unclear_success');
                    } ?>

                    <?php if ($this->session->flashdata('invoice_mail_sent_success')) {
                        echo $this->session->flashdata('invoice_mail_sent_success');
                    } ?>

                    <?php if ($this->session->flashdata('delete_success')) {
                        echo $this->session->flashdata('delete_success');
                    } ?>

                    &nbsp;
                    <?php if (!$this->session->flashdata('delete_success')) {
                        echo $this->session->flashdata('see_invoice');
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('not_success')) { ?>
        <br>
        <div class="col-md-6">
            <div class="panel panel-danger copyright-wrap" id="not-success-panel">
                <div class="panel-heading"><?php echo lang('unccessfull_text') ?>
                    <button type="button" class="close" data-target="#not-success-panel" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('email_config_error')) {
                        echo $this->session->flashdata('email_config_error');
                        echo '  ';
                        if ($is_admin == 'admin') {
                            if ($this->session->flashdata('configure_email')) {
                                echo $this->session->flashdata('configure_email');
                            }
                        }
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>


    <!-- Main content -->
    <section class="content">

        <?php if ($which_list == 'project_invoice') { ?>
            <!--projectroom menu starts-->
            <div class="row">
                <div class="col-md-4">
                    <?php echo $projectroom_menu_section ?>
                </div>
            </div>
            <!--projectroom menu ends-->
        <?php } ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php
                            if ($which_list == 'all_invoice') {
                                echo lang('page_subtitle_all_invoice_text');
                            }

                            if ($which_list == 'my_invoice') {
                                echo lang('page_subtitle_my_invoice_text');
                            }

                            if ($which_list == 'project_invoice') {
                                echo lang('page_subtitle_project_invoice_text');
                            }

                            ?>
                            <?php if ($which_list == 'project_invoice') { ?>
                                &nbsp;
                                <a href="<?php echo base_url() . 'projectroom_module/project_overview/' . $project_id ?>">
                                    <?php echo $project_name ?>
                                </a>
                            <?php } ?>
                        </h3>

                        <div style="padding-top: 1%;padding-bottom: 1%">
                            <?php echo lang('toggle_column_text') ?>
                            <a class="toggle-vis" data-column="0"><?php echo lang('column_invoice_number_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="1"><?php echo lang('column_project_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="2"><?php echo lang('column_client_name_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="3"><?php echo lang('column_invoice_total_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="4"><?php echo lang('column_invoice_status_text') ?></a>
                            -
                            <a class="toggle-vis"
                               data-column="5"><?php echo lang('column_invoice_payment_cleared_text') ?></a>
                            -
                            <a class="toggle-vis"
                               data-column="6"><?php echo lang('column_invoice_created_at_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="7"><?php echo lang('column_invoice_deadline_text') ?></a>
                            -
                            <a class="toggle-vis" data-column="8"><?php echo lang('column_action_text') ?></a>
                        </div>
                        <div>
                            <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                   border="0">
                                <tbody>
                                <tr id="filter_col0" data-column="0">
                                    <td align="center"><label><?php echo lang('column_invoice_number_text') ?></label>
                                    </td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col0_filter" type="text">
                                    </td>

                                    <td align="center"><label>regex</label></td>
                                    <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                    </td>

                                    <td align="center"><label>smart</label></td>
                                    <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                              type="checkbox"></td>
                                </tr>

                                <?php if ($which_list != 'project_invoice') { ?>
                                    <tr id="filter_col1" data-column="1">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_project_name_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col1_filter" type="text">
                                        </td>


                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col1_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col1_smart"
                                                                  checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                <?php } ?>

                                <?php if (!($which_list == 'my_invoice' || ($which_list == 'project_invoice' && $is_client == 'client'))) { ?>
                                    <tr id="filter_col2" data-column="2">
                                        <td align="center"><label
                                                    for=""><?php echo lang('column_client_name_text') ?></label></td>
                                        <td align="center">
                                            <input class="column_filter form-control" id="col2_filter" type="text">
                                        </td>

                                        <td align="center"><label>regex</label></td>
                                        <td align="center"><input class="column_filter" id="col2_regex" type="checkbox">
                                        </td>

                                        <td align="center"><label>smart</label></td>
                                        <td align="center"><input class="column_filter" id="col2_smart"
                                                                  checked="checked"
                                                                  type="checkbox"></td>
                                    </tr>
                                <?php } ?>

                                <tr id="filter_col4" data-column="4">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_invoice_status_text') ?></label></td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col4_filter" type="hidden">
                                        <select id="custom_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="open"><?php echo lang('option_open_text') ?></option>
                                            <option value="close"><?php echo lang('option_close_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="filter_col5" data-column="5">
                                    <td align="center"><label
                                                for=""><?php echo lang('column_invoice_payment_cleared_text') ?></label>
                                    </td>
                                    <td align="center">
                                        <input class="column_filter form-control" id="col5_filter" type="hidden">
                                        <select id="custom_clear_status_filter" class="form-control">
                                            <option value="all"><?php echo lang('option_all_text') ?></option>
                                            <option value="clear"><?php echo lang('option_clear_text') ?></option>
                                            <option value="unclear"><?php echo lang('option_unclear_text') ?></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="invoice-table"
                               class="table table-bordered table-hover table-responsive  dataTable">
                            <thead>
                            <tr>
                                <th><?php echo lang('column_invoice_number_text') ?></th>
                                <th><?php echo lang('column_project_name_text') ?></th>
                                <th><?php echo lang('column_client_name_text') ?></th>
                                <th><?php echo lang('column_invoice_total_text') ?></th>
                                <th><?php echo lang('column_invoice_status_text') ?></th>
                                <th><?php echo lang('column_invoice_payment_cleared_text') ?> </th>
                                <th><?php echo lang('column_invoice_created_at_text') ?></th>
                                <th><?php echo lang('column_invoice_deadline_text') ?></th>
                                <th><?php echo lang('column_action_text') ?></th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><?php echo lang('column_invoice_number_text') ?></th>
                                <th><?php echo lang('column_project_name_text') ?></th>
                                <th><?php echo lang('column_client_name_text') ?></th>
                                <th><?php echo lang('column_invoice_total_text') ?></th>
                                <th><?php echo lang('column_invoice_status_text') ?></th>
                                <th><?php echo lang('column_invoice_payment_cleared_text') ?> </th>
                                <th><?php echo lang('column_invoice_created_at_text') ?></th>
                                <th><?php echo lang('column_invoice_deadline_text') ?></th>
                                <th><?php echo lang('column_action_text') ?></th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<!---------------------------------------------------------------------------------------------------------->

<style>
    #invoice-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #invoice-table td,
    #invoice-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $(document).tooltip();
    })
</script>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'project_base_assets/base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


         $('#invoice-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_invoice_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_invoice_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            columns: [
                {data: "invoice_number"},
                {data: "project_name"},
                {data: "client_name"},
                {
                    data: {
                        _: "inv_tot.html",
                        sort: "inv_tot.dec"
                    }
                },
                {
                    data: {
                        _: "sts.html",
                        sort: "sts.int"
                    }
                },
                {
                    data: {
                        _: "pay_clr.html",
                        sort: "pay_clr.int"
                    }
                },
                {
                    data: {
                        _: "cr_dt.display",
                        sort: "cr_dt.timestamp"
                    }
                },
                {
                    data: {
                        _: "dl_dt.display",
                        sort: "dl_dt.timestamp"
                    }
                },

                {data: "action"}

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [8]} /*, { visible: false, targets: [2,5] }*/
            ],

            aaSorting: [[7, 'desc']],

            ajax: {
                url: "<?php echo base_url() . 'invoice_module/get_invoice_list_with_ajax' ?>",                   // json datasource
                type: "post",
                data: function (data) {
                    data.is_admin = '<?php if ($is_admin) {
                        echo $is_admin;
                    }?>';
                    data.is_client = '<?php if ($is_client) {
                        echo $is_client;
                    }?>';
                    data.is_staff = '<?php if ($is_staff) {
                        echo $is_staff;
                    }?>';
                    data.which_list = '<?php if ($which_list) {
                        echo $which_list;
                    }?>';
                    data.project_id = '<?php if ($project_id) {
                        echo $project_id;
                    }?>';

                },

                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>

<script>
    /*column toggle*/
    $(function () {

        var table = $('#invoice-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#invoice-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
        $('#custom_clear_status_filter').on('change', function () {

            if ($('#custom_clear_status_filter').val() == 'all') {
                $('#col5_filter').val('');
                filterColumn(5);
            } else {
                $('#col5_filter').val($('#custom_clear_status_filter').val());
                filterColumn(5);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {

        $('.delete-confirmation').click(function (e) {
            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_delete_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_delete_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_delete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

    }
</script>