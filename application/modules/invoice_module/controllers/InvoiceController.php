<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Created By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class InvoiceController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Invoice_model');

        $this->load->library('email');

        // application/libraries
        $this->load->library('MPDF/mpdf');
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');
        $this->load->library('custom_file_library');
        $this->load->library('custom_email_library');

    }

    /*----------------------------------------------------------------------------------------------------------------*/

    //for admin
    public function showAllInvoiceList()
    {
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        $this->showInvoiceList('all_invoice', 0);
    }

    //for client
    public function showMyInvoiceList()
    {
        if (!$this->ion_auth->in_group('client')) {
            redirect('users/auth/does_not_exist');
        }

        $this->showInvoiceList('my_invoice', 0);

    }


    public function getProjectroom_MenuSection($project_id)
    {
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        return $this->load->view('projectroom_module/projectroom_menu_section', $data, TRUE);
    }

    //from projectroom
    public function showProjectInvoiceList()
    {
        $project_id = $this->uri->segment(3);

        if ($this->ion_auth->in_group('client')) {
            $is_client = true;
        } else {
            $is_client = false;
        }

        if ($is_client) {
            $is_project_of_client = $this->isProjectOfClient($project_id, $this->session->userdata('user_id'));
        } else {
            $is_project_of_client = false;
        }

        /*
         * rule: only admin and project's client can see the invoice
         */
        if (!($this->ion_auth->is_admin() || ($this->ion_auth->in_group('client') && $is_project_of_client))) {
            redirect('users/auth/need_permission');
        }


        $this->showInvoiceList('project_invoice', $project_id);
    }

    public function isProjectOfClient($project_id, $client_id)
    {
        $a_project_row = $this->Invoice_model->getProject($project_id);

        if ($a_project_row) {

            if ($a_project_row->client_id == $client_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getCurrencyPosition()
    {
        $if_settings_type_exist = $this->custom_settings_library->ifSettingsTypeExist('currency_settings', 'currency_position');
        if ($if_settings_type_exist) {
            $currency_position_settings = $this->custom_settings_library->getASettings('currency_settings', 'currency_position');
            if ($currency_position_settings) {
                return $currency_position_settings->settings_value;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function showInvoiceList($which_list, $project_id)
    {
        $this->lang->load('invoice_list');

        $data['which_list'] = $which_list;
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }


        $data['invoices'] = array();

        if ($which_list == 'all_invoice') {
            $data['invoices'] = $this->Invoice_model->getAllInvoice();
            $data['project_name'] = '';
        }

        if ($which_list == 'my_invoice' && $data['is_client'] == 'client') {
            $data['invoices'] = $this->Invoice_model->getMyInvoice($this->session->userdata('user_id'));
            $data['project_name'] = '';
        }

        if ($which_list == 'project_invoice' && $project_id != 0) {
            $data['invoices'] = $this->Invoice_model->getProjectInvoice($project_id);
            $project_info = $this->Invoice_model->getProject($project_id);
            $data['project_name'] = $project_info->project_name;

            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id); //view
        }

        if ($data['invoices']) {
            $i = 0;
            foreach ($data['invoices'] as $an_invoice) {
                $a_pr_with_cl_with_cr_info =
                    $this->Invoice_model->getProjectWithClientWithCurrency($an_invoice->invoice_project_id);

                $data['invoices'][$i]->invoice_project_name = $a_pr_with_cl_with_cr_info->project_name;
                $data['invoices'][$i]->invoice_client_fullname =
                    $a_pr_with_cl_with_cr_info->first_name
                    . ' '
                    . $a_pr_with_cl_with_cr_info->last_name;
                $data['invoices'][$i]->currency_sign = $a_pr_with_cl_with_cr_info->currency_sign;

                $data['invoices'][$i]->invoice_created_at_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($an_invoice->invoice_created_at);

                $data['invoices'][$i]->invoice_deadline_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($an_invoice->invoice_deadline);

                $i++;
            }
        }

        $data['currency_position'] = $this->getCurrencyPosition();

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("invoice_module/invoice_list_page", $data);
        $this->load->view("common_module/footer");
    }

    /**
     *
     */
    public function getInvoiceListWithAjax()
    {
        $this->lang->load('invoice_list');

        $invoices = array();
        $table_data = array();

        $requestData = $_REQUEST;
        $is_admin = $requestData['is_admin'];
        $is_client = $requestData['is_client'];
        $is_staff = $requestData['is_staff'];

        $which_list = $requestData['which_list'];
        $project_id = $requestData['project_id'];
        //print_r($requestData);

        if ($project_id == 0) {
            $project_id = false;
        }

        $client_id = false;
        if ($which_list == 'my_invoice' && $is_client == 'client') {
            $client_id = $this->session->userdata('user_id');
        }


        $columns[0] = 'invoice_number';
        $columns[1] = 'invoice_project';
        $columns[2] = 'invoice_client';
        $columns[3] = 'invoice_total';
        $columns[4] = 'invoice_status';
        $columns[5] = 'invoice_payment_clear';
        $columns[6] = 'invoice_created_at';
        $columns[7] = 'invoice_clear_date';
        $columns[8] = 'action';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['invoice_number'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['invoice_project'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['invoice_client'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['invoice_status'] = $requestData['columns'][4]['search']['value'];
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['invoice_payment_clear'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        //before query , remember only one can be true between $staff_id and $client_id

        $totalData = $this->Invoice_model->countInvoicesByAjax(false, false, $project_id, $client_id, $which_list);


        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Invoice_model->countInvoicesByAjax($common_filter_value, $specific_filters, $project_id, $client_id, $which_list);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $invoices = $this->Invoice_model->getInvoicesByAjax($common_filter_value, $specific_filters, $order, $limit, $project_id, $client_id, $which_list);


        if ($invoices == false || empty($invoices) || $invoices == null) {
            $invoices = false;
            $table_data = false;
        }

        $last_query = $this->db->last_query();

        $currency_position = $this->custom_settings_library->getASettingsValue('currency_settings', 'currency_position');
        if (empty($currency_position) || $currency_position == '' || $currency_position == null) {
            $currency_position = false;
        }

        if ($invoices) {

            $i = 0;
            foreach ($invoices as $an_invoice) {


                $row = new stdClass();

                $row->invoice_number = $an_invoice->invoice_number;
                $row->invoice_title = $an_invoice->invoice_title;

                /*project name starts*/
                $project_tooltip = $an_invoice->project_name;
                $project_url = base_url() . 'projectroom_module/project_overview/' . $an_invoice->project_id;
                $project_name_anchor =
                    '<a ' . 'title="' . $project_tooltip . '"' . ' href="' . $project_url . '"' . ' >'
                    . $an_invoice->project_name
                    . '</a>';
                $row->project_name = $project_name_anchor;
                /*project name ends*/

                /*client name starts*/
                $client_tooltip = $an_invoice->client_first_name . ' ' . $an_invoice->client_last_name;
                $client_url = base_url() . 'user_profile_module/user_profile_overview/' . $an_invoice->client_user_id;
                $client_name_anchor = '<a ' . 'title="' . $client_tooltip . '"' . ' href="' . $client_url . '"' . ' >'
                    . $an_invoice->client_first_name . ' ' . $an_invoice->client_last_name
                    . '</a>';
                $row->client_name = $client_name_anchor;
                /*client name ends*/

                /*datetime starts*/
                $invoices[$i]->cr_dt = new stdClass();
                $invoices[$i]->dl_dt = new stdClass();

                $invoices[$i]->cr_dt->timestamp = $an_invoice->invoice_created_at;
                $invoices[$i]->dl_dt->timestamp = $an_invoice->invoice_deadline;

                if ($an_invoice->invoice_created_at == 0 || $an_invoice->invoice_created_at == false || $an_invoice->invoice_created_at == null) {
                    $invoices[$i]->cr_dt->display = $this->lang->line('no_date_text');
                } else {
                    $invoices[$i]->cr_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($an_invoice->invoice_created_at);
                }

                if ($an_invoice->invoice_deadline == 0 || $an_invoice->invoice_deadline == false || $an_invoice->invoice_deadline == null) {
                    $invoices[$i]->dl_dt->display = $this->lang->line('no_date_text');
                } else {
                    $invoices[$i]->dl_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($an_invoice->invoice_deadline);
                }

                $row->cr_dt = $invoices[$i]->cr_dt;
                $row->dl_dt = $invoices[$i]->dl_dt;
                /*datetime ends*/

                /*invoice total starts*/
                $invoices[$i]->inv_tot = new stdClass();
                $invoices[$i]->inv_tot->dec =  $an_invoice->invoice_total;

                $invoices[$i]->inv_tot->html = '';
                if (!$currency_position) {
                    $invoices[$i]->inv_tot->html = $an_invoice->currency_sign . ' ' . $an_invoice->invoice_total;
                } else {
                    if ($currency_position == 'left_along') {
                        $invoices[$i]->inv_tot->html = $an_invoice->currency_sign . ' ' . $an_invoice->invoice_total;
                    }
                    if ($currency_position == 'right_along') {
                        $invoices[$i]->inv_tot->html = $an_invoice->invoice_total . ' ' . $an_invoice->currency_sign;
                    }
                    if ($currency_position == 'left_far') {
                        $invoices[$i]->inv_tot->html = $an_invoice->currency_sign . ' &nbsp;&nbsp; ' . $an_invoice->invoice_total;
                    }
                    if ($currency_position == 'right_far') {
                        $invoices[$i]->inv_tot->html = $an_invoice->invoice_total . ' &nbsp;&nbsp; ' . $an_invoice->currency_sign;
                    }
                }

                $row->inv_tot = $invoices[$i]->inv_tot;

                /*invoice total ends*/

                /*open - close starts*/
                $invoices[$i]->sts = new stdClass();
                $invoices[$i]->sts->int = $an_invoice->invoice_status;

                if ($an_invoice->invoice_status == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('open_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_make_close_text');
                    $status_url = base_url() . 'invoice_module/' . $which_list . '/close_invoice/' . $an_invoice->invoice_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-lock" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('close_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_make_open_text');
                    $status_url = base_url() . 'invoice_module/' . $which_list . '/open_invoice/' . $an_invoice->invoice_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-unlock" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $invoices[$i]->sts->html = '';
                if ($this->ion_auth->is_admin()) {
                    $invoices[$i]->sts->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                } else {
                    $invoices[$i]->sts->html = $status_span;
                }

                $row->sts = $invoices[$i]->sts;
                /*open - close ends*/

                /*cleared - unclear starts*/
                $invoices[$i]->pay_clr = new stdClass();
                $invoices[$i]->pay_clr->int = $an_invoice->invoice_payment_clear;

                if ($an_invoice->invoice_payment_clear == 1) {

                    $pay_clr_status_span = '<span class = "label label-success">' . $this->lang->line('clear_text') . '</span>';

                    $pay_clr_status_tooltip = $this->lang->line('tooltip_make_unclear_text');
                    $pay_clr_status_url = base_url() . 'invoice_module/' . $which_list . '/unclear_invoice/' . $an_invoice->invoice_id;
                    $pay_clr_status_anchor_span = '<span class="label label-danger"><i class="fa fa-close" aria-hidden="true"></i></span>';
                    $pay_clr_status_anchor =
                        '<a ' . ' title="' . $pay_clr_status_tooltip . '"' . ' href="' . $pay_clr_status_url . '">' . $pay_clr_status_anchor_span . '</a>';

                } else {
                    $pay_clr_status_span = '<span class = "label label-warning">' . $this->lang->line('unclear_text') . '</span>';

                    $pay_clr_status_tooltip = $this->lang->line('tooltip_make_clear_text');
                    $pay_clr_status_url = base_url() . 'invoice_module/' . $which_list . '/clear_invoice/' . $an_invoice->invoice_id;
                    $pay_clr_status_anchor_span = '<span class="label label-success"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>';
                    $pay_clr_status_anchor =
                        '<a ' . ' title="' . $pay_clr_status_tooltip . '"' . ' href="' . $pay_clr_status_url . '">' . $pay_clr_status_anchor_span . '</a>';
                }

                $invoices[$i]->pay_clr->html = '';
                if ($this->ion_auth->is_admin()) {
                    $invoices[$i]->pay_clr->html = $pay_clr_status_span . '&nbsp; &nbsp;' . $pay_clr_status_anchor;
                } else {
                    $invoices[$i]->pay_clr->html = $pay_clr_status_span;
                }

                $row->pay_clr = $invoices[$i]->pay_clr;
                /*cleared - unclear end*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'invoice_module/view_invoice/' . $an_invoice->invoice_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $generate_pdf_tooltip = $this->lang->line('tooltip_generate_pdf_text');
                $generate_pdf_url = base_url() . 'invoice_module/generate_invoice_pdf/' . $an_invoice->invoice_id;
                $generate_pdf_anchor =
                    '<a ' . ' title="' . $generate_pdf_tooltip . '" ' . ' href="' . $generate_pdf_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa  fa-file-pdf-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $print_pdf_tooltip = $this->lang->line('tooltip_print_text');
                $print_pdf_url = base_url() . 'invoice_module/print_invoice/' . $an_invoice->invoice_id;
                $print_pdf_anchor =
                    '<a ' . ' title="' . $print_pdf_tooltip . '" ' . ' href="' . $print_pdf_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa  fa-print fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'invoice_module/edit_invoice/' . $an_invoice->invoice_id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'invoice_module/' . $which_list . '/delete_invoice/' . $an_invoice->invoice_id;
                $delete_anchor =
                    '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="delete-confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $invoices[$i]->action = '';
                if ($is_admin == 'admin' || $invoices[$i]->invoice_status == 1) {
                    $invoices[$i]->action .= $view_anchor . '&nbsp;&nbsp;';

                }

                if ($is_admin == 'admin' || ($invoices[$i]->invoice_status == 1 && $invoices[$i]->payment_clear == 0)) {
                    $invoices[$i]->action .= $generate_pdf_anchor . '&nbsp;&nbsp;';
                    $invoices[$i]->action .= $print_pdf_anchor . '&nbsp;&nbsp;';

                }


                if ($is_admin == 'admin') {
                    $invoices[$i]->action .= $edit_anchor . '&nbsp;&nbsp;';
                    $invoices[$i]->action .= $delete_anchor;
                }

                $row->action = $invoices[$i]->action;
                /*action ends*/

                if($which_list == 'project_invoice' && $is_client == 'client'){

                    /*
                     * remember project's client can be changed ,
                     *therefore invoice's client may differ from project's client
                     *
                     * In project invoice, only show the current client's invoices
                     */
                    if($an_invoice->invoice_client_id == $an_invoice->project_client_id){
                        $table_data[] = $row;
                    }

                }else{
                    $table_data[] = $row;
                }


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        $json_data['data'] = $table_data;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];

        $json_data['project_id'] = $project_id;
        $json_data['client_id'] = $client_id;
        $json_data['which_list'] = $which_list;*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));
    }


    /*----------------------------------------------------------------------------------------------------------------*/

    public function get_generated_invoice_number()
    {
        $invoice_prefix = 'INV_';

        $time_zone = $this->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('YmdHis');

        $invoice_number = $invoice_prefix . $datetime . '_R' . rand(10000, 99999);

        return $invoice_number;
    }

    public function addInvoice()
    {
        $data = array();
        $data['an_invoice_info'] = new stdClass();
        $data['an_invoice_info']->invoice_number = $this->get_generated_invoice_number();
        $data['an_invoice_info']->invoice_description = '';
        $data['an_invoice_info']->invoice_description = '';
        $data['an_invoice_info']->invoice_clear_date = '';
        $data['an_invoice_info']->invoice_clear_date_string = '';
        $data['an_invoice_info']->invoice_deadline_datestring = '';
        $data['an_invoice_info']->invoice_deadline = '';
        $data['an_invoice_info']->invoice_deadline_datestring = '';
        $data['an_invoice_info']->invoice_total = '';


        if ($this->input->post()) {
            /*
            # no php validation . all exeptions are checked by javascript.
            # if needed, write checkValidation();
                if (checkValidation()==true){
                $this->createInvoice();
                } else{
                set up $data array from post
                $this->getInvoiceForm('add_invoice', 0, 0, $data);
                }
            */
            $this->createInvoice('add_invoice', 0);
        } else {
            $this->getInvoiceForm('add_invoice', 0, 0, $data);
        }

    }

    public function addInvoice_ByProject()
    {
        $project_id = $this->uri->segment(3);
        $data = array();
        $data['an_invoice_info'] = new stdClass();
        $data['an_invoice_info']->invoice_number = $this->get_generated_invoice_number();
        $data['an_invoice_info']->invoice_description = '';
        $data['an_invoice_info']->invoice_clear_date = '';
        $data['an_invoice_info']->invoice_clear_date_string = '';
        $data['an_invoice_info']->invoice_deadline_datestring = '';
        $data['an_invoice_info']->invoice_deadline = '';
        $data['an_invoice_info']->invoice_deadline_datestring = '';
        $data['an_invoice_info']->invoice_total = '';

        if ($this->input->post()) {
            /*
            # no php validation . all exeptions are checked by javascript.
            # if needed, write checkValidation();
                if (checkValidation()==true){
                $this->createInvoice();
                } else{
                set up $data array from post
                $this->getInvoiceForm('add_invoice', 0, 0, $data);
                }
            */
            $this->createInvoice('add_invoice_by_project', $project_id);
        } else {
            $this->getInvoiceForm('add_invoice_by_project', $project_id, 0, $data);
        }

    }


    public function editInvoice()
    {
        $invoice_id = $this->uri->segment(3);
        $a_invoice = $this->Invoice_model->getInvoice($invoice_id);

        if ($a_invoice) {
            $project_id = $a_invoice->invoice_project_id;
        } else {
            $project_id = 0;
        }

        $data = array();
        if ($this->input->post()) {
            /*
            # no php validation . all exeptions are checked by javascript.
            # if needed, write checkValidation();
                if (checkValidation()==true){
                $this->updateInvoice();
                } else{
                set up $data array from post
                $this->getInvoiceForm('add_invoice', 0, 0, $data);
                }
            */
            $this->updateInvoice('edit_invoice', $invoice_id);
        } else {
            $this->getInvoiceForm('edit_invoice', $project_id, $invoice_id, $data);
        }


    }

    public function getInvoiceForm($which_form, $project_id, $invoice_id, $data)
    {
        $this->lang->load('invoice_form');

        $data['which_form'] = $which_form;
        $data['project_id'] = $project_id;

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
            redirect('users/auth/need_permission');
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        if ($which_form == 'add_invoice') {
            $data['form_action'] = base_url() . 'invoice_module/add_invoice';

            $data['invoice_items'] = null;
            $data['number_of_items'] = 0;
        }

        if ($which_form == 'add_invoice_by_project' && $project_id != 0) {
            $data['form_action'] = base_url() . 'invoice_module/add_invoice_by_project/' . $project_id;

            $data['invoice_items'] = null;
            $data['number_of_items'] = 0;
        }

        if ($which_form == 'edit_invoice' && $project_id != 0) {
            $data['form_action'] = base_url() . 'invoice_module/edit_invoice/' . $invoice_id;

            $data['an_invoice_info'] = $this->Invoice_model->getInvoice($invoice_id);

            $data['invoice_items'] = $this->Invoice_model->getInvoiceItems($invoice_id);
            $item_count = 0;
            foreach ($data['invoice_items'] as $an_invoice_item) {

                $item_count++;
            }
            $data['number_of_items'] = $item_count;

            $data['an_invoice_info']->invoice_deadline_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($data['an_invoice_info']->invoice_deadline);

            if (!($data['an_invoice_info']->invoice_clear_date == 0 || $data['an_invoice_info']->invoice_clear_date == '')) {

                $data['an_invoice_info']->invoice_clear_date_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($data['an_invoice_info']->invoice_clear_date);
            } else {

                $data['an_invoice_info']->invoice_clear_date_datestring = '';
            }


        }

        if ($project_id != 0) {
            $data['a_project_info_with_client_with_currency'] =
                $this->Invoice_model->getProjectWithClientWithCurrency($project_id);
        } else {
            $data['a_project_info_with_client_with_currency'] = null;
        }

        if (($which_form == 'add_invoice_by_project' || $which_form == 'edit_invoice') && $project_id != 0) {
            $data['projectroom_menu_section'] = $this->getProjectroom_MenuSection($project_id);
        } else {
            $data['projectroom_menu_section'] = null;
        }


        $data['dateformat'] = $this->custom_datetime_library->get_dateformat_PHP_to_jQueryUI();
        $data['maximum_number_of_item_row'] = 10;

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("invoice_module/invoice_form_page", $data);
        $this->load->view("common_module/footer");
    }

    //only acts when $which form == 'add_invoice'
    public function getAllNonDeletedProjectsWithClientWithCurrencyByAjax()
    {
        $this->lang->load('invoice_form');

        $search_filter = false;
        if (!empty($this->input->get('q'))) {
            $search_filter = $this->input->get('q');
        }

        $search_filter = trim($search_filter);

        $page = $this->input->get('page');
        if (!$this->input->get('page')) {
            $page = 1;
        }

        $limit = 10;

        $total_count = $this->Invoice_model->countAllNonDeletedProjectsWithWithCurrency($search_filter);
        $offset = ($page - 1) * $limit;

        //echo 'page:'.$page.' total_count:'.$total_count.' offset:'.$offset;die();
        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count; //bool

        $all_non_deleted_projects_with_client_with_currency =
            $this->Invoice_model->getAllNonDeletedProjectsWithWithCurrency($search_filter, $limit, $offset);
        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();
        if ($all_non_deleted_projects_with_client_with_currency) {


            foreach ($all_non_deleted_projects_with_client_with_currency as $a_nd_pr_w_cl_w_cr) {
                $p = array();
                $p['id'] = $a_nd_pr_w_cl_w_cr->project_id;
                $p['text'] = $a_nd_pr_w_cl_w_cr->project_name;
                $p['client_id'] = $a_nd_pr_w_cl_w_cr->client_id;
                $p['client_fullname'] = $a_nd_pr_w_cl_w_cr->first_name . ' ' . $a_nd_pr_w_cl_w_cr->last_name;
                $p['currency_id'] = $a_nd_pr_w_cl_w_cr->currency_id;
                $p['currency_short_name'] = $a_nd_pr_w_cl_w_cr->currency_short_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = 0;
            $p['text'] = $this->lang->line('no_project_text');
            $p['client_id'] = '';
            $p['client_fullname'] = $this->lang->line('no_client_text');
            $p['currency_id'] = '';
            $p['currency_short_name'] = $this->lang->line('no_currency_text');
            $items = $p;
            $json_data['items'][] = $items;
        }

        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        //$json_data['last_query'] = $last_query;
        echo json_encode($json_data);
    }

    public function createInvoice($which_form, $project_id)
    {
        $this->lang->load('invoice_form');

        $data['invoice_number'] = $this->input->post('invoice_number');
        $data['invoice_project_id'] = $this->input->post('invoice_project_id');
        $data['invoice_client_id'] = $this->input->post('invoice_client_id');
        $data['invoice_currency_id'] = $this->input->post('invoice_currency_id');
        $data['invoice_description'] = $this->input->post('invoice_description');
        $data['invoice_status'] = $this->input->post('invoice_status');
        $data['invoice_payment_clear'] = $this->input->post('invoice_payment_clear');

        $invoice_deadline = $this->input->post('invoice_deadline');
        $data['invoice_deadline'] =
            $this->custom_datetime_library
                ->convert_and_return_DateToTimestamp($invoice_deadline);

        $data['invoice_total'] = $this->input->post('invoice_total');
        $data['invoice_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data['invoice_modified_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        $this->Invoice_model->insertInvoice($data);
        $inserted_invoice_id = $this->db->insert_id();

        $item_name_array = $this->input->post('item_name');

        $i = 0;
        foreach ($item_name_array as $an_item_name) {

            $item_data['item_invoice_id'] = $inserted_invoice_id;

            $item_data['item_name'] = $_POST['item_name'][$i];
            $item_data['item_description'] = $_POST['item_description'][$i];
            $item_data['item_price'] = $_POST['item_price'][$i];
            $item_data['item_tax_rate'] = $_POST['item_tax_rate'][$i];
            $item_data['item_tax_amount'] = $_POST['item_tax_amount'][$i];
            $item_data['item_discount_rate'] = $_POST['item_discount_rate'][$i];
            $item_data['item_discount_amount'] = $_POST['item_discount_amount'][$i];
            $item_data['item_net_price'] = $_POST['item_net_price'][$i];
            $item_data['item_quantity'] = $_POST['item_quantity'][$i];
            $item_data['item_total'] = $_POST['item_total'][$i];

            $this->Invoice_model->insertInvoiceItems($item_data);

            $i++;
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $data['invoice_client_id'],                                             //2.    $created_for
            'invoice',                                                              //3.    $type
            $inserted_invoice_id,                                                   //4.    $type_id
            'created',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $data['invoice_project_id'],                                            //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $add_success_message =
            sprintf($this->lang->line('add_success_message_text'), $this->input->post('invoice_number'));

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $inserted_invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('add_success', $add_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);


        if ($which_form == 'add_invoice') {
            redirect('invoice_module/add_invoice');
        }

        if ($which_form == 'add_invoice_by_project' && $project_id != 0) {
            redirect('invoice_module/add_invoice_by_project/' . $project_id);
        }


    }

    public function updateInvoice($which_form, $invoice_id)
    {
        $this->lang->load('invoice_form');

        $invoice_id = $this->input->post('invoice_id');
        $invoice_number = $this->input->post('invoice_number');
        $invoice_project_id = $this->input->post('invoice_project_id');
        $invoice_client_id = $this->input->post('invoice_client_id');
        $data['invoice_currency_id'] = $this->input->post('invoice_currency_id');
        $data['invoice_description'] = $this->input->post('invoice_description');
        $data['invoice_status'] = $this->input->post('invoice_status');
        $data['invoice_payment_clear'] = $this->input->post('invoice_payment_clear');

        if ($this->input->post('invoice_clear_date')) {
            $invoice_clear_date = $this->input->post('invoice_clear_date');
            $data['invoice_clear_date'] =
                $this->custom_datetime_library
                    ->convert_and_return_DateToTimestamp($invoice_clear_date);
        } else {
            $data['invoice_clear_date'] = 0;
        }

        $invoice_deadline = $this->input->post('invoice_deadline');
        $data['invoice_deadline'] =
            $this->custom_datetime_library
                ->convert_and_return_DateToTimestamp($invoice_deadline);

        $data['invoice_total'] = $this->input->post('invoice_total');
        $data['invoice_modified_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        /*change list starts*/
        $invoice_info_from_db = $this->Invoice_model->getInvoice($invoice_id);
        $change_list = array();

        $change_list['change_type'] = 'invoice';


        if ($invoice_info_from_db->invoice_description != $data['invoice_description']) {
            $change_list['invoice_deadline']['need_to_convert_as'] = 'large_text';
            $change_list['invoice_deadline']['field_name'] = $this->lang->line('log_field_invoice_description_text');
            $change_list['invoice_deadline']['from'] = $invoice_info_from_db->invoice_description;
            $change_list['invoice_deadline']['to'] = $data['invoice_description'];
        }

        if ($invoice_info_from_db->invoice_status != $this->input->post('invoice_status')) {
            $change_list['invoice_status']['need_to_convert_as'] = 'invoice_status';
            $change_list['invoice_status']['field_name'] = $this->lang->line('log_field_invoice_status_text');
            $change_list['invoice_status']['from'] = $invoice_info_from_db->invoice_status;
            $change_list['invoice_status']['to'] = $data['invoice_status'];
        }

        if ($invoice_info_from_db->invoice_payment_clear != $this->input->post('invoice_payment_clear')) {
            $change_list['invoice_payment_clear']['need_to_convert_as'] = 'invoice_clear_status';
            $change_list['invoice_payment_clear']['field_name'] = $this->lang->line('log_field_invoice_payment_clear_text');
            $change_list['invoice_payment_clear']['from'] = $invoice_info_from_db->invoice_payment_clear;
            $change_list['invoice_payment_clear']['to'] = $data['invoice_payment_clear'];
        }

        if ($data['invoice_payment_clear'] == 1 && $this->input->post('invoice_clear_date')) {
            if ($invoice_info_from_db->invoice_clear_date != 0) {
                $db_inv_clr_dt_datestring =
                    $this->custom_datetime_library
                        ->convert_and_return_TimestampToDate($invoice_info_from_db->invoice_clear_date);
            } else {
                $db_inv_clr_dt_datestring = '';
            }

            if ($db_inv_clr_dt_datestring != $this->input->post('invoice_clear_date')) {
                $change_list['invoice_clear_date']['need_to_convert_as'] = 'date';
                $change_list['invoice_clear_date']['field_name'] = $this->lang->line('log_field_invoice_clear_date_text');
                $change_list['invoice_clear_date']['from'] = $invoice_info_from_db->invoice_clear_date;
                $change_list['invoice_clear_date']['to'] = $data['invoice_clear_date'];
            }
        }

        if ($invoice_info_from_db->invoice_deadline != 0) {
            $db_inv_deadline_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($invoice_info_from_db->invoice_deadline);
        } else {
            $db_inv_deadline_datestring = '';
        }

        if ($db_inv_deadline_datestring != $this->input->post('invoice_deadline')) {
            $change_list['invoice_deadline']['need_to_convert_as'] = 'date';
            $change_list['invoice_deadline']['field_name'] = $this->lang->line('log_field_invoice_deadline_text');
            $change_list['invoice_deadline']['from'] = $invoice_info_from_db->invoice_clear_date;
            $change_list['invoice_deadline']['to'] = $data['invoice_deadline'];
        }

        $currency_position = $this->getCurrencyPosition();
        $db_currency_info = $this->Invoice_model->getCurrency($invoice_info_from_db->invoice_currency_id);
        $old_currency_sign = $db_currency_info->currency_sign;
        $pst_currency_info = $this->Invoice_model->getCurrency($this->input->post('invoice_currency_id'));
        $new_currency_sign = $pst_currency_info->currency_sign;


        if ($invoice_info_from_db->invoice_total != $data['invoice_total']) {
            if ($currency_position == 'left_along') {
                $change_list['budget']['need_to_convert_as'] = '';
                $change_list['budget']['field_name'] = $this->lang->line('log_field_invoice_total_text');
                $change_list['budget']['from'] = $old_currency_sign . ' ' . $invoice_info_from_db->invoice_total;
                $change_list['budget']['to'] = $new_currency_sign . ' ' . $data['invoice_total'];

            } else if ($currency_position == 'right_along') {

                $change_list['budget']['need_to_convert_as'] = '';
                $change_list['budget']['field_name'] = $this->lang->line('log_field_invoice_total_text');
                $change_list['budget']['from'] = $invoice_info_from_db->invoice_total . ' ' . $old_currency_sign;
                $change_list['budget']['to'] = $data['invoice_total'] . ' ' . $new_currency_sign;

            } else if ($currency_position == 'left_far') {

                $change_list['budget']['need_to_convert_as'] = '';
                $change_list['budget']['field_name'] = $this->lang->line('log_field_invoice_total_text');
                $change_list['budget']['from'] = $old_currency_sign . ' ' . $invoice_info_from_db->invoice_total;
                $change_list['budget']['to'] = $new_currency_sign . ' ' . $data['invoice_total'];

            } else {

                //'right_far' or '';
                $change_list['budget']['need_to_convert_as'] = '';
                $change_list['budget']['field_name'] = $this->lang->line('log_field_invoice_total_text');
                $change_list['budget']['from'] = $invoice_info_from_db->invoice_total . ' ' . $old_currency_sign;
                $change_list['budget']['to'] = $data['invoice_total'] . ' ' . $new_currency_sign;
            }
        }


        if ($change_list) {
            $encoded_change_list = json_encode($change_list);
        } else {
            $encoded_change_list = '';
        }
        /*change list ends*/

        $this->Invoice_model->updateInvoice($invoice_id, $data);

        $this->Invoice_model->deleteInvoiceItems($invoice_id);

        $item_name_array = $this->input->post('item_name');

        $i = 0;
        foreach ($item_name_array as $an_item_name) {

            $item_data['item_invoice_id'] = $invoice_id;

            $item_data['item_name'] = $_POST['item_name'][$i];
            $item_data['item_description'] = $_POST['item_description'][$i];
            $item_data['item_price'] = $_POST['item_price'][$i];
            $item_data['item_tax_rate'] = $_POST['item_tax_rate'][$i];
            $item_data['item_tax_amount'] = $_POST['item_tax_amount'][$i];
            $item_data['item_discount_rate'] = $_POST['item_discount_rate'][$i];
            $item_data['item_discount_amount'] = $_POST['item_discount_amount'][$i];
            $item_data['item_net_price'] = $_POST['item_net_price'][$i];
            $item_data['item_quantity'] = $_POST['item_quantity'][$i];
            $item_data['item_total'] = $_POST['item_total'][$i];

            $this->Invoice_model->insertInvoiceItems($item_data);

            $i++;
        }


        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_client_id,                                                     //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'updated',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_project_id,                                                    //11.   $super_type_id
            '',                                                                     //12.   $other_information
            $encoded_change_list                                                    //13.   $change_list
        );
        /*creating log ends*/

        $update_success_message =
            sprintf($this->lang->line('update_success_message_text'), $this->input->post('invoice_number'));

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('update_success', $update_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        redirect('invoice_module/edit_invoice/' . $invoice_id);
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getInvoiceSender()
    {
        $user_info = $this->Invoice_model->getUser($this->session->userdata('user_id'));

        if ($user_info) {
            return $user_info->first_name . ' ' . $user_info->last_name;
        }
    }

    public function getSystemContactInfo()
    {

        $system_contact_info = new stdClass();
        $system_contact_info->system_contact_sender_name = $this->getInvoiceSender();

        $system_contact_info->system_contact_company_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        $system_contact_info->system_contact_email = $this->custom_settings_library->getASettingsValue('contact_settings', 'company_contact_email');
        $system_contact_info->system_contact_phone = $this->custom_settings_library->getASettingsValue('contact_settings', 'company_contact_phone');
        $system_contact_info->system_contact_address = $this->custom_settings_library->getASettingsValue('contact_settings', 'company_contact_address');

        if ($system_contact_info->system_contact_company_name == null || $system_contact_info->system_contact_company_name == '') {

            $system_contact_info->system_contact_company_name = $this->lang->line('unavailable_text');
        }

        if ($system_contact_info->system_contact_email == null || $system_contact_info->system_contact_email == '') {

            $system_contact_info->system_contact_email = $this->lang->line('unavailable_text');
        }
        if ($system_contact_info->system_contact_phone == null || $system_contact_info->system_contact_phone == '') {

            $system_contact_info->system_contact_phone = $this->lang->line('unavailable_text');
        }
        if ($system_contact_info->system_contact_address == null || $system_contact_info->system_contact_address == '') {

            $system_contact_info->system_contact_address = $this->lang->line('unavailable_text');
        }


        return $system_contact_info;
    }

    /*should get from payment settings. create one*/
    public function getPaymentMethodHelpText()
    {
        $payment_method_help_text =
            'This text is a demo payment method help text.
             As soon as a payment method is created , a new text will be provided';

        return $payment_method_help_text;
    }

    public function getInvoiceData($invoice_id)
    {
        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }


        $data['invoice_info'] = $this->Invoice_model->getInvoice($invoice_id);
        if ($data['invoice_info']->invoice_deadline != 0) {
            $data['invoice_info']->invoice_deadline_datestring = $this->custom_datetime_library
                ->convert_and_return_TimestampToDate($data['invoice_info']->invoice_deadline);
        } else {
            $data['invoice_info']->invoice_deadline_datestring = $this->lang->line('unavailable_text');
        }

        if ($data['invoice_info']->invoice_clear_date != 0) {
            $data['invoice_info']->invoice_clear_date_datestring = $this->custom_datetime_library
                ->convert_and_return_TimestampToDate($data['invoice_info']->invoice_clear_date);
        } else {
            $data['invoice_info']->invoice_clear_date_datestring = $this->lang->line('unavailable_text');
        }


        $data['invoice_items'] = $this->Invoice_model->getInvoiceItems($invoice_id);
        $data['invoice_client_detail_info'] = $this->Invoice_model->getDetailedClientInfo($data['invoice_info']->invoice_client_id);
        $data['invoice_project_info'] = $this->Invoice_model->getProject($data['invoice_info']->invoice_project_id);
        $data['invoice_currency_info'] = $this->Invoice_model->getCurrency($data['invoice_info']->invoice_currency_id);

        $data['system_contact_info'] = $this->getSystemContactInfo();
        $data['print_date_datestring'] = $this->custom_datetime_library->getCurrentDate();

        $data['currency_position'] = $this->getCurrencyPosition();
        $data['payment_method_help_text'] = $this->getPaymentMethodHelpText();

        return $data;
    }

    public function showInvoice()
    {
        $this->lang->load('invoice_details_page');

        $invoice_id = $this->uri->segment(3);

        $data = $this->getInvoiceData($invoice_id);

        $data['invoice_id'] = $invoice_id;


        if (
            ($data['is_client'] == 'client')
            &&
            ($this->session->userdata('user_id') != $data['invoice_info']->invoice_client_id)
            &&
            ($data['invoice_info']->invoice_status != 0 || $data['invoice_info']->invoice_deletion_status == 1)
        ) {
            redirect('users/auth/need_permission');
        }


        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("invoice_module/invoice_details_page", $data);
        $this->load->view("common_module/footer");

    }

    public function printInvoice()
    {
        $this->lang->load('invoice_details_page');

        $invoice_id = $this->uri->segment(3);

        $data = $this->getInvoiceData($invoice_id);

        $data['invoice_id'] = $invoice_id;

        $data['is_pdf'] = 'not_pdf';


        if (
            ($data['is_client'] == 'client')
            &&
            ($this->session->userdata('user_id') != $data['invoice_info']->invoice_client_id)
            &&
            ($data['invoice_info']->invoice_status != 0 || $data['invoice_info']->invoice_deletion_status == 1)
        ) {
            redirect('users/auth/need_permission');
        }


        /*do not load header,common left footer as this view contains the whole page*/
        $this->load->view("invoice_module/invoice_details_print_page", $data);
    }

    public function generateInvoicePdf()
    {
        $this->lang->load('invoice_details_page');

        $invoice_id = $this->uri->segment(3);

        $data = $this->getInvoiceData($invoice_id);

        $data['invoice_id'] = $invoice_id;

        $data['is_pdf'] = 'pdf';


        if (
            ($data['is_client'] == 'client')
            &&
            ($this->session->userdata('user_id') != $data['invoice_info']->invoice_client_id)
            &&
            ($data['invoice_info']->invoice_status != 0 || $data['invoice_info']->invoice_deletion_status == 1)
        ) {
            redirect('users/auth/need_permission');
        }

        $html = '';

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('RSPM');
        $mpdf->SetAuthor('rspm');
        $mpdf->SetDisplayMode('fullpage');

        $custom_invoice_stylesheet =
            file_get_contents(FCPATH . 'project_base_assets/custom-written-css-files/custom_invoice.css');

        /*do not load header,common left footer as this view contains the whole page*/
        $html .= $this->load->view('invoice_module/invoice_details_print_page', $data, TRUE);

        $name = $data['invoice_info']->invoice_number . '_PDF.pdf';


        $mpdf->WriteHTML($custom_invoice_stylesheet, 1);
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output($name, 'D');
        exit;

    }

    //unused
    /*public function getPdfSavePath()
    {
        $main_file_directory = $this->custom_file_library->getMainDirectory();
        return FCPATH.$main_file_directory.'/';
    }*/

    //for email
    /*not only returns the pdf content but the whole $data as this function is used to send invoice via email to client*/
    public function getInvoicePdf_WithData($invoice_id)
    {
        $this->lang->load('invoice_details_page');

        $data = $this->getInvoiceData($invoice_id);

        $data['invoice_id'] = $invoice_id;

        $data['is_pdf'] = 'pdf';


        if (
            ($data['is_client'] == 'client')
            &&
            ($this->session->userdata('user_id') != $data['invoice_info']->invoice_client_id)
            &&
            ($data['invoice_info']->invoice_status != 0 || $data['invoice_info']->invoice_deletion_status == 1)
        ) {
            redirect('users/auth/need_permission');
        }

        $html = '';

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('RSPM');
        $mpdf->SetAuthor('rspm');
        $mpdf->SetDisplayMode('fullpage');

        $custom_invoice_stylesheet =
            file_get_contents(FCPATH . 'project_base_assets/custom-written-css-files/custom_invoice.css');

        /*do not load header,common left footer as this view contains the whole page*/
        $html .= $this->load->view('invoice_module/invoice_details_print_page', $data, TRUE);

        $name = $data['invoice_info']->invoice_number . '_PDF.pdf';


        $mpdf->WriteHTML($custom_invoice_stylesheet, 1);
        $mpdf->WriteHTML($html, 2);

        $data['pdf_content'] = $mpdf->Output($name, 'S');
        $data['pdf_filename'] = $name;

        $data['invoice_html'] = $this->load->view('invoice_module/invoice_details_print_page', $data, TRUE);

        return $data;
    }

    //from admin to client
    public function emailInvoice()
    {
        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);

        $data = $this->getInvoicePdf_WithData($invoice_id);

        $config = $this->custom_email_library->getEmailConfig('html');

        //$config = false;

        if ($config == false) {

            $this->lang->load('email');


            $configure_email_url = base_url() . 'settings_module/email_settings';
            $configure_email_text = $this->lang->line('configure_email_text');
            $configure_email_anchor = "<a href='$configure_email_url'>$configure_email_text</a>";

            $this->session->set_flashdata('not_success', 'not_success');
            $this->session->set_flashdata('email_config_error', $this->lang->line('email_config_error_text'));
            $this->session->set_flashdata('configure_email', $configure_email_anchor);

            if ($which_list == 'all_invoice') {
                redirect('invoice_module/show_invoice_list/all');
            }
            if ($which_list == 'my_invoice') {
                redirect('invoice_module/show_invoice_list/my');
            }

            if ($which_list == 'project_invoice') {
                redirect('invoice_module/show_project_invoice_list/' . $data['invoice_info']->invoice_project_id);
            }
        } else {


            $this->lang->load('invoice_email');
            $pdf_content = $data['pdf_content'];
            $pdf_filename = $data['pdf_filename'];

            $invoice_html = $data['invoice_html'];

            $mail_subject = sprintf(
                $this->lang->line('invoice_for_project_text'),
                $data['invoice_project_info']->project_name,
                $data['invoice_info']->invoice_number
            );

            if ($config['protocol'] == 'smtp') {
                $mail_from = $config['smtp_user'];
            } else {
                $mail_from = 'rsmail@rspm.com';
            }

            $mail_from_name = 'RSPM'; //better if got from settings:'company_name'

            $mail_to = $data['invoice_client_detail_info']->email;


            //needed when in a loop
            //$this->email->clear();


            $this->email->initialize($config);
            $this->email->to($mail_to);
            $this->email->from($mail_from, $mail_from_name);
            $this->email->subject($mail_subject);
            $this->email->message($invoice_html);
            $this->email->attach($pdf_content, 'attachment', $pdf_filename, 'application/pdf');

            if ($this->email->send() == true) {


                $invoice_url = base_url() . 'settings_module/view_invoice/' . $data['invoice_info']->invoice_id;
                $invoice_number_text = $data['invoice_info']->invoice_number;
                $invoice_anchor = "<a href='$invoice_url'>$invoice_number_text</a>";

                $client_url = base_url() . 'user_profile_module/user_profile_overview/' . $data['invoice_info']->invoice_client_id;
                $client_fullname = $data['invoice_client_detail_info']->first_name . ' ' . $data['invoice_client_detail_info']->last_name;
                $client_anchor = "<a href='$client_url'>$client_fullname</a>";


                $invoice_mail_sent_success_text =
                    sprintf(
                        $this->lang->line('invoice_mail_sent_success_text'),
                        $invoice_anchor,
                        $client_anchor,
                        $data['invoice_client_detail_info']->email
                    );

                // write sprintf
                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('invoice_mail_sent_success', $invoice_mail_sent_success_text);


                if ($which_list == 'all_invoice') {
                    redirect('invoice_module/show_invoice_list/all');
                }
                if ($which_list == 'my_invoice') {
                    redirect('invoice_module/show_invoice_list/my');
                }

                if ($which_list == 'project_invoice') {
                    redirect('invoice_module/show_project_invoice_list/' . $data['invoice_info']->invoice_project_id);
                }
            } else {

                echo $this->email->print_debugger('headers');
                exit;
            }


        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function openInvoice()
    {
        $this->lang->load('invoice_list');

        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);
        $this->Invoice_model->makeInvoiceOpen($invoice_id);

        $invoice_info = $this->Invoice_model->getInvoice($invoice_id);

        $open_success_message =
            sprintf($this->lang->line('open_success_message_text'), $invoice_info->invoice_number);

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('open_success', $open_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_info->invoice_client_id,                                       //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'opened',                                                               //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_info->invoice_project_id,                                      //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        if ($which_list == 'all_invoice') {
            redirect('invoice_module/show_invoice_list/all');
        }
        if ($which_list == 'my_invoice') {
            redirect('invoice_module/show_invoice_list/my');
        }

        if ($which_list == 'project_invoice') {
            redirect('invoice_module/show_project_invoice_list/' . $invoice_info->invoice_project_id);
        }


    }

    public function closeInvoice()
    {
        $this->lang->load('invoice_list');

        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);
        $this->Invoice_model->makeInvoiceClose($invoice_id);

        $invoice_info = $this->Invoice_model->getInvoice($invoice_id);

        $close_success_message =
            sprintf($this->lang->line('close_success_message_text'), $invoice_info->invoice_number);

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('close_success', $close_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_info->invoice_client_id,                                       //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'closed',                                                               //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_info->invoice_project_id,                                      //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        if ($which_list == 'all_invoice') {
            redirect('invoice_module/show_invoice_list/all');
        }
        if ($which_list == 'my_invoice') {
            redirect('invoice_module/show_invoice_list/my');
        }

        if ($which_list == 'project_invoice') {
            redirect('invoice_module/show_project_invoice_list/' . $invoice_info->invoice_project_id);
        }

    }

    public function clearInvoice()
    {
        $this->lang->load('invoice_list');

        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);
        $this->Invoice_model->makeInvoiceClear($invoice_id);

        $current_timestamp = $this->custom_datetime_library->getCurrentTimestamp();
        $old_invoice_info = $this->Invoice_model->getInvoice($invoice_id);
        $this->Invoice_model->updateClearDate($invoice_id, $current_timestamp);

        $invoice_info = $this->Invoice_model->getInvoice($invoice_id);

        $clear_success_message =
            sprintf($this->lang->line('clear_success_message_text'), $invoice_info->invoice_number);

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('clear_success', $clear_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        /*change list starts*/
        if ($old_invoice_info->invoice_clear_date != 0) {
            $old_inv_clr_dt_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($old_invoice_info->invoice_clear_date);
        } else {
            $old_inv_clr_dt_datestring = '';
        }

        if ($invoice_info->invoice_clear_date != 0) {
            $inv_clr_dt_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($invoice_info->invoice_clear_date);
        } else {
            $inv_clr_dt_datestring = '';
        }

        if ($old_inv_clr_dt_datestring != $inv_clr_dt_datestring) {
            $change_list['invoice_clear_date']['need_to_convert_as'] = 'date';
            $change_list['invoice_clear_date']['field_name'] = $this->lang->line('log_field_invoice_clear_date_text');
            $change_list['invoice_clear_date']['from'] = $old_invoice_info->invoice_clear_date;
            $change_list['invoice_clear_date']['to'] = $invoice_info->invoice_clear_date;
        }

        if ($change_list) {
            $encoded_change_list = json_encode($change_list);
        } else {
            $encoded_change_list = '';
        }
        /*change list ends*/

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_info->invoice_client_id,                                       //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'cleared',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_info->invoice_project_id,                                      //11.   $super_type_id
            '',                                                                     //12.   $other_information
            $encoded_change_list                                                    //13.   $change_list
        );

        /*creating log ends*/

        if ($which_list == 'all_invoice') {
            redirect('invoice_module/show_invoice_list/all');
        }
        if ($which_list == 'my_invoice') {
            redirect('invoice_module/show_invoice_list/my');
        }

        if ($which_list == 'project_invoice') {
            redirect('invoice_module/show_project_invoice_list/' . $invoice_info->invoice_project_id);
        }


    }

    public function unclearInvoice()
    {
        $this->lang->load('invoice_list');

        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);
        $this->Invoice_model->makeInvoiceUnclear($invoice_id);

        $current_timestamp = 0;
        $old_invoice_info = $this->Invoice_model->getInvoice($invoice_id);
        $this->Invoice_model->updateClearDate($invoice_id, $current_timestamp);

        $invoice_info = $this->Invoice_model->getInvoice($invoice_id);

        $unclear_success_message =
            sprintf($this->lang->line('unclear_success_message_text'), $invoice_info->invoice_number);

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('unclear_success', $unclear_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        /*change list starts*/
        if ($old_invoice_info->invoice_clear_date != 0) {
            $old_inv_clr_dt_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($old_invoice_info->invoice_clear_date);
        } else {
            $old_inv_clr_dt_datestring = '';
        }

        if ($invoice_info->invoice_clear_date != 0) {
            $inv_clr_dt_datestring =
                $this->custom_datetime_library
                    ->convert_and_return_TimestampToDate($invoice_info->invoice_clear_date);
        } else {
            $inv_clr_dt_datestring = '';
        }

        if ($old_inv_clr_dt_datestring != $inv_clr_dt_datestring) {
            $change_list['invoice_clear_date']['need_to_convert_as'] = 'date';
            $change_list['invoice_clear_date']['field_name'] = $this->lang->line('log_field_invoice_clear_date_text');
            $change_list['invoice_clear_date']['from'] = $old_invoice_info->invoice_clear_date;
            $change_list['invoice_clear_date']['to'] = $invoice_info->invoice_clear_date;
        }

        if ($change_list) {
            $encoded_change_list = json_encode($change_list);
        } else {
            $encoded_change_list = '';
        }
        /*change list ends*/

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_info->invoice_client_id,                                       //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'uncleared',                                                            //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_info->invoice_project_id,                                      //11.   $super_type_id
            '',                                                                     //12.   $other_information
            $encoded_change_list                                                    //13.   $change_list
        );

        /*creating log ends*/

        if ($which_list == 'all_invoice') {
            redirect('invoice_module/show_invoice_list/all');
        }
        if ($which_list == 'my_invoice') {
            redirect('invoice_module/show_invoice_list/my');
        }

        if ($which_list == 'project_invoice') {
            redirect('invoice_module/show_project_invoice_list/' . $invoice_info->invoice_project_id);
        }

    }

    public function deleteInvoice()
    {
        $this->lang->load('invoice_list');

        $which_list = $this->uri->segment(2);
        $invoice_id = $this->uri->segment(4);
        $this->Invoice_model->deleteInvoice($invoice_id);

        $invoice_info = $this->Invoice_model->getInvoice($invoice_id);

        $delete_success_message =
            sprintf($this->lang->line('delete_success_message_text'), $invoice_info->invoice_number);

        $see_invoice_url = base_url() . 'invoice_module/view_invoice/' . $invoice_id;
        $see_invoice_text = $this->lang->line('see_invoice_text');
        $see_invoice_anchor = "<a href='$see_invoice_url'>$see_invoice_text</a>";

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('delete_success', $delete_success_message);
        $this->session->set_flashdata('see_invoice', $see_invoice_anchor);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            $invoice_info->invoice_client_id,                                       //2.    $created_for
            'invoice',                                                              //3.    $type
            $invoice_id,                                                            //4.    $type_id
            'deleted',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'client',                                                               //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            'project',                                                              //10.   $super_type
            $invoice_info->invoice_project_id,                                      //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        if ($which_list == 'all_invoice') {
            redirect('invoice_module/show_invoice_list/all');
        }
        if ($which_list == 'my_invoice') {
            redirect('invoice_module/show_invoice_list/my');
        }

        if ($which_list == 'project_invoice') {
            redirect('invoice_module/show_project_invoice_list/' . $invoice_info->invoice_project_id);
        }
    }


}