<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

            <?php
            if ($form_name == 'add_currency_form') {
                echo lang('page_title_add_text');
            } else {
                echo lang('page_title_edit_text');
            }
            ?>

            <small><?php echo lang('page_subtitle_text') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'settings_module/currency_settings' ?>"><i
                            class="fa fa-cogs"></i><?php echo lang('breadcrumb_home_text') ?></a></li>
            <li>
                <a href="<?php echo base_url() . 'currency_module' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
            </li>
            <li class="active">
                <?php
                if ($form_name == 'add_currency_form') {
                    echo lang('breadcrumb_add_page_text');
                } else {
                    echo lang('breadcrumb_edit_page_text');
                }

                ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                        <br><br>
                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php
                            if ($this->session->flashdata('validation_errors'))
                                echo $this->session->flashdata('validation_errors');
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php
                            if ($this->session->flashdata('currency_name_exists'))
                                echo $this->session->flashdata('currency_name').lang('already_exists');
                            ?>
                        </div>
                        <div class="col-md-2"></div>

                        <div class=" col-md-offset-2 col-md-8" style="color: maroon;font-size: larger">
                            <?php
                            if ($this->session->flashdata('currency_short_name_exists'))
                                echo $this->session->flashdata('currency_short_name').lang('already_exists');
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!-- form start -->
                    <form action="<?php echo base_url() . $form_action ?>" role="form" id="" method="post"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">

                                <?php if ($form_name == 'edit_currency_form') { ?>
                                    <input type="text" name="currency_id" class="form-control" value="<?php echo $a_currency->currency_id ?>">
                                <?php } ?>

                                <label for="currency_name"><?php echo lang('label_currency_name_text') ?></label>
                                <?php echo form_error('currency_name'); ?>
                                <input type="text" name="currency_name" class="form-control" id="currency_name"
                                       value="<?php
                                       if ($form_name == 'add_currency_form') {

                                           if ($this->session->flashdata('currency_name'))
                                               echo $this->session->flashdata('currency_name');
                                       } else {
                                           if ($a_currency) {
                                               echo $a_currency->currency_name;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_currency_name_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="currency_short_name"><?php echo lang('label_currency_short_name_text') ?></label>

                                <input type="text" name="currency_short_name" class="form-control"
                                       id="currency_short_name"
                                       value="<?php
                                       if ($form_name == 'add_currency_form') {

                                           if ($this->session->flashdata('currency_short_name'))
                                               echo $this->session->flashdata('currency_short_name');
                                       } else {
                                           if ($a_currency) {
                                               echo $a_currency->currency_short_name;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_currency_short_name_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="currency_sign">
                                    <?php echo lang('label_currency_sign_text') ?>
                                    &nbsp;
                                    <small>
                                        <a href="http://www.fileformat.info/info/unicode/category/Sc/list.htm"
                                           target="_blank">
                                            <?php echo lang('label_currency_sign_help_text') ?>
                                        </a>
                                    </small>
                                </label>

                                <input type="text" name="currency_sign" class="form-control" id="currency_sign"
                                       value="<?php
                                       if ($form_name == 'add_currency_form') {

                                           if ($this->session->flashdata('currency_sign'))
                                               echo $this->session->flashdata('currency_sign');
                                       } else {
                                           if ($a_currency) {
                                               echo $a_currency->currency_sign;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_currency_sign_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="conversion_rate"><?php echo lang('label_conversion_rate_text') ?></label>

                                <input type="text" name="conversion_rate" class="form-control" id="conversion_rate"
                                       value="<?php
                                       if ($form_name == 'add_currency_form') {

                                           if ($this->session->flashdata('conversion_rate'))
                                               echo $this->session->flashdata('conversion_rate');
                                       } else {
                                           if ($a_currency) {
                                               echo $a_currency->conversion_rate;
                                           }
                                       }
                                       ?>"
                                       placeholder="<?php echo lang('placeholder_conversion_rate_text') ?>">
                            </div>
                            <div class="form-group">
                                <label for="currency_status"><?php echo lang('label_currency_status_text') ?></label>

                                <select class="form-control" name="currency_status" id="currency_status">

                                    <option value="1"
                                        <?php
                                        if ($form_name == 'add_currency_form') {
                                            if ($this->session->flashdata('currency_status')
                                                && $this->session->flashdata('currency_status') == 1
                                            ) {
                                                echo 'selected';
                                            }
                                        } else {
                                            if ($a_currency) {
                                                if ($a_currency->conversion_rate == 1) {
                                                    echo 'selected';
                                                }
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_currency_status_enable_text') ?>
                                    </option>

                                    <option value="0"
                                        <?php
                                        if ($form_name == 'add_currency_form') {
                                            if ($this->session->flashdata('currency_status')
                                                && $this->session->flashdata('currency_status') == 0
                                            ) {
                                                echo 'selected';
                                            }
                                        } else {
                                            if ($a_currency) {
                                                if ($a_currency->conversion_rate == 0) {
                                                    echo 'selected';
                                                }
                                            }
                                        }
                                        ?>
                                            class="">
                                        <?php echo lang('option_currency_status_endisable_text') ?>
                                    </option>

                                </select>

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">


                            <button type="submit" id="btnsubmit"
                                    class="btn btn-primary">
                                <?php
                                if ($form_name == 'add_currency_form') {
                                    echo lang('button_submit_create_text');
                                } else {
                                    echo lang('button_submit_update_text');
                                }
                                ?>

                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->