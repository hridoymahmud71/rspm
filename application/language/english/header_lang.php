<?php

$lang['profile_text'] = 'Profile';
$lang['timeline_text'] = 'Timeline';
$lang['edit_profile_text'] = 'Edit Profile';

$lang['task_notification_running_tasks_text'] = 'Have Running tasks';
$lang['task_notification_no_running_tasks_text'] = 'No running tasks';
$lang['task_notification_see_my_tasks_text'] = 'See my tasks';

?>