<?php



/*common_left menu items*/

$lang['cl_menu_main_navigation_text'] = 'Main Navigation';
$lang['cl_menu_dashboard_text'] = 'Dashboard';

$lang['cl_menu_settings_text'] = 'Settings';
$lang['cl_menu_settings_sub_menu_general_settings_text'] = 'General Settings';
$lang['cl_menu_settings_sub_menu_contact_settings_text'] = 'Contact Settings';
$lang['cl_menu_settings_sub_menu_image_settings_text'] = 'Image Settings';
$lang['cl_menu_settings_sub_menu_file_settings_text'] = 'File Settings';
$lang['cl_menu_settings_sub_menu_currency_settings_text'] = 'Currency Settings';
$lang['cl_menu_settings_sub_menu_datetime_settings_text'] = 'Date & Time Settings';
$lang['cl_menu_settings_sub_menu_email_settings_text'] = 'Email Settings';


$lang['cl_menu_users_text'] = 'Users';
$lang['cl_menu_users_sub_menu_users_text'] = 'Users';
$lang['cl_menu_users_sub_menu_groups_text'] = 'Groups';

$lang['cl_menu_project_text'] = 'Project';
$lang['cl_menu_project_sub_menu_all_project_text'] = 'All Projects';
$lang['cl_menu_project_sub_menu_my_project_text'] = 'My Projects';
$lang['cl_menu_project_sub_menu_add_project_text'] = 'Add Project';

$lang['cl_menu_task_text'] = 'Task';
$lang['cl_menu_task_sub_menu_all_tasks_from_all_projects_text'] = 'All Tasks';
$lang['cl_menu_task_sub_menu_all_my_task_text'] = 'My Tasks';

$lang['cl_menu_file_manager_text'] = 'File Manager';
$lang['cl_menu_file_manager_sub_menu_all_files_text'] = 'All File';
$lang['cl_menu_file_manager_sub_menu_my_files_text'] = 'My File';


$lang['cl_menu_ticket_or_support_text'] = 'Ticket/Support';
$lang['cl_menu_ticket_or_support_sub_menu_all_tickets_text'] = 'All Tickets';
$lang['cl_menu_ticket_or_support_sub_menu_my_tickets_text'] = 'My Tickets';
$lang['cl_menu_ticket_or_support_sub_menu_create_ticket_text'] = 'Create Ticket';

$lang['cl_menu_invoice_text'] = 'Invoice';
$lang['cl_menu_invoice_sub_menu_all_invoices_text'] = 'All Invoices';
$lang['cl_menu_invoice_sub_menu_my_invoices_text'] = 'My Invoices';
$lang['cl_menu_invoice_sub_menu_create_invoice_text'] = 'Create Invoice';

$lang['cl_menu_contact_text'] = 'Contact';
$lang['cl_menu_contact_sub_menu_contact_page_text'] = 'Contact Page';

?>