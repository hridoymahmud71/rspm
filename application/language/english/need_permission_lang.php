<?php

$lang['permission_denied_text'] = 'Permission Denied !';
$lang['need_permission_text'] = 'Need Permission to Access This Page';
$lang['permission_description_text'] = 'Unfortunately You are not permitted to see this page.If you want access contact system admin.';
$lang['contact_link_text'] = 'Conact Link';
$lang['thank_you_text'] = 'Thank You';