<?php

/*page texts*/
$lang['page_title_text'] = 'Groups';
$lang['page_subtitle_text'] = 'Add Groups | Show Groups | Edit Groups | Delete Groups';
$lang['table_title_text'] = 'Group List';
$lang['no_group_found_text'] = 'No Group Is Found !';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Groups';
$lang['breadcrumb_page_text'] = 'Group List';

$lang['add_button_text'] = 'Add A Group';

/*Column names of the table*/
$lang['column_group_name_text'] = 'Group Name';
$lang['column_group_description_text'] = 'Group Description';
$lang['column_actions_text'] = 'Actions';

$lang['confirm_delete_text'] = 'Are You Sure To Delete This Group ? ';
$lang['delete_success_text'] = 'Succesfully deleted the group.';

$lang['successfull_text'] = 'Succesfull';
$lang['add_successfull_text'] = 'Succesfully added the group';

$lang['update_successfull_text'] = 'Succesfully updated the group.';


/*tooltip text*/
$lang['tooltip_view_text'] = 'View Group ';
$lang['tooltip_edit_text'] = 'Edit Group ';
$lang['tooltip_delete_text'] = 'Delete Group ';
$lang['tooltip_message_text'] = 'Mail/Message Group';




