<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *  Create directory
 *  
 * */


class Custom_file_library
{

    public $CI;

    public $if_settings_type_exists;
    public $all_file_settings = array();

    public function __construct()
    {

        $this->CI = &get_instance();


        $this->CI->load->library('session');
        $this->CI->load->library('custom_datetime_library');
        $this->CI->load->library('upload');

        //customized lib from modules/settings_module/libraries
        $this->CI->load->library('settings_module/custom_settings_library');

        $a_settings_code = 'file_settings';
        $this->if_settings_type_exists = $this->CI->custom_settings_library->ifSettingsTypeExist($a_settings_code);

        if ($this->if_settings_type_exists == true) {
            $this->all_file_settings = $this->CI->custom_settings_library->getSettings($a_settings_code);

        }


    }


    public function getMainDirectory()
    {
        if ($this->all_file_settings) {

            foreach ($this->all_file_settings as $a_file_settings) {

                if ($a_file_settings->settings_key == 'upload_path') {

                    if ($a_file_settings->settings_value != '') {

                        return $a_file_settings->settings_value;


                    } else {
                        return 'project_files';

                    }

                }

            }


        } else {
            return 'project_files';

        }


    }


    /*
     *  uploadFile gets called from the controllers
     * @params: $file_details  - array  //file details contains $_FILES['field_name']
     *          $field_name    - string
     *
     *          exmample:   $field_name = 'site_logo';
                            $file_details = $_FILES['site_logo'];
     *
     * @return: if true
     *              $uploaded_file_details - array
     *          else
     *              false   -   bool
     *
     * @flashdata: for errors:  file_upload_errors
     *
     *            for success:  file_upload_success
     *
     *
     *  */


    //uses oringinal 'upload' lib
    public function uploadFile($file_details, $field_name, $given_project_folder_name = false)
    {
        //file details contains $_FILES['field_name']

        $new_file_name = $this->getNewFileName($file_details['name']);

        if ($given_project_folder_name) {
            $file_config = $this->getFileConfig($new_file_name, $given_project_folder_name);
        } else {
            $file_config = $this->getFileConfig($new_file_name);
        }


        $this->CI->upload->initialize($file_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $this->CI->session->set_flashdata('file_upload_errors', $this->CI->upload->display_errors());
            return false;
        } else {
            $this->CI->session->set_flashdata('file_upload_success', 'File Successfully Uploaded');
        }

        //$this->upload->data() returns uploaded file's details
        $uploaded_file_details = $this->CI->upload->data();
        return $uploaded_file_details;

    }


    //executes one file at a time. no worry :)
    public function uploadMultipleFiles($file_details, $field_name, $given_project_folder_name = false)
    {
        //file details contains $_FILES['field_name']

        $new_file_name = $this->getNewFileName($file_details['name']);

        if ($given_project_folder_name) {
            $file_config = $this->getFileConfig($new_file_name, $given_project_folder_name);
        } else {
            $file_config = $this->getFileConfig($new_file_name);
        }

        $this->CI->upload->initialize($file_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $error['upload_errors'] = $this->CI->upload->display_errors();
            return $error;
        } else {
            //$this->upload->data() returns uploaded file's details
            $uploaded_file_details = $this->CI->upload->data();
            return $uploaded_file_details;
        }

    }

    public function getNewFileName($file_name)
    {
        $time_zone = $this->CI->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('Ymd_His');

        /*use this to get file name and ext*/
        $exploded_filename = explode(".", $file_name);
        $ext = end($exploded_filename);
        $filename_without_ext = basename($file_name, "." . $ext);
        $new_file_name = $filename_without_ext . '_' . $datetime . '_rd_' . rand(100000, 999999) . '.' . $ext;
        return $new_file_name;
    }

    public function getFileConfig($file_name, $given_project_folder_name = false)
    {
        $config['file_name'] = $file_name;
        $config['encrypt_name'] = FALSE;
        $config['overwrite'] = FALSE;

        if ($this->if_settings_type_exists == false) {

            $config['upload_path'] = FCPATH . 'project_files/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|doc|docx|pdf|txt';
            $config['max_size'] = 1024;
        } else {

            //upload_path
            if ($this->all_file_settings) {
                foreach ($this->all_file_settings as $a_file_settings) {
                    if ($a_file_settings->settings_key == 'upload_path') {
                        if ($a_file_settings->settings_value != '') {
                            if ($given_project_folder_name) {
                                $config['upload_path'] = FCPATH . $a_file_settings->settings_value . '/' . $given_project_folder_name . '/';
                            } else {
                                $config['upload_path'] = FCPATH . $a_file_settings->settings_value . '/';
                            }

                        } else {
                            $config['upload_path'] = FCPATH . 'project_files/';
                        }
                    }
                }
            }
        }

        //allowed_types
        if ($this->all_file_settings) {
            foreach ($this->all_file_settings as $a_file_settings) {
                if ($a_file_settings->settings_key == 'allowed_types') {
                    if ($a_file_settings->settings_value != '') {
                        $config['allowed_types'] = $a_file_settings->settings_value;
                    } else {
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|doc|docx|pdf|txt';
                    }
                }
            }
        }

        //max_size
        if ($this->all_file_settings) {
            foreach ($this->all_file_settings as $a_file_settings) {
                if ($a_file_settings->settings_key == 'max_size') {
                    if ($a_file_settings->settings_value != '') {
                        $config['max_size'] = intval($a_file_settings->settings_value);
                    } else {
                        $config['max_size'] = 1024;
                    }
                }
            }
        }

        return $config;
    }

    /*----------------------------------------------Dropzone Settings-------------------------------------------------*/

    public function uploadViaDropzone($move_from, $move_to)
    {
        move_uploaded_file($move_from, $move_to);
    }


    public function getDropzoneAllowedTypes()
    {
        if ($this->all_file_settings) {

            foreach ($this->all_file_settings as $a_file_settings) {

                if ($a_file_settings->settings_key == 'dropzone_allowed_types') {

                    if ($a_file_settings->settings_value != '') {

                        $types_array = explode('|', $a_file_settings->settings_value);

                        $i = 0;
                        $allowed_type_string = '';
                        foreach ($types_array as $a_type) {
                            if ($i != 0) {
                                $allowed_type_string .= ',.' . $a_type;
                            } else {
                                $allowed_type_string .= '.' . $a_type;
                            }

                            $i++;
                        }

                        return $allowed_type_string;

                    } else {
                        return '.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip';

                    }

                }

            }


        } else {
            return '.png,.jpg,.gif,.bmp,.jpeg,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip';

        }
    }

    public function getDropzoneMaxFileSize()
    {
        if ($this->all_file_settings) {

            foreach ($this->all_file_settings as $a_file_settings) {

                if ($a_file_settings->settings_key == 'dropzone_max_size') {

                    if ($a_file_settings->settings_value != '') {

                        return intval($a_file_settings->settings_value);


                    } else {
                        return 50;

                    }

                }

            }


        } else {
            return 50;

        }
    }

    public function getDropzoneMaxNumberOfFiles()
    {
        if ($this->all_file_settings) {

            foreach ($this->all_file_settings as $a_file_settings) {

                if ($a_file_settings->settings_key == 'dropzone_max_file_number') {

                    if ($a_file_settings->settings_value != '') {

                        return intval($a_file_settings->settings_value);


                    } else {
                        return 100;

                    }

                }

            }

        } else {
            return 50;
        }
    }


}