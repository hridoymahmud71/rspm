<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */


class Custom_log_library
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->library('settings_module/custom_settings_library');
        $this->CI->load->library('custom_datetime_library');
        $this->CI->load->library('custom_email_notification_library');

        $this->CI->load->model('log_module/Log_model');

    }

    public function createALog
    (
        /*params*/

        /*1*/
        $created_by,                        //id
        /*2*/
        $created_for,                       //id
        /*3*/
        $type,
        /*4*/
        $type_id,
        /*5*/
        $activity,
        /*6*/
        $activity_by,                       //a group
        /*7*/
        $activity_for,                      //a group
        /*8*/
        $sub_type,
        /*9*/
        $sub_type_id,
        /*10*/
        $super_type,
        /*11*/
        $super_type_id,
        /*12*/
        $other_information,                 //encoded json
        /*13*/
        $change_list                        //encoded json

    )
    {
        //$this->CI->lang->load('log_module/log');

        /*1*/
        $data['log_created_by'] = $created_by;
        $data['log_created_by_email'] = $this->getUserEmail($created_by);
        $data['log_created_by_full_name'] = $this->getUserFullName($created_by);

        //subtype='',subtype_id='' , supertype='',supertype_id=
        $data['log_created_by_route'] = $this->getRoute_2params('user_profile_route', $created_by);

        /*2*/
        $data['log_created_for'] = $created_for;
        $data['log_created_for_email'] = $this->getUserEmail($created_for);
        $data['log_created_for_full_name'] = $this->getUserFullName($created_for);
        //subtype='',subtype_id='' , supertype='',supertype_id=''
        $data['log_created_for_route'] = $this->getRoute_2params('user_profile_route', $created_for);

        /*3*/
        $data['log_type'] = $type;
        /*4*/
        $data['log_type_id'] = $type_id;

        $data['log_type_value'] = $this->getLogTypeValue($type, $type_id);
        $data['log_type_route'] = $this->getRoute_6params($type, $type_id, $sub_type, $sub_type_id, $super_type, $super_type_id);

        /*5*/
        $data['log_activity'] = $activity;
        /*6*/
        $data['log_activity_by'] = $activity_by;
        $data['log_activity_by_text'] = $this->getText($activity_by);
        /*7*/
        $data['log_activity_for'] = $activity_for;
        $data['log_activity_for_text'] = $this->getText($activity_for);

        /*8*/
        $data['log_sub_type'] = $sub_type;
        /*9*/
        $data['log_sub_type_id'] = $sub_type_id;

        $data['log_sub_type_value'] = $this->getLogTypeValue($sub_type, $sub_type_id);
        $data['log_sub_type_route'] = $this->getRoute_2params($sub_type, $sub_type_id);

        /*10*/
        $data['log_super_type'] = $super_type;
        /*11*/
        $data['log_super_type_id'] = $super_type_id;

        $data['log_super_type_value'] = $this->getLogTypeValue($super_type, $super_type_id);

        if ($type == 'file') {

            $data['log_super_type_route'] = $this->getRoute_6params($type, $type_id, $sub_type, $sub_type_id, $super_type, $super_type_id);

        } else {

            $data['log_super_type_route'] = $this->getRoute_2params($super_type, $super_type_id);
        }

        /*12*/
        $data['log_other_information'] = $other_information;

        /*13*/
        $data['log_change_list'] = $change_list;                                                            //encoded

        /*--------------------------------------------------*/

        /*14*/
        $data['log_date_format'] = $this->CI->custom_datetime_library->getDateFormat();
        $data['log_time_format'] = $this->CI->custom_datetime_library->getTimeFormat();

        /*15*/
        $data['log_created_at'] = $this->CI->custom_datetime_library->getCurrentTimestamp();

        $data['log_created_at_date'] =
            $this->CI->custom_datetime_library
                ->convert_and_return_TimestampToDate($data['log_created_at']);

        $data['log_created_at_time'] =
            $this->CI->custom_datetime_library
                ->convert_and_return_TimestampToTime($data['log_created_at']);

        $data['log_created_at_datetime'] =
            $this->CI->custom_datetime_library
                ->convert_and_return_TimestampToDateAndTime($data['log_created_at']);


        /*16*/
        $data['log_title_message'] = $this->getTitleMessage($data);
        /*17*/
        $data['log_message'] = $this->getMessage($data);


        $this->CI->Log_model->insertALog($data);


        $this->sendNotificationEmail($data);


    }

    public function sendNotificationEmail($data)
    {
        /*
         *
        if($data['log_type'] == 'something'){
            $this->CI->custom_email_notification_library->sendNotificationEmail($data);
        }
        //do this for multiple cases;

        */

        /*whether to send email notification or not*/

        $send_notification_settings_value = $this->CI->custom_settings_library->getASettingsValue('email_settings', 'send_email_notification');

        if ($send_notification_settings_value != null && $send_notification_settings_value == '1') {

            $this->CI->custom_email_notification_library->sendNotificationEmail($data);
        }
    }

    public function getUserFullName($user_id)
    {
        $user_row = $this->CI->Log_model->getUser($user_id);
        if ($user_row) {
            $user_full_name = $user_row->first_name . ' ' . $user_row->last_name;
            return $user_full_name;
        } else {
            return '';
        }

    }

    public function getUserEmail($user_id)
    {
        $user_row = $this->CI->Log_model->getUser($user_id);
        if ($user_row) {
            $user_email = $user_row->email;
            return $user_email;
        } else {
            return '';
        }
    }

    public function getRoute_2params($type, $type_id)
    {
        if ($type == '') {
            return '';
        }

        //for this cas not actually type or type id, but works fine anyway
        if ($type == 'user_profile_route') {

            return 'user_profile_module/user_profile_overview/' . $type_id;
        }

        if ($type == 'project') {

            return 'projectroom_module/project_overview/' . $type_id;
        }


        return '';

    }

    public function getRoute_6params($type, $type_id, $sub_type, $sub_type_id, $super_type, $super_type_id)
    {
        if ($type == '') {
            return '';
        }


        if ($type == 'project') {

            return 'projectroom_module/project_overview/' . $type_id;
        }

        if ($type == 'task') {

            return 'task_module/task_overview/' . $super_type_id . '/' . $type_id;
        }

        if ($type == 'file') {
            if ($super_type == 'project') {
                return 'projectroom_module/project_overview/' . $super_type_id;
            }

            if ($super_type == 'task') {
                $task_id = $super_type_id;
                $a_task_info = $this->CI->Log_model->getTask($task_id);
                if ($a_task_info) {
                    return 'task_module/task_overview/' . $a_task_info->project_id . '/' . $task_id;

                }
            }
        }

        if ($type == 'invoice') {

            return 'invoice_module/view_invoice/' . $type_id;
        }

        if ($type == 'ticket') {
            return 'support_module/view_ticket/' . $type_id;
        }

        /*ticket response is not a separate entity so the route is same as the ticket*/
        if ($type == 'ticket_response') {
            return 'support_module/view_ticket/' . $type_id;
        }

        return '';
    }

    /*name or title mostly */
    public function getLogTypeValue($type, $type_id)
    {
        if ($type != '' && $type_id != 0) {

            /*----------------------*/
            if ($type == 'project') {

                $a_project_row = $this->CI->Log_model->getProject($type_id);

                if ($a_project_row) {
                    return $a_project_row->project_name;
                } else {
                    return '';
                }
            }
            /*----------------------*/

            /*----------------------*/
            if ($type == 'task') {
                $a_task_row = $this->CI->Log_model->getTask($type_id);

                if ($a_task_row) {
                    return $a_task_row->task_number;
                } else {
                    return '';
                }
            }
            /*----------------------*/

            /*----------------------*/
            if ($type == 'file') {

                if ($type_id != 0 || $type_id != '') {
                    $a_file_row = $this->CI->Log_model->getFile($type_id);

                    if ($a_file_row) {
                        return $a_file_row->file_name;
                    } else {
                        return '';
                    }
                } else {
                    return '';
                }

            }
            /*----------------------*/

            /*----------------------*/
            if ($type == 'invoice') {
                $an_invoice_row = $this->CI->Log_model->getInvoice($type_id);

                if ($an_invoice_row) {
                    return $an_invoice_row->invoice_number;
                } else {
                    return '';
                }
            }
            /*----------------------*/

            /*----------------------*/
            if ($type == 'ticket') {
                $a_ticket_row = $this->CI->Log_model->getTicket($type_id);

                if ($a_ticket_row) {
                    return $a_ticket_row->ticket_number;
                } else {
                    return '';
                }
            }
            /*----------------------*/

            /*----------------------*/
            if ($type == 'ticket_response') {
                $a_ticket_row = $this->CI->Log_model->getTicket($type_id);

                if ($a_ticket_row) {
                    return $a_ticket_row->ticket_number;
                } else {
                    return '';
                }
            }
            /*----------------------*/

            return '';
        } else {
            return '';
        }

    }

    //$something must be a string
    public function getText($something)
    {
        $this->CI->lang->load('log_module/log');

        if ($something != '') {

            $text_for_something = $this->CI->lang->line($something . '_text');

            return $text_for_something;
        } else {
            return '';
        }
    }

    /*unused func*/
    public function getStringText($something)
    {
        $this->CI->lang->load('log_module/log');

        if ($something != '') {

            $string_text_for_something = $this->CI->lang->line($something . '_string_text');

            return $string_text_for_something;
        } else {
            return '';
        }
    }

    /*------------------------------MESSAGES STARTS-------------------------------------------------------------------*/
    public function getTitleMessage($data)
    {
        $this->CI->lang->load('log_module/log');

        $title_message = '';

        /*print_r($data);*/

        if ($data['log_activity_by_text'] != '') {
            $log_activity_by_text = $data['log_activity_by_text'];
        } else {
            $log_activity_by_text = '';
        }

        if ($data['log_activity_for'] != '') {
            $log_activity_for_text = $data['log_activity_for_text'];
        } else {
            $log_activity_for_text = '';
        }

        $log_cr_by_fn = $data['log_created_by_full_name'];
        $log_cr_by_url = base_url() . $data['log_created_by_route'];

        if ($log_activity_by_text != '') {
            $log_cr_by_anchor = "<a href='$log_cr_by_url'>$log_cr_by_fn</a><small> ($log_activity_by_text) </small>";
        } else {
            $log_cr_by_anchor = "<a href='$log_cr_by_url'>$log_cr_by_fn</a>";
        }


        $log_cr_for_fn = $data['log_created_for_full_name'];
        $log_cr_for_url = base_url() . $data['log_created_for_route'];

        if ($log_activity_for_text != '') {
            $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a><small> ($log_activity_for_text) </small>";
        } else {
            $log_cr_for_anchor = "<a href='$log_cr_for_url'>$log_cr_for_fn</a>";
        }

        $log_type = $data['log_type'];
        $log_activity = $data['log_activity'];

        $log_type_text = $this->getText($log_type);
        $log_type_value = $data['log_type_value'];

        if ($log_type_value != '') {
            $log_type_value_strong = "<strong> ($log_type_value) </strong>";
            $log_type_text_with_value = $log_type_text . $log_type_value_strong;
        } else {
            $log_type_text_with_value = $log_type_text;
        }


        /* ----------setting up title message starts---------- */

        /* -----2 params starts-----*/
        if ($log_activity == 'created') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('created_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'updated') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('updated_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'deactivated') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('deactivated_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'activated') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('activated_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'deleted') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('deleted_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'completed') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('completed_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'priority_changed') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('priority_changed_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'staffs_changed_announce') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('staffs_changed_announce_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'uploaded_file') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('file_uploaded_by_string_text'), $log_cr_by_anchor);
            $title_message .= '</p>';
        }

        if ($log_activity == 'opened') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('opened_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'closed') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('closed_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'cleared') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('cleared_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'uncleared') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('uncleared_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'marked_solved') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('marked_solved_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'marked_unsolved') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('marked_unsolved_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        if ($log_activity == 'created_response_for_ticket') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('created_response_for_ticket_string_text'),
                    $log_cr_by_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        /* -----2 params ends-----*/

        /* -----3 params starts-----*/

        if ($log_activity == 'assigned_staff') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('assigned_staff_string_text'),
                    $log_cr_by_anchor,
                    $log_cr_for_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'deassigned_staff') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('deassigned_staff_string_text'),
                    $log_cr_by_anchor,
                    $log_cr_for_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'allocated_client') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('allocated_client_string_text'),
                    $log_cr_by_anchor,
                    $log_cr_for_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }
        if ($log_activity == 'deallocated_client') {
            $title_message .= '<p>';
            $title_message .=
                sprintf($this->CI->lang->line('deallocated_client_string_text'),
                    $log_cr_by_anchor,
                    $log_cr_for_anchor,
                    $log_type_text_with_value
                );
            $title_message .= '</p>';
        }

        /* -----3 params ends-----*/

        /* ----------setting up title message ends---------- */


        /*setting up belongs to message starts*/
        $belongs_to_message = '';

        $log_type = $data['log_type'];
        $log_type_id = $data['log_type_id'];
        $log_type_value = $data['log_type_value'];
        $log_type_route = $data['log_type_route'];

        $log_super_type = $data['log_super_type'];
        $log_super_type_id = $data['log_super_type_id'];
        $log_super_type_value = $data['log_super_type_value'];
        $log_super_type_route = $data['log_super_type_route'];

        if (
            $log_type != ''
            && $log_type_id != ''
            && $log_super_type != ''
            && $log_super_type_id != ''

        ) {
            $log_type_text = $this->getText($log_type);
            $log_type_url = base_url() . $log_type_route;
            $log_type_anchor = "<a href='$log_type_url'>$log_type_value</a><strong> ($log_type_text) </strong>";

            $log_super_type_text = $this->getText($log_super_type);
            $log_super_type_url = base_url() . $log_super_type_route;
            $log_super_type_anchor = "<a href='$log_super_type_url'>$log_super_type_value</a><strong> ($log_super_type_text) </strong>";


            $belongs_to_message .= "<p style='font-size: smaller'>";
            $belongs_to_message .=
                sprintf($this->CI->lang->line('belongs_to_string_text'),
                    $log_type_anchor,
                    $log_super_type_anchor
                );
            $belongs_to_message .= '</p>';

        } else if
        (
            $log_type != ''
            && ($log_type_id == '' || $log_type_id == 0)
            && $log_super_type != ''
            && $log_super_type_id != ''
        ) {

            $log_type_text = $this->getText($log_type);

            $log_super_type_text = $this->getText($log_super_type);
            $log_super_type_url = base_url() . $log_super_type_route;
            $log_super_type_anchor = "<a href='$log_super_type_url'>$log_super_type_value</a><strong> ($log_super_type_text) </strong>";


            $belongs_to_message .= "<p style='font-size: smaller'>";
            $belongs_to_message .=
                sprintf($this->CI->lang->line('belongs_to_string_text'),
                    $log_type_text,
                    $log_super_type_anchor
                );
            $belongs_to_message .= '</p>';


        } else {
            //do nothing
        }
        /*setting up belongs to message ends*/

        return $title_message . $belongs_to_message;

    }

    // changelist mesage and other_info messages
    public function getMessage($data)
    {
        $this->CI->lang->load('log_module/log');

        $message = '';
        $change_list_message = $this->getChangelistMessage($data);
        $other_information_message = $this->getOtherInformationMessage($data);

        $message .= $other_information_message;
        $message .= $change_list_message;

        return $message;
    }

    public function getOtherInformationMessage($data)
    {
        $this->CI->lang->load('log_module/log');
        $other_information_message = '';

        if ($data['log_other_information'] != '') {
            $decoded_other_information = json_decode($data['log_other_information'], 1);
        } else {
            $decoded_other_information = null;
        }


        if ($decoded_other_information != null) {
            foreach ($decoded_other_information as $a_decoded_other_info_key => $a_decoded_other_info_val) {

                /*--------------------------------------------------*/
                if ($a_decoded_other_info_key == 'uploaded_file_list') {
                    $file_list_id_string = $a_decoded_other_info_val;

                    $file_list_id_array = explode(',', $file_list_id_string);

                    $file_anchors_string = '';
                    $i = 0;
                    foreach ($file_list_id_array as $a_file_id) {

                        $a_file_route = base_url() . 'file_manager_module/file_overview/' . $a_file_id;
                        $a_file_name = $this->getLogTypeValue('file', $a_file_id);
                        $a_file_anchor = "<a href='$a_file_route'>$a_file_name</a>";

                        if ($i != 0) {
                            $file_anchors_string .= ' , ' . '<br>' . $a_file_anchor;
                        } else {
                            $file_anchors_string .= '<br>' . $a_file_anchor;
                        }

                        $i++;
                    }


                    if ($i > 1) {
                        //single file
                        $other_information_message .= '<p>';
                        $other_information_message .=
                            sprintf($this->CI->lang->line('uploaded_files_are_string_text'), $file_anchors_string);
                        $other_information_message .= '</p>';

                    } else {
                        //multiple files
                        $other_information_message .= '<p>';
                        $other_information_message .=
                            sprintf($this->CI->lang->line('uploaded_file_is_string_text'), $file_anchors_string);
                        $other_information_message .= '</p>';
                    }

                }
                /*----------------------------------------------------*/


                /*----------------------------------------------------*/
                if ($a_decoded_other_info_key == 'file_belongs_to_project') {
                    if ($a_decoded_other_info_val == 0) {
                        $other_information_message .= '<p>';
                        $other_information_message .= $this->CI->lang->line('file_belongs_to_no_project_text');
                        $other_information_message .= '</p>';
                    }
                }
                /*----------------------------------------------------*/


                /*----------------------------------------------------*/
                if ($a_decoded_other_info_key == 'task_belongs_to_project') {

                    if ($a_decoded_other_info_val != 0) {

                        $project_id = $a_decoded_other_info_val;

                        $project_text = $this->getText('project');
                        $project_name = $this->getLogTypeValue('project', $project_id);
                        $project_route = base_url() . 'projectroom_module/project_overview/' . $project_id;
                        $project_anchor = "<a href='$project_route'>$project_name</a><small> ($project_text) </small>";


                        if ($data['log_super_type'] == 'task' && $data['log_super_type_id'] != 0) {

                            $task_id = $data['log_super_type_id'];

                            $task_text = $this->getText('task');
                            $task_number = $this->getLogTypeValue('task', $task_id);
                            $task_route = base_url() . 'task_module/task_overview/' . $project_id . '/' . $task_id;

                            $task_anchor = "<a href='$task_route'>$task_number</a><small> ($task_text) </small>";

                            $other_information_message .= '<p>';
                            $other_information_message .=
                                sprintf($this->CI->lang->line('task_belongs_to_project_string_text'),
                                    $task_anchor,
                                    $project_anchor
                                );
                            $other_information_message .= '</p>';

                        }

                    }
                }
                /*----------------------------------------------------*/

            }
        }

        return $other_information_message;

    }

    public function getChangelistMessage($data)
    {
        $this->CI->lang->load('log_module/log');

        $change_list_messages = '';

        $decoded_change_list = array();
        if ($data['log_change_list'] != '') {
            $decoded_change_list = json_decode($data['log_change_list'], 1);
        } else {
            $decoded_change_list = null;
        }

        /*print_r($decoded_change_list);*/


        if ($decoded_change_list != null) {
            foreach ($decoded_change_list as $a_change_key => $a_change_val) {

                if (gettype($a_change_val) != 'array' && $a_change_key == 'change_type') {

                    if ($a_change_val != '') {
                        $change_title_text = $this->CI->lang->line($a_change_val . '_text');
                        $change_title_message =
                            sprintf($this->CI->lang->line('change_title_message_text'), $change_title_text);
                    } else {
                        $change_title_message = $this->CI->lang->line('default_change_title_message_text');
                    }

                    $change_list_messages .= '<p>';
                    $change_list_messages .= $change_title_message;
                    $change_list_messages .= '</p>';
                }

                if (gettype($a_change_val) == 'array') {

                    /*----------------------------------------------------------*/
                    /*converting user_id to username with url*/
                    if ($a_change_val['need_to_convert_as'] == 'user_profile') {

                        $user_name_from = $this->getUserFullName($a_change_val['from']);
                        $user_route_from = $this->getRoute_2params('user_profile_route', $a_change_val['from']);
                        $user_from_url = base_url() . $user_route_from;
                        $user_name_anchor_from = "<a href='$user_from_url'>$user_name_from</a>";


                        $user_name_to = $this->getUserFullName($a_change_val['to']);
                        $user_route_to = $this->getRoute_2params('user_profile_route', $a_change_val['to']);
                        $user_to_url = base_url() . $user_route_to;
                        $user_name_anchor_to = "<a href='$user_to_url'>$user_name_to</a>";


                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $user_name_anchor_from,
                                $user_name_anchor_to);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /* concating '%' symbol */
                    if ($a_change_val['need_to_convert_as'] == 'user_list') {


                        if ($a_change_val['from'] != '') {

                            $from_users_id = explode(',', $a_change_val['from']);

                            //changing user-ids to users' fullnames with url
                            $from_users_names_anchors_array = array();
                            foreach ($from_users_id as $a_from_user_id) {

                                $a_from_user_fullname = $this->getUserFullName($a_from_user_id);
                                $a_from_user_route = $this->getRoute_2params('user_profile_route', $a_from_user_id);

                                $a_from_user_url = base_url() . $a_from_user_route;
                                $a_from_user_name_anchor = "<a href='$a_from_user_url'>$a_from_user_fullname</a>";

                                $from_users_names_anchors_array[] = $a_from_user_name_anchor;
                            }
                            $from_users_names_anchors_string = implode(',', $from_users_names_anchors_array);
                        } else {

                            $from_users_names_anchors_string = $this->CI->lang->line('none_text');
                        }

                        if ($a_change_val['to'] != '') {

                            $to_users_id = explode(',', $a_change_val['to']);

                            //changing user-ids to users' fullnames with url
                            $to_users_names_anchors_array = array();
                            foreach ($to_users_id as $a_to_user_id) {

                                $a_to_user_fullname = $this->getUserFullName($a_to_user_id);
                                $a_to_user_route = $this->getRoute_2params('user_profile_route', $a_to_user_id);

                                $a_to_user_url = base_url() . $a_to_user_route;
                                $a_to_user_name_anchor = "<a href='$a_to_user_url'>$a_to_user_fullname</a>";

                                $to_users_names_anchors_array[] = $a_to_user_name_anchor;
                            }
                            $to_users_names_anchors_string = implode(',', $to_users_names_anchors_array);
                        } else {

                            $to_users_names_anchors_string = $this->CI->lang->line('none_text');
                        }


                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $from_users_names_anchors_string,
                                $to_users_names_anchors_string);

                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /*converting timestamp to date string*/
                    if ($a_change_val['need_to_convert_as'] == 'date') {

                        if ($a_change_val['from'] != '' && $a_change_val['from'] != 0) {
                            $date_from =
                                $this->CI->custom_datetime_library
                                    ->convert_and_return_TimestampToDate($a_change_val['from']);
                        } else {
                            $date_from = $this->CI->lang->line('none_text');
                        }

                        if ($a_change_val['to'] != '' && $a_change_val['to'] != 0) {
                            $date_to =
                                $this->CI->custom_datetime_library
                                    ->convert_and_return_TimestampToDate($a_change_val['to']);
                        } else {
                            $date_to = $this->CI->lang->line('none_text');
                        }

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $date_from,
                                $date_to);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /*converting timestamp to datetime string*/
                    if ($a_change_val['need_to_convert_as'] == 'datetime') {

                        if ($a_change_val['from'] != '' && $a_change_val['from'] != 0) {
                            $date_from =
                                $this->CI->custom_datetime_library
                                    ->convert_and_return_TimestampToDateAndTime($a_change_val['from']);
                        } else {
                            $date_from = $this->CI->lang->line('none_text');
                        }

                        if ($a_change_val['to'] != '' && $a_change_val['to'] != 0) {
                            $date_to =
                                $this->CI->custom_datetime_library
                                    ->convert_and_return_TimestampToDateAndTime($a_change_val['to']);
                        } else {
                            $date_to = $this->CI->lang->line('none_text');
                        }

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $date_from,
                                $date_to);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /* concating '%' symbol */
                    if ($a_change_val['need_to_convert_as'] == 'percentage') {

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $a_change_val['from'] . '%',
                                $a_change_val['to'] . '%');

                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /* Status make 1,0 to Active,Inactive */
                    if ($a_change_val['need_to_convert_as'] == 'status') {

                        if ($a_change_val['from'] == 0) {
                            $status_from_text = $this->CI->lang->line('status_inactive_text');
                        } else {
                            $status_from_text = $this->CI->lang->line('status_active_text');
                        }
                        if ($a_change_val['to'] == 0) {
                            $status_to_text = $this->CI->lang->line('status_inactive_text');
                        } else {
                            $status_to_text = $this->CI->lang->line('status_active_text');
                        }

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $status_from_text,
                                $status_to_text);

                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /* Status make 1,0 to Open,Close */
                    if ($a_change_val['need_to_convert_as'] == 'invoice_status') {

                        if ($a_change_val['from'] == 0) {
                            $status_from_text = $this->CI->lang->line('status_close_text');
                        } else {
                            $status_from_text = $this->CI->lang->line('status_open_text');
                        }
                        if ($a_change_val['to'] == 0) {
                            $status_to_text = $this->CI->lang->line('status_close_text');
                        } else {
                            $status_to_text = $this->CI->lang->line('status_close_text');
                        }

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $status_from_text,
                                $status_to_text);

                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/

                    /*----------------------------------------------------------*/
                    /* Status make 1,0 to Open,Close */
                    if ($a_change_val['need_to_convert_as'] == 'invoice_clear_status') {

                        if ($a_change_val['from'] == 0) {
                            $status_from_text = $this->CI->lang->line('status_unclear_text');
                        } else {
                            $status_from_text = $this->CI->lang->line('status_clear_text');
                        }
                        if ($a_change_val['to'] == 0) {
                            $status_to_text = $this->CI->lang->line('status_unclear_text');
                        } else {
                            $status_to_text = $this->CI->lang->line('status_clear_text');
                        }

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $status_from_text,
                                $status_to_text);

                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    if ($a_change_val['need_to_convert_as'] == 'priority') {

                        $priority_from = $this->getText($a_change_val['from']);
                        $priority_to = $this->getText($a_change_val['to']);

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $priority_from,
                                $priority_to);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    if ($a_change_val['need_to_convert_as'] == 'large_text') {

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_text'), $a_change_val['field_name']);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                    /*----------------------------------------------------------*/
                    /*field that does'nt need to convert */
                    if ($a_change_val['need_to_convert_as'] == '') {

                        $change_list_messages .= '<p>';
                        $change_list_messages .=
                            sprintf($this->CI->lang->line('change_string_text'),
                                $a_change_val['field_name'],
                                $a_change_val['from'],
                                $a_change_val['to']);
                        $change_list_messages .= '</p>';
                    }
                    /*----------------------------------------------------------*/


                }


            }

        }


        return $change_list_messages;
    }
    /*------------------------------MESSAGES ENDS-------------------------------------------------------------------*/


}
