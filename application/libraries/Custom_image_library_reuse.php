<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *  Create directory
 *      at project root create: project_images
 *      inside project root create: thumb
 * */


class Custom_image_library {

    public $CI;

    public function __construct()
    {

        $this->CI = & get_instance();

        //$this->CI->load->model('settings_module/SettingsModel'); //add if necessary

        $this->CI->load->library('image_lib');
        $this->CI->load->library('session');

    }


    /*
     *  uploadImage gets called from the controllers
     * @params: $file_details  - array  //file details contains $_FILES['field_name']
     *          $field_name    - string
     *
     *          exmample:   $field_name = 'site_logo';
                            $file_details = $_FILES['site_logo'];
     *
     * @return: if true
     *              $image_details - array
     *          else
     *              false   -   bool
     *
     * @flashdata: for errors:  image_upload_errors
     *                          image_resize_errors
     *            for success:  image_upload_success
     *                          image_resize_success
     *
     *  */
    public function uploadImage($file_details,$field_name)
    {
        //file details contains $_FILES['field_name']
        $image_config = $this->getImageConfig($file_details['name']);

        $this->CI->load->library('upload', $image_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $this->CI->session->set_flashdata('image_upload_errors', $this->CI->upload->display_errors());
            return false ;
        } else {
            $this->CI->session->set_flashdata('image_upload_success', 'Image Successfully Uploaded');
        }

        //$this->upload->data() returns uploaded file's details
        $image_details = $this->CI->upload->data();

        $thumb_config = $this->getThumbConfig($image_details);

        $this->CI->image_lib->initialize($thumb_config);

        if (!$this->CI->image_lib->resize()) {
            $this->CI->session->set_flashdata('image_resize_errors', $this->CI->image_lib->display_errors());
            return false ;
        } else {
            $this->CI->session->set_flashdata('image_resize_success', 'Image Successfully Resized');
            //image details is returned only after resizing
            return $image_details;
        }
    }


    public function getImageConfig($image_name)
    {
        $config['encrypt_name'] = TRUE;                                 //should come from  settings table
        $config['file_name'] = $image_name;
        $config['upload_path'] = './project_images/';                   //should come from  settings table
        $config['allowed_types'] = 'gif|jpg|png|jpeg';                  //should come from  settings table
        $config['max_size'] = 100;                                      //should come from  settings table
        $config['max_width'] = 1000;                                    //should come from  settings table
        $config['max_height'] = 1000;                                   //should come from  settings table

        return $config;
    }


    public function getThumbConfig($image_details)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = './project_images/' . $image_details['file_name'];     //should come from  settings table
        $config['new_image'] = './project_images/thumb/' . $image_details['file_name'];  //should come from  settings table
        $config['create_thumb'] = TRUE;                                                  //should come from  settings table
        $config['thumb_marker'] = '';                                                    //should come from  settings table
        $config['maintain_ratio'] = TRUE;                                                //should come from  settings table
        $config['width'] = 75;                                                           //should come from  settings table
        $config['height'] = 75;                                                          //should come from  settings table
        return $config;
    }

}