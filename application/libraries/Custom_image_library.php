<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *   This Library depends on the Settings_model
 *      Remember to create a settings model if there is none
 *          or
 *      Put the configs manually
 * */

class Custom_image_library
{

    public $CI;


    public $if_settings_type_exists;
    public $all_image_settings = array();

    public function __construct()
    {

        $this->CI = &get_instance();

        $this->CI->load->library('upload');
        $this->CI->load->library('image_lib');
        $this->CI->load->library('session');

        //customized lib from modules/settings_module/libraries
        $this->CI->load->library('settings_module/custom_settings_library');

        $this->CI->lang->load('image');


        $a_settings_code = 'image_settings';
        $this->if_settings_type_exists = $this->CI->custom_settings_library->ifSettingsTypeExist($a_settings_code);

        if ($this->if_settings_type_exists == true) {
            $this->all_image_settings = $this->CI->custom_settings_library->getSettings($a_settings_code);

        }

    }


    public function getMainImageDirectory()
    {
        if ($this->all_image_settings) {

            foreach ($this->all_image_settings as $a_image_settings) {

                if ($a_image_settings->settings_key == 'upload_path') {

                    if ($a_image_settings->settings_value != '') {

                        return $a_image_settings->settings_value;


                    } else {
                        return 'project_images';

                    }

                }

            }


        } else {
            return 'project_images';

        }


    }


    /*
     *  uploadImage gets called from the controllers
     * @params: $file_details  - array  //file details contains $_FILES['field_name']
     *          $field_name    - string
     *
     *          exmample:   $field_name = 'site_logo';
                            $file_details = $_FILES['site_logo'];
     *
     * @return: if true
     *              $image_details - array
     *          else
     *              false   -   bool
     *
     * @flashdata: for errors:  image_upload_errors
     *                          image_resize_errors
     *            for success:  image_upload_success
     *                          image_resize_success
     *
     *  */
    public function uploadImage($file_details, $field_name)
    {
        //file details contains $_FILES['field_name']
        $image_config = $this->getImageConfig($file_details['name']);

        $this->CI->upload->initialize($image_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $this->CI->session->set_flashdata('image_upload_errors', $this->CI->upload->display_errors());
            return false;
        } else {
            $this->CI->session->set_flashdata('image_upload_success', $this->CI->lang->line('image_upload_success'));
        }

        //$this->upload->data() returns uploaded file's details
        $image_details = $this->CI->upload->data();

        $thumb_config = $this->getThumbConfig($image_details);

        $this->CI->image_lib->initialize($thumb_config);

        //create_thumb
        if ($this->all_image_settings) {
            foreach ($this->all_image_settings as $a_image_settings) {
                if ($a_image_settings->settings_key == 'create_thumb') {
                    if ($a_image_settings->settings_value == 1) {

                        if (!$this->CI->image_lib->resize()) {
                            $this->CI->session->set_flashdata('image_resize_errors', $this->CI->image_lib->display_errors());
                            return false;
                        } else {
                            $this->CI->session->set_flashdata('image_resize_success', $this->CI->lang->line('image_resize_success'));
                            //image details is returned only after resizing
                            return $image_details;
                        }

                    }
                }
            }
        }

    }



    /*
     * ----------read carefully----------------------------------------------------
     *
     * This func returns image details with success ond error messages together
     *
     * call the function and print the return array to understand
     *
     * */
    public function uploadImage_revisedFunc($file_details, $field_name)
    {
        //file details contains $_FILES['field_name']
        $image_config = $this->getImageConfig($file_details['name']);

        $this->CI->upload->initialize($image_config);

        //do_upload uploads the file

        $messages = array();
        $return_array = array();

        if (!$this->CI->upload->do_upload($field_name)) {
            $messages['image_upload_error'] = $this->CI->upload->display_errors();
            $return_array['image_upload_error'][] = $messages['image_upload_error'];
            $return_array['image_upload_success'][] = 'no_upload_success';
            $image_details = $this->CI->upload->data();
            $return_array['image_details'][] = $image_details;
            return $return_array;

        } else {
            $messages['image_upload_success'] = $this->CI->lang->line('image_upload_success');
            $return_array['image_upload_success'][] = $messages['image_upload_success'];
            $return_array['image_upload_error'][] = 'no_upload_error';

            /*$image_details = $this->CI->upload->data();
            $return_array[] = $image_details;*/

        }

        //$this->upload->data() returns uploaded file's details
        $image_details = $this->CI->upload->data();

        $thumb_config = $this->getThumbConfig($image_details);

        $this->CI->image_lib->initialize($thumb_config);

        //create_thumb
        if ($this->all_image_settings) {
            foreach ($this->all_image_settings as $a_image_settings) {
                if ($a_image_settings->settings_key == 'create_thumb') {
                    if ($a_image_settings->settings_value == 1) {

                        if (!$this->CI->image_lib->resize()) {
                            $messages['image_resize_error'] = $this->CI->image_lib->display_errors();
                            $return_array['image_resize_error'][] = $messages['image_resize_error'];
                            $return_array['image_resize_success'][] = 'no_resize_success';

                            $return_array['image_details'][] = $image_details;

                            return $return_array;
                        } else {
                            $messages['image_resize_success'] = $this->CI->lang->line('image_resize_success');
                            $return_array['image_resize_success'][] = $messages['image_resize_success'];
                            $return_array['image_resize_error'][] = 'no_resize_error';

                            $image_details = $this->CI->upload->data();
                            $return_array['image_details'][] = $image_details;

                            return $return_array;
                        }

                    }
                }
            }
        }
    }


    public function getImageConfig($image_name)
    {
        if ($this->if_settings_type_exists == false) {
            $config['encrypt_name'] = TRUE;
            $config['file_name'] = $image_name;
            $config['upload_path'] = FCPATH.'project_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 100;
            $config['max_width'] = 1000;
            $config['max_height'] = 1000;
        } else {
            //encrypt_name
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'encrypt_name') {
                        if ($a_image_settings->settings_value == 1) {
                            $config['encrypt_name'] = TRUE;
                        } else {
                            $config['encrypt_name'] = FALSE;
                        }
                    }
                }
            }

            //upload_path
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'upload_path') {
                        if ($a_image_settings->settings_value != '') {
                            $config['upload_path'] = FCPATH.$a_image_settings->settings_value.'/';
                        } else {
                            $config['upload_path'] = FCPATH.'project_images/';
                        }
                    }
                }
            }

            //allowed_types
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'allowed_types') {
                        if ($a_image_settings->settings_value != '') {
                            $config['allowed_types'] = $a_image_settings->settings_value;
                        } else {
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        }
                    }
                }
            }

            //max_size
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'max_size') {
                        if ($a_image_settings->settings_value != '') {
                            $config['max_size'] = intval($a_image_settings->settings_value);
                        } else {
                            $config['max_size'] = 100;
                        }
                    }
                }
            }

            //max_width
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'max_width') {
                        if ($a_image_settings->settings_value != '') {
                            $config['max_width'] = intval($a_image_settings->settings_value);
                        } else {
                            $config['max_width'] = 1000;
                        }
                    }
                }
            }

            //max_height
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'max_width') {
                        if ($a_image_settings->settings_value != '') {
                            $config['max_height'] = intval($a_image_settings->settings_value);
                        } else {
                            $config['max_height'] = 1000;
                        }
                    }
                }
            }


        }


        return $config;
    }


    public function getThumbConfig($image_details)
    {
        if ($this->if_settings_type_exists == false) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = FCPATH.'project_images/' . $image_details['file_name'];
            $config['new_image'] = FCPATH.'project_images/thumb/' . $image_details['file_name'];
            $config['create_thumb'] = TRUE;
            $config['thumb_marker'] = '';
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 75;
        } else {

            $config['image_library'] = 'gd2';

            //source_image
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'upload_path') {
                        if ($a_image_settings->settings_value != '') {
                            $config['source_image'] = FCPATH.$a_image_settings->settings_value. '/' . $image_details['file_name'];
                        } else {
                            $config['source_image'] = FCPATH.'project_images/' . $image_details['file_name'];
                        }
                    }
                }
            }

            //new_image
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'thumb_source') {
                        if ($a_image_settings->settings_value != '') {
                            $main_img_dir = $this->getMainImageDirectory();
                            $config['new_image'] = FCPATH.$main_img_dir.'/'. $a_image_settings->settings_value .'/'. $image_details['file_name'];
                        } else {
                            $config['new_image'] = FCPATH.'project_images/thumb/' . $image_details['file_name'];
                        }
                    }
                }
            }

            //create_thumb
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'create_thumb') {
                        if ($a_image_settings->settings_value == 1) {
                            $config['create_thumb'] = TRUE;
                        } else {
                            $config['create_thumb'] = FALSE;
                        }
                    }
                }
            }

            //thumb_marker
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'thumb_marker') {
                        if ($a_image_settings->settings_value != '') {
                            $config['thumb_marker'] = $a_image_settings->settings_value;
                        } else {
                            $config['thumb_marker'] = '';
                        }
                    }
                }
            }

            //maintain_ratio
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'maintain_ratio') {
                        if ($a_image_settings->settings_value == 1) {
                            $config['maintain_ratio'] = TRUE;
                        } else {
                            $config['maintain_ratio'] = FALSE;
                        }
                    }
                }
            }

            //thumb_width
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'thumb_width') {
                        if ($a_image_settings->settings_value != '') {
                            $config['width'] = $a_image_settings->settings_value;
                        } else {
                            $config['width'] = '';
                        }
                    }
                }
            }

            //thumb_height
            if ($this->all_image_settings) {
                foreach ($this->all_image_settings as $a_image_settings) {
                    if ($a_image_settings->settings_key == 'thumb_height') {
                        if ($a_image_settings->settings_value != '') {
                            $config['height'] = $a_image_settings->settings_value;
                        } else {
                            $config['height'] = '';
                        }
                    }
                }
            }


        }

        return $config;
    }

}