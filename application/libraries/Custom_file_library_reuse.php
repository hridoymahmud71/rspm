<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *  Create directory
 *  
 * */


class Custom_file_library {

    public $CI;

    public function __construct()
    {

        $this->CI = & get_instance();

        //$this->CI->load->model('settings_module/SettingsModel'); //add if necessary

        $this->CI->load->library('session');
        $this->CI->load->library('upload');
        //$this->CI->load->library('custom_datetime_library');

    }


    /*
     *  uploadFile gets called from the controllers
     * @params: $file_details  - array  //file details contains $_FILES['field_name']
     *          $field_name    - string
     *
     *          exmample:   $field_name = 'site_logo';
                            $file_details = $_FILES['site_logo'];
     *
     * @return: if true
     *              $uploaded_file_details - array
     *          else
     *              false   -   bool
     *
     * @flashdata: for errors:  file_upload_errors
     *
     *            for success:  file_upload_success
     *
     *
     *  */
    public function uploadFile($file_details,$field_name)
    {
        //file details contains $_FILES['field_name']
        $file_config = $this->getFileConfig($file_details['name']);

        $this->CI->upload->initialize($file_config);

        //use this while multi upload
        //$this->CI->upload->initialize($file_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $this->CI->session->set_flashdata('file_upload_errors', $this->CI->upload->display_errors());
            return false ;
        } else {
            $this->CI->session->set_flashdata('file_upload_success', 'File Successfully Uploaded');
        }

        //$this->upload->data() returns uploaded file's details
        $uploaded_file_details = $this->CI->upload->data();
        return $uploaded_file_details;

    }


    public function getFileConfig($file_name)
    {
        date_default_timezone_set('Asia/Dhaka');
        $datetime = date('Y_m_d__H_i_s');

        /*use this to get file name and ext*/
        $exploded_filename = explode(".", $file_name);
        $ext = end($exploded_filename);
        $filename_without_ext = basename($file_name, ".".$ext );
        $new_file_name = $filename_without_ext.'__'.$datetime.'.'.$ext;

        $config['file_name'] = $new_file_name;
        $config['encrypt_name'] = FALSE;
        $config['overwrite'] = FALSE;
        $config['upload_path'] = './project_files/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|doc|docx|pdf|ppt|pptx|txt';
        $config['max_size'] = 1000;

        return $config;
    }


    

}